"use strict";

/**
 * represents the current ayla user.
 * Ayla users are kind of funny, because they, for the moment, only know how to represent the current user. As such
 * they can be constructed empty and will fetch themselves. In the future this pattern may need to change.
 */

define([
	"app",
	"bluebird",
	"common/AylaConfig",
	"common/model/ayla/mixin.AylaBacked",
	"common/model/ayla/mixin.metaDataProperties",
	"common/util/utilities",
	"consumer/dashboard/models/FullDashboardTileOrder.model",
	"consumer/dashboard/models/TileOrderCollection"
], function (App, P, AylaConfig, AylaBacked, MetaDataMixin, utilities) {

	App.module("Models", function (Models, App, B, Mn, $, _) {
		/*
		 NOTE: we currently do not support changing email addresses as it looks like it will invalidate our login...
		 From ayla:
		 Updates the email of the current user. A new login is required for the user to continue to
		 operate in the system. The caller must ensure the correctness of the new email address. Any
		 errors such as inadvertent typos could result in the user not being able to log in. Recommend
		 that the UI confirm the new email address before this API is called.
		 */

		Models.AylaUser = B.Model.extend({
			defaults: {
				// id
				email: null,

				//name
				firstname: null,
				lastname: null,

				// phone
				phone: null,
				phone_country_code: null,

				// address
				street: null,
				city: null,
				state: null,
				zip: null,
				country: null,

				// account status
				approved: null,
				created_at: null,

				//meta data properties
				dataCollectionOff: null,
				language: null
			},
			_metaDataProperties: ["language"],
			initialize: function () {
				this.set("tileOrderCollection", new App.Consumer.Dashboard.Models.FullDashboardTileOrderModel());

				// flag used for showing welcome tile
				this.dashboardFirstLoad = false;
			},
			isMetaDataLoaded: function () {
				var metaDataValues = _.values(this.pick(this._metaDataProperties));

				if (this._metaDataProperties.length > 0) {
					// if any meta data elements are not null we are loaded
					return _.reduce(metaDataValues, function (isLoaded, value) {
						return isLoaded || value !== null;
					}, false);
				}
				return true;
			},
			/**
			 * has this model been loaded from ayla yet?
			 * @returns {boolean}
			 */
			isLoaded: function () {
				return !!this.get("email") && this.isMetaDataLoaded();
			},
			_validateEquivalence: function (val1, val2, errorMessage) {
				if (val1 !== val2) {
					return {
						valid: false,
						message: errorMessage
					};
				}

				return {valid: true};
			},

			hasLanguage: function () {
				return !!this.get("language");
			},

			getCurrentGatewayTileOrder: function () {
				var tileOrderCollection = this.get("tileOrderCollection"),
					subCollection = tileOrderCollection.get(App.getCurrentGatewayDSN());

				if (!subCollection) {
					tileOrderCollection.setTileOrderCollection(App.getCurrentGateway());
					subCollection = tileOrderCollection.get(App.getCurrentGatewayDSN());
				}

				return subCollection;
			},

			getTileOrderForGateway: function (gatewayObj) {
				var dsn;
				if (gatewayObj instanceof B.Model) {
					dsn = gatewayObj.get("dsn");
				} else {
					dsn = gatewayObj;
				}

				var tileOrderCollection = this.get("tileOrderCollection"),
					subCollection = tileOrderCollection.get(dsn);

				if (!subCollection) {
					tileOrderCollection.setTileOrderCollection(App.salusConnector.getDevice(dsn));
					subCollection = tileOrderCollection.get(dsn);
				}

				return subCollection;
			},

			/**
			 * do basic password validation (used for create user and update)
			 * @param pass1
			 * @param pass2
			 * @returns {*}
			 */
			validatePassword: function (pass1, pass2) {
				var result = this._validateEquivalence(pass1, pass2, "common.validation.passwords.notMatching");

				if (!result.valid) {
					this.setFieldError("password", result.message);
					return result;
				}

				result = App.validate.password(pass1);

				if (!result.valid) {
					this.setFieldError("password", result.message);
				}

				return result;
			},

			validateEmail: function (email1, email2) {
				var result = this._validateEquivalence(email1, email2, "common.validation.email.notMatching");

				if (!result.valid) {
					this.setFieldError("email", result.message);

					return result;
				}

				result = App.validate.email(email1);

				if (!result.valid) {
					this.setFieldError("email", result.message);
				}

				return result;
			},
			/**
			 * update the password on the server
			 * @param passOld
			 * @param pass1
			 * @param pass2
			 * @returns {*}
			 */
			updatePassword: function (passOld, pass1, pass2) {
				var that = this;

				return this.saveWithPromise(function (resolve, reject) {
					var validation = that.validatePassword(pass1, pass2);

					if (!validation.valid) {
						reject(validation.message);
					}

					var model = {};

					model[that._apiWrapperObjectName] = {"current_password": passOld, "password": pass1};
					that.save(null, {data: JSON.stringify(model)});
				});
			},
			_getUrlRenderModel: function () {
				var model = {},
					subject = App.translate("email.signup_confirmation.emailSubject");

				if (this.idAttribute) {
					model.id = this.get(this.idAttribute);
				}

				// Handles the case where we want to send using this URL but do not have any params
				model.query_string_params = "";

				// If this is a new user, then we need to send an environment specific welcome e-mail
				if (this.isNew()) {
					model.query_string_params = utilities.appendEmailTemplateParameters("", "signup_confirmation", subject);
				}

				return model;
			},
			/**
			 * handles the added data need for create user save
			 * @param base
			 */
			renderModelForPersistNew: function (base) {
				// Filter out unecessary properties from the model before we ajax out to create a new user
				base.user = _.pick(base.user, ["email", "firstname", "lastname", "country", "phone", "phone_country_code"]);
			},

			/**
			 * special case of the base class to handel the case where we need extra data for user creation
			 * @returns {*}
			 */
			renderModelForPersist: function () {
				var model = this.renderModelForPersistBase();

				if (this.isNew()) {
					this.renderModelForPersistNew(model);
				}

				return model;
			},
			/**
			 * this is a wrapper for save handling the fact that we need to pre-transform our data befor saving
			 */
			persist: function (pass1, pass2) {
				var that = this;

				return this.saveWithPromise(function (resolve, reject) {
					var validation, model;

					if (pass1 || pass2) {
						validation = that.validatePassword(pass1, pass2);

						if (!validation.valid) {
							reject(validation.message);
							return;
						}
					}

					model = that.renderModelForPersist();

					if (pass1) {
						model.user.password = pass1;
					}

					return that.save(null, {
						data: JSON.stringify(model),
						error: that._onPersistError
					});
				}).then(function (userObj) {
					var result;

					if (App.salusConnector.isLoggedIn()) {
						result = that._persistMetaData();
					} else {
						result = true;
						App.salusConnector.setAylaUser(userObj);
					}

					return result;
				});
			},

			addToTileOrder: function (referenceId, referenceType) {
				this.getCurrentGatewayTileOrder().addNewTile(referenceId, referenceType);
			},

			refresh: function () {
				this._detailPromise = this.fetch()
					.timeout(AylaConfig.stdTimeoutMs)
					.then(this.fetchDataCollection)
					.then(this.fetchMetaData);

				return this._detailPromise;
			},

			saveDataCollection: function () {
				return App.salusConnector.saveDataCollection(this.get("dataCollectionOff"));
			},

			deleteTileOrder: function () {
				var that = this;

				return App.salusConnector.makeAjaxCall(AylaConfig.endpoints.userProfile.tileOrder.delete, null, "DELETE", "text").then(function () {
					that.unset("tileOrderCollection");
				});
			},

			saveTileOrder: function (tileOrderArray) {
				var that = this, payload = {
					datum: {
						value: JSON.stringify(tileOrderArray)
					}
				};

				return App.salusConnector.makeAjaxCall(AylaConfig.endpoints.userProfile.tileOrder.save, payload, "PUT", null, null, [404])
					.catch(function (err) {
						if (err.status === 404) {
							App.warn("Making New Tile Order");
							return that._makeTileOrderProp(tileOrderArray);
						}

						throw err;
					});
			},

			fetchTileOrder: function () {
				var that = this;

				return App.salusConnector.makeAjaxCall(AylaConfig.endpoints.userProfile.tileOrder.fetch, null, "GET", null, null, [404])
					.then(function (data) {
						var tileOrderParse = JSON.parse(data.datum.value);

						if (_.isArray(tileOrderParse)) {
							return that.deleteTileOrder().then(function () {
								that.dashboardFirstLoad = true;

								App.warn("Making New Tile Order");
								return that._makeTileOrderProp({});
							});
						}

						var gatewayDSNs = App.salusConnector.getGatewayCollection().map(function (gateway) {
							return gateway.get("dsn");
						});

						var pickRightPropsFunc = function (tileModel) {
							// only include these 3 attributes, should clean up old tile orders this way
							return _.pick(tileModel, ["referenceId", "referenceType"]);
						};

						var returnObj = {};
						for (var key in tileOrderParse) {
							if (_.contains(gatewayDSNs, key)) {
								var gatewayTileOrderArray = tileOrderParse[key];
								// Filter the duplicates
								var tempArray = [];
								var newArray = [];
								_.each(gatewayTileOrderArray, function(item) {
									var temp = item.referenceType + item.referenceId;
									if (_.indexOf(tempArray, temp) === -1) {
										tempArray.push(temp);
										newArray.push(item);
									}
								});
								if (newArray.length > 0) {
									gatewayTileOrderArray = newArray;
								}
								returnObj[key] = _.map(gatewayTileOrderArray, pickRightPropsFunc);
							}
						}

						return returnObj;
					}).catch(function (err) {
						if (err.status === 404) {
							that.dashboardFirstLoad = true;

							App.warn("Making New Tile Order");
							return that._makeTileOrderProp({});
						}

						throw err;
					});
			},

			_makeTileOrderProp: function (tileOrderArray) {
				var payload = {
					datum: {
						key: "tileOrder",
						value: JSON.stringify(tileOrderArray)
					}
				};

				return App.salusConnector.makeAjaxCall(AylaConfig.endpoints.userProfile.tileOrder.create, payload, "POST").then(function (data) {
					return JSON.parse(data.datum.value);
				});
			},

			changePassword: function (newPassword, oldPassword) {
				var payload = {
					"user": {
						"current_password": oldPassword,
						"password": newPassword
					}
				};

				return App.salusConnector.makeAjaxCall(AylaConfig.endpoints.userProfile.changePassword, payload, "PUT", "json");
			},
			onCleanUp: function () {
				var tileOrder = this.get("tileOrderCollection");
				if (tileOrder) {
					tileOrder.destroy();
				}
			}
		}).mixin([AylaBacked, MetaDataMixin], {
			fetchUrl: AylaConfig.endpoints.userProfile.fetch,
			saveUrl: AylaConfig.endpoints.userProfile.save,
			apiWrapperObjectName: "user",
			clobberSync: false,
			metaDataFetchUrl: AylaConfig.endpoints.userProfile.metaData.fetch,
			metaDataCreateUrl: AylaConfig.endpoints.userProfile.metaData.create,
			metaDataSaveUrl: AylaConfig.endpoints.userProfile.metaData.save
		});
	});

	return App.Models.AylaUser;
});