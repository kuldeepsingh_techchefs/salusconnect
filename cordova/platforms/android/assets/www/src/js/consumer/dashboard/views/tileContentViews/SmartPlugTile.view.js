"use strict";

define([
	"app",
	"moment",
	"common/constants",
	"consumer/consumerTemplates",
	"consumer/dashboard/views/tileContentViews/mixin.TileContentView"
], function (App, moment, constants, consumerTemplates, TileContentViewMixin) {

	App.module("Consumer.Dashboard.Views.TileContentViews", function (Views, App, B, Mn, $, _) {
		Views.SmartPlugTile = {};

		Views.SmartPlugTile.FrontView = Mn.ItemView.extend({
			className: "smart-plug-tile",
			attributes: {
				role: "button"
			},
			template: consumerTemplates["dashboard/tile/equipmentWithSwitcherTileFront"],
			bindings: {
				":el": {
					observe: "OnOff",
					onGet: function (val) {
//						if (val && moment(val.get("getterProperty").get("data_updated_at")) <= moment(val.get("setterProperty").get("data_updated_at"))) {
//							return false;
//						}
						return val ? val.getProperty() : val;
					},
					update: function ($el, val) {
						if (val !== false) {
							if (val === constants.smartPlugOnOff.ON) {
								$el.removeClass("off").addClass("on");
							} else if (val === constants.smartPlugOnOff.OFF || true) {
								$el.removeClass("on").addClass("off");
							}
						}
					}
				},
				".switcher": {
					observe: "OnOff",
					onGet: function (prop) {
						return prop ? prop.getProperty() : prop;
					},
					update: function ($el, val) {
                        if (val !== false) {
							if (val === constants.smartPlugOnOff.ON) {
                                $el.toggleClass("on", true);
							} else if (val === constants.smartPlugOnOff.OFF || true) {
                                $el.toggleClass("on", false);
							}
						}
					}
				}
			},
			templateHelpers: function () {
				return {
					isLargeTile: this.isLargeTile
				};
			}          
		}).mixin([TileContentViewMixin]);

		Views.SmartPlugTile.BackView = Mn.ItemView.extend({
			className: "smart-plug-tile",
			template: consumerTemplates["dashboard/tile/equipmentWithSwitcherTileBack"],
			events: {
				"click .equipment-switch": "equipmentSwitchClick",
                "click @ui.viewUsage": "goToUsage"
			},
			attributes: {
//				role: "button"
			},
            ui: {
				viewUsage: ".bb-view-usage"
			},
			bindings: {
				":el": {
					observe: "OnOff",
					onGet: function (prop) {
//						if (prop && moment(prop.get("getterProperty").get("data_updated_at")) <= moment(prop.get("setterProperty").get("data_updated_at"))) {
//							return false;
//						}
						return prop ? prop.getProperty() : prop;
					},
					update: function ($el, val) {
						if (val !== false) {
							if (val === constants.smartPlugOnOff.ON) {
								$el.removeClass("off").addClass("on");
							} else if (val === constants.smartPlugOnOff.OFF || true) {
								$el.removeClass("on").addClass("off");
							}
						}
					}
				},
				".equipment-switch": {
					observe: "OnOff",
					onGet: function (prop) {
						return prop ? prop.getProperty() : prop;
					},
					update: function ($el, val) {
                        if (val !== false) {
							if (val === constants.smartPlugOnOff.ON) {
                                $el.toggleClass("on", true);
							} else if (val === constants.smartPlugOnOff.OFF || true) {
                                $el.toggleClass("on", false);
							}
						}
						
					}
				}
			},
			initialize: function () {
				var that = this;
				
                
				_.bindAll(this, "equipmentSwitchClick");
                
				this.model.getDevicePropertiesPromise().then (function () {
					that.listenTo(that.model.get("OnOff"), "propertiesSynced", that.hideSmallTileSpinner);
					that.listenTo(that.model.get("OnOff"), "propertiesFailedSync", function () {
						App.Consumer.Controller.showDeviceSyncError(that.model);
						//that.hideSmallTileSpinner();
					});
				});

			},
			equipmentSwitchClick: function (/*event*/) {
				var that = this;
				if (!this.spinning && this.model.isOnline() && !this.model.isLeaveNetwork()) {
					this.showSpinner(this.tileSpinnerOptions);
					//this.showSmallTileSpinner();

					this.model.toggleSmartPlug().then(function () {
						that.hideSpinner();
					});
				}
			},
            goToUsage: function (/*evt*/) {
				var dsn = this.model.get("dsn");

				App.Consumer.shouldScrollTo = "#bb-graph-region";
				App.navigate("equipment/myEquipment/" + dsn);
			}
		}).mixin([TileContentViewMixin]);
	});

	return App.Consumer.Dashboard.Views.SmartPlugTile;
});
