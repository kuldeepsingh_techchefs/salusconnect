"use strict";

define([
	"app"
], function (App) {

	App.module("Consumer.Equipment.Models", function (Models, App, B) {
		Models.HistoryTableModel = B.Model.extend({
			defaults: {
				sortBy: "DATE",
				sortDesc: true,
				maxTableItems: 6
			},
			sortByEnum: {
				"USER": "user",
				"DATE": "dateTime",
				"TIME": "dateTime",
				"EVENT": "event"
			}
		});
	});

	return App.Consumer.Equipment.Models.HistoryTableModel;
});