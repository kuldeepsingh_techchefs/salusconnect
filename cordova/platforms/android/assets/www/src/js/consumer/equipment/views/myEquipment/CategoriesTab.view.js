"use strict";

define([
	"app",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusTabContent",
	"consumer/dashboard/views/tileContentViews/CategoryTile.view"
], function (App, templates, SalusTabView) {

	App.module("Consumer.Equipment.Views", function (Views, App, B, Mn) {
		Views.Categories = Mn.CompositeView.extend({
			template: templates["equipment/myEquipment/categoriesComposite"],
			childView: App.Consumer.Dashboard.Views.CategoryTileView,
			childViewContainer: ".list-container",
			initialize: function () {
				this.collection = this.options.collection;
			},
            onRender: function() {
                if(this.collection && this.collection.length === 0) {
                    var key = "myStatus.addNewGateway",
                        clickDestination = "setup/gateway";

                    this.addNewAutomationTile = new App.Consumer.Equipment.Views.AddNewTileView({
                        i18nTextKey: key,
                        size: "regular",
                        classes: "add-gateway-tile",
                        clickDestination: clickDestination
                    });
                    this.$el.prepend(this.addNewAutomationTile.render().$el);
                }
            }
		});

		Views.CategoriesTabView = Mn.LayoutView.extend({
			id: "bb-categories-tab",
			template: templates["equipment/myEquipment/categoriesTab"],
			regions: {
				"categoriesRegion": "#bb-categories-region"
			},
			onRender: function () {
				var that = this;

				this.showSpinner(null, this.$el.find("#bb-categories-region"));

				App.salusConnector.getDataLoadPromise(["devices"]).then(function (/*arrayOfData*/) {
                    var collection = App.salusConnector.getDeviceCollectionSortedByCategory();
                    if(collection && collection.length === 0) {
                        that.$(".bb-no-gateway-text").removeClass("hidden");
                    }
                    that.categoriesRegion.show(new Views.Categories({
                        collection: collection
                    }));
				});
			}
		}).mixin([SalusTabView]);
	});

	return App.Consumer.Equipment.Views.CategoriesTabView;
});