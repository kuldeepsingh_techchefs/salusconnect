"use strict";

define([
	"app",
	"moment",
	"bluebird",
	"common/constants",
	"common/config",
	"common/util/equipmentUtilities",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/mixins/mixin.thermostatArcView",
	"consumer/views/mixins/mixin.FC600ArcView",
	"consumer/views/mixins/mixin.batteryLevel",
	"roundslider",
	"consumer/dashboard/views/tileContentViews/thermostatSpecific/ModeMenu.view"
], function (App, moment, P, constants, config, equipUtils, consumerTemplates, SalusView, ThermostatArcViewMixin, FC600ArcViewMixin, BatteryLevel) {

	/**
	 * these are the views that will be shown in the status widget dependent on device type
	 */
	App.module("Consumer.Equipment.Views", function (Views, App, B, Mn, $, _) {
		Views.StatusWidgetContentMonitoringDevice = Mn.ItemView.extend({
			template: consumerTemplates["equipment/widgets/statusWidgetMonitoringDevice"],
			className: "status-widget-monitoring col-xs-12",
            ui: {
				frontIcon: ".bb-equipment-icon"
			},
            initialize: function () {
                var that = this;
                _.bindAll(this, "_addMonitorBinding");

                var prop = "ErrorIASZSAlarmed1";
                this.listenTo(this.model.get(prop), "propertiesSynced", function() {
					that.hideSpinner();
				});
				this.listenTo(this.model.get(prop), "propertiesFailedSync", function () {
					that.hideSpinner();
					App.Consumer.Controller.showDeviceSyncError(that.model);
				});
				
            },
			onRender: function () {
                var vClass = "window-monitor";
                if(this.model.modelType === constants.modelTypes.WINDOWMONITOR){
                    vClass = "window-monitor";
                }
                else if(this.model.modelType === constants.modelTypes.DOORMONITOR){
                    vClass = "door-monitor";
                }
                this.ui.frontIcon.addClass(vClass);
                this._addMonitorBinding("." + vClass);
			},
            _addMonitorBinding: function (vClass) {
				this.addBinding(null, vClass, {
					observe: "ErrorIASZSAlarmed1",
					onGet: function (val) {
						return val ? val.getProperty() : val;
					},
					update: function ($el, val) {
						if (val !== false) {
							$el.removeClass("noopen open");
							if (val === 0) {
								$el.addClass("noopen");
							} else if (val === 1) {
								$el.addClass("open");
							}
						}
					}
				});
			}
            
		}).mixin([SalusView, BatteryLevel]);

		/**
		 * to be used with smart plug or water heater
		 */
		Views.StatusWidgetContentWithSwitcher = Mn.ItemView.extend({
			template: consumerTemplates["equipment/widgets/statusWidgetWithSwitcher"],
			className: "status-widget-switcher col-xs-12",
			ui: {
				icon: ".bb-equipment-icon",
				switcher: ".bb-status-switcher",
                lockHotwaterRow: ".bb-hotwater-lock-row"
			},
           
			events: {
				"click @ui.switcher": "_toggleState",
                "click .bb-hotwater-lock-icon": "toggleLockHotwater"
			},
			modelEvents: function () {
				// for water heater
				if (!this.model.isSmartPlug()) {
					return {
						"setting:HoldType": "handleSetHoldType",
						"change:cachedMode": "handleChangeCachedMode"
					};
				} else {
					return false;
				}
			},
			initialize: function () {
				var that = this, prop = this.model.isSmartPlug() ? "OnOff" : "HoldType";

				_.bindAll(this, "_toggleState", "_addSPBinding", "_addSPSwitcherBinding", "_addWHBinding", "_addWHSwitcherBinding", "_addWHLockBinding", "_addWHTextLockBinding", "handleSetHoldType", "handleChangeCachedMode");

				this.spinnerOptions = {
					textKey: constants.spinnerTextKeys.processing,
					greyBg: true
				};
                
                if (this.model.modelType === constants.modelTypes.WATERHEATER) {
					this.listenTo(this.model.get("LockKey"), "propertiesSynced", this.hideSpinner);
					this.listenTo(this.model.get("LockKey"), "propertiesFailedSync", function () {
						that.hideSpinner();
						App.Consumer.Controller.showDeviceSyncError(that.model);
					});
				} 

				this.listenTo(this.model.get(prop), "propertiesSynced", function() {
					that.hideSpinner();
				});
				this.listenTo(this.model.get(prop), "propertiesFailedSync", function () {
					that.hideSpinner();
					App.Consumer.Controller.showDeviceSyncError(that.model);
				});
			},
			onRender: function () {
				if (this.model.isSmartPlug()) {
					this.ui.icon.addClass("smart-plug");
					this._addSPBinding();
					this.ui.switcher.addClass("on-off");
					this._addSPSwitcherBinding();
				} else {
					this.ui.icon.addClass("water-heater");
					this._addWHBinding();
					this.ui.switcher.addClass("on-off-auto");
					this._addWHSwitcherBinding();
                    
				}
                if(this.model.modelType === constants.modelTypes.WATERHEATER){
                    this._addWHLockBinding();
                    this._addWHTextLockBinding();
                }
			},
			/**
			 * setup a binding for smart plug
			 * @private
			 */
			_addSPBinding: function () {
				this.addBinding(null, ".smart-plug", {
					observe: "OnOff",
					onGet: function (val) {
//						if (val && moment(val.get("getterProperty").get("data_updated_at")) <= moment(val.get("setterProperty").get("data_updated_at"))) {
//							return false;
//						}
						return val ? val.getProperty() : val;
					},
					update: function ($el, val) {
						if (val !== false) {
							$el.removeClass("on off");
							if (val === constants.smartPlugOnOff.ON) {
								$el.addClass("on");
							} else if (val === constants.smartPlugOnOff.OFF || true) {
								// default off
								$el.addClass("off");
							}
						}
					}
				});
			},
			_addSPSwitcherBinding: function () {
				this.addBinding(null, ".bb-status-switcher", {
					observe: "OnOff",
					onGet: function (val) {
						return val ? val.getProperty() : val;
					},
					update: function ($el, val) {
						$el.removeClass("on off");
						if (val === constants.smartPlugOnOff.ON) {
							$el.addClass("on");
						} else if (val === constants.smartPlugOnOff.OFF || true) {
							// default off
							$el.addClass("off");
						}
					}
				});
			},
			/**
			 * setup a binding for water heater
			 * @private
			 */
			_addWHBinding: function () {
				this.addBinding(null, ".water-heater", {
					observe: "RunningMode",
					onGet: function (prop) {
						return prop ? prop.getProperty() : prop;
					},
					update: function ($el, val) {
						$el.removeClass("on off");
						if (val === constants.runningModeTypes.HEAT) {
							$el.addClass("on");
						} else if (val === constants.runningModeTypes.OFF || true) {
							// default off
							$el.addClass("off");
						}
					}
				});
			},
			_addWHSwitcherBinding: function () {
				this.addBinding(null, ".bb-status-switcher", {
					observe: "HoldType",
					onGet: function (prop) {
						return prop ? prop.getProperty() : prop;
					},
					update: function ($el, val) {
						$el.removeClass("on auto off");

						if (val === constants.waterHeaterHoldTypes.CONTINUOUS || val === constants.waterHeaterHoldTypes.BOOST) {
							$el.addClass("on");
						} else if (val === constants.waterHeaterHoldTypes.SCHEDULE) {
							$el.addClass("auto");
						} else if (val === constants.waterHeaterHoldTypes.OFF || true) {
							// default off
							$el.addClass("off");
						}
					}
				});
			},
            _addWHLockBinding: function () {
				this.addBinding(null, ".bb-hotwater-lock-icon", {
					observe: "LockKey",
                    onGet: function (prop) {
                        return prop ? prop.getProperty() : prop;
                    },
                    update: function ($el, val) {
                        // show state - 1 means locked
                        if (!!val) {
                            $el.addClass("lock").removeClass("unlock");
                        } else {
                            $el.addClass("unlock").removeClass("lock");
                        }
                    }
				});
			},
            _addWHTextLockBinding: function () {
				this.addBinding(null, ".bb-hotwater-lock-text", {
					observe: "LockKey",
                    onGet: function (prop) {
							return prop ? prop.getProperty() : prop;
						},
						update: function ($el, val) {
							// show state - 1 means locked
							if (!!val) {
								$el.text(App.translate("equipment.status.hotwater.lock"));
							} else {
								$el.text(App.translate("equipment.status.hotwater.unlock"));
							}
						}
				});
			},
			_toggleState: function (/*evt*/) {
				if (!this.spinning) {
					if (this.model.isSmartPlug()) {
						this.showSpinner(this.spinnerOptions);
						this.model.toggleSmartPlug();
					} else {
						// spinner added later
						this.model.switchWaterHeaterMode();
					}
				}
			},
            toggleLockHotwater: function (/*event*/) {
                var that = this;

                this.showSpinner({
                    textKey: constants.spinnerTextKeys.processing,
                    greyBg: true
                });

                return this.model.getDevicePropertiesPromise().then(function () {
                    var val = that.model.getPropertyValue("LockKey");

                    return that.model.setProperty("LockKey", !!val ? 0 : 1);
                }).catch(function (err) {
                    that.hideSpinner();

                    return P.reject(err);
                });
			},
			handleSetHoldType: function () {
				this.showSpinner(this.spinnerOptions);
			},
			handleChangeCachedMode: function (modeObj) {
				var mode = modeObj.mode, modes = constants.waterHeaterHoldTypes;

				this.ui.switcher.toggleClass("on", mode === modes.CONTINUOUS)
						.toggleClass("auto", mode === modes.SCHEDULE)
						.toggleClass("off", mode === modes.OFF);
			}
		}).mixin([SalusView]);
		
		Views.StatusWidgetContentThermostat = Mn.ItemView.extend({
			className: "status-widget-thermostat col-xs-12",
			template: consumerTemplates["equipment/widgets/statusWidgetThermostat"],
			ui: {
				lockThermostatRow: ".bb-thermostat-lock-row"
			},
			bindings: function () {
				var observedProp = this.model.isIT600 ? "LockKey" : "KeyLock";

				return {
					".bb-thermostat-lock-text": {
						observe: observedProp,
						onGet: function (prop) {
							return prop ? prop.getProperty() : prop;
						},
						update: function ($el, val) {
							// show state - 1 means locked
							if (!!val) {
								$el.text(App.translate("equipment.status.thermostat.lock"));
							} else {
								$el.text(App.translate("equipment.status.thermostat.unlock"));
							}
						}
					},
					".bb-thermostat-lock-icon": {
						observe: observedProp,
						onGet: function (prop) {
							return prop ? prop.getProperty() : prop;
						},
						update: function ($el, val) {
							// show state - 1 means locked
							if (!!val) {
								$el.addClass("lock").removeClass("unlock");
							} else {
								$el.addClass("unlock").removeClass("lock");
							}
						}
					}
				};
			},
			events: {
				"click .bb-thermostat-lock-icon": "toggleLockThermostat"
			},
			initialize: function () {
				var that = this;

				if (this.model.isIT600) {
					this.listenTo(this.model.get("LockKey"), "propertiesSynced", this.hideSpinner);
					this.listenTo(this.model.get("LockKey"), "propertiesFailedSync", function () {
						that.hideSpinner();
						App.Consumer.Controller.showDeviceSyncError(that.model);
					});
				} else {
					this.listenTo(this.model.get("KeyLock"), "propertiesSynced", this.hideSpinner);
					this.listenTo(this.model.get("KeyLock"), "propertiesFailedSync", function () {
						that.hideSpinner();
						App.Consumer.Controller.showDeviceSyncError(that.model);
					});
				}
			},
			onRender: function () {
				// Triggers the polling function on the thermostat mixin. Also used by tile view, hence the name
				//this.trigger("tile:flipToBack");
				//this.model.trigger("tile:flipToBack");
			},
			toggleLockThermostat: function (/*event*/) {
				var that = this;

				this.showSpinner({
					textKey: constants.spinnerTextKeys.processing,
					greyBg: true
				});

				return this.model.getDevicePropertiesPromise().then(function () {
					var val = that.model.getPropertyValue("LockKey");

					return that.model.setProperty("LockKey", !!val ? 0 : 1);
				}).catch(function (err) {
					that.hideSpinner();

					return P.reject(err);
				});
			},
			onBeforeDestroy: function () {
				// Triggers the killing of the polling function on the thermostat mixin. Also used by tile view, hence the name
				//this.trigger("tile:flipToFront");
			}
		}).mixin([ThermostatArcViewMixin, SalusView]);

		Views.StatusWidgetContentFC600 = Mn.ItemView.extend({
			className: "status-widget-thermostat status-widget-fc600 col-xs-12",
			template: consumerTemplates["equipment/widgets/statusWidgetFC600"],
			ui: {
				lockThermostatRow: ".bb-thermostat-lock-row"
			},
			bindings: function () {
				var observedProp = "LockKey";

				return {
					".bb-thermostat-lock-text": {
						observe: observedProp,
						onGet: function (prop) {
							return prop ? prop.getProperty() : prop;
						},
						update: function ($el, val) {
							// show state - 1 means locked
							if (!!val) {
								$el.text(App.translate("equipment.status.thermostat.lock"));
							} else {
								$el.text(App.translate("equipment.status.thermostat.unlock"));
							}
						}
					},
					".bb-thermostat-lock-icon": {
						observe: observedProp,
						onGet: function (prop) {
							return prop ? prop.getProperty() : prop;
						},
						update: function ($el, val) {
							// show state - 1 means locked
							if (!!val) {
								$el.addClass("lock").removeClass("unlock");
							} else {
								$el.addClass("unlock").removeClass("lock");
							}
						}
					}
				};
			},
			events: {
				"click .bb-thermostat-lock-icon": "toggleLockThermostat"
			},
			initialize: function () {
				var that = this;

				this.listenTo(this.model.get("LockKey"), "propertiesSynced", this.hideSpinner);
				this.listenTo(this.model.get("LockKey"), "propertiesFailedSync", function () {
					that.hideSpinner();
					App.Consumer.Controller.showDeviceSyncError(that.model);
				});
			},
			onRender: function () {
			},
			toggleLockThermostat: function (/*event*/) {
				var deviceSetting = this.model.getPropertyValue("DeviceSetting");
				if (deviceSetting[0] === "1") {
					var that = this;

					this.showSpinner({
						textKey: constants.spinnerTextKeys.processing,
						greyBg: true
					});

					return this.model.getDevicePropertiesPromise().then(function () {
						var val = that.model.getPropertyValue("LockKey");

						return that.model.setProperty("LockKey", !!val ? 0 : 1);
					}).catch(function (err) {
						that.hideSpinner();

						return P.reject(err);
					});
				}
			},
			onBeforeDestroy: function () {
			}
		}).mixin([FC600ArcViewMixin, SalusView]);

		Views.StatusWidgetContentEnergyMeter = Mn.ItemView.extend({
			template: consumerTemplates["equipment/widgets/statusWidgetEnergyMeter"],
			className: "status-widget-energy-meter col-xs-12"
		}).mixin([SalusView, BatteryLevel]);

		Views.StatusWidgetContentGeneric = Mn.ItemView.extend({
			template: consumerTemplates["equipment/widgets/statusWidgetGeneric"],
			className: "status-widget-generic col-xs-12",
			ui: {
				icon: ".bb-equipment-icon"
			},
			initialize: function (options) {
				this.iconPath = options.iconPath;
			},
			onRender: function () {
				this.ui.icon.css("background-image", "url(" + this.iconPath + ")");
			}
		}).mixin([SalusView]);

		// this is for the rows at the bottom of some of the devices
		// I think it's for the groups, but it's also for events
		Views.StatusWidgetCollectionView = Mn.CollectionView.extend({
			className: "equip-status-table col-xs-12",
			getChildView: function () {
				if (this.mode === "groups") {
					return Views.StatusWidgetGroupRow;
				} else if (this.mode === "events") {
					return Views.StatusWidgetEventRow;
				}
			},
			initialize: function (options) {
				this.mode = options.type;

				// TODO: mocking
				if (this.mode === "events") {
					this.collection = new B.Collection([
						{timing: "Now", description: "On for 10 minutes"},
						{timing: "Later", description: "Something else happens"}
					]);
				} else if (this.mode === "groups") {
					this.collection = new B.Collection([
						{name: "Group 1"},
						{name: "Group 2"}
					]);
				}
			}
		}).mixin([SalusView]);

		// rows to fill out the collection view above.
		Views.StatusWidgetEventRow = Mn.ItemView.extend({
			template: consumerTemplates["equipment/widgets/statusEventRow"],
			className: "event-row row",
			bindings: {
				".bb-event-timing": "timing",
				".bb-event-description": "description"
			}
		}).mixin([SalusView]);

		Views.StatusWidgetGroupRow = Mn.ItemView.extend({
			template: consumerTemplates["equipment/widgets/statusGroupRow"],
			className: "group-row row",
			bindings: {
				".bb-group-name": "name"
			}
		}).mixin([SalusView]);
	});

	return App.Consumer.Equipment.Views;
});