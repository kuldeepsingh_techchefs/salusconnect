"use strict";

define([
	"app",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/SalusButtonPrimary.view"
], function (App, consumerTemplates, SalusViewMixin, SalusPrimaryButtonView) {

	App.module("Consumer.Login.Views", function (Views, App, B, Mn, $, _) {
		Views.ChangePasswordConfirmationModalView = Mn.LayoutView.extend({
			className: "modal-body",
			template: consumerTemplates["login/changePasswordConfirmationModal"],
			regions: {
				centerBtnRegion: "#bb-center-btn-area"
			},
			events: {
				"keydown": "handleEnterButtonPress"
			},
			initialize: function () {
				_.bindAll(this, "handleSignInClick");

				this.signInBtn = new SalusPrimaryButtonView({
					id: "bb-sign-in-btn",
					classes: "width100",
					buttonTextKey: "login.forgotPasswordConfirmation.button",
					clickedDelegate: this.handleSignInClick
				});
			},
			handleSignInClick: function () {
				App.hideModal();
				App.navigate("login/signIn");
			},
			handleEnterButtonPress: function (event) {
				if (event.which && event.which === 13) {
					this.handleSignInClick();
				}
			},
			onRender: function () {
//				this.$("#bb-secondary-label").text(App.translate("common.labels.saved"));
//
				this.centerBtnRegion.show(this.signInBtn);
			}
		}).mixin([SalusViewMixin]);
	});


	return App.Consumer.Login.Views.ChangePasswordConfirmationModalView;
});