"use strict";

define([
	"app",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/SalusButtonPrimary.view",
	"consumer/models/SalusCheckboxViewModel",
	"consumer/views/SalusCheckbox.view",
	"consumer/settings/views/EmptyList.view"
], function (App, consumerTemplates, SalusView) {

	App.module("Consumer.Schedules.Views.Modal", function (Views, App, B, Mn) {
		Views.IndividualControlRow = Mn.ItemView.extend({
			template: false,
			className: "individual-control-row",
			bindings: {
				":el": "name"
			}
		}).mixin([SalusView]);

		Views.IndividualControlCollection = Mn.CollectionView.extend({
			childView: Views.IndividualControlRow,
			emptyView: App.Consumer.Settings.Views.EmptyListView,
			emptyViewOptions: {
				tagName: "div",
				i18nTextKey: "schedules.edit.modal.emptyThermostats"
			},
			className: "individual-controls-table"
		}).mixin([SalusView]);

		return App.Consumer.Schedules.Views.Modal.IndividualControlCollection;
	});
});