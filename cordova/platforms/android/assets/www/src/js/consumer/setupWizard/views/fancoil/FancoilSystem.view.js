"use strict";

define([
	"app",
	"consumer/consumerTemplates",
    "consumer/views/SalusModal.view",
	"consumer/models/SalusRadioViewModel",
	"consumer/views/SalusRadio.view",
	"consumer/views/SalusButtonPrimary.view",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/mixins/mixin.registeredRegions",
    "consumer/setupWizard/views/fancoil/FancoilTerminal.view"
], function (App, consumerTemplates, SalusModal) {

	App.module("Consumer.SetupWizard.Fancoil.Views", function (Views, App, B, Mn, $, _) {

		Views.FancoilSystemLayout = Mn.LayoutView.extend({
			template: consumerTemplates["setupWizard/fancoil/fancoilType"],
			className: "container",
			regions: {
				"radioPipe2": ".bb-2-pipe",
				"radioPipe4": ".bb-4-pipe",
				"nextButton": ".bb-next-button"
			},
			
			events: {
				"change input[name=fancoilSystem]": "handleRadioChange"
			},
			initialize: function () {
				_.bindAll(this, "handleRadioChange", "handleNextButtonClicked");

				var pipe4Checked = false;
                this.fancoilType = 0;
                if(this.model.getPropertyValue("FanCoilType") === 1){
                    pipe4Checked = true;
                    this.fancoilType = 1;
                }
				this.registerRegion("radioPipe2", new App.Consumer.Views.RadioView({
					model: new App.Consumer.Models.RadioViewModel({
						value: 0,
						name: "fancoilSystem",
						isChecked: !pipe4Checked,
						classes: "pull-right data-collection-radio"
					})
				}));

				this.registerRegion("radioPipe4", new App.Consumer.Views.RadioView({
					model: new App.Consumer.Models.RadioViewModel({
						value: 1,
						name: "fancoilSystem",
						isChecked: pipe4Checked,
						classes: "pull-right data-collection-radio"
					})
				}));

				this.nextButtonView = this.registerRegion("nextButton", new App.Consumer.Views.SalusButtonPrimaryView({
					id: "save-data-collection-btn",
					buttonTextKey: "common.labels.next",
					classes: "center-relative padding-l-30 padding-r-30",
					clickedDelegate: this.handleNextButtonClicked
				}));
			},

			handleRadioChange: function () {
				var selected = this.$("input[name=fancoilSystem]").filter(":checked").val();
				this.fancoilType = parseInt(selected);
				
			},
			handleNextButtonClicked: function () {
                var that = this;
				var setPropertyPromise,device = this.model;
				setPropertyPromise = device.setProperty("FanCoilType", this.fancoilType);
				setPropertyPromise.then(function(){
					App.hideModal();
					$(".modal-backdrop").remove();
					var nextView = new App.Consumer.SetupWizard.Fancoil.Views.FancoilTerminalLayout({
						model: that.model
					 });
					 App.modalRegion.show(new SalusModal({
						 contentView: nextView,
						 size: "modal-lg"
					 }));

					 App.showModal(); 
				});
				
			}
		}).mixin([App.Consumer.Views.Mixins.SalusView, App.Consumer.Views.Mixins.RegisteredRegions]);
	});

	return App.Consumer.SetupWizard.Fancoil.Views.FancoilSystemLayout;
});