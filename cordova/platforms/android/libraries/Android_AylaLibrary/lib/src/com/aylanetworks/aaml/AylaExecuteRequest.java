//
//  AylaExecuteRequests.java
//  Ayla Mobile Library
//
//  Created by Daniel Myers on 8/15/12.
//  Copyright (c) 2012 Ayla Networks. All rights reserved.
//
package com.aylanetworks.aaml;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.InterruptedIOException;
import java.net.SocketException;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashSet;

import javax.net.ssl.SSLHandshakeException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.NoHttpResponseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.ExecutionContext;
import org.apache.http.protocol.HTTP;
import org.apache.http.protocol.HttpContext;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.text.TextUtils;



public class AylaExecuteRequest extends IntentService {
	private int responseCode;
	private int method;
	private String message, entity;
	private ArrayList <AylaParcelableNVPair> params;
	private ArrayList <AylaParcelableNVPair> headers;
	private HttpRequestBase request, saveRequest;
	private ResultReceiver receiver;
	private String url;
	private boolean async;
	private int requestRetryCounter = 0;

	public AylaExecuteRequest() {
		super("executeRestRequest");
	}

	// asynchronous service interface
	@Override
	protected void onHandleIntent(Intent intent) {
		params = intent.getParcelableArrayListExtra("params");
		headers = intent.getParcelableArrayListExtra("headers");
		url = intent.getStringExtra("url");
		receiver = (ResultReceiver) intent.getParcelableExtra("receiver");
		method = (int) intent.getIntExtra("method", 1);
		entity = intent.getStringExtra("entity");
		async = intent.getBooleanExtra("async", true);
		
		// complete for Host based calls
		AylaRestService rs = new AylaRestService();
		rs.jsonResults = intent.getStringExtra("result");
		rs.subTaskFailed = intent.getIntExtra("subTask", 1);
		rs.responseCode = intent.getIntExtra("responseCode", 1);
		rs.info = intent.getStringExtra("info");
		
		try {
			execute(method, rs);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// sync interface
	protected AylaCallResponse handleIntent(Intent intent, AylaRestService rs) throws Exception{
		params = intent.getParcelableArrayListExtra("params");
		headers = intent.getParcelableArrayListExtra("headers");
		url = intent.getStringExtra("url");
		receiver = (ResultReceiver) intent.getParcelableExtra("receiver");
		method = (int) intent.getIntExtra("method", 1);
		entity = intent.getStringExtra("entity");
		async = intent.getBooleanExtra("async", false);
		try {
			return execute(method, rs);
		} catch (Exception e) {
			e.printStackTrace();
			Bundle bundle = new Bundle();
			bundle.putString("result", e.getLocalizedMessage()); //  response
			responseCode = AylaNetworks.AML_GENERAL_EXCEPTION; // djm check response code
			return new AylaCallResponse(responseCode, bundle);
		}
	}

    /**
     * Returns true if the supplied method supports offline results.
     * @param method ID of the method to check
     * @return True if the method supports an offline response, false otherwise
     */
    private static boolean supportsOfflineResponse(int method) {
        switch (method) {
            case AylaRestService.GET_DEVICES_LANMODE:
            case AylaRestService.GET_NODES_LANMODE:
            case AylaRestService.CREATE_DATAPOINT_LANMODE:
            case AylaRestService.GET_DATAPOINT_LANMODE:
            case AylaRestService.GET_DATAPOINTS_LANMODE:
            case AylaRestService.GET_PROPERTIES_LANMODE:
            case AylaRestService.GET_PROPERTY_DETAIL_LANMODE:
            case AylaRestService.POST_USER_LOGIN:

			// These APIs are used during wifi setup / registration where we may not be
			// connected to the cloud, but rather to the device's wifi AP.
            case AylaRestService.SET_DEVICE_CONNECT_TO_NETWORK:
            case AylaRestService.GET_MODULE_WIFI_STATUS:
            case AylaRestService.START_NEW_DEVICE_SCAN_FOR_APS:
            case AylaRestService.GET_NEW_DEVICE_STATUS:
            case AylaRestService.PUT_NEW_DEVICE_TIME:
            case AylaRestService.DELETE_DEVICE_WIFI_PROFILE:
            case AylaRestService.CONNECT_NEW_DEVICE_TO_SERVICE:
            case AylaRestService.SET_HOST_NETWORK_CONNECTION:
            case AylaRestService.DELETE_HOST_NETWORK_CONNECTION:
            case AylaRestService.DELETE_HOST_NETWORK_CONNECTIONS:
            case AylaRestService.CONFIRM_NEW_DEVICE_TO_SERVICE_CONNECTION:
            	
            case AylaRestService.RETURN_HOST_SCAN:
            case AylaRestService.RETURN_HOST_NETWORK_CONNECTION:
            case AylaRestService.CONNECT_TO_NEW_DEVICE:
            case AylaRestService.GET_NEW_DEVICE_SCAN_RESULTS_FOR_APS:                 

			// While this call should require network access, it is made right after re-connecting
			// to the original wifi network during wifi setup, and on certain devices the
			// check for reachability fails before this call is made. Including this API
			// in the list here allows these devices to complete setup.
            case AylaRestService.GET_NEW_DEVICE_CONNECTED:
            
            // for same-lan wifi setup
            case AylaRestService.POST_LOCAL_REGISTRATION:
            case AylaRestService.DELETE_NETWORK_PROFILE_LANMODE:
            case AylaRestService.SEND_NETWORK_PROFILE_LANMODE:
            case AylaRestService.GET_NEW_DEVICE_WIFI_STATUS:

            // Confirm reachability state is finalized locally, either service or device, or both. 
            case AylaRestService.REACHABILITY:
                return true;
        }
        return false;
    }
    
    /**
     * Returns true if the supplied method supports LAN mode results.
     * @param method ID of the method to check
     * @return True if the method supports an LAN mode response, false otherwise
     */
    private static boolean supportsLanModeResponse(int method) {
        switch (method) {
            case AylaRestService.PUT_LOCAL_REGISTRATION:
            case AylaRestService.POST_LOCAL_REGISTRATION:
            case AylaRestService.PROPERTY_CHANGE_NOTIFIER:
//            case AylaRestService.SECURE_SETUP_SESSION_COMPLETED:
            case AylaRestService.REACHABILITY:
            case AylaRestService.GET_DEVICES_LANMODE:
            case AylaRestService.GET_NODES_LANMODE:
            case AylaRestService.GET_PROPERTIES_LANMODE:
            case AylaRestService.GET_PROPERTY_DETAIL_LANMODE:
            case AylaRestService.GET_DATAPOINT_LANMODE:
            case AylaRestService.GET_DATAPOINTS_LANMODE:
            case AylaRestService.CREATE_DATAPOINT_LANMODE:
            	
            // For secure wifi setup
            case AylaRestService.SEND_NETWORK_PROFILE_LANMODE:
            // For delete wifi profile secure session
            case AylaRestService.DELETE_NETWORK_PROFILE_LANMODE:
            	
                return true;
        }
        return false;
    }


	private AylaCallResponse execute(int method, AylaRestService rs) throws Exception
	{
		requestRetryCounter = 3;
		AylaCallResponse commitResponse = null;
		Bundle responseBundle = null;

        if (!AylaReachability.isCloudServiceAvailable()) {
            if (AylaReachability.isDeviceLanModeAvailable() && supportsLanModeResponse(method)) {
                // We aren't connected to the cloud, but we are connected to the LAN mode device
                AylaSystemUtils.saveToLog("%s, %s, %s:%d, %s", "V", "ExecuteRequest", "lanMode", method, "execute");
            } else if (!supportsOfflineResponse(method)) {
                // Make sure the method supports cached results if we cannot reach the service.\
                // return failure here
                AylaSystemUtils.saveToLog("%s, %s, %s:%d, %s", "E", "ExecuteRequest", "!cloud && !supportOffline", method, "execute");
                responseBundle = new Bundle();
                responseBundle.putString("result", rs.jsonResults); //  response v3.07
                responseBundle.putInt("subTask", rs.subTaskFailed);
                responseCode = AylaNetworks.AML_ERROR_UNREACHABLE;
    
                if (async) {
                    receiver.send(responseCode, responseBundle);
                } else {
                    commitResponse =  new AylaCallResponse(responseCode, responseBundle);
                }
                return commitResponse;
            } else {
                AylaSystemUtils.saveToLog("%s, %s, %s:%d, %s", "V", "ExecuteRequest", "cached", method, "execute");
            }
        }
        

		switch(method)
		{
		case AylaRestService.CREATE_GROUP_ZIGBEE:
		//case AylaRestService.GET_GROUP_ZIGBEE:
		//case AylaRestService.GET_GROUPS_ZIGBEE:
		case AylaRestService.UPDATE_GROUP_ZIGBEE:
		case AylaRestService.TRIGGER_GROUP_ZIGBEE:
		case AylaRestService.DELETE_GROUP_ZIGBEE:
		{
			if (!url.equals(AylaSystemUtils.ERR_URL)) {
				if (method == AylaRestService.CREATE_GROUP_ZIGBEE) {
					request = new HttpPost(url);
				} else 
				if (method == AylaRestService.UPDATE_GROUP_ZIGBEE || method == AylaRestService.TRIGGER_GROUP_ZIGBEE) {
					request = new HttpPut(url);
				} else
				if (method == AylaRestService.GET_GROUPS_ZIGBEE || method == AylaRestService.GET_GROUP_ZIGBEE) {
					request = new HttpGet(url);
				}else
				if (method == AylaRestService.DELETE_GROUP_ZIGBEE) {
					request = new HttpDelete(url);
				}
				
				for(NameValuePair h : headers) {
					request.addHeader(h.getName(), h.getValue());
				}
				if(!params.isEmpty()) {
					((HttpPost) request).setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
				}
				if (!TextUtils.isEmpty(entity)) {
					StringEntity se = new StringEntity(entity, HTTP.UTF_8);
					if (method == AylaRestService.CREATE_GROUP_ZIGBEE) {
						((HttpPost) request).setEntity(se);
					} else 
					if (method == AylaRestService.UPDATE_GROUP_ZIGBEE || method == AylaRestService.TRIGGER_GROUP_ZIGBEE) {
							((HttpPut) request).setEntity(se);
					} else { // get or delete
						for(NameValuePair h : headers) {
							request.addHeader(h.getName(), h.getValue());
						}
					}
				}
				commitResponse = commit(rs);
				break;
			}
		}
		case AylaRestService.CREATE_BINDING_ZIGBEE:
		//case AylaRestService.GET_BINDING_ZIGBEE:
		//case AylaRestService.GET_BINDINGS_ZIGBEE:
		case AylaRestService.UPDATE_BINDING_ZIGBEE:
		case AylaRestService.DELETE_BINDING_ZIGBEE:
		{
			if (!url.equals(AylaSystemUtils.ERR_URL)) {
				if (method == AylaRestService.CREATE_BINDING_ZIGBEE) {
					request = new HttpPost(url);
				} else 
				if (method == AylaRestService.UPDATE_BINDING_ZIGBEE) {
					request = new HttpPut(url);
				} else
				if (method == AylaRestService.GET_BINDINGS_ZIGBEE || method == AylaRestService.GET_BINDING_ZIGBEE) {
					request = new HttpGet(url);
				}else
				if (method == AylaRestService.DELETE_BINDING_ZIGBEE) {
					request = new HttpDelete(url);
				}
				
				for(NameValuePair h : headers) {
					request.addHeader(h.getName(), h.getValue());
				}
				if(!params.isEmpty()) {
					((HttpPost) request).setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
				}
				if (!TextUtils.isEmpty(entity)) {
					StringEntity se = new StringEntity(entity, HTTP.UTF_8);
					if (method == AylaRestService.CREATE_BINDING_ZIGBEE) {
						((HttpPost) request).setEntity(se);
					} else 
					if (method == AylaRestService.UPDATE_BINDING_ZIGBEE) {
							((HttpPut) request).setEntity(se);
					} else { // get or delete
						for(NameValuePair h : headers) {
							request.addHeader(h.getName(), h.getValue());
						}
					}
				}
				commitResponse = commit(rs);
				break;
			}
		}
		case AylaRestService.CREATE_SCENE_ZIGBEE:
		//case AylaRestService.GET_SCENE_ZIGBEE:
		//case AylaRestService.GET_SCENES_ZIGBEE:
		case AylaRestService.UPDATE_SCENE_ZIGBEE:
		case AylaRestService.RECALL_SCENE_ZIGBEE:
		case AylaRestService.DELETE_SCENE_ZIGBEE:
		{
			if (!url.equals(AylaSystemUtils.ERR_URL)) {
				if (method == AylaRestService.CREATE_SCENE_ZIGBEE) {
					request = new HttpPost(url);
				} else 
				if (method == AylaRestService.UPDATE_SCENE_ZIGBEE || method == AylaRestService.RECALL_SCENE_ZIGBEE) {
					request = new HttpPut(url);
				} else
				if (method == AylaRestService.GET_SCENES_ZIGBEE || method == AylaRestService.GET_SCENE_ZIGBEE) {
					request = new HttpGet(url);
				}else
				if (method == AylaRestService.DELETE_SCENE_ZIGBEE) {
					request = new HttpDelete(url);
				}
				
				for(NameValuePair h : headers) {
					request.addHeader(h.getName(), h.getValue());
				}
				if(!params.isEmpty()) {
					((HttpPost) request).setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
				}
				if (!TextUtils.isEmpty(entity)) {
					StringEntity se = new StringEntity(entity, HTTP.UTF_8);
					if (method == AylaRestService.CREATE_SCENE_ZIGBEE) {
						((HttpPost) request).setEntity(se);
					} else 
						if (method == AylaRestService.UPDATE_SCENE_ZIGBEE || method == AylaRestService.RECALL_SCENE_ZIGBEE) {
							((HttpPut) request).setEntity(se);
					} else { // get or delete
						for(NameValuePair h : headers) {
							request.addHeader(h.getName(), h.getValue());
						}
					}
				}
				commitResponse = commit(rs);
				break;
			}
		}
		case AylaRestService.CREATE_USER_SHARE:
		case AylaRestService.GET_USER_SHARE:
		//case AylaRestService.GET_USER_SHARES:
		case AylaRestService.UPDATE_USER_SHARE:
		case AylaRestService.DELETE_USER_SHARE:
		{
			if (!url.equals(AylaSystemUtils.ERR_URL)) {
				if (method == AylaRestService.CREATE_USER_SHARE) {
					request = new HttpPost(url);
				} else 
				if (method == AylaRestService.UPDATE_USER_SHARE) {
					request = new HttpPut(url);
				} else
				if (method == AylaRestService.GET_USER_SHARE) {
					request = new HttpGet(url);
				}else
				if (method == AylaRestService.DELETE_USER_SHARE) {
					request = new HttpDelete(url);
				}
				
				for(NameValuePair h : headers) {
					request.addHeader(h.getName(), h.getValue());
				}
				if(!params.isEmpty()) {
					((HttpPost) request).setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
				}
				if (!TextUtils.isEmpty(entity)) {
					StringEntity se = new StringEntity(entity, HTTP.UTF_8);
					if (method == AylaRestService.CREATE_USER_SHARE) {
						((HttpPost) request).setEntity(se);
					} else 
					if (method == AylaRestService.UPDATE_USER_SHARE) {
							((HttpPut) request).setEntity(se);
					} else { // get or delete
						for(NameValuePair h : headers) {
							request.addHeader(h.getName(), h.getValue());
						}
					}
				}
				commitResponse = commit(rs);
				break;
			} else { // return errors to handler
				responseBundle = new Bundle();
				responseBundle.putString("result", rs.jsonResults); //  response v3.07
				responseBundle.putInt("subTask", rs.subTaskFailed);
				responseCode = rs.responseCode;

				receiver.send(responseCode, responseBundle);
				break;
			}
		}
		case AylaRestService.LOGIN_THROUGH_OAUTH :	// Compound objects, no service API, just return to handler
		{
			responseBundle = new Bundle();
			responseBundle.putString("result", rs.jsonResults);
			responseBundle.putInt("subTask", rs.subTaskFailed);
			responseCode = rs.responseCode;

			receiver.send(responseCode, responseBundle);
			break;
		}
		case AylaRestService.CREATE_USER_CONTACT:
		case AylaRestService.UPDATE_USER_CONTACT:
//		case AylaRestService.GET_USER_CONTACT:
//		case AylaRestService.GET_USER_CONTACT_LIST:
		case AylaRestService.DELETE_USER_CONTACT:
		{
			if (!AylaSystemUtils.ERR_URL.equals(url)) {
				
				if (method == AylaRestService.CREATE_USER_CONTACT) {
					request = new HttpPost(url);
				} else if (method == AylaRestService.UPDATE_USER_CONTACT) {
					request = new HttpPut(url);
				} 
//				else if (method == AylaRestService.GET_USER_CONTACT 
//						|| method == AylaRestService.GET_USER_CONTACT_LIST) {
//					request = new HttpGet(url);
//				} 
				else if (method == AylaRestService.DELETE_USER_CONTACT) {
					request = new HttpDelete(url);
				} else {
					// TODO: make log indicating something wrong.
				}
				
				for (NameValuePair h : headers) {
					request.addHeader(h.getName(), h.getValue());
				}
				
				if (!params.isEmpty()) {
					((HttpPost) request).setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
				}
				if (!TextUtils.isEmpty(entity)) {
					StringEntity se = new StringEntity(entity, HTTP.UTF_8);
					if (method == AylaRestService.CREATE_USER_CONTACT) {
						((HttpPost)request).setEntity(se);
					} else if (method == AylaRestService.UPDATE_USER_CONTACT) {
						((HttpPut)request).setEntity(se);
					} else { // get or delete
						for (NameValuePair h : headers) {
							request.addHeader(h.getName(), h.getValue());
						}
					}
				}
				commitResponse = commit(rs);
			} else {
				responseBundle = new  Bundle();
				responseBundle.putString("result", rs.jsonResults);
				responseBundle.putInt("subTask", rs.subTaskFailed);
				responseCode = rs.responseCode;
				
				receiver.send(responseCode, responseBundle);
			}// end of if error_url.equals(url)
			break;
		}
		case AylaRestService.CREATE_USER_METADATA:
		case AylaRestService.GET_USER_METADATA_BY_KEY:
		//case AylaRestService.GET_USER_METADATA:
		case AylaRestService.UPDATE_USER_METADATA:
		case AylaRestService.DELETE_USER_METADATA:
		{
			if (!url.equals(AylaSystemUtils.ERR_URL)) {
				if (method == AylaRestService.CREATE_USER_METADATA) {
					request = new HttpPost(url);
				} else 
				if (method == AylaRestService.UPDATE_USER_METADATA) {
					request = new HttpPut(url);
				} else
				if (method == AylaRestService.GET_USER_METADATA_BY_KEY) {
					request = new HttpGet(url);
				}else
				if (method == AylaRestService.DELETE_USER_METADATA) {
					request = new HttpDelete(url);
				}
				
				for(NameValuePair h : headers) {
					request.addHeader(h.getName(), h.getValue());
				}
				if(!params.isEmpty()) {
					((HttpPost) request).setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
				}

				if (!TextUtils.isEmpty(entity)) {
					StringEntity se = new StringEntity(entity, HTTP.UTF_8);
					if (method == AylaRestService.CREATE_USER_METADATA) {
						((HttpPost) request).setEntity(se);
					} else 
					if (method == AylaRestService.UPDATE_USER_METADATA) {
							((HttpPut) request).setEntity(se);
					} else { // get or delete
						for(NameValuePair h : headers) {
							request.addHeader(h.getName(), h.getValue());
						}
					}
				}
				commitResponse = commit(rs);
				break;
			} else { // return errors to handler
				responseBundle = new Bundle();
				responseBundle.putString("result", rs.jsonResults); //  response v3.07
				responseBundle.putInt("subTask", rs.subTaskFailed);
				responseCode = rs.responseCode;

				receiver.send(responseCode, responseBundle);
				break;
			}
		}
		case AylaRestService.CREATE_DEVICE_METADATA:
		case AylaRestService.GET_DEVICE_METADATA_BY_KEY:
		//case AylaRestService.GET_DEVICE_METADATA:
		case AylaRestService.UPDATE_DEVICE_METADATA:
		case AylaRestService.DELETE_DEVICE_METADATA:
		{
			if (!url.equals(AylaSystemUtils.ERR_URL)) {
				if (method == AylaRestService.CREATE_DEVICE_METADATA) {
					request = new HttpPost(url);
				} else 
				if (method == AylaRestService.UPDATE_DEVICE_METADATA) {
					request = new HttpPut(url);
				} else
				if (method == AylaRestService.GET_DEVICE_METADATA_BY_KEY) {
					request = new HttpGet(url);
				}else
				if (method == AylaRestService.DELETE_DEVICE_METADATA) {
					request = new HttpDelete(url);
				}
				
				for(NameValuePair h : headers) {
					request.addHeader(h.getName(), h.getValue());
				}
				if(!params.isEmpty()) {
					((HttpPost) request).setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
				}

				if (!TextUtils.isEmpty(entity)) {
					StringEntity se = new StringEntity(entity, HTTP.UTF_8);
					if (method == AylaRestService.CREATE_DEVICE_METADATA) {
						((HttpPost) request).setEntity(se);
					} else 
					if (method == AylaRestService.UPDATE_DEVICE_METADATA) {
							((HttpPut) request).setEntity(se);
					} else { // get or delete
						for(NameValuePair h : headers) {
							request.addHeader(h.getName(), h.getValue());
						}
					}
				}
				commitResponse = commit(rs);
				break;
			} else { // return errors to handler
				responseBundle = new Bundle();
				responseBundle.putString("result", rs.jsonResults); //  response v3.07
				responseBundle.putInt("subTask", rs.subTaskFailed);
				responseCode = rs.responseCode;

				receiver.send(responseCode, responseBundle);
				break;
			}
		}
		case AylaRestService.CREATE_LOG_IN_SERVICE:
		{
			if (!url.equals(AylaSystemUtils.ERR_URL)) {
				if (method == AylaRestService.CREATE_LOG_IN_SERVICE) {
					request = new HttpPost(url);
				}
				
				for(NameValuePair h : headers) {
					request.addHeader(h.getName(), h.getValue());
				}
				if(!params.isEmpty()) {
					((HttpPost) request).setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
				}
				if (!TextUtils.isEmpty(entity)) {
					StringEntity se = new StringEntity(entity, HTTP.UTF_8);
					if (method == AylaRestService.CREATE_LOG_IN_SERVICE) {
						((HttpPost) request).setEntity(se);
					}
				}
				commitResponse = commit(rs);
				break;
			} else { // return errors to handler
				responseBundle = new Bundle();
				responseBundle.putString("result", rs.jsonResults); //  response v3.07
				responseBundle.putInt("subTask", rs.subTaskFailed);
				responseCode = rs.responseCode;

				receiver.send(responseCode, responseBundle);
				break;
			}
		}
		case AylaRestService.CREATE_SCHEDULE_ACTION:
		case AylaRestService.UPDATE_SCHEDULE_ACTION:
		case AylaRestService.DELETE_SCHEDULE_ACTION:
		{
			if (!url.equals(AylaSystemUtils.ERR_URL)) {
				if (method == AylaRestService.CREATE_SCHEDULE_ACTION) {
					request = new HttpPost(url);
				} else if (method == AylaRestService.UPDATE_SCHEDULE_ACTION) {
					request = new HttpPut(url);
				} else {
					request = new HttpDelete(url);
				}
				
				for(NameValuePair h : headers) {
					request.addHeader(h.getName(), h.getValue());
				}
				if(!params.isEmpty()) {
					((HttpPost) request).setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
				}
				if (!TextUtils.isEmpty(entity)) {
					StringEntity se = new StringEntity(entity, HTTP.UTF_8);
					if (method == AylaRestService.CREATE_SCHEDULE_ACTION) {
						((HttpPost) request).setEntity(se);
					} else if (method == AylaRestService.UPDATE_SCHEDULE_ACTION) {
						((HttpPut) request).setEntity(se);
					} else { // delete
						for(NameValuePair h : headers) {
							request.addHeader(h.getName(), h.getValue());
						}
					}
				}
				commitResponse = commit(rs);
				break;
			} else { // return errors to handler
				responseBundle = new Bundle();
				responseBundle.putString("result", rs.jsonResults); //  response v3.07
				responseBundle.putInt("subTask", rs.subTaskFailed);
				responseCode = rs.responseCode;

				receiver.send(responseCode, responseBundle);
				break;
			}
		}
		//case AylaRestService.CREATE_SCHEDULE:
		case AylaRestService.UPDATE_SCHEDULE:
		case AylaRestService.CLEAR_SCHEDULE:
		//case AylaRestService.DELETE_SCHEDULE:
		case AylaRestService.CREATE_SCHEDULE_ACTIONS: // only called on (url.equals(AylaSystemUtils.ERR_URL)
		case AylaRestService.DELETE_SCHEDULE_ACTIONS: // only called on (url.equals(AylaSystemUtils.ERR_URL)
		{
			if (!url.equals(AylaSystemUtils.ERR_URL)) {
				//if (method == AylaRestService.CREATE_SCHEDULE) {
				//	request = new HttpPost(url);
				//} else 
				if (method == AylaRestService.UPDATE_SCHEDULE ||
						   method == AylaRestService.CLEAR_SCHEDULE) {
					request = new HttpPut(url);
				} else {
					request = new HttpDelete(url);
				}
				
				for(NameValuePair h : headers) {
					request.addHeader(h.getName(), h.getValue());
				}
				if(!params.isEmpty()) {
					((HttpPost) request).setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
				}
				if (!TextUtils.isEmpty(entity)) {
					StringEntity se = new StringEntity(entity, HTTP.UTF_8);
					//if (method == AylaRestService.CREATE_SCHEDULE) {
					//	((HttpPost) request).setEntity(se);
					//} else 
					if (method == AylaRestService.UPDATE_SCHEDULE ||
					    method == AylaRestService.CLEAR_SCHEDULE) {
							((HttpPut) request).setEntity(se);
					} else { // delete
						for(NameValuePair h : headers) {
							request.addHeader(h.getName(), h.getValue());
						}
					}
				}
				commitResponse = commit(rs);
				break;
			} else { // return errors to handler
				responseBundle = new Bundle();
				responseBundle.putString("result", rs.jsonResults); //  response v3.07
				responseBundle.putInt("subTask", rs.subTaskFailed);
				responseCode = rs.responseCode;

				receiver.send(responseCode, responseBundle);
				break;
			}
		}
		case AylaRestService.UPDATE_DEVICE:
		{
			if (method == AylaRestService.UPDATE_DEVICE && !url.equals(AylaSystemUtils.ERR_URL) ) {
				request = new HttpPut(url);

				for(NameValuePair h : headers) {
					request.addHeader(h.getName(), h.getValue());
				}
				if(!params.isEmpty()) {
					((HttpPost) request).setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
				}
				
				if (!TextUtils.isEmpty(entity)) {
					StringEntity se = new StringEntity(entity, HTTP.UTF_8);
					((HttpPut) request).setEntity(se);
				}
				commitResponse = commit(rs);
				break;
			} else { // return errors to handler
				responseBundle = new Bundle();
				responseBundle.putString("result", rs.jsonResults); //  response v3.07
				responseBundle.putInt("subTask", rs.subTaskFailed);
				responseCode = rs.responseCode;

				receiver.send(responseCode, responseBundle);
				break;
			}
		}
		case AylaRestService.SEND_NETWORK_PROFILE_LANMODE:
		case AylaRestService.DELETE_NETWORK_PROFILE_LANMODE:
			
		case AylaRestService.GET_DEVICES_LANMODE:
		case AylaRestService.GET_NODES_LANMODE:
		case AylaRestService.CREATE_DATAPOINT_LANMODE:
		case AylaRestService.GET_DATAPOINT_LANMODE:
		case AylaRestService.GET_DATAPOINTS_LANMODE:
		case AylaRestService.GET_PROPERTIES_LANMODE:
		case AylaRestService.GET_PROPERTY_DETAIL_LANMODE:
		{
			responseBundle = new Bundle();
			responseBundle.putString("result", rs.jsonResults); //  response v3.07
			responseBundle.putInt("subTask", rs.subTaskFailed);
			responseCode = rs.responseCode;

			if (async) { 
				receiver.send(responseCode, responseBundle);
			} else {
				commitResponse =  new AylaCallResponse(responseCode, responseBundle);
			}
			break;
		}
		case AylaRestService.GET_DATAPOINT_BLOB_SAVE_TO_FILE:
		{
			if (!url.equals(AylaSystemUtils.ERR_URL)) {
				String combinedParams = "";
				if(!params.isEmpty()) {
					combinedParams += "?";
					for(NameValuePair p : params) {
						String paramString = p.getName() + "=" + URLEncoder.encode(p.getValue(),"UTF-8");
						if(combinedParams.length() > 1) {
							combinedParams  +=  "&" + paramString;
						}
						else {
							combinedParams += paramString;
						}
					}
				}
				request = new HttpGet(url + combinedParams);
//				for(NameValuePair h : headers) {
//					request.addHeader(h.getName(), h.getValue());
//				}
				commitResponse = commit(rs);
			} else { // return errors to handler
				responseBundle = new Bundle();
				responseBundle.putString("result", rs.jsonResults);
				responseBundle.putInt("subTask", rs.subTaskFailed);
				responseCode = rs.responseCode;

				receiver.send(responseCode, responseBundle);
			}
			break;
		}
		case AylaRestService.GET_DEVICES:
		case AylaRestService.GET_DEVICE_DETAIL:
		case AylaRestService.GET_NEW_DEVICE_CONNECTED:
		case AylaRestService.GET_REGISTERED_NODES:
		case AylaRestService.GET_DEVICE_NOTIFICATIONS:
		case AylaRestService.GET_APP_NOTIFICATIONS:
		case AylaRestService.GET_PROPERTIES:
		case AylaRestService.GET_PROPERTY_DETAIL:
		case AylaRestService.GET_DATAPOINTS:
		case AylaRestService.GET_DATAPOINT_BLOB:
		case AylaRestService.GET_PROPERTY_TRIGGERS:
		case AylaRestService.GET_APPLICATION_TRIGGERS:
		case AylaRestService.GET_REGISTRATION_CANDIDATE:
		case AylaRestService.GET_GATEWAY_REGISTRATION_CANDIDATES:
		case AylaRestService.GET_MODULE_REGISTRATION_TOKEN:
		case AylaRestService.GET_NEW_DEVICE_STATUS:
		case AylaRestService.GET_NEW_DEVICE_SCAN_RESULTS_FOR_APS:
		case AylaRestService.GET_MODULE_WIFI_STATUS: 
		case AylaRestService.GET_NEW_DEVICE_PROFILES:
		case AylaRestService.GET_DEVICE_LANMODE_CONFIG:
		case AylaRestService.GET_USER_INFO:
		case AylaRestService.GET_SCHEDULES:
		case AylaRestService.GET_SCHEDULE:
		case AylaRestService.GET_SCHEDULE_ACTIONS:
		case AylaRestService.GET_SCHEDULE_ACTIONS_BY_NAME:
		case AylaRestService.GET_TIMEZONE:
		case AylaRestService.GET_USER_METADATA:
		case AylaRestService.GET_DEVICE_METADATA:
		case AylaRestService.GET_USER_SHARES:
		case AylaRestService.GET_USER_RECEIVED_SHARES:
		case AylaRestService.GET_GROUP_ZIGBEE:
		case AylaRestService.GET_GROUPS_ZIGBEE:
		case AylaRestService.GET_BINDING_ZIGBEE:
		case AylaRestService.GET_BINDINGS_ZIGBEE:
		case AylaRestService.GET_SCENE_ZIGBEE:
		case AylaRestService.GET_SCENES_ZIGBEE:
		case AylaRestService.GET_USER_CONTACT:
		case AylaRestService.GET_USER_CONTACT_LIST:
		{
			if (!url.equals(AylaSystemUtils.ERR_URL)) {
				String combinedParams = "";
				
				if(params!= null && !params.isEmpty()) {
					combinedParams += "?";
					for(NameValuePair p : params) {
                        if ( p.getName() != null && p.getValue() != null ) {
                            String paramString = p.getName() + "=" + URLEncoder.encode(p.getValue(), "UTF-8");
                            if (combinedParams.length() > 1) {
                                combinedParams += "&" + paramString;
                            } else {
                                combinedParams += paramString;
                            }
                        }
					}
				}
				request = new HttpGet(url + combinedParams);
				for(NameValuePair h : headers) {
					request.addHeader(h.getName(), h.getValue());
				}
				commitResponse = commit(rs);
				
			} else { // return errors to handler
				responseBundle = new Bundle();
				responseBundle.putString("result", rs.jsonResults);
				responseBundle.putInt("subTask", rs.subTaskFailed);
				responseCode = rs.responseCode;

				receiver.send(responseCode, responseBundle);
			}
			break;
		} 
		case AylaRestService.POST_USER_LOGIN:
		case AylaRestService.POST_USER_LOGOUT:
		case AylaRestService.POST_USER_SIGNUP:
		case AylaRestService.POST_USER_RESEND_CONFIRMATION:
		case AylaRestService.POST_USER_RESET_PASSWORD:
		case AylaRestService.POST_USER_REFRESH_ACCESS_TOKEN:
		case AylaRestService.POST_USER_OAUTH_LOGIN:
		case AylaRestService.POST_USER_OAUTH_AUTHENTICATE_TO_SERVICE:
		{
			if (!url.equals(AylaSystemUtils.ERR_URL)) {
				request = new HttpPost(url);
				for(NameValuePair h : headers) {
					request.addHeader(h.getName(), h.getValue());
				}
				if(!params.isEmpty()) {
					((HttpPost) request).setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
				}

				if (!TextUtils.isEmpty(entity)) {
					StringEntity se = new StringEntity(entity, HTTP.UTF_8);
					((HttpPost) request).setEntity(se);
				}
				commitResponse = commit(rs);
				break;
			} else { // return user sign-up errors to handler
				responseBundle = new Bundle();
				responseBundle.putString("result", rs.jsonResults); //  response v3.07
				responseBundle.putInt("subTask", rs.subTaskFailed);
				responseCode = rs.responseCode;

				receiver.send(responseCode, responseBundle);
				break;
			}
		}
		case AylaRestService.CREATE_DATAPOINT_BLOB_POST_TO_FILE:
		{
			/* Interact with amazon S3 
			 * authorization mechanism and param already in url 
			 * do not need header.*/
			if (!url.equals(AylaSystemUtils.ERR_URL)) {
				request = new HttpPut(url);
//				for(NameValuePair h : headers) {
//					if (!TextUtils.equals(h.getName(), "Authorization")) {
//						request.addHeader(h.getName(), h.getValue());
//					}
//				}
				request.addHeader("Content-Type" , "application/octet-stream");
				if(!params.isEmpty()) {
					((HttpPut) request).setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
				}
				if (!TextUtils.isEmpty(entity)) {
					StringEntity se = new StringEntity(entity, HTTP.UTF_8);
					((HttpPut) request).setEntity(se);
				}
				commitResponse = commit(rs);
			} else { // return user sign-up errors to handler
				responseBundle = new Bundle();
				responseBundle.putString("result", rs.jsonResults); //  response v3.07
				responseBundle.putInt("subTask", rs.subTaskFailed);
				responseCode = rs.responseCode;

				receiver.send(responseCode, responseBundle);
			}
			break;
		}
		case AylaRestService.PUT_USER_CHANGE_PASSWORD:
		case AylaRestService.PUT_RESET_PASSWORD_WITH_TOKEN:
		case AylaRestService.PUT_USER_CHANGE_INFO:
		case AylaRestService.PUT_USER_SIGNUP_CONFIRMATION:
		case AylaRestService.UPDATE_PROPERTY_TRIGGER:
		case AylaRestService.UPDATE_APPLICATION_TRIGGER:
		case AylaRestService.UPDATE_DEVICE_NOTIFICATION:
		case AylaRestService.UPDATE_APP_NOTIFICATION:
		case AylaRestService.PUT_DEVICE_FACTORY_RESET:
		case AylaRestService.BLOB_MARK_FETCHED:
		case AylaRestService.BLOB_MARK_FINISHED:
		case AylaRestService.IDENTIFY_NODE:
		{
			if (!url.equals(AylaSystemUtils.ERR_URL)) {
				request = new HttpPut(url);
				for(NameValuePair h : headers) {
					request.addHeader(h.getName(), h.getValue());
				}
				if(!params.isEmpty()) {
					((HttpPut) request).setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
				}
				if (!TextUtils.isEmpty(entity)) {
					StringEntity se = new StringEntity(entity, HTTP.UTF_8);
					((HttpPut) request).setEntity(se);
				}
				commitResponse = commit(rs);
			} else { // return user sign-up errors to handler
				responseBundle = new Bundle();
				responseBundle.putString("result", rs.jsonResults); //  response v3.07
				responseBundle.putInt("subTask", rs.subTaskFailed);
				responseCode = rs.responseCode;

				receiver.send(responseCode, responseBundle);
			}
			break;
		}
		case AylaRestService.CREATE_DATAPOINT:
		case AylaRestService.CREATE_DATAPOINT_BLOB:
		case AylaRestService.CREATE_PROPERTY_TRIGGER:
		case AylaRestService.CREATE_APPLICATION_TRIGGER:
		case AylaRestService.START_NEW_DEVICE_SCAN_FOR_APS:
		case AylaRestService.REGISTER_DEVICE:
	//	case AylaRestService.SET_DEVICE_CONNECT_TO_NETWORK:
		case AylaRestService.POST_LOCAL_REGISTRATION:
		case AylaRestService.OPEN_REGISTRATION_WINDOW:
		case AylaRestService.CREATE_TIMEZONE:
		case AylaRestService.CREATE_DEVICE_NOTIFICATION:
		case AylaRestService.CREATE_APP_NOTIFICATION:
		{
			request = new HttpPost(url);
			for(NameValuePair h : headers) {
				request.addHeader(h.getName(), h.getValue());
			}
			if(!params.isEmpty()) {
				((HttpPost) request).setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
			}
			if (!TextUtils.isEmpty(entity)) {
                StringEntity se = new StringEntity(entity, "UTF-8");
				((HttpPost) request).setEntity(se);
			}
			commitResponse = commit(rs);
			break;
		}

        case AylaRestService.SET_DEVICE_CONNECT_TO_NETWORK:
        {
           // request = new HttpPost(url);

            String urlQueryParams = "";
            if(!params.isEmpty()) {
                urlQueryParams += "?";
                for(NameValuePair p : params) {
                    String paramString = p.getName() + "=" + URLEncoder.encode(p.getValue(),"UTF-8");
                    if(urlQueryParams.length() > 1) {
                        urlQueryParams  +=  "&" + paramString;
                    }
                    else {
                        urlQueryParams += paramString;
                    }
                }

            }

            url += urlQueryParams;
            request = new HttpPost(url);
            for(NameValuePair h : headers) {
                request.addHeader(h.getName(), h.getValue());
            }
            if (!TextUtils.isEmpty(entity)) {
                StringEntity se = new StringEntity(entity, "UTF-8");
                ((HttpPost) request).setEntity(se);
            }
          
            commitResponse = commit(rs);
            break;
        }

		case AylaRestService.PUT_LOCAL_REGISTRATION:
		case AylaRestService.PUT_NEW_DEVICE_TIME:
		case AylaRestService.PUT_DATAPOINT: // used to mark blob fetched
		case AylaRestService.UPDATE_TIMEZONE:
		{
			request = new HttpPut(url);
			for(NameValuePair h : headers) {
				request.addHeader(h.getName(), h.getValue());
			}
			if(!params.isEmpty()) {
				((HttpPut) request).setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
			}
			if (!TextUtils.isEmpty(entity)) {
				StringEntity se = new StringEntity(entity, "UTF-8");
				((HttpPut) request).setEntity(se);
			}
			commitResponse = commit(rs);
			break;
		}

		case AylaRestService.DELETE:
		case AylaRestService.DESTROY_DEVICE_NOTIFICATION:
		case AylaRestService.DESTROY_APP_NOTIFICATION:
		case AylaRestService.DESTROY_PROPERTY_TRIGGER:
		case AylaRestService.DESTROY_APPLICATION_TRIGGER:
		case AylaRestService.UNREGISTER_DEVICE:
		case AylaRestService.DELETE_DEVICE_WIFI_PROFILE:
		case AylaRestService.DELETE_USER:
		{
			request = new HttpDelete(url);
			for(NameValuePair h : headers) {
				request.addHeader(h.getName(), h.getValue());
			}
			commitResponse = commit(rs);
			break;
		}
//		case AylaRestService.SECURE_SETUP_SESSION_COMPLETED:
		case AylaRestService.PROPERTY_CHANGE_NOTIFIER:
		{
			responseBundle = new Bundle();
			responseBundle.putString("result", rs.jsonResults); //  response v3.07
			responseBundle.putInt("subTask", rs.subTaskFailed);
			responseCode = rs.responseCode;

			receiver.send(responseCode, responseBundle);
			break;
		}
		case AylaRestService.REACHABILITY:
		{
			responseBundle = new Bundle();
			responseBundle.putString("result", rs.jsonResults); //  response v3.07
			responseBundle.putInt("subTask", rs.subTaskFailed);
			responseCode = rs.responseCode;

			receiver.send(responseCode, responseBundle);
			break;
		}
		case AylaRestService.REGISTER_NEW_DEVICE:			// Compound objects, no service API, just return to handler
		{
			// wait for completion if it's a synchronous call
			if (async == false) { 
				while (rs.jsonResults == null) {
					try { //NOP
					      Thread.sleep(100);
					   } catch (InterruptedException e) {
					      e.printStackTrace();
					   }
					   continue;
				}
			}
			
			responseBundle = new Bundle();
			responseBundle.putString("result", rs.jsonResults); //  response v3.07
			responseBundle.putInt("subTask", rs.subTaskFailed);
			responseCode = rs.responseCode;
			if (async) {
				receiver.send(responseCode, responseBundle);
			} else {
				commitResponse = new AylaCallResponse(responseCode, responseBundle);
			}
			break;
		}
		case AylaRestService.CONNECT_TO_NEW_DEVICE: // Compound objects no service API, just return to handler
		case AylaRestService.RETURN_HOST_WIFI_STATE: // Using host local calls, no service API, just return to handler
		case AylaRestService.RETURN_HOST_SCAN:
		case AylaRestService.RETURN_HOST_NETWORK_CONNECTION:
		case AylaRestService.SET_HOST_NETWORK_CONNECTION:
		case AylaRestService.DELETE_HOST_NETWORK_CONNECTION:
		case AylaRestService.DELETE_HOST_NETWORK_CONNECTIONS:
		case AylaRestService.RETURN_HOST_DNS_CHECK:
		case AylaRestService.GROUP_ACK_ZIGBEE:
		case AylaRestService.BINDING_ACK_ZIGBEE:
		case AylaRestService.SCENE_ACK_ZIGBEE:
		case AylaRestService.GET_NODES_CONNECTION_STATUS:
		{
			// wait for completion if it's a synchronous call
			if (async == false) { 
				while (rs.jsonResults == null) {
					try { //NOP
					      Thread.sleep(100);
					   } catch (InterruptedException e) {
					      e.printStackTrace();
					   }
					   continue;
				}
			}
			
			responseBundle = new Bundle();
			responseBundle.putString("result", rs.jsonResults); //  response v3.07
			responseBundle.putInt("subTask", rs.subTaskFailed);
			responseCode = rs.responseCode;
			if (async) {

				receiver.send(responseCode, responseBundle);
			} else {

				commitResponse = new AylaCallResponse(responseCode, responseBundle);
			}
			break;
		}
		case AylaRestService.CONNECT_NEW_DEVICE_TO_SERVICE:
		case AylaRestService.CONFIRM_NEW_DEVICE_TO_SERVICE_CONNECTION: // Compound object
		case AylaRestService.GET_NEW_DEVICE_WIFI_STATUS: // compound object
		{
			responseBundle = new Bundle();
			responseBundle.putString("result", rs.jsonResults); //  response v3.07
			responseBundle.putInt("subTask", rs.subTaskFailed);
			responseCode = rs.responseCode;

			receiver.send(responseCode, responseBundle);
			break;
		}
		default:
			AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "E", "ExecuteRequest", "method", "unknown", "execute");
			break;
		}
		return commitResponse;
	}
	
	public static DefaultHttpClient getThreadSafeClient()  {
	    DefaultHttpClient client = new DefaultHttpClient();
	    ClientConnectionManager mgr = client.getConnectionManager();
	    HttpParams params = client.getParams();
	    client = new DefaultHttpClient(new ThreadSafeClientConnManager(params, mgr.getSchemeRegistry()), params);
	    return client;
	}

	static HttpClient client = null;

	/**
	 * @param rs
	 * @return
	 */
	private AylaCallResponse commit(AylaRestService rs) {
		if (client == null) {
			client = getThreadSafeClient();
		}
		HttpProtocolParams.setUserAgent(client.getParams(), "Android");
		HttpContext httpContext = new BasicHttpContext();
		Bundle responseBundle = new Bundle();
		HttpResponse httpResponse;
		InputStream instream = null;
		
		if (requestRetryCounter > 0) {
			saveRequest = request;
		}

		try {
			httpResponse = client.execute(request);
			responseCode = httpResponse.getStatusLine().getStatusCode();
			message = httpResponse.getStatusLine().getReasonPhrase();
			HttpEntity entity = httpResponse.getEntity();

			if (entity != null) {
				String initialResponse = null;
				String response = null;
				
				if (method == AylaRestService.GET_DATAPOINT_BLOB_SAVE_TO_FILE) {
					initialResponse = AylaBlob.saveBlobToFile(null, entity);
				} else { // is a json string
					instream = entity.getContent();
					initialResponse = convertStreamToString(instream);
				}
				
				if (responseCode >= 200 && responseCode < 300) {
					switch (method) { // remove containers from response
					case AylaRestService.GET_DEVICES:
					case AylaRestService.GET_GATEWAY_REGISTRATION_CANDIDATES:
						response = AylaDevice.stripContainers(initialResponse, method);
						break;
					case AylaRestService.GET_REGISTERED_NODES:
						response = AylaDeviceNode.stripContainers(initialResponse, rs);
						break;
					// case AylaRestService.UPDATE_DEVICE:
					case AylaRestService.GET_DEVICE_DETAIL:
					case AylaRestService.GET_REGISTRATION_CANDIDATE:
					case AylaRestService.REGISTER_DEVICE:
					case AylaRestService.GET_NEW_DEVICE_CONNECTED:
						response = AylaDevice.stripContainer(initialResponse, method);
						break;
					case AylaRestService.CREATE_DEVICE_NOTIFICATION:
					case AylaRestService.UPDATE_DEVICE_NOTIFICATION:
						response = AylaDeviceNotification.stripContainer(initialResponse, method);
						break;
					case AylaRestService.GET_DEVICE_NOTIFICATIONS:
						response = AylaDeviceNotification.stripContainers(initialResponse);
						break;
					case AylaRestService.CREATE_APP_NOTIFICATION:
					case AylaRestService.UPDATE_APP_NOTIFICATION: 
						response = AylaAppNotification.stripContainer(initialResponse, method);
						break;
					case AylaRestService.GET_APP_NOTIFICATIONS:
						response = AylaAppNotification.stripContainers(initialResponse);
						break;
					case AylaRestService.GET_PROPERTIES:
						response = AylaProperty.stripContainers(initialResponse, rs.info);
						break;
					case AylaRestService.GET_PROPERTY_DETAIL:
						response = AylaProperty.stripContainer(initialResponse);
						break;
						
					case AylaRestService.GET_DATAPOINTS:
						response = AylaDatapoint.stripContainers(initialResponse);
						break;
					case AylaRestService.CREATE_DATAPOINT:
						response = AylaDatapoint.stripContainer(initialResponse);
						break;
					case AylaRestService.GET_DATAPOINT_BLOB:
					case AylaRestService.CREATE_DATAPOINT_BLOB:
					case AylaRestService.CREATE_DATAPOINT_BLOB_POST_TO_FILE:
					case AylaRestService.BLOB_MARK_FETCHED:
					case AylaRestService.BLOB_MARK_FINISHED:
						response = AylaBlob.stripContainer(initialResponse, method);
						break;
						
					case AylaRestService.CREATE_PROPERTY_TRIGGER:
					case AylaRestService.UPDATE_PROPERTY_TRIGGER:
						response = AylaPropertyTrigger.stripContainer(initialResponse, method);
						break;
					case AylaRestService.GET_PROPERTY_TRIGGERS:
						response = AylaPropertyTrigger.stripContainers(initialResponse);
						break;
					case AylaRestService.CREATE_APPLICATION_TRIGGER:
					case AylaRestService.UPDATE_APPLICATION_TRIGGER:
						response = AylaApplicationTrigger.stripContainer(initialResponse, method);
						break;
					case AylaRestService.GET_APPLICATION_TRIGGERS:
						response = AylaApplicationTrigger.stripContainers(initialResponse);
						break;
					case AylaRestService.GET_NEW_DEVICE_SCAN_RESULTS_FOR_APS:
						response = AylaModule.stripScanContainerAndReturnAPs(initialResponse);
						break;
					case AylaRestService.GET_MODULE_WIFI_STATUS:
						response = AylaModule.stripDeviceWiFiStatusContainers(initialResponse);
						break;
					case AylaRestService.POST_USER_LOGIN:
					case AylaRestService.POST_USER_REFRESH_ACCESS_TOKEN:
						AylaUser.user.updatedAt = System.currentTimeMillis(); // set timestamp used to calc access token expiry
						response = initialResponse;
						break;
					case AylaRestService.POST_USER_LOGOUT:
						AylaUser.user.setauthHeaderValue("none"); // nullify the current access_id
						response = initialResponse;
						break;
					//case AylaRestService.CREATE_SCHEDULE:
					case AylaRestService.UPDATE_SCHEDULE:
					case AylaRestService.GET_SCHEDULE:
						response = AylaSchedule.stripContainer(initialResponse);
						break;
					case AylaRestService.GET_SCHEDULES:
						response = AylaSchedule.stripContainers(initialResponse);
						break;
					case AylaRestService.GET_SCHEDULE_ACTIONS:
					case AylaRestService.GET_SCHEDULE_ACTIONS_BY_NAME:
						response = AylaScheduleAction.stripContainers(initialResponse);
						break;
					case AylaRestService.UPDATE_SCHEDULE_ACTION:
						response = AylaScheduleAction.stripContainer(initialResponse);
						break;
					case AylaRestService.CREATE_TIMEZONE:
					case AylaRestService.UPDATE_TIMEZONE:
					case AylaRestService.GET_TIMEZONE:
						response = AylaTimezone.stripContainer(initialResponse);
						break;
					case AylaRestService.CREATE_DEVICE_METADATA:
					case AylaRestService.UPDATE_DEVICE_METADATA:
					case AylaRestService.GET_DEVICE_METADATA_BY_KEY:
					case AylaRestService.GET_DEVICE_METADATA:
					case AylaRestService.CREATE_USER_METADATA:
					case AylaRestService.UPDATE_USER_METADATA:
					case AylaRestService.GET_USER_METADATA_BY_KEY:
					case AylaRestService.GET_USER_METADATA:
						response = AylaDatum.stripContainer(initialResponse, method);
						break;
					case AylaRestService.CREATE_USER_SHARE:
					case AylaRestService.UPDATE_USER_SHARE:
					case AylaRestService.GET_USER_SHARE:
					case AylaRestService.GET_USER_SHARES:
					case AylaRestService.GET_USER_RECEIVED_SHARES:
						response = AylaShare.stripContainer(initialResponse, method);
						break;
					case AylaRestService.GET_USER_CONTACT:
					case AylaRestService.GET_USER_CONTACT_LIST:
					case AylaRestService.CREATE_USER_CONTACT:
					case AylaRestService.UPDATE_USER_CONTACT:
					case AylaRestService.DELETE_USER_CONTACT:
						response = AylaContact.stripContainer(initialResponse, method);
						break;
					case AylaRestService.CREATE_GROUP_ZIGBEE:
					case AylaRestService.UPDATE_GROUP_ZIGBEE:
					case AylaRestService.GET_GROUP_ZIGBEE:
					case AylaRestService.DELETE_GROUP_ZIGBEE:
						response = AylaCommProxy.stripGroupContainer(initialResponse, method);
						break;
					case AylaRestService.GET_GROUPS_ZIGBEE:
						response = AylaCommProxy.stripGroupContainers(initialResponse);
						break;

					case AylaRestService.CREATE_BINDING_ZIGBEE:
					case AylaRestService.UPDATE_BINDING_ZIGBEE:
					case AylaRestService.GET_BINDING_ZIGBEE:
					case AylaRestService.DELETE_BINDING_ZIGBEE:
						response = AylaCommProxy.stripBindingContainers(initialResponse, method);
						break;
					case AylaRestService.GET_BINDINGS_ZIGBEE:
						response = AylaCommProxy.stripBindingContainers(initialResponse, method);
						break;
					case AylaRestService.CREATE_SCENE_ZIGBEE:
					case AylaRestService.UPDATE_SCENE_ZIGBEE:
					case AylaRestService.RECALL_SCENE_ZIGBEE:
					case AylaRestService.GET_SCENE_ZIGBEE:
					case AylaRestService.DELETE_SCENE_ZIGBEE:
						response = AylaCommProxy.stripSceneContainer(initialResponse, method);
						break;
					case AylaRestService.GET_SCENES_ZIGBEE:
						response = AylaCommProxy.stripSceneContainers(initialResponse);
						break;
					default:
						response = initialResponse; // no container
					}
					// For Debug!
//					if ( method == AylaRestService.GET_DEVICE_LANMODE_CONFIG ) {
//						AylaSystemUtils.saveToLog("%s, %s, response:%s.", "D", "AylaExecuteRequest", ""+response);    
//					}
					
				} else {
					if (initialResponse != null && !initialResponse.equals("")) {
						response = initialResponse;
					} else {
						response = message;	// avoid http encoded response message
					}
				}
				responseBundle.putString("result", response);
				if (async) {
					receiver.send(responseCode, responseBundle);
				}

				return new AylaCallResponse(responseCode, responseBundle);
			} else {
				if (responseCode >= 200 && responseCode < 300) { // special case 204:No Content rc
					// NOP
				} else {
					AylaSystemUtils.saveToLog("%s, %s, %s:%d, %s:%s %s", "E", "ExecuteRequest", "method", method, "responseCode", responseCode, "commit.NoContent");
				}
				responseBundle.putString("result", message);
				if (async) {
					receiver.send(responseCode, responseBundle);
				}
				
				return  new AylaCallResponse(responseCode, responseBundle);
			}
		} catch (ClientProtocolException e)  {
			String eMsg = (e.getLocalizedMessage() == null) ? e.toString() : e.getLocalizedMessage();
			AylaSystemUtils.saveToLog("%s, %s, %s:%d, %s:%s %s", "E", "ExecuteRequest", "Error", AylaNetworks.AML_CLIENT_PROTOCOL_EXCEPTION, "eMsg", eMsg, "commit.ClientProtocolException");
			responseBundle.putString("result", eMsg);
			if (async) {
				receiver.send(AylaNetworks.AML_CLIENT_PROTOCOL_EXCEPTION, responseBundle);
			}
			if (client != null) {
				client.getConnectionManager().shutdown();
				client = null;
			}
			e.printStackTrace();
			return  new AylaCallResponse(AylaNetworks.AML_CLIENT_PROTOCOL_EXCEPTION, responseBundle);
		} catch (IOException e) {
			String eMsg = (e.getLocalizedMessage() == null) ? e.toString() : e.getLocalizedMessage();
			if (retryRequest(e, eMsg, requestRetryCounter, httpContext)) {
					requestRetryCounter--;
					AylaSystemUtils.saveToLog("%s, %s, %s:%d, %s:%s %s", "E", "ExecuteRequest", "Error", AylaNetworks.AML_IO_EXCEPTION, "eMsg", eMsg, "commit.retry");
					AylaSystemUtils.sleep(1000);
					if (client != null) {
						client.getConnectionManager().shutdown();
						client = null;
					}
					request = saveRequest;
					commit(rs);
			} else {
				AylaSystemUtils.saveToLog("%s, %s, %s:%d, %s:%s %s", "E", "ExecuteRequest", "Error", AylaNetworks.AML_IO_EXCEPTION, "eMsg", eMsg, "commit.IoException");
	
				responseBundle.putString("result", eMsg);
				if (async) {
					receiver.send(AylaNetworks.AML_IO_EXCEPTION, responseBundle);
				}
				if (client != null) {
					client.getConnectionManager().shutdown();
					client = null;
				}
				e.printStackTrace();
				return  new AylaCallResponse(AylaNetworks.AML_IO_EXCEPTION, responseBundle);
			}
		} catch (Exception e) {    	
			String eMsg = (e.getLocalizedMessage() == null) ? e.toString() : e.getLocalizedMessage();
			AylaSystemUtils.saveToLog("%s, %s, %s:%d, %s:%s", "E", "ExecuteRequest", "Error", AylaNetworks.AML_GENERAL_EXCEPTION, "eMsg", eMsg, "commit.GeneralException");

			responseBundle.putString("result", eMsg);
			if (async) {
				receiver.send(AylaNetworks.AML_GENERAL_EXCEPTION, responseBundle);
			}
			if (client != null) {
				client.getConnectionManager().shutdown();
				client = null;
			}
			return  new AylaCallResponse(AylaNetworks.AML_GENERAL_EXCEPTION, responseBundle);
		} finally {
			requestRetryCounter--;
			if (instream != null) {
				try {
					instream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				if (client != null) {
					client.getConnectionManager().shutdown();
					client = null;
				}
			}
		}
		responseBundle.putString("result", "UNKOWN ERROR");
		return  new AylaCallResponse(AylaNetworks.AML_GENERAL_EXCEPTION, responseBundle);
	}

	// Retry I/O exception errors
    private static HashSet<Class<?>> exceptionWhitelist = new HashSet<Class<?>>();
    private static HashSet<Class<?>> exceptionBlacklist = new HashSet<Class<?>>();

    static {
        // Retry if the server dropped connection on us
        exceptionWhitelist.add(NoHttpResponseException.class);
        // retry-this, since it may happens as part of a Wi-Fi to 3G failover
        exceptionWhitelist.add(UnknownHostException.class);
        // retry-this, since it may happens as part of a Wi-Fi to 3G failover
        exceptionWhitelist.add(SocketException.class);

        // never retry timeouts
        exceptionBlacklist.add(InterruptedIOException.class);
        // never retry SSL handshake failures
        exceptionBlacklist.add(SSLHandshakeException.class);
    }
    private Boolean retryRequest(IOException exception, String eMsg, int requestRetryCounter, HttpContext context) {
        Boolean retry = true;
        
       Boolean b = (Boolean) context.getAttribute(ExecutionContext.HTTP_REQ_SENT);
       Boolean sent = (b != null && b.booleanValue());

        if(requestRetryCounter <= 0) {
            // Do not retry if over max retry count
            retry = false;
        } else if (exceptionBlacklist.contains(exception.getClass())) {
            // immediately cancel retry if the error is blacklisted
            retry = false;
        } else if (eMsg.contains("timed out") ||
        		   eMsg.contains("refused"))
        {
        	// catch all time out cases
        	retry = false;
        } else if (exceptionWhitelist.contains(exception.getClass())) {
            // immediately retry if error is white listed
            retry = true;
        }  else if (!sent) {
            // for most other errors, retry 
           retry = true;
        }

        return retry;
    }
    
    // Stop the Intent Service when called by garbage collection
    protected void finalize() { 
    	stopSelf(); 
    } 
    
    // ----------------- Blob Support -------------------------------
	private static String convertStreamToString(InputStream is) {

		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				String eMsg = (e.getLocalizedMessage() == null) ? e.toString() : e.getLocalizedMessage();
				AylaSystemUtils.saveToLog("%s, %s, %s:%d, %s:%s", "E", "ExecuteRequest", "Error", AylaNetworks.AML_GENERAL_EXCEPTION, "eMsg", eMsg, "commit.convertStreamToString");
			}
		}
		return sb.toString();
	}	
}





