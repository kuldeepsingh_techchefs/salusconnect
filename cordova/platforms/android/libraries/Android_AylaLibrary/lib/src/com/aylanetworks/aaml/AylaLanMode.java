//
//  AylaLanMode.java
//  Ayla Mobile Library
//
//  Created by Daniel Myers on 11/13/12.
//  Copyright (c) 2012 Ayla Networks. All rights reserved.
//
package com.aylanetworks.aaml;

import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
//import android.util.Log;



import java.io.IOException;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Map;
import java.util.Queue;

import com.aylanetworks.aaml.AylaHttpServer.Response.Status;
import com.aylanetworks.aaml.enums.CommandEntityBaseType;
import com.google.gson.annotations.Expose;


public class AylaLanMode extends AylaSystemUtils {
	//{"key_exchange":{"ver":1,"random_1": "...","time_1": "...","proto": 1,"key_id": 1}}
	class AylaKeyExchangeContainer {
		@Expose
		AylaKeyExchange keyExchange;
	}
	class AylaKeyExchange {
		@Expose
		int ver;
		@Expose
		String random_1;
		@Expose
		long time_1;
		@Expose
		int proto;
		@Expose
		int key_id;
		@Expose
		String sec;
	}

	class AylaBase64CryptoContainer {
		@Expose
		String enc;
		@Expose
		String sign;
	}
	class AylaCryptoContainer {
		@Expose
		AylaCryptoEncap enc;
		@Expose
		String sign;
	}
	class AylaCryptoEncap {
		@Expose
		int seqNo;
		@Expose
		AylaCryptoEncapData data;
	}
	class AylaCryptoEncapData {
		@Expose
		String name;
		@Expose
		String value;
	}

	static String serverIpAddress = null;
	static AylaTimer sessionTimer = null;

	static int seq_no = 0;

	private static AylaHttpServer httpServer = null;

	public static AylaDevice device = null; 	// currently selected lan mode device
	static AylaDevice devices[] = null; // lan mode devices registered to this user
	static AylaDevice savedLanModeDevice; // previous current lan mode device

	private static Deque <AylaLanCommandEntity> commandsSendQueue = new ArrayDeque<AylaLanCommandEntity>(); // Since API 9
//	private static Queue<AylaLanCommandEntity> commandsSendQueue = new LinkedList<AylaLanCommandEntity>();  // Since API 1.
	private static Hashtable<String, AylaRestService> commandsOutstanding = new Hashtable<String, AylaRestService>();
	static int commandsOutstandingCount = 0;
	static int nextCommandOutstandingId = 0;
	static int oneStatus = 200;

	static String discoveredLanIp;
	static AylaDiscovery discovery = null; // v3.31
	
	static int saveLanipKeyId = -1;
	static boolean gotLanConfigOnce = true;

	static boolean isSecureSetupSession = false;
	
	// State of the session with device
	static lanModeSession sessionState = lanModeSession.DOWN;
	public enum lanModeSession {DOWN, LOCAL_REGISTRATION, KEY_EXCHANGE, UP, UNKNOWN}

	/**
	 * @return the current LAN Mode Session State
	 */
	public static lanModeSession getSessionState() {
		return sessionState;
	}

	private static class aylaHTTPD extends AylaHttpServer
	{
		// start the HTTP server
		aylaHTTPD() throws IOException {
			super(serverPortNumber);
			saveToLog("%s, %s, %s:%s, %s:%d, %s", "I", "AylaLanMode", "serverIpAddress", serverIpAddress,
					"serverPortNumber", serverPortNumber, "aylaHTTPD()");
		}

		
		/* Key exchange for lanMode. */       
		private Response processLanModeKeyExchange(final AylaKeyExchangeContainer keyContainer, final String jsonString) {
			// Check preconditions/requirements
			AylaEncryption.version = keyContainer.keyExchange.ver;
			AylaEncryption.proto_1 = keyContainer.keyExchange.proto;
			AylaEncryption.key_id_1 = keyContainer.keyExchange.key_id;

			if (AylaEncryption.version != 1) {
				AylaSystemUtils.saveToLog("%s, %s, %s:%d, %s", "E", "AylaLanMode", "version", AylaEncryption.version, "Response_keyExchange3");
				AylaDevice.lanModeSessionFailed(); //v2.13_ENG
				return new AylaHttpServer.Response(Status.UPGRADE_REQUIRED, MIME_JSON, jsonString);
			}
			
			if ( device == null || device.lanModeConfig == null ||  device.lanModeConfig.lanipKeyId == null) {
				AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "W", "AylaLanMode", "lanModeConfig", "null", "Response_keyExchange4");
				
				if (device != null && gotLanConfigOnce == false) { // v3.00
					gotLanConfigOnce = true;
					getLanModeConfig(); // try once to get a new lanKeyId
				}
				AylaDevice.lanModeSessionFailed(); //v2.13_ENG
				return new AylaHttpServer.Response(Status.METHOD_FAILURE, MIME_JSON, jsonString);
			}
			gotLanConfigOnce = false;
			
			int lanipKeyId = device.lanModeConfig.lanipKeyId.intValue();
			if (AylaEncryption.key_id_1 != lanipKeyId) {
				
				if ( AylaLanMode.isSecureSetupSession ) { // secure setup session 
					AylaEncryptionHelper helper = AylaEncryptionHelper.getInstance();
//					helper.init(IAML_SECURITY_KEY_SIZE.IAML_SECURITY_KEY_SIZE_1024); 
					byte[] pub = helper.getPublicKeyPKCS1V21Encoded();
					
					// pass in public key raw bytes.
					AylaDevice.startLanModeSession(
							device
							, AylaRestService.POST_LOCAL_REGISTRATION
							, true
							, pub);
					
				} else { // normal lan mode session
					String lanModeConfigName = AML_CACHE_LAN_CONFIG_PREFIX + device.dsn;
					AylaCache.save(AML_CACHE_LAN_CONFIG, device.dsn, "");	// clear existing lan mode config info
					
					if (saveLanipKeyId != lanipKeyId) {
						saveLanipKeyId = lanipKeyId;
						getLanModeConfig(); // try once to get a new lanKeyId
					}
					
					AylaSystemUtils.saveToLog("%s, %s, %s:%d, %s:%d, %s:%s, %s", "W", "AylaLanMode",
							"key_id_1", AylaEncryption.key_id_1, "lanipKeyId", lanipKeyId,
							"lanModeConfigName", lanModeConfigName, "Response_keyExchange5");
					AylaDevice.lanModeSessionFailed(); //v2.13_ENG
				}
				return new AylaHttpServer.Response(Status.PRECONDITION_FAILED, MIME_JSON, jsonString);
			} // end of key_id matching. 
			
			
			saveLanipKeyId = -1; // reset bad key flag
			
			if (AylaEncryption.proto_1 != 1) { 
				AylaSystemUtils.saveToLog("%s, %s, %s:%d, %s", "E", "AylaLanMode", "proto", AylaEncryption.proto_1, "Response_keyExchange5");
				AylaDevice.lanModeSessionFailed(); //v2.13_ENG
				return new AylaHttpServer.Response(Status.UPGRADE_REQUIRED, MIME_JSON, jsonString);
			}

			// save crypto values from device
			AylaEncryption.sRnd_1 = keyContainer.keyExchange.random_1;
			AylaEncryption.nTime_1 = keyContainer.keyExchange.time_1;

			// generate crypto values for device
			AylaEncryption.sRnd_2 = AylaEncryption.randomToken(16);
			AylaEncryption.nTime_2 = System.nanoTime();

			// generate seed values & new session keys
			int rc = AylaEncryption.generateSessionKeys(null);
			if (rc != AylaNetworks.AML_ERROR_OK) {
				AylaSystemUtils.saveToLog("%s, %s, %s:%d, %s:%d, %s:%d, %s", "E", "AylaLanMode", "version", AylaEncryption.version,
						"proto", AylaEncryption.proto_1, "key_id_1", AylaEncryption.key_id_1, "Response_keyExchange");
				AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s:%s, %s", "E", "AylaLanMode", "random_1", AylaEncryption.sRnd_1,
						"time_1", AylaEncryption.nTime_1, "Response_keyExchange");
				AylaDevice.lanModeSessionFailed(); //v2.13_ENG
				return new AylaHttpServer.Response(Status.CERT_ERROR, MIME_JSON, jsonString);
			} 

			// successful key generation
			
			// send app crypto seed values to device
			String jsonText = "{";
			jsonText = jsonText + "\"random_2\":" + "\"" + AylaEncryption.sRnd_2 + "\"";
			jsonText = jsonText + ",\"time_2\":" + AylaEncryption.nTime_2.longValue();
			jsonText = jsonText + "}";
			AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "I", "AylaLanMode", "jsonText", jsonText, "Response_keyExchange6");
			
			// Begin a new lan mode session
			AylaDevice.lanModeSessionInit();
		
			//return keys
			return new AylaHttpServer.Response(Status.OK, MIME_JSON, jsonText);
		}// end of processLanModeKeyExchange            
		
		
		
		/* key exchange for secure setup. */
		private Response processSecureSetupKeyExchange(final AylaKeyExchangeContainer keyContainer, final String jsonString) {
			
			AylaEncryption.version = keyContainer.keyExchange.ver;
			AylaEncryption.proto_1 = keyContainer.keyExchange.proto;
			AylaEncryption.sec = keyContainer.keyExchange.sec;
			
			// save crypto values from device
			AylaEncryption.sRnd_1 = keyContainer.keyExchange.random_1;
			AylaEncryption.nTime_1 = keyContainer.keyExchange.time_1;
			
			// generate crypto values for device
			AylaEncryption.sRnd_2 = AylaEncryption.randomToken(16);
			AylaEncryption.nTime_2 = System.nanoTime();   
			
			// Generate seed values and new session keys. 
			byte[] base64DecodedSec = AylaEncryptionHelper.decode(AylaEncryption.sec.getBytes());
			byte[] decrypt = AylaEncryptionHelper.getInstance().decrypt(base64DecodedSec);   
			String decryptbased64 = AylaEncryptionHelper.encode(decrypt);
//			String data = new String(decrypt);
			saveToLog("%s, %s, random key:%s.", "D", "AylaLanMode.processSecureSetupKeyExchange", decryptbased64);
			Map<String, String> param = new HashMap<String, String>();  
			param.put(AylaEncryption.keyAylaEncryptionType, AylaEncryption.valueAylaEncryptionTypeWifiSetupRSA);
//			param.put(AylaEncryption.keyAylaEncryptionData, data);
			param.put(AylaEncryption.keyAylaEncryptionData, decryptbased64);
			
			int rc = AylaEncryption.generateSessionKeys(param);                   
			if (rc != AylaNetworks.AML_ERROR_OK) {
//				AylaSystemUtils.saveToLog("%s, %s, %s:%d, %s:%d, %s:%d, %s", "E", "AylaLanMode", "version", AylaEncryption.version,
//						"proto", AylaEncryption.proto_1, "key_id_1", AylaEncryption.key_id_1, "Response_keyExchange");
				AylaSystemUtils.saveToLog("%s, %s, %s:%d, %s:%d, %s", "E", "AylaLanMode", "version", AylaEncryption.version,
						"key_id_1", AylaEncryption.key_id_1, "Response_keyExchange");
				AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s:%s, %s", "E", "AylaLanMode", "random_1", AylaEncryption.sRnd_1,
						"time_1", AylaEncryption.nTime_1, "Response_keyExchange");
				AylaDevice.lanModeSessionFailed(); //v2.13_ENG
				AylaLanMode.isSecureSetupSession = false;     
				return new AylaHttpServer.Response(Status.CERT_ERROR, MIME_JSON, jsonString);
			} 
			
			StringBuilder sb = new StringBuilder();
			sb.append("{")
				.append("\"random_2\":" + "\"" + AylaEncryption.sRnd_2 + "\"")
				.append(",\"time_2\":" + AylaEncryption.nTime_2.longValue())
				.append("}");               
			
			AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "I", "AylaLanMode", "entity", sb.toString(), "Response_keyExchange_secure_setup");
			
			// Begin a new Lan Mode session
			AylaDevice.lanModeSessionInit();
			
			// return keys
			return new AylaHttpServer.Response(Status.OK, MIME_JSON, sb.toString());                  
		}// end of processSecureSetupKeyExchange               
		
		
		
		//-------------------------- Begin Main HTTPD response method ----------------------
		@Override
		public Response serve(String uri, Method method, Map<String, String> header, Map<String, String> parms, Map<String, String> files) {
			AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s:%s, %s:%s, %s:%s, %s", "I", "AylaLanMode", "uri", uri, 
					                  "method", method.toString(),"header", header, "parms", parms, "Response");
			
			// TODO: Extract all the uri as constant in proper place. 
			if (uri.contains("key_exchange")) { // initiate session
				// parse out key exchange information
				String jsonString = parms.get("jsonString");
				AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "I", "AylaLanMode", "jsonString", jsonString, "Response_keyExchange1");

				AylaKeyExchangeContainer keyExchangeContainer = null;
				try {
					keyExchangeContainer = AylaSystemUtils.gson.fromJson(jsonString, AylaKeyExchangeContainer.class);
				}
				catch (Exception e) {
					AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "E", "AylaLanMode", "jsonString_parseError", jsonString, "Response_keyExchange2");
					e.printStackTrace();
					AylaDevice.lanModeSessionFailed(); //v2.13_ENG
					return new AylaHttpServer.Response(Status.BAD_REQUEST, MIME_JSON, jsonString);
				}
				
				// jsonString = "{\"key_exchange\":{\"ver\":1,\"random_1\":\"abc\",\"time_1\":123,\"proto\":1,\"key_id\":1}}"; // for lanMode keyExchange
				// jsonString = "{\"key_exchange\":{\"ver\":1,\"random_1\":\"abc\",\"time_1\":123,\"proto\":1,\"sec\":"0EC7ade"}}"; // for secure setup.
				if (TextUtils.isEmpty(keyExchangeContainer.keyExchange.sec)) {
					return processLanModeKeyExchange(keyExchangeContainer, jsonString);
				} else {
					return processSecureSetupKeyExchange(keyExchangeContainer, jsonString);
				}
			}// uri.contains("key_exchange") ends here

			else if (uri.contains("commands")) {


				if (sessionState != lanModeSession.UP) {
					AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s:%s, %s", "W", "AylaLanMode", "serverIpAddress", serverIpAddress, "HTTP_FORBIDDEN_403 sessionState", sessionStateMsg[sessionState.ordinal()], "Response_commands");
					String html = "<html><head><head><body><h1>" + "LAN mode session is down" + "</h1></body></html>";
					return new AylaHttpServer.Response(Status.FORBIDDEN, MIME_HTML, html);
				}

				AylaLanCommandEntity cmdEntity = nextInSendQueue();
				
				if (cmdEntity != null && AylaEncryption.appSignKey != null) {
					if (!Method.GET.equals(method)) {
						AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s:%s, %s", "E", "AylaLanMode", "serverIpAddress", serverIpAddress, "HTTP_METHOD_FAILURE_405", "error", "Response_commands");
						String html = "<html><head><head><body><h1>" + "Unsupported command" + "</h1></body></html>";
						return new AylaHttpServer.Response(Status.METHOD_FAILURE, MIME_HTML, html);
					}
					
					AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "I", "AylaLanMode", "cmdEntity.jsonStr", cmdEntity.jsonStr, "Response_commands");
					String jsonText = AylaEncryption.encryptEncapsulateSign(seq_no++, cmdEntity.jsonStr, AylaEncryption.appSignKey);
					AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "I", "AylaLanMode", "jsonText", jsonText, "Response_commands");
					
					deQueueSend();
					
					Status statusCode = Status.OK;
					if (nextInSendQueue() != null) {
						statusCode = Status.PARTIAL_CONTENT; // let device know there are more properties enqueued
					}
					
					/*
					 * TODO: For both command and datapoints, need to be consistent like entity/rs pair. 
					 * now only hardcode for SEND_NETWORK_PROFILE_LANMODE 
					 * */
					final AylaRestService rs = getCommandsOutstanding(cmdEntity.cmdId + "");
					if ( rs != null && rs.RequestType == AylaRestService.SEND_NETWORK_PROFILE_LANMODE ) {
						removeCommandsOutstanding(cmdEntity.cmdId + "");
						new Thread(new Runnable(){
							public void run() {
								try {
									// Hardcode to add a little latency
									// Make sure AylaHttpServer.Response arrive to device.
									Thread.sleep(5000);
									
									// TODO: this is not a good idea to kill the session here. Needs to improve.
									// Kill lan mode after the one time send efforts.
									AylaSetup.exitSecureSetupSession();
									
									AylaModule.returnToMainActivity(rs, "", 204, AylaRestService.SEND_NETWORK_PROFILE_LANMODE);
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						}).start();
					}
					
		            device.lanModeWillSendEntity(cmdEntity);
					AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "I", "AylaLanMode", "commandsJson", jsonText, "Response_commands");
					return new AylaHttpServer.Response(statusCode, MIME_JSON, jsonText);
				} else {
					byte[] sign = {0,1,2,3,4};
					if (AylaEncryption.appSignKey != null) {
						sign = AylaEncryption.appSignKey;
					} else {
						AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "E", "AylaLanMode", "signKey", "null", "Response_commands");
					}
					String jsonText = AylaEncryption.encryptEncapsulateSign(seq_no++,  "{}", sign);
					return new AylaHttpServer.Response(Status.OK, MIME_JSON, jsonText);
				}
			} else if (uri.contains("datapoint")) { // receive property change from the device
				String jsonEncodedEncapsulated = parms.get("jsonString");
				AylaBase64CryptoContainer base64CryptoContainer = AylaSystemUtils.gson.fromJson(jsonEncodedEncapsulated,AylaBase64CryptoContainer.class);

				if (base64CryptoContainer == null) { // there is no packet
					AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "E", "AylaLanMode", "base64Conatiner", "null", "Response_datapoint");
					return new AylaHttpServer.Response(Status.BAD_REQUEST, MIME_JSON, "");
				}

				// enc: base64.decode -> decrypt -> UTF8 -> {seq_no & data}
				String encapText = AylaEncryption.unencodeDecrypt(base64CryptoContainer.enc);
				// String encapText = "{\"seq_no\":0,\"data\":{\"name\":\"Blue_button\",\"value\":1}}";
				if (encapText == null) { // there is no payload
					AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "E", "AylaLanMode", "encapText", "null", "Response_datapoint");
					return new AylaHttpServer.Response(Status.BAD_REQUEST, MIME_JSON, "");
				}
				byte[] bCryptoEncapText = stringToBytes(encapText, null);

				// compare signatures: sign: base64.decode -> enc.signature
				byte[] bThisDevSignature = AylaEncryption.decode(base64CryptoContainer.sign);
				if (bThisDevSignature == null) { // there is no signature
					AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "E", "AylaLanMode", "signature", "null", "Response_datapoint");
					return new AylaHttpServer.Response(Status.BAD_REQUEST, MIME_JSON, "");
				}
				String sThisDevSignature = bytesToString(bThisDevSignature,"error converting signature bytes to string");

				// calculate the signature for this packet
				byte[] sign = {0,1,2,3,4};
				if (AylaEncryption.devSignKey != null) {
					sign = AylaEncryption.devSignKey;
				} else {
					AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "E", "AylaLanMode", "signKey", "null", "Response_datapoint");
				}
				byte[] bCalcDevSignature = AylaEncryption.hmacForKeyAndData(sign, bCryptoEncapText);
				String sCalcDevSignature = bytesToString(bCalcDevSignature,"error converting signature bytes to string");

				if (!sCalcDevSignature.equals(sThisDevSignature)) {
					AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "E", "AylaLanMode", "signature", Status.UNAUTHORIZED, "Response_datapoint");
					return new AylaHttpServer.Response(Status.UNAUTHORIZED, MIME_JSON, "");
				}

				// --------- authenticated packet -----------------
				sessionTimer.stop(); 	// don't send keep-alive for another cycle
				int returnStatus = 200;	// assume success 200

				//get restful parameters (only for command responses)
				AylaRestService rs = null;
				int cmdStatus = 200;
				int count = -1;
				String cmdIdStr = parms.get("cmd_id");
				String cmdStatusStr = parms.get("status");
//				AylaLanCommandEntity entity = nextInSendQueue();
				
				// create a json object and extract the property
				AylaCryptoEncap cryptoEncap = null;
				AylaCryptoEncapData cryptoEncapData = null;
				try {
					cryptoEncap = AylaSystemUtils.gson.fromJson(encapText, AylaCryptoEncap.class);
					cryptoEncapData = cryptoEncap.data;
				} catch (Exception e) {			
					AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "E", "AylaLanMode", "encapText", encapText, "Response_datapoint.gson");
					returnStatus = 400; // 400 BAD_REQUEST
				}
				
				if(device == null) {
					AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "W", "AylaLanMode", "device", "null", "Response_datapoint.gson");
				}
				else {
					if (!TextUtils.isEmpty(cmdIdStr))
					{ //It's a command resp
						cmdStatus = Integer.parseInt(cmdStatusStr);
						rs = getCommandsOutstanding(cmdIdStr);
						removeCommandsOutstanding(cmdIdStr);
						
						count = getCommandsOutstandingCount();
						oneStatus = device.lanModeUpdateProperty(cmdIdStr, cmdStatus, cryptoEncapData.name, cryptoEncapData.value, rs, count);
						
						if(returnStatus > 399) {
							// replace oneStatus with returnStatus if returnStatus is 4xx or 5xx
							oneStatus = returnStatus;
						}
					}
					else { //it's a property update
						returnStatus = device.lanModeUpdateProperty(cryptoEncapData.name, cryptoEncapData.value, true);
					}
				}
				
				oneStatus = returnStatus;
				Status finalReturnStatus;
				switch (oneStatus) {
					case 200:
						finalReturnStatus = Status.OK;
						break;
					case 400:
						finalReturnStatus = Status.BAD_REQUEST;
						break;
					case 404:
						finalReturnStatus = Status.NOT_FOUND;
						break;
					default:
						finalReturnStatus = Status.NOT_IMPLEMENTED;
				}

				if (oneStatus != 200) {
					AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "E", "AylaLanMode", "HTTP Status", finalReturnStatus.getDescription(), "Response_datapoint");
				} else {
					AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s:%s, %s", "I", "AylaLanMode", "cmdIdStr", cmdIdStr!=null?cmdIdStr:"null", "fromDeviceStatus", cmdStatusStr!=null?cmdStatusStr:"null", "Response_datapoint");
				}

				sessionTimer.setInterval(sessionInterval); // restart the session interval

				return new AylaHttpServer.Response(finalReturnStatus, MIME_JSON, "");
			} else if (uri.contains("local_lan/connect_status")) {
				// The uri is what we send to the module, in this case the secure setup session establish request.
				AylaSystemUtils.saveToLog("%s, %s, %s.", "D", "AylaLanMode.serve", "local_lan/connect_status branch");
				return new AylaHttpServer.Response(Status.OK, MIME_HTML, "");
			} else if (uri.contains("delete_wifi_profile")) { 
				// special case to handle delete_wifi_profile, for unit test, will not expose the API, will not document it.
				new Thread(new Runnable(){

					@Override
					public void run() {
						// Wait until the response is sent then shut down the session.
						// TODO: hard code is not good, do we need to disable here or 
						// returnToMainActivity() and let the notifier callback define the behavior.
						AylaSystemUtils.sleep(1000); 
						disable();    
						AylaNotify.returnToMainActivity(null, "", 204, AylaRestService.DELETE_NETWORK_PROFILE_LANMODE, false);                
					}// end of run    
					
				}).start();
				
				AylaSystemUtils.saveToLog("%s, %s, %s.", "D", "AylaLanMode.serve", "delete_wifi_profile");
//				return new AylaHttpServer.Response(html);
//				return new AylaHttpServer.Response(Status.OK, MIME_HTML, "");
				return new AylaHttpServer.Response(Status.NO_CONTENT, MIME_HTML, "");
			} else {
				AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s:%s, %s", "E", "AylaLanMode", "serverIpAddress", serverIpAddress, "parmsBuf", "error", "Response");
				String html = "<html><head><head><body><h1>" + "Unknown URI" + "</h1></body></html>";
				return new AylaHttpServer.Response(Status.NOT_FOUND, MIME_HTML, html);
			}
		}
	}
	
	//-------------------------- End Main HTTPD response method ----------------------
	static AylaRestService getLanModeConfig() {
		AylaSystemUtils.saveToLog("%s, %s, %s, %s", "I", "AylaLanModeConfig", "entry", "getLanModeConfig");
		
		if (!device.lanEnabled) { // v2.30
			AylaDevice.lanModeSessionFailed();	// device is not lan mode enabled, notify app, v2.30
			return null; // v2.30
		} // v2.30
		
		// check storage, else retrieve from cloud service, 
		String jsonLanModeConfig = AylaCache.get(AML_CACHE_LAN_CONFIG, device.dsn); // AylaSystemUtils.loadSavedSetting(lanModeConfigName, "");
		if (AylaReachability.isWiFiConnected(null) && !TextUtils.isEmpty(jsonLanModeConfig) ) {
			// Use saved values
			try {
				AylaLanModeConfig lanModeConfig = AylaSystemUtils.gson.fromJson(jsonLanModeConfig,AylaLanModeConfig.class);
				device.lanModeConfig = lanModeConfig;
				
				AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "I", "AylaLanModeConfig", "lanModeConfigStorage", jsonLanModeConfig, "getLanModeConfig");
				
				//if (device.lanEnabled) { v2.30
					//Set the secure session refresh timer
					int interval = (device.lanModeConfig.keepAlive.intValue()*1000) - AML_LAN_MODE_TIMEOUT_SAFETY; //v2.30
					sessionTimer.setInterval(interval); //v2.30
					
					Boolean haveDataToSend = false; // don't follow key exchange with a get commands
					AylaDevice.startLanModeSession(device, AylaRestService.POST_LOCAL_REGISTRATION, haveDataToSend); // begin a new secure session with this device.
					return null; // v2.30
				//} v2.30
			} catch (Exception e) {
				AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "E", "AylaLanModeConfig", "lanModeConfigStorage", e.getCause(), "getLanModeConfig");
				
			}
			AylaDevice.lanModeSessionFailed();	// device is not reachable or lanEnabled, notify app, v1.67_ENG
			return null;
		} 
		if (AylaReachability.isCloudServiceAvailable()) {
			device.lanModeConfig = null; // set this in the handler
			Number devKey = device.getKey().intValue(); // Handle gson LazilyParsedNumber;
			// String url = "https://ads-dev.aylanetworks.com/apiv1/devices/###/lan.json";
			String url = String.format(Locale.getDefault(), "%s%s%d%s%s", deviceServiceBaseURL(), "devices/", devKey, "/lan", ".json");
			AylaRestService restService = new AylaRestService(getLanModeConfigHandle, url, AylaRestService.GET_DEVICE_LANMODE_CONFIG);
			
			saveToLog("%s, %s, %s:%s, %s", "I", "AylaLanModeConfig", "url", url, "getLanModeConfig");
			restService.execute();

			return restService;
		} else {
			AylaDevice.lanModeSessionFailed();	// device is not reachable or lanEnabled, notify app, v1.67_ENG
			return null;
		}
	}

	// called from AylaDevice
	static final Handler getLanModeConfigHandle = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults =  (msg.obj != null) ? (String)msg.obj : "";
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				try {
					AylaLanModeConfigContainer lanModeConfigContainer = AylaSystemUtils.gson.fromJson(jsonResults,AylaLanModeConfigContainer.class);
					AylaLanModeConfig lanModeConfig = lanModeConfigContainer.lanip;
					
					if (device != null) {
						// Save lan mode config info for the session and in preferences
						device.lanModeConfig = lanModeConfig;
						jsonResults = AylaSystemUtils.gson.toJson(device.lanModeConfig,AylaLanModeConfig.class);
						AylaCache.save(AML_CACHE_LAN_CONFIG, device.dsn, jsonResults);
						
						//Set the secure session refresh timer
						int interval = (device.lanModeConfig.keepAlive.intValue()*1000) - AML_LAN_MODE_TIMEOUT_SAFETY;
						sessionTimer.setInterval(interval);
					} else {
						AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "E", "AylaLanModeConfig", "AylaLanMode.device", "null", "getLanConfig_handler_OK");
						AylaDevice.lanModeSessionFailed();	// device is not lanEnabled, notify app, v1.67
						return; //v2.30
					}
					
					AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "I", "AylaLanModeConfig", "lanModeConfig", jsonResults, "getLanConfig_handler_OK");
					
					Boolean haveDataToSend = false; // don't follow key exchange with a get commands
					AylaDevice.startLanModeSession(device, AylaRestService.POST_LOCAL_REGISTRATION, haveDataToSend); // begin a new secure session with this device.
					return;
				} catch (Exception e) {
					AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "E", "AylaLanModeConfig", e.getCause() , jsonResults, "getLanConfig_handler_OK_e");
					
				}
			} else {
				String severity = "E";
				if (msg.arg1 == 404) { 	// 404 == lan mode not enabled for this device "Not Found"
					try {
						severity = "I";
						device.lanModeConfig = new AylaLanModeConfig();
						device.lanModeConfig.status = jsonResults; 
						jsonResults = AylaSystemUtils.gson.toJson(device.lanModeConfig,AylaLanModeConfig.class);
						AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "I", "AylaLanModeConfig", "lanModeConfig", jsonResults, "getLanConfig_handler_404");
						
						AylaCache.save(AML_CACHE_LAN_CONFIG, device.dsn, jsonResults);
						AylaDevice.lanModeSessionFailed();	// v2.11a
						return; 
					} catch (Exception e) {
						AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "E", "AylaLanModeConfig", "lanModeConfig", e.getCause(), "getLanConfig_handler_404");
						
					}
				}
				saveToLog("%s, %s, %s:%d, %s", severity, "AylaLanModeConfig", "msg", msg.arg1, "getLanConfig_handler_err");
				
				if (device != null) {
					device.lanModeConfig = null;
				}
			}
			AylaDevice.lanModeSessionFailed();	// device is not lanEnabled, notify app, v1.67_ENG
		}		
	};

	/**
	 * Used for session extension/keep-alive with the module
	 */
	static int sessionInterval = 30000-AML_LAN_MODE_TIMEOUT_SAFETY; // default secure session keep-alive value
	private static void sessionTimer() {
		saveToLog("%s, %s, %s:%s, %s", "I", "AylaLanMode", "interval", sessionInterval, "sessionTimer");
		sessionTimer = new AylaTimer(sessionInterval, new Runnable() { // allow for delivery/busy time
			public void run() {
				AylaDevice.extendLanModeSession(AylaRestService.PUT_LOCAL_REGISTRATION, false);
			} 
		});
	}

	// --------------------------------------- BEGIN LAN MODE ENABLEMENT & DISABLEMENT ------------------------
	/**
	 * Enables the application (activity) to use LAN Mode secure communication. In addition to enabling the application, the current device must
	   also be LAN Mode enabled
	 * @return can be AML_ERROR_OK or AML_ERROR_FAIL
	 */
	public static int enable(Handler notifierHandler, Handler reachabilityHandler) {
		lanModeState = lanMode.ENABLED; // must ENABLE for LAN Mode operation, DISABLE is the default
		saveToLog("%s, %s, %s:%s, %s", "I", "AylaLanMode", "lanModeState", lanModeMsg[lanModeState.ordinal()], "enable");
		
		AylaLanMode.discovery.initialize(); // start mdns discovery thread 

		// Initialize queues
		clearSendQueue();				// queue commands for the device
		clearCommandsOutstanding(); // look up async commands response

		// save handlers for callback to main activity
		AylaNotify.register(notifierHandler);
		
		// call once to start the sessionTimer
		if (sessionTimer != null) {
			sessionTimer.stop();
		}
		sessionTimer();

		AylaReachability.register(reachabilityHandler);
		
		String sIpAddr = getLocalIpv4Address();
		serverIpAddress = (sIpAddr == null) ? serverIpAddress : sIpAddr;

		Thread thread = new Thread(new Runnable() {
			public void run() {
				try {
					if (httpServer == null) {
						httpServer = new aylaHTTPD();
						httpServer.start();
					}
					
					if (httpServer != null) {

						lanModeState = lanMode.RUNNING;
					} else {
						lanModeState = lanMode.FAILED;
					}
				} catch (IOException e) {
					if (e.getMessage().contains("Address already in use")) {
						lanModeState = lanMode.RUNNING;
					} else {
						lanModeState = lanMode.FAILED;
						e.printStackTrace();
					}
				}
			}
		});

        lanModeState = lanMode.STOPPED;
		thread.start();
		while (lanModeState == lanMode.STOPPED) { // wait for server to start, v2.21_ENG
			//JVM friendly NOP
			try {
			      Thread.sleep(10);
			   } catch (InterruptedException e) {
			      e.printStackTrace();
			   }
			   continue;
		}
		
		return AML_ERROR_OK; // OK, but should only call once to enable lan mode
	}

	/**
	 * General resume method. Should always be called when the app is coming to the foreground.
	 * Will refresh the access token if needed.
	 * Will start the HTTP server if LAN Mode is enabled
	 * Resume LAN mode communication and reboot http server. Typically called after LAN mode communication is paused.
	 * @return can be AML_ERROR_OK or AML_ERROR_FAIL
	 */
	public static int resume() {
		if (lanModeState == lanMode.DISABLED) {
			saveToLog("%s, %s, %s:%s, %s", "I", "AylaLanMode", "lanModeState", lanModeMsg[lanModeState.ordinal()], "resume");
			Boolean waitForResults = true;
			AylaReachability.determineReachability(waitForResults); // determine connectivity, reachablity, and notify registered handlers
			if (AylaReachability.getConnectivity() == AML_REACHABILITY_UNREACHABLE) {
				AylaSystemUtils.serviceReachableTimeout = -1; // don't test service reachability unless connectivityListner resume from background
			}
			
			AylaUser.refreshAccessTokenOnExpiry(DEFAULT_ACCESS_TOKEN_REFRESH_THRRESHOLD);	// auto refresh access token if seconds to expiry is less than threshold
			return AML_ERROR_OK; // call enable for resume/disable to work
		}
		
		if (AylaNetworks.appContext == null) {
			// app unloaded by OS
			return AML_ERROR_FAIL;
		}
		
		String sIpAddr = getLocalIpv4Address();
		serverIpAddress = (sIpAddr == null) ? serverIpAddress : sIpAddr;

		lanModeState = lanMode.STOPPED;
		Thread thread = new Thread(new Runnable() {
			public void run() {
				try {
					if (httpServer == null) {
						httpServer = new aylaHTTPD();
						httpServer.start();
					}
					
					if (httpServer != null) {
						lanModeState = lanMode.RUNNING;
					} else {
						lanModeState = lanMode.FAILED;
					}
				} catch (IOException e) {
					if (e.getMessage().contains("Address already in use")) {
						lanModeState = lanMode.RUNNING;
					} else {
						lanModeState = lanMode.FAILED;
						e.printStackTrace();
					}
				}

				if (lanModeState == lanMode.RUNNING) {
					AylaReachability.determineReachability(true); // determine connectivity, reachablity, and notify registered handlers
					if (AylaReachability.getConnectivity() == AML_REACHABILITY_UNREACHABLE) {
						AylaSystemUtils.serviceReachableTimeout = -1; // don't test service reachability unless connectivityListner or resume from background
					}
					
					
					saveToLog("%s, %s, %s:%s, %s", "I", "AylaLanMode", "lanModeState", lanModeMsg[lanModeState.ordinal()], "resume");
				} else {
					saveToLog("%s, %s, %s:%s, %s", "E", "AylaLanMode", "lanModeState", lanModeMsg[lanModeState.ordinal()], "resume");
				}

			}
		});
		thread.start();

		while (lanModeState == lanMode.STOPPED) { 	// wait for server to start
			//JVM friendly NOP
			try {
			      Thread.sleep(10);
			   } catch (InterruptedException e) {
			      e.printStackTrace();
			   }
			   continue;
		}
		
		AylaUser.refreshAccessTokenOnExpiry(DEFAULT_ACCESS_TOKEN_REFRESH_THRRESHOLD);	// auto refresh access token if seconds to expiry is less than threshold
		
		return AML_ERROR_OK; 
	}

	/**
	 * Pause LAN mode communication. This method will also stop current connection to the LME device and stop http server. 
	 * @return can be AML_ERROR_OK or AML_ERROR_FAIL
	 * 
	 * pausedByUser indicates user changed activities, so the app is still running
	 */
	public static int pause(Boolean pausedByUser) {
		
		if (!pausedByUser) {  // going into background vs next device
			AylaSystemUtils.serviceReachableTimeout = AylaNetworks.AML_SERVICE_REACHABLE_TIMEOUT; // always test reachability when coming out of the background
		}
		
		if (lanModeState != lanMode.DISABLED) {
			if (!pausedByUser) {  // going into background vs next device, intent, fragment, etc
				if (device != null) {
					device.lanModeDisable(); // save properties and kill LME session
				}
			}
			
			// stop HTTPD
			if (httpServer != null) {
				httpServer.stop();
				httpServer = null;
				lanModeState = lanMode.STOPPED;
				if (sessionTimer != null) {
					sessionTimer.stop();
				}
			} else {
				lanModeState = lanMode.FAILED;
			}
		}
		
		saveToLog("%s, %s, %s:%s, %s:%s, %s", "I", "AylaLanMode", "lanModeState", lanModeMsg[lanModeState.ordinal()],
				                                   "serviceReachabilityTimeout", AylaSystemUtils.serviceReachableTimeout, "pause");
		
		return AML_ERROR_OK;
	}
	
	/**
	 * This method will stop communication with all LME devices, stop the HTTP server, timers, etc. Buffered information of last connected LME device will be cleared.
	 * @return can be AML_ERROR_OK or AML_ERROR_FAIL
	 */
	public static int disable() {
		if (lanModeState == lanMode.RUNNING) {
			pause(false); // stop httpd & timer
		}
		
		lanModeState = lanMode.DISABLED;
		AylaLanMode.discovery.exit(); // stop mdns discovery thread
		
		saveToLog("%s, %s, %s:%s, %s", "I", "AylaLanMode", "lanModeState", lanModeMsg[lanModeState.ordinal()], "disable");
		return AML_ERROR_OK;
	}

	// --------------------------------------- END LAN MODE ENABLEMENT & DISABLEMENT ------------------------

	// ---------------------------------- Lan Mode Support Methods ------------------------------------
	public static void sendToLanModeDevice(AylaLanCommandEntity entity, AylaRestService rs) {
		if (lanModeState == lanMode.RUNNING) {
			AylaLanCommandEntity aQueuedEntity = nextInSendQueue();
			
			enQueueSend(entity); // queue up the datapoint to send to the device
			putCommandsOutstanding(entity.cmdId, rs); // hash rs for when device responds
					
			if (aQueuedEntity == null) { // already queued up
				sessionTimer.stop();
				AylaDevice.extendLanModeSession(AylaRestService.PUT_LOCAL_REGISTRATION, true); // notify device to get commands
				
				sessionTimer.setInterval(sessionInterval); // restart the session interval
			}
		} else {
			saveToLog("%s, %s, %s:%s, %s", "E", "AylaLanMode", "lanModeState", lanModeMsg[lanModeState.ordinal()], "sendToLanModeDevice");
		}
	}



	// Save commandId and restService for handling device async response
	private static  void putCommandsOutstanding(int cmdId, AylaRestService rs) {
		if (cmdId != AML_COMMAND_ID_NOT_USED) {
			commandsOutstanding.put(Integer.toString(cmdId), rs);
			commandsOutstandingCount++;
		}
	}

	private static int getCommandsOutstandingCount() {
		return commandsOutstandingCount;
	}

	private static AylaRestService getCommandsOutstanding(String cmdIdStr) {
		return commandsOutstanding.get(cmdIdStr);
	}

	private static void removeCommandsOutstanding(String cmdId) {
		if (commandsOutstanding.remove(cmdId)!=null 
				&& commandsOutstandingCount > 0) {
			commandsOutstandingCount--;
		}
	}

	public static synchronized int nextCommandOutstandingId() {
		return nextCommandOutstandingId++;
	}
	
	public static void clearCommandsOutstanding() {
		commandsOutstanding.clear();
		commandsOutstandingCount = 0;
	}

	// FIFO Queue
	// Queue command requests for the device
	private static void enQueueSend(final AylaLanCommandEntity entity) {
		commandsSendQueue.offer(entity); // queue datapoint
	}

	private static AylaLanCommandEntity nextInSendQueue() {
		return commandsSendQueue.peek(); // get highest priority item
	}

	private static void deQueueSend() {
		commandsSendQueue.poll(); // remove highest priority item
	}
	
	public static void clearSendQueue() {
		commandsSendQueue.clear();
	}
}





