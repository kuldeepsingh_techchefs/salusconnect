//
//  AylaRegistration.java
//  Ayla Mobile Library
//
//  Created by Daniel Myers on 9/06/12.
//  Copyright (c) 2012 Ayla Networks. All rights reserved.
//
package com.aylanetworks.aaml;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import com.google.gson.annotations.Expose;

import android.os.Handler;
import android.os.Message;

/**
 * 
 * Get registration candidate device from the Ayla device service. Save the LAN IP address & DSN.
 *
 */
public class AylaRegistration extends AylaSystemUtils {
	@Expose
	private String regtoken;
	@Expose
	private String lanIpAddress;

	private static AylaDevice regCandidate;
	private static AylaDevice[] regCandidates;
	
	private static boolean isGateway;

	private static AylaRestService registerNewDeviceRS;

	@Override
	public String toString()
	{
		StringBuilder result = new StringBuilder();
		String NEW_LINE = System.getProperty("line.separator");

		result.append(this.getClass().getName() + " Object {" + NEW_LINE);
		result.append(" regToken: " + regtoken + NEW_LINE);
		result.append(" regCandidate: " + regCandidate + NEW_LINE);
		result.append(" lanIpAddress: " + lanIpAddress + NEW_LINE);
		result.append("}");
		return result.toString();
	}

	/**
	 * ---------------------- Register a New Device ---------------------------
	 *
	 * Prerequisites:
	 *   a) The local Ayla device has completed setup and connected to the Ayla device service within the last hour
	 *   b) The local Ayla device and the phone/pad/pod/tablet running this code are connected to the same WLAN
	 *
	 * Steps
	 *   a) Get registration candidate device from the Ayla device service. Save the LAN IP address & DSN.
	 *   b) Get registration token from the local device using the LAN IP address
	 *   c) Register the local device with the Ayla device service using the Ayla local device registration token and DSN
	 *
	 * Returns
	 *   Success
	 *      A newly registered Ayla device
	 *   Failure
	 *     Ayla error code indicating which step failed
	 * 
	 * ---------------------------
	 * 
	 * @param mHandle is where result would be returned.
	 * @param targetDsn is DSN string of target device.
	 * @return AylaRestService object
	 */
	public static AylaRestService registerNewDevice(Handler mHandle, AylaDevice device) {
		saveToLog("%s, %s, %s:%s, %s:%s, %s", "I", "Registration", "targetDsn", device.dsn, "regMode", device.registrationType, "registerNewDevice");
		String url = "";
		boolean async = (mHandle == null) ? false : true;
		
		registerNewDeviceRS = new AylaRestService(mHandle, url, AylaRestService.REGISTER_NEW_DEVICE);
		
		registerNewDeviceBegin(mHandle, device, async);
		return registerNewDeviceRS;
	}
	
	protected static AylaRestService registerNewDeviceBegin(Handler mHandle, AylaDevice device, boolean async) {
		
		isGateway = device.isGateway();

		if (device.registrationType.equals(AML_REGISTRATION_TYPE_SAME_LAN) || device.registrationType.equals(AML_REGISTRATION_TYPE_BUTTON_PUSH)) {
			getRegistrationCandidate(null, device.dsn, device.registrationType, async);
		} else if (device.registrationType.equals(AML_REGISTRATION_TYPE_AP_MODE)) {
			if (device.setupToken != null) {
				registerDevice(device.dsn, null, device.setupToken, async);
			} else {
				saveToLog("%s, %s:%s, %s", "E", "Registration", "error", "Setup token not found", "registerNewDevice");
				returnToMainActivity(registerNewDeviceRS, "Setup token not found", AML_NO_ITEMS_FOUND , AML_REGISTER_NEW_DEVICE);
			}
		} else if (device.registrationType.equals(AML_REGISTRATION_TYPE_DISPLAY)) {
			if (device.registrationToken != null && !device.registrationToken.equals("")) {
				registerDevice(device.dsn, device.registrationToken, null, async);
			} else {
				saveToLog("%s, %s:%s, %s", "E", "Registration", "error", "Invalid registration token", "registerNewDevice");
				returnToMainActivity(registerNewDeviceRS, "Invalid registration token", AML_NO_ITEMS_FOUND , AML_REGISTER_NEW_DEVICE);
			}
		} else if (device.registrationType.equals(AML_REGISTRATION_TYPE_DSN)) {
			if (device.dsn != null && !device.dsn.equals("")) {
				registerDevice(device.dsn, null, null, async);
			} else {
				saveToLog("%s, %s:%s, %s", "E", "Registration", "error", "Device DSN is missing", "registerNewDevice");
				returnToMainActivity(registerNewDeviceRS, "Device DSN is missing", AML_NO_ITEMS_FOUND , AML_REGISTER_NEW_DEVICE);
			}
		} else if (device.registrationType.equals(AML_REGISTRATION_TYPE_NONE)) {
				saveToLog("%s, %s:%s, %s", "I", "Registration", "registrationType", "AML_REGISTRATION_TYPE_NONE", "registerNewDevice");
				returnToMainActivity(registerNewDeviceRS, "{}", AML_ERROR_OK , AML_REGISTER_NEW_DEVICE);
		} else {
			saveToLog("%s, %s:%s, %s", "E", "Registration", "error", "Registration type not found", "registerNewDevice"); 
			
			AylaSetup.clear(); // delete cached unsupported registration type
			returnToMainActivity(registerNewDeviceRS, "Not Found", AML_ERROR_NOT_FOUND, AML_REGISTER_NEW_DEVICE);
		}
		
		return registerNewDeviceRS;
	}

	
	
	protected static AylaRestService getCandidates(Handler mHandle, AylaDeviceGateway gateway) {
		String url = "";
		boolean async = (mHandle == null) ? false : true;
		
		registerNewDeviceRS = new AylaRestService(mHandle, url, AylaRestService.GET_GATEWAY_REGISTRATION_CANDIDATES); // save handle
		saveToLog("%s, %s, %s:%s, %s:%s, %s", "I", "Registration", "targetDsn", gateway.dsn, "regMode", AML_REGISTRATION_TYPE_NODE, "getCandidates");
		
		isGateway = true;
		getRegistrationCandidate(mHandle, gateway.dsn, AML_REGISTRATION_TYPE_NODE, async);

		return registerNewDeviceRS;
	}
	
	// -------------------------------- getRegistrationCandidate ---------------------
	/**
	 * Use AylaDevice.stripContainer to remove "device:" wrapper from API JSON response
	 * 
	 * @param targetDsn: Device Serial Number of the new device
	 * @param targetRegType: registration type of the new device
	 * @return
	 */
	private static AylaRestService getRegistrationCandidate(Handler mHandle, String targetDsn, String targetRegType, boolean async) {
		// String url = "http://ads-dev.aylanetworks.com/apiv1/devices/register.json";
		String url = String.format("%s%s%s", deviceServiceBaseURL(), "devices/register", ".json");
		if (targetDsn != null) {
			url = url + "?dsn=" + targetDsn;
			if (targetRegType != null) {
				url = url + "&regtype=" + targetRegType;
			}
		} else
		if (targetRegType != null) {
			url = url + "?regtype=" + targetRegType;
		}
		
		int method = AylaRestService.GET_REGISTRATION_CANDIDATE;	// assume wifi device registration
		if (isGateway) {
			method = AylaRestService.GET_GATEWAY_REGISTRATION_CANDIDATES;
		}
		
		AylaRestService rs = null;
		if (async) {
			if (isGateway) {
				rs = new AylaRestService(mHandle, url, method);						// return node candidates to app
			} else {
				rs = new AylaRestService(getRegistrationCandidate, url, method);
				
			}
			rs.execute();
		} else {
			rs = new AylaRestService(null, url, method);
			Message callResponse = rs.execute();
			doGetRegistrationCandidate(callResponse, false);
		}
		saveToLog("%s, %s, %s:%s, %s", "I", "Registration", "url", url, "getRegistrationCandidate");
		return rs;
	}

	private static final Handler getRegistrationCandidate = new Handler() {
		public void handleMessage(Message msg) {
			doGetRegistrationCandidate(msg, true);
		}		
	};
	
	private static void doGetRegistrationCandidate(Message msg, boolean async) {
		String jsonResults = (String)msg.obj;
		if (msg.what == AylaNetworks.AML_ERROR_OK) {
			if (isGateway == false) {
				regCandidate = AylaSystemUtils.gson.fromJson(jsonResults,  AylaDevice.class);
				String lanIp = regCandidate.lanIp;
				saveToLog("%s, %s, %s:%s, %s", "I", "Registration", "productName", regCandidate.productName, "getRegistrationCandidate.Handler");
	
				getModuleRegistrationToken(lanIp, async);
			} else {
				regCandidates = AylaSystemUtils.gson.fromJson(jsonResults,  AylaDevice[].class);
				if (regCandidates != null) {
					saveToLog("%s, %s, %s:%s, %s", "I", "Registration", "dsn", regCandidates[0].dsn, "getRegistrationCandidates.Handler");
					saveToLog("%s, %s, %s:%d, %s", "I", "Registration", "count", regCandidates.length, "getRegistrationCandidates.Handler");
				}
				
				returnToMainActivity(registerNewDeviceRS, jsonResults, msg.arg1, 0); // return to main with reg candidates
			}
		} else {
			saveToLog("%s, %s, %s:%d, %s, %s", "E", "Registration", "error", msg.arg1, msg.obj, "getRegistrationCandidate.Handler");
			returnToMainActivity(registerNewDeviceRS, jsonResults, msg.arg1, AML_GET_REGISTRATION_CANDIDATE);
		}
	}


	//----------------------------------- getModuleRegistrationToken ------------------------------
	/**
	 * Returns the new device registration token to the handler
	 * Required for AML_REGISTRATION_TYPE_SAME_LAN registration
	 * Nice to have for AML_REGISTRATION_TYPE_BUTTON_PUSH (DSN is required)
	 * Not required for AML_REGISTRATION_TYPE_AP_MODE
	 * Not required for AML_REGISTRATION_TYPE_DISPLAY
	 * Not required for AML_REGISTRATION_TYPE_DSN
	 * 
	 * @param lanIp - WLAN IP address
	 * @return registration token
	 */
	private static AylaRestService getModuleRegistrationToken(String lanIp, boolean async) {
		// http://192.168.0.1/regtoken.json
		String url = String.format("%s%s", lanIpServiceBaseURL(lanIp), "regtoken.json");
		saveToLog("%s, %s, %s:%s, %s", "I", "Registration", "lanIpAddress", lanIp, "getModuleRegistrationToken");
		
		AylaRestService rs = null;
		if (async) {
			rs = new AylaRestService(getModuleRegistrationToken, url, AylaRestService.GET_MODULE_REGISTRATION_TOKEN);
			rs.execute(); 
		} else {
			rs = new AylaRestService(null, url, AylaRestService.GET_MODULE_REGISTRATION_TOKEN);
			Message callResponse = rs.execute();
			doGetModuleRegistrationToken(callResponse, false);
		}
		saveToLog("%s, %s, %s:%s, %s", "I", "Registration", "url", url, "getModuleRegistrationToken");
		return rs;
	}
	
	private static final Handler getModuleRegistrationToken = new Handler() {
		public void handleMessage(Message msg) {
			doGetModuleRegistrationToken(msg, true);
		}		
	};
	
	private static void doGetModuleRegistrationToken(Message msg, boolean async) {
		String jsonResults = (String)msg.obj;
		if (msg.what == AylaNetworks.AML_ERROR_OK) {
			AylaRegistration regToken = AylaSystemUtils.gson.fromJson(jsonResults,  AylaRegistration.class);
			saveToLog("%s, %s, %s:%s, %s", "I", "Registration", "regToken", "regToken.regtoken", "getModuleRegistrationToken.Handler");

			registerDevice(regCandidate.dsn, regToken.regtoken, null, async);
		} else {
			saveToLog("%s, %s, %s:%d, %s, %s", "E", "Registration", "error", msg.arg1, msg.obj, "getModuleRegistrationToken.Handler");
			returnToMainActivity(registerNewDeviceRS, jsonResults, msg.arg1, AML_GET_MODULE_REGISTRATION_TOKEN);
		}
	}

	protected static AylaRestService registerCandidate(Handler mHandle, AylaDeviceNode node) {
		saveToLog("%s, %s, %s:%s, %s:%s, %s", "I", "Registration", "targetDsn", node.dsn, "regMode", node.registrationType, "registerCandidate");
		String url = "registerCandidate";
		boolean async = (mHandle == null) ? false : true;
		
		registerNewDeviceRS = new AylaRestService(mHandle, url, AylaRestService.REGISTER_NEW_DEVICE); // save handle

		registerDevice(node.dsn, null, null, async);

		return registerNewDeviceRS;
	}

	//-------------------------------- registerDevice ---------------------------------------------
	private static AylaRestService registerDevice(String dsn, String regToken, String setupToken, boolean async) {
		String url = String.format("%s%s%s", deviceServiceBaseURL(), "devices", ".json");
		
		String tokenType = null, token = null;
		if (setupToken != null) {
			tokenType =  "\"setup_token\":";
			token = setupToken;
		} else
		if (regToken != null) {
			tokenType = "\"regtoken\":";
			token = regToken;
		}

		//{"device":{"dsn":"AC000WT00000999","regtoken":"4d54f2"}}
		String regDeviceJson = 			"{\"device\":{";
		if (dsn != null) { 		// not used in AP setup mode registration type
			regDeviceJson = regDeviceJson + "\"dsn\":" + "\"" + dsn + "\"";
			if (token != null) {
				regDeviceJson = regDeviceJson + ",";
			}
		}
		if (token != null) {	// no token with Dsn registration type
			regDeviceJson = regDeviceJson + tokenType + "\"" + token + "\"";
		}
		regDeviceJson = regDeviceJson + "}}";
		
		AylaRestService rs = null;
		if (async) {
			rs = new AylaRestService(registerDevice, url, AylaRestService.REGISTER_DEVICE);
			rs.setEntity(regDeviceJson);
			rs.execute(); 
		} else {
			rs = new AylaRestService(null, url, AylaRestService.REGISTER_DEVICE);
			rs.setEntity(regDeviceJson);
			Message callResponse = rs.execute();
			doRegisterDevice(callResponse, false);
		}
		
		saveToLog("%s, %s, %s:%s, %s:%s, %s", "I", "Registration", "url", url, "regParams", "regDeviceJson", "registerDevice");
		
		return rs;
	}

	private static final Handler registerDevice = new Handler() {
		public void handleMessage(Message msg) {
			doRegisterDevice(msg, true);
		}		
	};

	private static void doRegisterDevice(Message msg, boolean async) {
		String jsonResults = (String)msg.obj;
		if (msg.what == AylaNetworks.AML_ERROR_OK) {
			
			AylaDevice newDevice = AylaSystemUtils.gson.fromJson(jsonResults,  AylaDevice.class);
			saveToLog("%s, %s, %s:%s, %s", "I", "Registration", "productName", newDevice.productName, "registerDevice.Handler");
			
			returnToMainActivity(registerNewDeviceRS, jsonResults, msg.arg1, 0);
		} else {
			saveToLog("%s, %s, %s:%d, %s, %s", "E", "Registration", "error", msg.arg1, msg.obj, "registerDevice.Handler");
			returnToMainActivity(registerNewDeviceRS, jsonResults, msg.arg1, AML_REGISTER_DEVICE);
		}
	}
	
	/**
	 * Used to open, close and return the current value of the gateway join registration window.
	 * When open, nodes are recognized by the gateway as registerable
	 * If callParms are null, the remaining seconds of the join window are returned
	 * 
	 * @param mHandle
	 * @param gateway
	 * @param callParams
	 *     "duration" : The number of seconds to open the join window, optional
	 *                  Max: 255 seconds, Min: 0 which will close the join window
	 * @return
	*/
	public static AylaRestService openRegistrationWindow(Handler mHandle, AylaDeviceGateway gateway, Map<String, String> callParams) {
		
		String paramKey = null;
		String paramValue = null;
		String durationStr = null;
		Integer duration = 200;		// gateway default
		
		// get optional fields
		
		// Join window time out, defaults to 200
		if (callParams != null) {
			
	    	paramKey = AylaDevice.kAylaDeviceJoinWindowDuration;	// "duration"
			paramValue = (String)callParams.get(paramKey);
			if (paramValue != null) {
				duration = Integer.valueOf(paramValue);
				durationStr = "?" + paramKey + "=";
			
				if (duration > 255) {
					durationStr = durationStr + "255";
				} else {
					durationStr = durationStr + duration.toString();
				}
			}
		}
		
		//String url = "http://ads-dev.aylanetworks.com/apiv1/devices/<deviceId>/registration_window.json";
		Number gwKey = gateway.getKey().intValue(); // Handle gson LazilyParsedNumber;
		String url = String.format("%s%s%d%s%s", deviceServiceBaseURL(), "devices/", gwKey, "/registration_window", ".json");
		if (durationStr != null) {
			url = url + durationStr;
		}
		
		AylaRestService rs = new AylaRestService(mHandle, url, AylaRestService.OPEN_REGISTRATION_WINDOW);
		saveToLog("%s, %s, %s:%s, %s", "I", "Registration", "url", url, "openRegistrationWindow");

		rs.execute(); 

		return rs;
	}
	
	/**
	 * Convenience method to close the gateway join registration window
	 * Simply calls openRegistrationWindow with an open time of 0 seconds
	 * 
	 * @param mHandle
	 * @param gateway
	 * @return
	 */
	public static AylaRestService closeRegistrationWindow(Handler mHandle, AylaDeviceGateway gateway) {
		final String joinWindowOpenTime = "0";								// 0 will close the join registration window
		Map<String, String> callParams = new HashMap<String, String>();
		
		callParams.put(AylaDevice.kAylaDeviceJoinWindowDuration, joinWindowOpenTime);
		return openRegistrationWindow(mHandle, gateway, callParams);
	}
	


	/**
	 * return to main Activity
	 * @param jsonResults
	 *   JSON response from Ayla device service
	 * @param responseCode
	 *   HTTP response code
	 * @param subTaskErrorId
	 *   AML specific code identifying where an error occurred. Set responseCode to <200...299> & this value to zero on no error
	 */
	private static void returnToMainActivity(AylaRestService rs, String thisJsonResults, int thisResponseCode, int thisSubTaskId) {
		rs.jsonResults = thisJsonResults;
		rs.responseCode = thisResponseCode;
		rs.subTaskFailed = thisSubTaskId;
		
		rs.execute();
	}

	/**
	 * Unregister Device from Ayla Device Service
	 * @param mHandle is where result would be returned.
	 * @param device is the device to be unregistered
	 * @return AylaRestService object
	 */
	public static AylaRestService unregisterDevice(AylaDevice device) {
		return unregisterDevice(null, device);
	}
	public static AylaRestService unregisterDevice(Handler mHandle, AylaDevice device) {
		Number devKey = device.getKey().intValue(); // Handle gson LazilyParsedNumber;
		String url = String.format(Locale.getDefault(), "%s%s%d%s", deviceServiceBaseURL(), "devices/", devKey, ".json");
		AylaRestService rs = new AylaRestService(mHandle, url, AylaRestService.UNREGISTER_DEVICE);
		saveToLog("%s, %s, %s:%s, %s", "I", "Registration", "url", url, "unregisterDevice");

		if (mHandle != null) { // is an async request
			rs.execute(); 
		}
		return rs;
	}
}


