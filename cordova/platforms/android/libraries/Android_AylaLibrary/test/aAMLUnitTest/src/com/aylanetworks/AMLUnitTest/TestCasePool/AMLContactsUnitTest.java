/*
 * AMLContactsUnitTest.java
 * Ayla Mobile Library
 * 
 * Created by Di Wang on 05/04/2015.
 * Copyright (c) 2015 Ayla Networks Inc. All rights reserved.
 * */

package com.aylanetworks.AMLUnitTest.TestCasePool;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.aylanetworks.AMLUnitTest.AMLUnitTestConfig;
import com.aylanetworks.AMLUnitTest.facility.Test;
import com.aylanetworks.AMLUnitTest.facility.TestSequencer;
import com.aylanetworks.AMLUnitTest.interfaces.DefaultScreen;
import com.aylanetworks.AMLUnitTest.interfaces.ITestResultDisplayable;
import com.aylanetworks.AMLUnitTest.interfaces.IUnitTestable;
import com.aylanetworks.aaml.AylaContact;
import com.aylanetworks.aaml.AylaNetworks;
import com.aylanetworks.aaml.AylaSystemUtils;
import com.aylanetworks.aaml.AylaUser;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.os.Message;
import android.util.Log;



@SuppressLint({ "HandlerLeak", "DefaultLocale" })
public class AMLContactsUnitTest implements IUnitTestable{

		private final String tag = AMLContactsUnitTest.class.getSimpleName();   
		
		private ITestResultDisplayable mScreen = null;
		private TestSequencer testSequencer = null;
		private Runnable mEndHandler = null;
		
		private AylaUser mUser = null;
		private AylaContact mContact = null;
//		private int mTestsFailed; // TODO: do we need statistics for module?
		
		public AMLContactsUnitTest() {
			mScreen = new DefaultScreen();
		}
		
		public AMLContactsUnitTest(final ITestResultDisplayable screen) {
			mScreen = screen;
		}

		@Override
		public void onResume() {
			if (mScreen == null) {
				mScreen = new DefaultScreen();
			}
			mScreen.init();  
			
			initTestSuite();
			
			if (mUser == null) { 
				// If mUser is not set, it is not a part of a Test Suite, need to prepare everything. 
				AylaNetworks.onResume();
			}
		}// end of onResume     
		
		
		@Override
		public void start() {
			if (testSequencer == null) {
				onResume();
			}
			testSequencer.nextTest().execute();
		}// end of start           
		
		
		@Override
		public void onPause() {
			if (testSequencer == null) {
				onResume();
			}
			
			AylaNetworks.onPause(false);
		}// end of onDestroy
		
		
		@Override
		public void onDestroy() {
			AylaNetworks.onDestroy();
			//TODO: GC work done here.
			testSequencer = null;
			mUser = null;
		}// end of onDestroy          
		
		
		@Override
		public void onEnd() {
			if (mEndHandler!=null) {
				mEndHandler.run();
			}
		}

		@Override
		public void setEndHandler(Runnable r) {
			mEndHandler = r;
		}
		
		@Override
		public void setResultScreen(ITestResultDisplayable screen) {
			if (screen == null) {
				mScreen = new DefaultScreen();
			} else {
				mScreen = screen;
			}
		}// end of setResultScreen         
		
		
		@Override
		public boolean isRunning() {
			if (testSequencer != null) {
				return testSequencer.isTestSuiteRunning();
			}
			return false;
		}// end of isRunning                     
		
		
		// --------------------  End of Interface ---------------------
		
		public void setUser(AylaUser user) {
			mUser = user; 
			mUser.email = AMLUnitTestConfig.userName;
		}// end of setUser
		
		
		private void initTestSuite() {
			List<Test> list = new ArrayList<Test>();
			if (mUser == null) {
				list.addAll(preconditionTestList);
			}
			list.addAll(contactAPITestList);
			testSequencer = new TestSequencer(list);
		}// end of initTestSuite         
		
		
		private List<Test> preconditionTestList = new ArrayList<Test>(Arrays.asList(
				new Test() { public void execute() { amlTestNothing(); }}
				, new Test() { public void execute() { setupUser(); }}
				));
		
		
		private List<Test> contactAPITestList = new ArrayList<Test>(Arrays.asList(
				new Test() { public void execute() { amlTestCreateContacts(); }}
				, new Test() { public void execute() { amlTestGetAllContactsForCurrentUser(); }}
				, new Test() { public void execute() { amlTestGetContactByOEM(); }}
				, new Test() { public void execute() { amlTestUpdateContacts(); }}
				, new Test() { public void execute() { amlTestGetContactByDisplayName(); }}
				, new Test() { public void execute() { amlTestDeleteContacts(); }}
				));
		
	// FIX: Temporarily take out settings tests, since they interfere with
	// cached values (BUG AML-40)
	private void amlTestNothing() {
		String passMsg = String.format("%s, %s", "P", "TestNothing");
		amlTestStatusMsg("Pass", "TestNothing", passMsg);
		testSequencer.nextTest().execute();
	}// end of amlTestNothing
		
	
	private void setupUser() {
		if (AMLUnitTestConfig.gblAmlAsyncMode) {
			Log.d(tag, 
					"user name:" + AMLUnitTestConfig.userName + 
					" \npassword:" + AMLUnitTestConfig.password + 
					" \nappId:" + AMLUnitTestConfig.appId + 
					" \nappSecrect:" + AMLUnitTestConfig.appSecret);
			AylaUser.login(login2, 
					AMLUnitTestConfig.userName, 
					AMLUnitTestConfig.password, 
					AMLUnitTestConfig.appId, 
					AMLUnitTestConfig.appSecret);
		} else {
			// TODO: Add handling for Sync Mode
		}
	}// end of setupUser          
	
	
	private final Handler login2 = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				AylaUser user = AylaSystemUtils.gson.fromJson(jsonResults, AylaUser.class);
				user = AylaUser.setCurrent(user);
				mUser = user;
				mUser.email = AMLUnitTestConfig.userName;
				String passMsg = String.format("%s, %s, %s:%d, %s", "P", 
						"amlTest", "secondsToExpiry", 
						user.accessTokenSecondsToExpiry(), "login2");
				amlTestStatusMsg("Pass", "ContactUnitTest login2", passMsg);
				testSequencer.nextTest().execute();
			} else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "login2");
				amlTestStatusMsg("Fail", "" + "", errMsg);
			}
		}// end of handleMessage  
	};
	
		
		private void amlTestCreateContacts() {
			if (mUser == null) {
				amlTestStatusMsg("Fail", "amlTestCreateContacts", "mUser is null.");
				return ;
			}
			
			mContact = new AylaContact();
//			mContacts.id = 1;  // cloud will get you an ID once successfully created.
			mContact.firstname = "Ayla";
			mContact.lastname = "Networks";
			mContact.displayName = "Ayla rocks";
			mContact.email = "support@aylanetworks.com";
			mContact.country = "USA";
			mContact.oemModels = new String[2];
			mContact.oemModels[0] = "ledevb";
			mContact.oemModels[1] = "zigbee1";
			
			Log.d(tag, "amlTestCreateContacts().");
			
			mContact.create(mCreateContactHandler, null);
		}// end of amlTestCreateContacts
		
		
		private final Handler mCreateContactHandler = new Handler() {
			public void handleMessage(Message msg) {
				String jsonResults = (String)msg.obj;
				Log.d(tag, "mCreateContactHandler.handleMessage(), jsonResults:" + jsonResults);
				if (msg.what == AylaNetworks.AML_ERROR_OK) {
					AylaContact contact = AylaSystemUtils.gson.fromJson(jsonResults, AylaContact.class);
					mContact = contact;
					
					String passMsg = String.format("%s, %s, %s", "P", "amlTest", "amlTestCreateContacts");
					amlTestStatusMsg("Pass", "mCreateContactHandler", passMsg);
					
					testSequencer.nextTest().execute();
				} else {
					// TODO: msg.arg1 == 422?
					String errMsg = String.format("%s, %s, %s:%d, %s, %s.", 
							"F", 
							"amlTest", 
							"error", 
							msg.arg1, 
							msg.obj, 
							"mCreateContactHandler");
					amlTestStatusMsg("Fail", "mCreateContactHandler", errMsg);
				}
			}// end of handleMessage
		};
		
		
		private void amlTestGetAllContactsForCurrentUser() {
			if (mUser == null) {
				amlTestStatusMsg("Fail", "amlTestGetAllContactsForCurrentUser", "mUser is null.");
				return ;
			}
			Log.d(tag, "amlTestGetAllContactsForCurrentUser().");
			AylaContact.get(mGetAllContactsForCurrentUserHandler, null);
		}// end of amlTestGetAllContactsForCurrentUser     
		
		
		private final Handler mGetAllContactsForCurrentUserHandler = new Handler() {
			public void handleMessage(Message msg) {
				String jsonResult = (String)msg.obj;
				Log.d(tag, "mGetContactsForSpecifiedUserHandler.handleMessage(), jsonResults:" + jsonResult);
				if (msg.what == AylaNetworks.AML_ERROR_OK) {
					AylaContact[] contacts = AylaSystemUtils.gson.fromJson(jsonResult, AylaContact[].class);
					
					for (AylaContact ac : contacts) {
						if (mContact.id == ac.id) {
							mContact = ac;
						}
					}
					String passMsg = String.format("%s, %s, %s.", "P", "amlTest", "mGetContactsForSpecifiedUserHandler");      
					amlTestStatusMsg("Pass", "mGetContactsForSpecifiedUserHandler", passMsg);
					testSequencer.nextTest().execute();
				} else {
					String errMsg = String.format("%s, %s, %s:%d, %s, %s.", 
							"F", 
							"amlTest", 
							"error", 
							msg.arg1, 
							jsonResult, 
							"mGetContactsForSpecifiedUserHandler");
					amlTestStatusMsg("Fail", "mGetContactsForSpecifiedUserHandler", errMsg);
				}
			}// end of handleMessage  
		};
		
		
		private void amlTestGetContactByOEM() {
			if (mUser == null) {
				amlTestStatusMsg("Fail", "amlTestGetContactByOEM", "mUser is null.");
				return ;
			}
			Log.d(tag, "amlTestGetContactByOEM()");
			Map<String, String> param = new HashMap<String, String>();
			param.put(AylaContact.kAylaContactOEMModels, "ledevb");
			
			AylaContact.get(mGetContactByOEMHandler, param);
		}// end of amlTestGetContactByOEM
		
		
		private final Handler mGetContactByOEMHandler = new Handler() {
			public void handleMessage(Message msg) {
				String jsonResult = (String)msg.obj;
				Log.d(tag, "mGetContactByOEMHandler.handleMessage(), jsonResults:" + jsonResult);
				if (msg.what == AylaNetworks.AML_ERROR_OK) {
					AylaContact[] contacts = AylaSystemUtils.gson.fromJson(jsonResult, AylaContact[].class);
					for (AylaContact ac : contacts) {
						if (mContact.id == ac.id) {
							mContact = ac;
						}
					}
					
					String passMsg = String.format("%s, %s, %s.", "P", "amlTest", "mGetContactByOEMHandler");      
					amlTestStatusMsg("Pass", "mGetContactByOEMHandler", passMsg);
					
					testSequencer.nextTest().execute();
				} else {
					String errMsg = String.format("%s, %s, %s:%d, %s, %s.", 
							"F", 
							"amlTest", 
							"error", 
							msg.arg1, 
							jsonResult, 
							"mGetContactByOEMHandler");
					amlTestStatusMsg("Fail", "mGetContactByOEMHandler", errMsg);
				}
			}// end of handleMessage  
		};
		
		
		private void amlTestUpdateContacts() {
			if (mUser == null) {
				amlTestStatusMsg("Fail", "amlTestUpdateContacts", "mUser is null.");
				return ;
			}
			
			if (mContact == null) {
				amlTestStatusMsg("Fail", "amlTestUpdateContacts", "mContacts is null.");
				return ;
			}
			mContact.email = "changed_email@email.com";
			mContact.country = "changed_country";
			mContact.displayName = "Hello World";
			
			Log.d(tag, "amlTestUpdateContacts().");
			mContact.update(mUpdateContactHandler);
		}// end of amlTestUpdateContacts()                 
		
		
		private final Handler mUpdateContactHandler = new Handler() {
			public void handleMessage(Message msg) {
				String jsonResults = (String)msg.obj;
				Log.d(tag, "mUpdateContactHandler.handleMessage(), jsonResults:" + jsonResults);
				if (msg.what == AylaNetworks.AML_ERROR_OK) {
					AylaContact contacts = AylaSystemUtils.gson.fromJson(jsonResults, AylaContact.class);
					mContact = contacts;
					
					String passMsg = String.format("%s, %s, %s", "P", "amlTest", "amlTestUpdateContacts");
					amlTestStatusMsg("Pass", "mUpdateContactHandler", passMsg);
					testSequencer.nextTest().execute();
				} else {
					// TODO: msg.arg1 == 422?
					String errMsg = String.format("%s, %s, %s:%d, %s, %s.", 
							"F", 
							"amlTest", 
							"error", 
							msg.arg1, 
							msg.obj, 
							"mUpdateContactHandler");
					amlTestStatusMsg("Fail", "mUpdateContactHandler", errMsg);
				}
			}// end of handleMessage  
		};
		
		
		private void amlTestGetContactByDisplayName() {
			if (mContact == null) {
				amlTestStatusMsg("Fail", "amlTestGetSpecifiedContact", "mContacts is null.");
				return ;
			}
			
			Map<String, String> params = new HashMap<String, String>();           
//			params.put(AylaContact.kAylaContactContactId, mContact.id+"");
			params.put(AylaContact.kAylaContactDisplayName, "Hello World");
			Log.d(tag, "amlTestGetSpecifiedContact().");
			AylaContact.get(mGetContactByDisplayNameHandler, params);
		}// end of amlTestGetSpecifiedContact          
		
		
		private final Handler mGetContactByDisplayNameHandler = new Handler() {
			public void handleMessage(Message msg) {
				String jsonResult = (String)msg.obj;
				Log.d(tag, "mGetSpecifiedContactHandler.handleMessage(), jsonResults:" + jsonResult);
				if (msg.what == AylaNetworks.AML_ERROR_OK) {
					String passMsg = String.format("%s, %s, %s.", 
							"P",   
							"amlTest",  
							"mGetSpecifiedContactHandler");
					amlTestStatusMsg("Pass", "mGetSpecifiedContactHandler", passMsg);
					testSequencer.nextTest().execute();
				} else {
					String errMsg = String.format("%s, %s, %s:%d, %s, %s.", 
							"F",
							"amlTest",
							"error",
							msg.arg1,
							jsonResult,
							"mGetSpecifiedContactHandler");
					amlTestStatusMsg("Fail", "mGetSpecifiedContactHandler", errMsg);
				}
			}// end of handleMessage  
		};
		
		
		private void amlTestDeleteContacts() {
			if (mUser == null) {
				amlTestStatusMsg("Fail", "amlTestDeleteContacts", "mUser is null.");
				return ;
			}
			Log.d(tag, "amlTestDeleteContacts().");
			mContact.delete(mDeleteContactHandler);
		}// end of amlTestDeleteContacts                  
		
		
		private final Handler mDeleteContactHandler = new Handler() {
			public void handleMessage(Message msg) {
				String jsonResult = (String)msg.obj;
				Log.d(tag, "mDeleteContactHandler.handleMessage(), jsonResults:" + jsonResult);
				
				if (msg.what == AylaNetworks.AML_ERROR_OK) {
					String passMsg = String.format("%s, %s, %s:%s, %s.", "P", "amlTest", "return", "success", "mDeleteContactHandler");          
					amlTestStatusMsg("Pass", "mDeleteContactHandler", passMsg);
					
					testSequencer.nextTest().execute(); 
				} else {
					String errMsg = String.format("%s, %s, %s:%d, %s, %s.", "F", "amlTest", "error", msg.arg1, msg.obj, "mDeleteContactHandler");       
					amlTestStatusMsg("Fail", "mDeleteContactHandler", errMsg);
				}
			}// end of handleMessage  
		};
		

		
		//TODO: extract as a common utility function
		private void amlTestStatusMsg(String passFail, String methodName, String methodMsg) {
			String testMsg = "";
			if (passFail !=null && passFail.contains("Pass")) {
				AylaSystemUtils.saveToLog("\n%s", methodMsg);
				testMsg = String.format("%s %s", methodName, "passed");
				
				if (!passFail.contains("Sync")) {
					mScreen.displayFailMessage(testMsg + "\n\n");
				} else {
					Log.i(tag, testMsg);
				}
			} else if (passFail !=null && passFail.contains("Fail")) {
				AylaSystemUtils.saveToLog("\n%s", methodMsg);
				testMsg = String.format("%s %s.", methodName, "failed");
				if (!passFail.contains("Sync")) {
					mScreen.displayPassMessage(testMsg + "\n\n"); 
				} else {
					Log.e(tag, testMsg);
				}
			} else {
				// TODO: passFail null or no match.
			}
			AylaSystemUtils.saveToLog("%s", testMsg);
			
		}// end of amlTestStatusMsg     

		

}// end of AMLContactsUnitTest











