/*
 * TestSequencer.java
 * Ayla Mobile Library
 * 
 * Created by Di Wang on 05/04/2015.
 * Copyright (c) 2015 Ayla Networks Inc. All rights reserved.
 * */


package com.aylanetworks.AMLUnitTest.facility;



import java.util.List;


public class TestSequencer {
	
	private final int mInvalidIndex = -1;
	private final int mStartingIndex = 0;
	
	private int nextTestIndex = 0;
	private List<Test> testSuite = null;
	
	public TestSequencer(List<Test> testSuiteTestList) {
		nextTestIndex = mStartingIndex;
		testSuite = testSuiteTestList;
	}
	
	public void reset() {
		if ( !isTestSuiteRunning() ) {
			nextTestIndex = mStartingIndex;
		}
	}// end of reset     
	
	public synchronized Test nextTest() {
		if (nextTestIndex < testSuite.size()) {
			return testSuite.get(nextTestIndex++);
		} else {
			nextTestIndex++;		// This matters for isTestSuiteRunning()
			return new Test() {		// End the call chain, by executing an empty function
				public void execute() {}
			};
		}
	}// end of nextTest
	
	public synchronized int numberOfTests() {
		return testSuite.size();
	}

	public synchronized void endTestSuite() {
		if (testSuite != null) {
			nextTestIndex = testSuite.size();		// Fast forward past the last test.  More nextTest() calls will not do anything.
		} else {
			nextTestIndex = mInvalidIndex;
		}
	}

	public synchronized boolean isTestSuiteRunning() {
		return (nextTestIndex < testSuite.size() ) && (nextTestIndex != mInvalidIndex) && (nextTestIndex != mStartingIndex);
	}
}// end of TestSequencer class         









