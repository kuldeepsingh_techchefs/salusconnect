/*
       Licensed to the Apache Software Foundation (ASF) under one
       or more contributor license agreements.  See the NOTICE file
       distributed with this work for additional information
       regarding copyright ownership.  The ASF licenses this file
       to you under the Apache License, Version 2.0 (the
       "License"); you may not use this file except in compliance
       with the License.  You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing,
       software distributed under the License is distributed on an
       "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
       KIND, either express or implied.  See the License for the
       specific language governing permissions and limitations
       under the License.
 */

package com.salus.connect.solution;

import android.annotation.TargetApi;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.webkit.ConsoleMessage;
import android.webkit.CookieManager;
import android.webkit.WebView;

import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;
import org.apache.cordova.*;
import org.json.JSONException;
import org.json.JSONObject;

import com.aylanetworks.aaml.*;

import java.io.File;
import java.io.IOException;

public class MainActivity extends CordovaActivity
{
    // TODO: Change these variables to be config driven
    // TODO: Set this regular expression string to match your OEM product SSID
    final static String gblAmlDeviceSsidRegex1 = "((^VXS)[0-9A-Fa-f]{12}";

    // TODO: Use these application ID and secret temporarily. Then use the ones issued to your company by Ayla Networks
    static String appId = "ApplicationPrefix-id";

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        AylaNetworks.init(this, gblAmlDeviceSsidRegex1, appId);

        // enable the android webview to except file scheme cookies
        // http://developer.android.com/reference/android/webkit/CookieManager.html#setAcceptFileSchemeCookies(boolean)
        CookieManager.setAcceptFileSchemeCookies(true);

        // TODO: remove this before production
        WebView.setWebContentsDebuggingEnabled(true);

        // TODO: add in code to change service type depending on what mode we are in
        AylaSystemUtils.serviceType = AylaNetworks.AML_DEVELOPMENT_SERVICE;
        AylaCache.clearAll(); // forces aAML to access the service to pick up the new device information
        AylaSystemUtils.saveCurrentSettings();

//        writeLogToFile();
        
        // Set by <content src="index.html" /> in config.xml
        loadUrl(launchUrl);
    }


    private void writeLogToFile() {
        if ( isExternalStorageWritable() ) {

            File appDirectory = new File( Environment.getExternalStorageDirectory() + "/salusConnect" );
            File logDirectory = new File( appDirectory + "/log" );
            File logFile = new File( logDirectory, "logcat" + System.currentTimeMillis() + ".txt" );

            // create app folder
            if ( !appDirectory.exists() ) {
                appDirectory.mkdir();
            }

            // create log folder
            if ( !logDirectory.exists() ) {
                logDirectory.mkdir();
            }

            // clear the previous logcat and then write the new one to the file
            try {
                Process process = Runtime.getRuntime().exec( "logcat -c");
                process = Runtime.getRuntime().exec( "logcat -f " + logFile);
            } catch ( IOException e ) {
                e.printStackTrace();
            }

        } else if ( isExternalStorageReadable() ) {
            // only readable
        } else {
            // not accessible
        }
    }

    /* Checks if external storage is available for read and write */
    private boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if ( Environment.MEDIA_MOUNTED.equals( state ) ) {
            return true;
        }
        return false;
    }

    /* Checks if external storage is available to at least read */
    private boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if ( Environment.MEDIA_MOUNTED.equals( state ) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals( state ) ) {
            return true;
        }
        return false;
    }


}
