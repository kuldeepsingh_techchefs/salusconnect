//
//  ConnectLoggerPlugin.m
//  Connected-Solution
//
//

#import "ConnectLoggerPlugin.h"
#import <Cordova/CDVPlugin.h>

@implementation ConnectLoggerPlugin

- (void)mailComposeController:(MFMailComposeViewController *)controller
          didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    // Check the result or perform other tasks.
    
    // Dismiss the mail compose view controller.
    UIViewController *rootView = [UIApplication sharedApplication].keyWindow.rootViewController;
    [rootView dismissViewControllerAnimated:YES completion:nil];
}

/*
 * Attaches log file and opens email window so user can specify an address to send it to
 * @param - doesn't matter
 * NOTE: Sending emails will NOT work on a simulator, must be tested on an actual device
 */
- (void) sendErrorLogs:(CDVInvokedUrlCommand *)args {
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setSubject:@"iOS Crash Report"];

    //add attachment
    NSString *filePath = [self getFilePathFromDate:[NSDate date]];
    NSData *myData = [NSData dataWithContentsOfFile:filePath];
    [mc addAttachmentData:myData mimeType:@"text/plain" fileName:[self getCurrFileName:false date:[NSDate date]]];
    
    if (![MFMailComposeViewController canSendMail]) {
        NSLog(@"Mail services are not available.");
        return;
    }
    
    // Present mail view controller on screen
    UIViewController *rootView = [UIApplication sharedApplication].keyWindow.rootViewController;
    [rootView presentViewController:mc animated:YES completion:nil];
}

/*
 * @param withDir - should the file name be prepended with /logs/
 * @param date - date to be used to generate the file name
 * @returns NSString* for filename
 */
-(NSString*) getCurrFileName:(BOOL)withDir date:(NSDate*)date {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd";
    NSString* dateString = [formatter stringFromDate:date];
    NSString* file = @"salusConnectLog_";
    if (withDir) {
        return [NSString stringWithFormat:@"/logs/%@%@.txt", file, dateString];
    } else {
        return [NSString stringWithFormat:@"%@%@.txt", file, dateString];
    }

}

/*
 * @param date - date to be used for the file path
 * @returns NSString* for the full file path
 */
- (NSString*) getFilePathFromDate:(NSDate *)date {
    NSString* fileName = [self getCurrFileName:true date:date];
    return [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject] stringByAppendingPathComponent:fileName];
}

// write the given information to the log file
- (void) writeToLog:(CDVInvokedUrlCommand *)args {
    // get the strings to write
    NSString* messageType = [args argumentAtIndex:0];
    NSString* message = [[args argumentAtIndex:1] valueForKey:@"0"]; // get message from NSDictionary
    NSString* stringToWrite = [NSString stringWithFormat:@"Type - %@: \"%@\"\n", messageType, message];
    
    // init fileManager and path w/ file name
    NSFileManager *filemgr = [NSFileManager defaultManager];
    NSString *filePath = [self getFilePathFromDate:[NSDate date]];
    
    NSError *error;
    // Check if file exists
    if ([filemgr fileExistsAtPath:filePath] == YES) {
        //Log file exists
        NSFileHandle *file;
        NSMutableData *data;
        
        const char *bytestring = [stringToWrite UTF8String];
        data = [NSMutableData dataWithBytes:bytestring length:strlen(bytestring)];
        file = [NSFileHandle fileHandleForUpdatingAtPath: filePath];
        
        if (file == nil) {
            NSLog(@"Failed to open log file");
        }
        
        [file seekToEndOfFile];
        [file writeData: data];
        [file closeFile];
    } else {
        //Log file not found, so delete yesterday's and create a new one
        NSDateComponents *components = [[NSDateComponents alloc] init];
        [components setDay:-1];
        NSDate* yesterday = [[NSCalendar currentCalendar] dateByAddingComponents:components toDate:[NSDate date] options:0];
        NSString* oldPath = [self getFilePathFromDate:yesterday];
        if ([filemgr removeItemAtPath:oldPath error: NULL]  == YES) {
            //Removed old log successfully
        } else {
            //Failed to remove old log, it's possible there wasn't one.
        }
        
        /* begin creating new log file */
        //create 'logs' directory if necessary
        NSArray* appPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString* documentsDirectory = [appPath objectAtIndex:0]; // Get documents folder
        NSString* dataPath = [documentsDirectory stringByAppendingPathComponent:@"/logs"];
        if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath]) {
            [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error]; //Create folder
        }
        
        //and write the file
        [stringToWrite writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:&error];
    }
}

@end