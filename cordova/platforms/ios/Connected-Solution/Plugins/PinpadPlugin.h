//
//  PinpadPlugin.h
//  Connected-Solution
//
//  Created by BradTaylor on 9/11/15.
//

#import <Cordova/CDVPlugin.h>
#import "UICKeyChainStore.h"

@interface PinpadPlugin : CDVPlugin

    -(void) havePinSet:(CDVInvokedUrlCommand *) command;
    -(void) setPin:(CDVInvokedUrlCommand *) command;
    -(void) checkPin:(CDVInvokedUrlCommand *) command;
    -(void) removePin:(CDVInvokedUrlCommand *) command;

@end
