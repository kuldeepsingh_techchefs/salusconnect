//
//  AylaContact.m
//  iOS_AylaLibrary
//
//  Created by Yipei Wang on 1/13/15.
//  Copyright (c) 2015 Ayla Networks. All rights reserved.
//

#import "AylaContact.h"
#import "AylaNetworks.h"
#import "AylaApiClient.h"
#import "AylaErrorSupport.h"

@interface AylaContact ()

@property (nonatomic, strong, readwrite) NSString *updatedAt;

@end

@implementation AylaContact

- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    if(!self) return nil;
    
    if(self) {
        [self updateWithDictionary:dictionary];
    }
    
    return self;
}

+ (NSOperation *)createContact:(AylaContact *)contact withParams:(NSDictionary *)callParams
                       success:(void (^)(AylaResponse *resp, AylaContact *createdContact))successBlock
                       failure:(void (^)(AylaError *error))failureBlock
{
    NSMutableDictionary *errors = [NSMutableDictionary new];
    if(!contact) {
        [errors setObject:@"can't be blank" forKey:kAylaContactParamContact];
    }
    
    if(errors.count > 0) {
        AylaError *error = [AylaError createWithCode:AML_USER_INVALID_PARAMETERS httpCode:0 nativeError:nil andErrorInfo:errors];
        failureBlock(error);
        return nil;
    }
    
    NSDictionary *params = @{kAylaContactParamContact: [contact toServiceDictionary]};
    return
    [[AylaApiClient sharedUserServiceInstance] postPath:@"api/v1/users/contacts.json" parameters:params
                                                        success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                            AylaContact *contact = [[AylaContact alloc] initWithDictionary:responseObject];
                                                            AylaResponse *resp = [AylaResponse new]; resp.httpStatusCode = operation.response.statusCode;
                                                            saveToLog(@"%@, %@, %@:%@, %@", @"I", @"Contact", @"contact", @"created", @"createContact");
                                                            successBlock(resp, contact);
                                                        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                            saveToLog(@"%@, %@, %@:%ld, %@:%ld, %@", @"E", @"Contact", @"NSError.code", (long)error.code, @"http", (long)operation.response.statusCode, @"createContact");
                                                            failureBlock([AylaError createWithCode:AML_AYLA_ERROR_FAIL httpCode:operation.response.statusCode nativeError:error andErrorInfo:operation.responseObject]);
                                                        }];
}

+ (NSOperation *)getAllWithParams:(NSDictionary *)callParams
                          success:(void (^)(AylaResponse *resp, NSArray *contacts))successBlock
                          failure:(void (^)(AylaError *error))failureBlock
{
    return
    [[AylaApiClient sharedUserServiceInstance] getPath:@"api/v1/users/contacts.json" parameters:callParams
                                                success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                    NSArray *contactsDict = responseObject;
                                                    NSMutableArray *contacts = [NSMutableArray array];
                                                    for(NSDictionary *contactInfo in contactsDict) {
                                                        AylaContact *contact = [[AylaContact alloc] initWithDictionary:contactInfo];
                                                        [contacts addObject:contact];
                                                    }
                                                    AylaResponse *resp = [AylaResponse new]; resp.httpStatusCode = operation.response.statusCode;
                                                    saveToLog(@"%@, %@, %@:%ld, %@", @"I", @"contact", @"contacts", contacts.count, @"getAllWithParams");
                                                    successBlock(resp, contacts);
                                                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                    saveToLog(@"%@, %@, %@:%ld, %@:%ld, %@", @"E", @"Contact", @"NSError.code", (long)error.code, @"http", (long)operation.response.statusCode, @"getAllWithParams");
                                                    failureBlock([AylaError createWithCode:AML_AYLA_ERROR_FAIL httpCode:operation.response.statusCode nativeError:error andErrorInfo:operation.responseObject]);
                                                }];
}

+ (NSOperation *)getWithId:(NSNumber *)contactId
                          success:(void (^)(AylaResponse *resp, AylaContact *contact))successBlock
                          failure:(void (^)(AylaError *error))failureBlock
{
    NSMutableDictionary *errors = [NSMutableDictionary new];
    if(!contactId) {
        [errors setObject:@"can't be blank" forKey:kAylaContactParamId];
    }
    
    if(errors.count > 0) {
        AylaError *error = [AylaError createWithCode:AML_USER_INVALID_PARAMETERS httpCode:0 nativeError:nil andErrorInfo:errors];
        failureBlock(error);
        return nil;
    }

    return
    [[AylaApiClient sharedUserServiceInstance] getPath:[NSString stringWithFormat:@"api/v1/users/contacts/%@.json",  contactId] parameters:nil
                                               success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                   AylaContact *contact = [[AylaContact alloc] initWithDictionary:responseObject];
                                                   AylaResponse *resp = [AylaResponse new]; resp.httpStatusCode = operation.response.statusCode;
                                                   saveToLog(@"%@, %@, %@:%@, %@", @"I", @"Contact", @"contact", contact.displayName, @"getWithId");
                                                   successBlock(resp, contact);
                                               } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                   saveToLog(@"%@, %@, %@:%ld, %@:%ld, %@", @"E", @"Contact", @"NSError.code", (long)error.code, @"http", (long)operation.response.statusCode, @"getWithId");
                                                   failureBlock([AylaError createWithCode:AML_AYLA_ERROR_FAIL httpCode:operation.response.statusCode nativeError:error andErrorInfo:operation.responseObject]);
                                               }];
}

+ (NSOperation *)updateContact:(AylaContact *)contact withParams:(NSDictionary *)callParams
                   success:(void (^)(AylaResponse *resp, AylaContact *updatedContact))successBlock
                   failure:(void (^)(AylaError *error))failureBlock
{
    NSMutableDictionary *errors = [NSMutableDictionary new];
    if(!contact.id) {
        [errors setObject:@"can't be blank" forKey:kAylaContactParamId];
    }
    
    if(errors.count > 0) {
        AylaError *error = [AylaError createWithCode:AML_USER_INVALID_PARAMETERS httpCode:0 nativeError:nil andErrorInfo:errors];
        failureBlock(error);
        return nil;
    }
    
    return
    [[AylaApiClient sharedUserServiceInstance] putPath:[NSString stringWithFormat:@"api/v1/users/contacts/%@.json", contact.id] parameters:callParams
                                               success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                   [contact updateWithDictionary:(NSDictionary *)responseObject];
                                                   AylaResponse *resp = [AylaResponse new]; resp.httpStatusCode = operation.response.statusCode;
                                                   saveToLog(@"%@, %@, %@:%@, %@", @"I", @"Contact", @"contact", contact.displayName, @"getWithId");
                                                   successBlock(resp, contact);
                                               } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                   saveToLog(@"%@, %@, %@:%ld, %@:%ld, %@", @"E", @"Contact", @"NSError.code", (long)error.code, @"http", (long)operation.response.statusCode, @"getWithId");
                                                   failureBlock([AylaError createWithCode:AML_AYLA_ERROR_FAIL httpCode:operation.response.statusCode nativeError:error andErrorInfo:operation.responseObject]);
                                               }];
}


+ (NSOperation *)deleteContact:(AylaContact *)contact
                       success:(void (^)(AylaResponse *resp))successBlock
                       failure:(void (^)(AylaError *error))failureBlock
{
    NSMutableDictionary *errors = [NSMutableDictionary new];
    if(!contact.id) {
        [errors setObject:@"can't be blank" forKey:kAylaContactParamId];
    }
    
    if(errors.count > 0) {
        AylaError *error = [AylaError createWithCode:AML_USER_INVALID_PARAMETERS httpCode:0 nativeError:nil andErrorInfo:errors];
        failureBlock(error);
        return nil;
    }
    
    return [[AylaApiClient sharedUserServiceInstance] deletePath:[NSString stringWithFormat:@"api/v1/users/contacts/%@.json", contact.id]
                   parameters:nil
                      success:^(AFHTTPRequestOperation *operation, id responseObject) {
                          AylaResponse * response = [AylaResponse new]; response.httpStatusCode = operation.response.statusCode;
                          saveToLog(@"%@, %@, %@:%@, %@", @"I", @"Contact", @"contact", contact.displayName, @"deleteContact");
                          successBlock(response);
                      } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                          saveToLog(@"%@, %@, %@:%ld, %@:%ld, %@", @"E", @"Contact", @"NSError.code", (long)error.code, @"http", (long)operation.response.statusCode, @"deleteContact");
                           failureBlock([AylaError createWithCode:AML_AYLA_ERROR_FAIL httpCode:operation.response.statusCode nativeError:error andErrorInfo:operation.responseObject]);
                      }];
}

- (void)updateWithDictionary:(NSDictionary *)dictionary
{
    if([dictionary objectForKey:kAylaContactParamContact]) {dictionary = dictionary[kAylaContactParamContact]; };
    
    self.id = [dictionary objectForKey:kAylaContactParamId];
    self.firstName = [dictionary objectForKey:kAylaContactParamFirstName];
    self.lastName = [dictionary objectForKey:kAylaContactParamLastName];
    self.displayName = [dictionary objectForKey:kAylaContactParamDisplayName];
    self.email = [dictionary objectForKey:kAylaContactParamEmail];
    self.phoneCountryCode = [dictionary objectForKey:kAylaContactParamPhoneCountryCode];
    self.phoneNumber = [dictionary objectForKey:kAylaContactParamPhoneNumber];
    self.streetAddress = [dictionary objectForKey:kAylaContactParamStreetAddress];
    self.zipCode = [dictionary objectForKey:kAylaContactParamZipCode];
    self.country = [dictionary objectForKey:kAylaContactParamCountry];
    self.updatedAt = [dictionary objectForKey:kAylaContactParamUpdatedAt];
}

- (NSDictionary *)toServiceDictionary {
    NSDictionary *contactInfos =
    @{
      kAylaContactParamFirstName: _firstName?:[NSNull null],
      kAylaContactParamLastName: _lastName?:[NSNull null],
      kAylaContactParamDisplayName: _displayName?:[NSNull null],
      kAylaContactParamEmail: _email?:[NSNull null],
      kAylaContactParamPhoneCountryCode: _phoneCountryCode?:[NSNull null],
      kAylaContactParamPhoneNumber: _phoneNumber?:[NSNull null],
      kAylaContactParamStreetAddress: _streetAddress?:[NSNull null],
      kAylaContactParamZipCode: _zipCode?:[NSNull null],
      kAylaContactParamCountry: _country?:[NSNull null],
      };
    return contactInfos;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"<%@: %p, Id: %@, FirstName: %@, LastName: %@, DisplayName: %@, Email: %@, PhoneCountryCode: %@, PhoneNumber: %@> ", NSStringFromClass([self class]), self, self.id, self.firstName, self.lastName, self.displayName, self.email, self.phoneCountryCode, self.phoneNumber];
}

static NSString * const kAylaContactParamId = @"id";
static NSString * const kAylaContactParamUpdatedAt = @"updated_at";
static NSString * const kAylaContactParamContact = @"contact";
static NSString * const kAylaContactParamContacts = @"contacts";

@end

NSString * const kAylaContactParamFirstName = @"firstname";
NSString * const kAylaContactParamLastName = @"lastname";
NSString * const kAylaContactParamDisplayName = @"display_name";
NSString * const kAylaContactParamEmail = @"email";
NSString * const kAylaContactParamPhoneCountryCode = @"phone_country_code";
NSString * const kAylaContactParamPhoneNumber = @"phone_number";
NSString * const kAylaContactParamStreetAddress = @"street_address";
NSString * const kAylaContactParamZipCode = @"zip_code";
NSString * const kAylaContactParamCountry = @"country";
NSString * const kAylaContactParamFilter = @"filter";
