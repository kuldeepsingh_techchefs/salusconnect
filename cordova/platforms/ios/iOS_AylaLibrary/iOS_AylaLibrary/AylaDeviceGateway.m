//
//  AylaDeviceGateway.m
//  iOS_AylaLibrary
//
//  Created by Yipei Wang on 7/11/14.
//  Copyright (c) 2015 Ayla Networks. All rights reserved.
//

#import "AylaDeviceGateway.h"
#import "AylaNetworks.h"
#import "AylaApiClient.h"
#import "AylaDeviceSupport.h"
#import "AylaErrorSupport.h"
#import "AylaDeviceNode.h"
#import "AylaCacheSupport.h"
#import "AylaRegistration.h"
#import "AylaLanModeSupport.h"
#import "AylaNotify.h"

@implementation AylaDeviceGateway

static NSString * const kAylaDeviceType = @"device_type";
static NSString * const kAylaGatewayType = @"gateway_type";
static NSString * const kAylaGatewayTypeZigbee = @"Zigbee";

static NSString * const kAylaNodeType = @"node_type";
static NSString * const kAylaNodeTypeZigbee = @"Zigbee";

#pragma mark - init
//override
- (instancetype)initDeviceWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    if(self) {
        [self updateWithDictionary:dictionary];
    }
    return self;
}

//override
- (void)updateWithDictionary:(NSDictionary *)deviceDictionary
{
    [super updateWithDictionary:deviceDictionary];
    NSDictionary *attributes = deviceDictionary[@"device"];
    self.gatewayType = [attributes objectForKey:kAylaGatewayType]?:nil;
}

+ (Class)deviceClassFromDeviceDictionary:(NSDictionary *)dictionary
{
    NSDictionary *attributes = dictionary[@"device"];
    Class deviceClass;
    if(attributes &&
       attributes[kAylaGatewayType]) {
        deviceClass = [AylaDeviceGateway deviceClassFromGatewayType:attributes[kAylaGatewayType]];
    }
    else if(attributes &&
            [attributes[kAylaDeviceType] isEqualToString:kAylaDeviceTypeGateway]) {
        deviceClass = [AylaDeviceGateway class];
    }
    return deviceClass?:[AylaDevice class];
}


//------------------------------ Gateway apis ---------------------------------
#pragma mark - gateway apis
- (NSOperation *)getNodes:(NSDictionary *)callParams
         success:(void (^)(AylaResponse *response, NSArray *nodes))successBlock
         failure:(void (^)(AylaError *err))failureBlock
{
    if(!self.key) {
        AylaError *error = [AylaError createWithCode:AML_USER_INVALID_PARAMETERS httpCode:0 nativeError:nil andErrorInfo:@{kAylaDeviceTypeGateway: @"is invalid."}];
        failureBlock(error);
        return nil;
    }
    else if(!self.gatewayType) {
        AylaError *error = [AylaError createWithCode:AML_USER_INVALID_PARAMETERS httpCode:0 nativeError:nil andErrorInfo:@{kAylaGatewayType: @"is invalid."}];
        failureBlock(error);
        return nil;
    }
    
    if(!self.nodes) {
        __unused BOOL retrievedFromCache = [self initNodesFromCache];
    }
    
    NSString *path = [NSString stringWithFormat:@"devices/%@/nodes.json", self.key];
    return [[AylaApiClient sharedDeviceServiceInstance] getPath:path parameters:nil
        success:^(AFHTTPRequestOperation *operation, id responseObject) {
            int i = 0;
            NSMutableDictionary *nodes = [NSMutableDictionary new];
            for(NSDictionary *nodeDictionary in responseObject) {
                Class nodeClass = [AylaDeviceNode deviceClassFromDeviceDictionary:nodeDictionary];
                AylaDeviceNode *node = [[nodeClass alloc] initDeviceWithDictionary:nodeDictionary];
                if([node isKindOfClass:[AylaDeviceNode class]]){
                    node.gateway = self;
                    [nodes setObject:node forKey:node.dsn];
                }
                else {
                    NSDictionary *attributes = nodeDictionary[@"device"];
                    saveToLog(@"%@, %@, %@:%@, %@", @"E", @"AylaGateway", @"invalid node", attributes[@"dsn"], @"getNodes");
                }
                i++;
            }
            saveToLog(@"%@, %@, %@:%d, %@", @"I", @"AylaGateway", @"count", i, @"getNodes");
            [self updateNodesWithNodeList:nodes saveToCache:YES];
            
            AylaResponse *resp = [AylaResponse new];
            resp.httpStatusCode = operation.response.statusCode;
            successBlock(resp, nodes.allValues);
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            saveToLog(@"%@, %@, %@:%ld, %@:%ld, %@", @"E", @"AylaGateway", @"NSError.code", (long)error.code, @"http", (long)operation.response.statusCode, @"getNodes");
            failureBlock([AylaError createWithCode:AML_AYLA_ERROR_FAIL httpCode:operation.response.statusCode nativeError:error andErrorInfo:operation.responseObject]);;
        }];
}

- (void)updateNodesFromGlobalDeviceList:(NSArray *)devices
{
    NSMutableDictionary *nodes = [NSMutableDictionary new];
    [devices enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        AylaDevice *device = obj;
        if([device isKindOfClass:[AylaDeviceNode class]]) {
            AylaDeviceNode *node = obj;
            if([node.gatewayDsn isEqualToString:self.dsn]) {
                node.gateway = self;
                [nodes setObject:node forKey:node.dsn];
            }
        }
    }];
    [self updateNodesWithNodeList:nodes saveToCache:YES];
}

//static final NSObject *nodesLock = [NSObject new];
- (void)updateNodesWithNodeList:(NSMutableDictionary *)nodes saveToCache:(BOOL)saveToCache
{
    if(!self.nodes) { self.nodes = nodes; }
    else {
        @synchronized(self.nodes) {
        //find one to be updated/ removed
            [self.nodes.allValues enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                AylaDeviceNode *node = obj;
                AylaDeviceNode *update = [nodes objectForKey:node.dsn];
                if(update) {
                    [self updateNode:node withNewNode:update];
                }
                else {
                    [self.nodes removeObjectForKey:node.dsn];
                }
            }];
            
            [nodes.allValues enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                AylaDeviceNode *update = obj;
                AylaDeviceNode *node = [self.nodes objectForKey:update.dsn];
                if(!node){
                    update.gateway = self;
                    [self.nodes setObject:update forKey:update.dsn];
                }
            }];
        };
    }
    if([AylaLanMode lanModeState] != DISABLED) {

        AylaDevice *device = [AylaLanMode device];
        if(device
           && [device isKindOfClass:[AylaDeviceGateway class]]
           && [device.dsn isEqualToString:self.dsn]
           && device != self) {
            AylaDeviceGateway *gateway = (AylaDeviceGateway *)device;
            [gateway updateNodesWithNodeList:[nodes mutableCopy] saveToCache:NO];
        }

        if(saveToCache) {
            [AylaCache save:AML_CACHE_NODE withIdentifier:self.dsn andObject:[nodes.allValues mutableCopy]];
        }
    }
}

- (void)updateNode:(AylaDeviceNode *)node withNewNode:(AylaDeviceNode *)update
{
    node.connectionStatus = update.connectionStatus;
    node.swVersion = update.swVersion;
    node.retrievedAt = update.retrievedAt;
}

- (BOOL)initNodesFromCache
{
    // Currently only use stored properties info to set default, would be deprecated later
    NSMutableArray *arr = [AylaCache get:AML_CACHE_NODE withIdentifier:self.dsn];
    
    // the properties have already been cached read it, otherwise go back to original steps
    if(arr){
        @synchronized(self.nodes){
            if(!self.nodes) {
                self.nodes = [NSMutableDictionary new];
            }
            
            for(AylaDeviceNode *node in arr){
                node.gateway = self;
                [self.nodes setValue:node forKey:node.dsn];
            }
        }
        
        saveToLog(@"%@, %@, %@, %@", @"I", @"AylaGateway", @"nodes", @"initNodesFromCache");
        return YES;
    }
    return NO;
}

//Light weight connection status request
- (NSOperation *)getNodesConnectionStatus:(NSDictionary *)callParams
                          success:(void (^)(AylaResponse *response, NSArray *connectionStatus))successBlock
                          failure:(void (^)(AylaError *err))failureBlock
{
    static NSString *kTextGatewayConnectionStatus = @"conn_status";

    if([self isLanModeActive]) {
        //in Lan Mode
        return
        [self getProperties:@{@"names": @[kTextGatewayConnectionStatus]}
            success:^(AylaResponse *response, NSArray *properties) {
                __block NSArray *connectionStatus = [NSArray new];
                if(properties.count > 0){
                    AylaProperty *connStatus = [properties objectAtIndex:0];
                    connectionStatus = [self updateNodesConnStatus:connStatus.datapoint.sValue toBeNotified:NO];
                }
                successBlock(response, connectionStatus);
            } failure:^(AylaError *err) {
                failureBlock(err);
            }];
    }
    else {
        return
        [self getNodes:nil
            success:^(AylaResponse *response, NSArray *nodes) {
                NSMutableArray *connectionStatus = [NSMutableArray new];
                [nodes enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    AylaDeviceNode *node = obj;
                    if(node.dsn && node.connectionStatus) {
                        [connectionStatus addObject:@{
                                                      @"dsn": node.dsn?:@"",
                                                      @"mac": node.mac,
                                                      @"status": node.connectionStatus
                                                      }];
                    }
                    else {
                        saveToLog(@"%@, %@, %@:%@, %@", @"E", @"AylaGateway", @"node", @"invalid node object", @"getNodesConnectionStatus");
                    }
                }];
                successBlock(response, connectionStatus);
            } failure:^(AylaError *err) {
                failureBlock(err);
            }];
    }
}

- (NSArray *)updateNodesConnStatus:(NSString *)connStatusValue toBeNotified:(BOOL)toBeNotified
{
    static NSString *kTextConnectionStatusOnline = @"Online";
    static NSString *kTextConnectionStatusOffline = @"Offline";
    
    NSError *jsonError = nil;
    NSData *data = [connStatusValue dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableArray *connectionStatus = [NSMutableArray new];
    
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
    if(!jsonError) {
        
        NSArray *macs = [json objectForKey:@"mac"];
        NSArray *conns = [json objectForKey:@"value"];
        
        __block AylaDeviceGateway *lanDevice = self;
        [macs enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            
            NSString *mac = obj;
            AylaDeviceNode *node = [lanDevice findNodeWithMacAddress:mac];
            NSString *status = [[conns objectAtIndex:idx] boolValue]? kTextConnectionStatusOnline: kTextConnectionStatusOffline;
            if(node) {
                node.connectionStatus = status;
            }
            else {
                saveToLog(@"%@, %@, %@:%@, %@", @"W", @"AylaGateway", @"mac", @"no node found", @"getNodesConnectionStatus");
            }
            [connectionStatus addObject:@{
                                          @"dsn": node.dsn?:@"",
                                          @"mac": mac,
                                          @"status": status
                                          }];
        }];
    }
    else {
        saveToLog(@"%@, %@, %@:%@, %@", @"E", @"AylaGateway", @"jsonError", connStatusValue, @"getNodesConnectionStatus");
    }
    
    if(toBeNotified &&
       connectionStatus.count > 0) {
        dispatch_sync(dispatch_get_main_queue(), ^{
            [AylaNotify setNotifyOutstandingCounter: [AylaNotify notifyOutstandingCounter]+1];
            NSDictionary *returnNotify = [AylaNotify initNotifyDictionaryWithType:AML_NOTIFY_TYPE_NODE dsn:self.dsn status:200 description:nil key:@"conn_status" values:connectionStatus];
            [AylaNotify returnNotify:returnNotify];
        });
    }
    
    return connectionStatus;
}

//---------------------------- enrollment apis -----------------------
#pragma mark - enrollment apis
- (NSOperation *)openRegistrationJoinWindow:(NSDictionary *)callParams
                                    success:(void (^)(AylaResponse *response))successBlock
                                    failure:(void (^)(AylaError *err))failureBlock
{
    return [AylaRegistration openRegistrationWindow:self params:callParams success:successBlock failure:failureBlock];
}

- (NSOperation *)getCandidates:(NSDictionary *)callParams
                       success:(void (^)(AylaResponse *response, NSArray *candidates))successBlock
                       failure:(void (^)(AylaError *err))failureBlock
{
    if(!self.dsn) {
        AylaError *error = [AylaError createWithCode:AML_USER_INVALID_PARAMETERS httpCode:0 nativeError:nil andErrorInfo:@{kAylaDeviceTypeGateway: @"is invalid."}];
        failureBlock(error);
        return nil;
    }
    
    return
    [AylaRegistration getRegistrationCandidatesWithDsn:self.dsn andRegistrationType:AML_REGISTRATION_TYPE_NODE success:successBlock failure:failureBlock];
}

- (NSOperation *)registerCandidate:(AylaDeviceNode *)candidate
                         success:(void (^)(AylaResponse *response, AylaDevice *regiseredNode))successBlock
                         failure:(void (^)(AylaError *err))failureBlock
{
    return [AylaRegistration registerDevice:candidate.dsn regToken:nil setupToken:nil success:successBlock failure:failureBlock];
}

- (NSOperation *)closeRegistrationJoinWindow:(NSDictionary *)callParams
                                    success:(void (^)(AylaResponse *response))successBlock
                                    failure:(void (^)(AylaError *err))failureBlock
{
    return [AylaRegistration closeRegistrationWindow:self params:callParams success:successBlock failure:failureBlock];
}

//---------------------------- node pass-through apis --------------------------
#pragma mark - node pass-through apis
/**
 * It's better to have all node apis called here. 
 * NOTE All pass-through apis will first try to find the corresponding node/property from called gateway instance. Only when it can't be found, apis will use user input node/property object instead.
 */
- (NSOperation *)getNodePropertiesWithNode:(AylaDeviceNode *)requestNode
                                   params:(NSDictionary *)callParams
                           success:(void (^)(AylaResponse *response, NSArray *properties))successBlock
                           failure:(void (^)(AylaError *err))failureBlock
{
    // If node info was found in gateway object, use that object to send request. Otherwise use request node.
    AylaDeviceNode *node = [self findNodeWithDsn:requestNode.dsn]?:requestNode;
    if(!node) {
        AylaError *error = [AylaError createWithCode:AML_USER_INVALID_PARAMETERS
                                            httpCode:0
                                         nativeError:nil
                                        andErrorInfo:@{@"node": @"is invalid."}];
        failureBlock(error);
        return nil;
    }
    return [node getProperties:callParams success:successBlock failure:failureBlock];
}

- (NSOperation *)createNodeDatapoint:(AylaDatapoint *)datapoint
                              onNode:(AylaDeviceNode *)requestNode
                            property:(AylaProperty *)requestProperty
                             success:(void (^)(AylaResponse *response, AylaDatapoint *datapoint))successBlock
                             failure:(void (^)(AylaError *err))failureBlock
{
    // If node info was found in gateway object, use that object to send request. Otherwise use request node.
    AylaDeviceNode *node = [self findNodeWithDsn:requestNode.dsn]?:requestNode;
    if(!node) {
        AylaError *error = [AylaError createWithCode:AML_USER_INVALID_PARAMETERS
                                            httpCode:0
                                         nativeError:nil
                                        andErrorInfo:@{@"node": @"is invalid."}];
        failureBlock(error);
        return nil;
    }
    AylaProperty *property = [node findProperty:requestProperty.name]?:requestProperty;
    if(!property) {
        AylaError *error = [AylaError createWithCode:AML_USER_INVALID_PARAMETERS
                                            httpCode:0
                                         nativeError:nil
                                        andErrorInfo:@{@"property": @"is invalid."}];
        failureBlock(error);
        return nil;
    }
    return [property createDatapoint:datapoint success:successBlock failure:failureBlock];
}

- (NSOperation *)getDatapointsByActivity:(NSDictionary *)callParams
                              withNode:(AylaDeviceNode *)requestNode
                            property:(AylaProperty *)requestProperty
                             success:(void (^)(AylaResponse *response, NSArray *dataPoints))successBlock
                             failure:(void (^)(AylaError *err))failureBlock
{
    // If node info was found in gateway object, use that object to send request. Otherwise use request node.
    AylaDeviceNode *node = [self findNodeWithDsn:requestNode.dsn]?:requestNode;
    if(!node) {
        AylaError *error = [AylaError createWithCode:AML_USER_INVALID_PARAMETERS
                                            httpCode:0
                                         nativeError:nil
                                        andErrorInfo:@{@"node": @"is invalid."}];
        failureBlock(error);
        return nil;
    }
    AylaProperty *property = [node findProperty:requestProperty.name]?:requestProperty;
    if(!property) {
        AylaError *error = [AylaError createWithCode:AML_USER_INVALID_PARAMETERS
                                            httpCode:0
                                         nativeError:nil
                                        andErrorInfo:@{@"property": @"is invalid."}];
        failureBlock(error);
        return nil;
    }
    return [property getDatapointsByActivity:callParams success:successBlock failure:failureBlock];
}

- (AylaDeviceNode *)findNodeWithDsn:(NSString *)nodeDsn
{
    if(!self.nodes) {
        return nil;
    }
    if([self.nodes objectForKey:nodeDsn]) {
        return [self.nodes objectForKey:nodeDsn];
    }
    return nil;
}

- (AylaDeviceNode *)findNodeWithMacAddress:(NSString *)macAddr
{
    for(AylaDeviceNode *node in self.nodes.allValues) {
        if([macAddr isEqualToString:node.mac]) {
            return node;
        }
    }
    return nil;
}

- (AylaDevice *)lanModeEdptFromDsn:(NSString *)dsn
{
    AylaDevice *device = nil;
    if([dsn isEqualToString:self.dsn]) {
        device = self;
    }
    else {
        device = [self findNodeWithDsn:dsn];
    }
    return device;
}

static NSString * const kAylaDeviceGatewayClassNameZigbee = @"AylaDeviceZigbeeGateway";
+ (Class)deviceClassFromGatewayType:(NSString *)gatewayType
{
    static Class AylaDeviceGatewayClassZigbee;
    static Class AylaDeviceClass;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        AylaDeviceClass = [AylaDevice deviceClassFromClassName:kAylaDeviceClassName];
        AylaDeviceGatewayClassZigbee = [AylaDevice deviceClassFromClassName:kAylaDeviceGatewayClassNameZigbee];
    });

    if(AylaDeviceGatewayClassZigbee &&
       gatewayType &&
       [gatewayType isEqualToString:kAylaGatewayTypeZigbee]) {
        return AylaDeviceGatewayClassZigbee;
    }
    return AylaDeviceClass;
}

//--------------------caching helper methods----------------------

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [super encodeWithCoder:encoder];
    [encoder encodeObject:_gatewayType forKey:@"gatewayType"];
}

- (id)initWithCoder:(NSCoder *)decoder
{
    AylaDeviceGateway *gateway = [super initWithCoder:decoder];
    gateway.gatewayType = [decoder decodeObjectForKey:@"gatewayType"];
    
    return gateway;
}

//--------------------------helpful methods----------------------------
- (id)copyWithZone:(NSZone *)zone
{
    id copy = [super copyWithZone:zone];
    if (copy) {
        AylaDeviceGateway *_copy = copy;
        _copy.gatewayType = [_gatewayType copy];
    }
    return copy;
}

@end

NSString * const kAylaDeviceGatewayRegWindowDuration = @"duration";
