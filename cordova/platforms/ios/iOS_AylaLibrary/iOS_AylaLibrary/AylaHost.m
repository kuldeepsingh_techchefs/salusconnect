//
//  AylaHost.m
//  Ayla Mobile Library
//
//  Created by Yipei Wang on 1/22/13.
//  Copyright (c) 2015 Ayla Networks. All rights reserved.
//

#import "AylaNetworks.h"
#import "AylaHost.h"
#import "AylaSetupSupport.h"
#import "AylaModuleSupport.h"
#import "AylaDeviceSupport.h"
#import "AylaLanModeSupport.h"
#import "AylaSecuritySupport.h"
#import "AylaErrorSupport.h"

@implementation AylaHost

+ (void)connectToNewDeviceContinued:
        /*success:*/ (void (^)(AylaResponse *, AylaModule *))successBlock
        failure: (void (^)(AylaError *))failureBlock
{
    [AylaSetup init];
    
    if(![self isNewDeviceConnected]){
        AylaError *err = [AylaError new];
        err.errorCode = AML_NO_DEVICE_CONNECTED;
        err.httpStatusCode = 0;
        NSDictionary *description = [[NSDictionary alloc] initWithObjectsAndKeys:@"Invalid new device SSID.", @"error", nil];
        err.errorInfo = description;
        err.nativeErrorInfo = nil;
        
        failureBlock(err);
        return;
    }
    
    [AylaSetup setHostNewDeviceSsid:[self returnHostNetworkConnection]];
    [AylaSetup setHostNewDevicePassword:@""];
    [AylaSetup setHostNewDeviceSecurityType:AML_OPEN];
    [AylaSetup setConnectedMode:AML_CONNECTED_TO_HOST];
    //saveToLog(@"%@, %@, %@:%@, %@", @"I", @"AylaSetup", @"newDeviceSsid", curSsid, @"AylaHost.connectToNewDevice");
    
    [[AylaSetup newDevice] getNewDeviceDetail:
         /*success:*/^(AylaResponse *response, AylaModule *newDevice){
             AylaSetup.newDevice.dsn = newDevice.dsn;
             AylaSetup.newDevice.deviceService = newDevice.deviceService;
             AylaSetup.newDevice.lastConnectMtime = newDevice.lastConnectMtime;
             AylaSetup.newDevice.version = newDevice.version;
             AylaSetup.newDevice.apiVersion = newDevice.apiVersion;
             AylaSetup.newDevice.build = newDevice.build;
             AylaSetup.newDevice.mac = newDevice.mac;
             AylaSetup.newDevice.registrationType = newDevice.registrationType;
             AylaSetup.newDevice.features = newDevice.features;
             AylaSetup.newDevice.lanIp = @"192.168.0.1"; // Set default lan ip
             saveToLog(@"%@, %@, %@:%@, %@:%@, %@:%@, %@", @"I", @"AylaSetup", @"dsn", newDevice.dsn, @"mac", newDevice.mac, @"build", newDevice.build, @"AylaHost.connectToNewDevice.getNewDeviceDetail");
             // Set new device time
             NSNumber *newTime = [NSNumber numberWithInt:[[NSDate date] timeIntervalSince1970]];
                       
             void (^blockContinued)() = ^(){
                 [[AylaSetup newDevice] setNewDeviceTime:newTime
                         success:^(AylaResponse *response){
                             saveToLog(@"%@, %@, %@:%d, %@", @"I", @"AylaSetup", @"deviceNewTime", newTime, @"AylaHost.connectToNewDevice.setNewDeviceTime");
                             [AylaSetup setLastMethodCompleted:AML_SETUP_TASK_CONNECT_TO_NEW_DEVICE];
                             
                             // Do retrieve wifi status history
                             [AylaSetup getNewDeviceWiFiStatus:^(AylaResponse *response, AylaWiFiStatus *wifiStatus){} failure:^(AylaError *err){}];
                             successBlock(response, AylaSetup.newDevice);
                         } failure:^(AylaError *err) {
                             saveToLog(@"%@, %@, %@:%d, %@", @"E", @"AylaHost", @"Failed", err.errorCode , @"AylaSetup.newDevice.setNewDeviceTime");
                             failureBlock(err);
                         }
                  ];
             };
             
            if(AylaSetup.newDevice.features && [AylaSetup.newDevice.features containsObject:@"rsa-ke"]) {
                // Key exchange required
                 if([AylaLanMode lanModeState]!= RUNNING)
                     [AylaLanMode enable];
                     
                 if([AylaLanMode device]) {
                     [[AylaLanMode device] lanModeDisable];
                 }
                 
                 [AylaLanMode setDevice: [AylaSetup newDevice]];
                 [AylaLanMode inSetupMode:YES];
                 [[AylaLanMode device] readLanModeConfigFromCache];
                 
                 __block BOOL getResp = NO;
                 __block NSObject *synchronizedObj = [NSObject new];
                 [AylaSetup continueBlock:^(BOOL isEastablished) {
                     [AylaSetup continueBlock:nil];
                     
                     @synchronized(synchronizedObj) {
                         getResp = YES;
                         if(isEastablished) {
                             blockContinued();
                         }
                         else {
                             dispatch_async(dispatch_get_main_queue(), ^{
                                 AylaError *error = [AylaError new]; error.errorCode = AML_ERROR_FAIL;
                                 error.nativeErrorInfo = nil;
                                 error.errorInfo = [[NSDictionary alloc] initWithObjectsAndKeys:@"key exchange failed", @"error", nil];
                                 failureBlock(error);
                             });
                         }
                     }
                }];
                 
                 [AylaSecurity startKeyNegotiation:[AylaSetup continueBlock]];
                 NSUInteger addtions = [AylaSecurity isRSAKeyPairAvailable]? AML_SECURITY_KEY_EXCHANGE_RSA_WITH_KEY_PAIR:AML_SECURITY_KEY_EXCHANGE_RSA_WITHOUT_KEY_PAIR; // two timeout values
                 double delayInSeconds = DEFAULT_SETUP_WIFI_HTTP_TIMEOUT+addtions;
                 dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
                 dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                     @synchronized(synchronizedObj){
                         if(!getResp){
                             dispatch_async(dispatch_get_main_queue(), ^{
                                 AylaError *error = [AylaError new]; error.errorCode = AML_ERROR_FAIL;
                                 error.nativeErrorInfo = nil;
                                 error.errorInfo = [[NSDictionary alloc] initWithObjectsAndKeys:@"key exchange failed", @"error", nil];
                                 failureBlock(error);
                             });
                             [AylaSetup continueBlock:nil];
                         }
                     }
                 });
             }
             else {
                 blockContinued();
             }
         } failure:^(AylaError *err) {
             saveToLog(@"%@, %@, %@:%d, %@", @"E", @"AylaHost", @"Failed", err.errorCode , @"AylaSetup.newDevice.getNewDeviceDetail");
             failureBlock(err);
         }
     ];
}


+ (void)connectToNewDevice:
        /*success:*/(void (^)(AylaResponse *response, AylaModule * newDevice))successBlock
        failure:(void (^)(AylaError *err))failureBlock
{    
    __block NSUInteger failureTries = 3;
    void (^__block _failureBlock)(AylaError *);
    void (^__block __weak __failureBlock)(AylaError *);
    __failureBlock = _failureBlock = ^(AylaError *err) {
        if(err.errorCode == AML_NO_DEVICE_CONNECTED) {
            failureBlock(err);
        }
        else if(--failureTries>0){
            saveToLog(@"%@, %@, %@, %@:%d, %@", @"E", @"AylaSetup", @"Failed", @"retries", failureTries, @"AylaHost.connectToNewDevice");
            [AylaHost connectToNewDeviceContinued:^(AylaResponse *response, AylaModule *newDevice) {
                successBlock(response, newDevice);
            } failure:__failureBlock];
        }
        else {
            failureBlock(err);
        }
    };
    [AylaHost connectToNewDeviceContinued:
            /*success:*/ ^(AylaResponse *response, AylaModule *newDevice) {
                successBlock(response, newDevice);
            } failure: _failureBlock];
    
}

+ (NSString *)returnHostNetworkConnection
{
    NSArray *iface = (__bridge_transfer id)CNCopySupportedInterfaces();
    NSDictionary* info = (__bridge_transfer id) CNCopyCurrentNetworkInfo((__bridge CFStringRef)([iface objectAtIndex:0]));
    if([info valueForKeyPath:@"SSID"] == nil){
        return nil;
    }
    return [NSString stringWithString:[info valueForKeyPath:@"SSID"]];
}


+ (Boolean)matchDeviceSsidRegex:(NSString *)ssid
{
    NSError *error = NULL;
    static NSRegularExpression *regex;
    if(regex==NULL){
        regex = [NSRegularExpression regularExpressionWithPattern:deviceSsidRegex
            options:NSRegularExpressionCaseInsensitive
            error:&error];
    }
    NSUInteger numberOfMatches = [regex numberOfMatchesInString:ssid
                                                        options:0
                                                        range:NSMakeRange(0, [ssid length])];
    return numberOfMatches == 0? NO:YES;
}

+ (Boolean)isNewDeviceConnected
{
    NSString * curSsid = [AylaHost returnHostNetworkConnection];
    if(curSsid==NULL || ![AylaHost matchDeviceSsidRegex:curSsid]){
        return false;
    }
    return true;
}

@end
