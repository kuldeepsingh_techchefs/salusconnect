//
//  AylaLanCommandEntity.m
//  Ayla Mobile Library
//
//  Created by Yipei Wang on 3/20/13.
//  Copyright (c) 2015 Ayla Networks. All rights reserved.
//

#import "AylaLanCommandEntity.h"

@implementation AylaLanCommandEntity

@synthesize jsonString = _jsonString;
@synthesize cmdId = _cmdId;
@synthesize baseType = _baseType;

- (id)initWithParams:(int)cmdId jsonString:(NSString *)jsonString type:(int)baseType
{
    self = [super init];
    if(self!= nil){
        _cmdId = cmdId;
        _jsonString = jsonString;
        _baseType = baseType;
    }
    return self;
}

@end