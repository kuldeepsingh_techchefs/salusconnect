//
//  AylaLanMode.h
//  Ayla Mobile Library
//
//  Created by Yipei Wang on 2/8/13.
//  Copyright (c) 2015 Ayla Networks. All rights reserved.
//

#import "AylaSystemUtils.h"

#define POST_LOCAL_REGISTRATION 570
#define PUT_LOCAL_REGISTRATION 700

@class AylaLanCommandEntity;
@interface AylaLanMode : AylaSystemUtils{
    enum lanModeSession {DOWN, LOCAL_REGISTRATION, KEY_EXCHANGE, UP, UNKNOWN};
}

/**
 * Enables the application (activity) to use LAN Mode secure communication. In addition to enabling the application, the current device must 
   also be LAN Mode enabled
 * @params notifyHandle is app provided notifyHandler
 * @params reachabilityHandle is app provided notifyHandler
 * @result would be SUCCESS or ERROR
 */
+ (int)  enableWithNotifyHandle:
            /* notifyHandle*/ (void(^)(NSDictionary*)) notifyHandle
            ReachabilityHandle: (void(^)(NSDictionary*)) reachabilityHandle;

/**
 * Enables the application (activity) to use LAN Mode secure communication. In addition to enabling the application, the current device must
   also be LAN Mode enabled
 * @return can be AML_ERROR_OK or AML_ERROR_FAIL
 */
+ (int)  enable;

/**
 * This method will stop communication with all LME devices, stop the HTTP server, timers, etc. Buffered information of last connected LME device will be cleared.
 * @return can be AML_ERROR_OK or AML_ERROR_FAIL
 */
+ (int)  disable;

/**
 * Resume LAN mode communication and reboot http server. Typically called after LAN mode communication is paused.
 * @return can be AML_ERROR_OK or AML_ERROR_FAIL
 */
+ (int)  resume;

/**
 * Pause LAN mode communication. This method will also stop current connection to the LME device and stop http server. 
 * @return can be AML_ERROR_OK or AML_ERROR_FAIL
 */
+ (int)  pause;

/**
 * Cancel a read-from-memory flag. Must be used when property change(s) from the module is notified to app and app is not going to take this/these change(s) from library.
 */
+ (void) notifyAcknowledge;

/**
 * This method is used to register new reachability handler.
 */
+ (void) registerReachabilityHandle: (void(^)(NSDictionary *)) handle;

/**
 * This method is used to register new notify handler.
 */
+ (void) registerNotifyHandle: (void(^)(NSDictionary *))handle;

@end