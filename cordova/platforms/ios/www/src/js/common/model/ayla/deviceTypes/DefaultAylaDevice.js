"use strict";

define([
	"app",
	"common/constants",
	"common/AylaConfig",
	"common/model/ayla/deviceTypes/mixin.BaseDevice"
], function (App, constants, AylaConfig, BaseAylaDevice) {

	App.module("Models", function (Models, App, B) {
		/**
		 * this is a wrapper for testing ayla base device. it allows us to construct one and run unit tests and such
		 */
		Models.DefaultAylaDevice = B.Model.extend({
			modelType: constants.modelTypes.GENERIC,
			tileType: "wud",
			defaults: {
				device_category_id: 0,
				device_category_icon_url: App.rootPath("/images/icons/dashboard/icon_wud.svg"),
				device_category_type: App.translate("equipment.myEquipment.categories.types.wirelessUncontrollableDevices"),
				equipment_page_icon_url: App.rootPath("/images/icons/myEquipment/icon_wud_grey.svg")
			}
		}).mixin([BaseAylaDevice]);
	});

	return App.Models.DefaultAylaDevice;
});
