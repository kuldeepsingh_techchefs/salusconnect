"use strict";

define([
	"i18next",
	"underscore"
], function (i18n, _) {

	// based on Ayla SDK validator
	var emailValidationRegex = new RegExp(/^[0-9a-z._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/i),
		zipRegex = new RegExp(/^\d{1,9}$/),
		wordsRegex = new RegExp(/^[a-zA-Z ]+$/),
		mobileNumberRegex = new RegExp(/^\s*$|^(\s)*((\([2-9][0-9]{2}\))|([2-9][0-9]{1}))([-\.\s]?)([0-9]{3})([-\.\s]?)([0-9]{4,50})(\s)*(?:[\-\.\ \\\/]?(?:#|ext\.?|extension|x)[\-\.\ \\\/]?(\d+))?$/),
		validators = {};
	/**
	 * validate the regex the same way the sdk validates it. Note! there are more rigorous email validation
	 * regexps out there, but this is the one used in the native SDKs
	 * @param email
	 * @returns {*|boolean}
	 */
	validators.email = function (email) {
		var isString = email && (typeof email === "string");
		email = email.trim();
		var match = emailValidationRegex.exec(email);
		return isString && !!match && match.length > 0 ? {valid: true} : {
			valid: false,
			message: "common.validation.email.notValid"
		};
	};

	validators.mobileNumber = function (mobileNumber) {
		var isString = mobileNumber && (typeof  mobileNumber === "string");
		mobileNumber = mobileNumber.trim();
		var match = mobileNumberRegex.exec(mobileNumber);
		return isString && !!match && match.length > 0 ? {valid: true} : {
			valid: false,
			message: "common.validation.mobile.notValid"
		};
	};

	/**
	 * validate password matches Ayla rules.
	 * rules:
	 * 6 >= pass.length <= 14
	 * 1 uppercase
	 * 1 lowercase
	 * 1 symbol or number
	 *
	 * @param pass
	 * @returns {*|boolean} isValid
	 */
	validators.password = function (pass) {
		if (pass && pass.length >= 6) {
			if (pass.length > 14) {
				return {
					valid: false,
					message: "common.validation.passwords.tooLong"
				};
			}

			/* look ahead for 1 lower, 1 upper, 1 digit or symbol */
			if (pass.match(/^(?=.*[a-z])(?=.*[A-Z])((?=.*\d)|(?=.*[-+_!@#$%^&*.,?)(=<>`"';:~|/\\])).+$/)) {
				return {valid: true};
			} else {
				return {
					valid: false,
					message: "common.validation.passwords.tooSimple"
				};
			}
		} else {
			return {
				valid: false,
				message: "common.validation.passwords.tooShort"
			};
		}
	};

	validators.ipAddress = function (ipString) {
        var valid = false, message;
		if (_.isString(ipString)) {
            if (ipString === "") {
				return true;
			}
            var re = /^(\d+)\.(\d+)\.(\d+)\.(\d+)$/g; //匹配IP地址的正则表达式
            if(re.test(ipString)){
                if(RegExp.$1 < 256 && RegExp.$2 < 256 && RegExp.$3 < 256 && RegExp.$4 < 256) {
                    valid = true;
                }
            }
            message = valid ? null : "common.validation.ipAddress";
            return {
                valid: valid,
                message: message
            };
		}
	};

	/**
	 * Validates the zip to max of 9 digits
	 * @param zip
	 * @returns {*}
	 */
	validators.zip = function (zip) {
		if (zip && zip.match(zipRegex)) {
			return {
				valid: true
			};
		} else {
			return {
				valid: false,
				message: "common.validation.zip.notValid"
			};
		}
	};

	/**
	 * Allow only words characters is a-zA-Z and spaces. (no digits)
	 * @param words
	 * @returns {*}
	 */
	validators.words = function (words) {
		if (words && words.match(wordsRegex) && words.trim() !== "") {
			return {
				valid: true
			};
		} else {
			return {
				valid: false,
				message: "common.validation.wordsOnly"
			};
		}
	};

	/**
	 *
	 * @param
	 * @returns {*}
	 */
	validators.intRange = function (number, min, max) {
		min = min || 0;
		max = max || 0;
		var num = parseInt(number);

		if (isNaN(num)) {
			num = min - 1;
		}

		if (min <= num && num <= max) {
			return {
				valid: true
			};
		} else {
			return {
				valid: false,
				messageText: i18n.t("common.validation.integerRangeFormatted", {
					min: min,
					max: max
				})
			};
		}
	};

	validators.intX100Range = function (number, min, max) {
		min = min * 100 || 0;
		max = max * 100 || 0;
		var num = number;

		if (isNaN(num)) {
			num = min - 1;
		}

		if (min <= num && num <= max) {
			return {
				valid: true
			};
		} else {

			if (min !== 0) {
				min = min / 100;
			}

			if (max !== 0) {
				max = max / 100;
			}

			return {
				valid: false,
				messageText: i18n.t("common.validation.numberRangeFormatted", {
					min: min.toFixed(2),
					max: max.toFixed(2)
				})
			};
		}
	};

	return validators;

});