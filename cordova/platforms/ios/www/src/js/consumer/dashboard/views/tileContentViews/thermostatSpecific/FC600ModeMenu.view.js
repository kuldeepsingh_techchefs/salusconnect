"use strict";

define([
	"app",
	"common/constants",
	"common/config",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/dashboard/views/tileContentViews/thermostatSpecific/FrostPointModal.view",
	"consumer/views/SalusModal.view",
	"consumer/dashboard/models/ModeItemCollection.model"
], function (App, constants, config, consumerTemplates, SalusView, FrostPointModal, SalusModalView) {

	App.module("Consumer.Dashboard.Views.TileContentViews.ThermostatSpecific", function (Views, App, B, Mn, $, _) {
		Views.ModeMenu = {};

		Views.ModeMenu.MenuRow = Mn.ItemView.extend({
			className: "mode-item-row",
			template: consumerTemplates["dashboard/tile/thermostatModeMenuRow"],
			triggers: {
				"click": "click:row"
			},
			attributes: {
				role: "button"
			},
			onRender: function () {
				this.$el.addClass(this.model.get("modeColorClass"));
			}
		}).mixin([SalusView]);

		Views.ModeMenu.MenuContainer = Mn.CollectionView.extend({
			className: "bb-mode-menu mode-menu color-white",
			template: false,
			childView: Views.ModeMenu.MenuRow,
			childEvents: {
				"click:row": "_handleRowClicked"
			},
			initialize: function (options) {
				_.bindAll(this,
					"showSetFrostPointModal",
					"_buildModeMenuItemCollectionThermostat",
					"_buildFanModeItemCollection",
					"_buildHoldModeMenuItemCollection",
					"_handleRowClicked",
					"_setActiveMode",
					"_setFanMode",
					"_setHoldMode",
					"updateActive",
					"_updateHoldModeMenuCollection"
				);

				if (options.holdMenu) {
					this.listenTo(this.model, "change:HoldType", this._updateHoldModeMenuCollection);
					this.collection = this._buildHoldModeMenuItemCollection();
				} else if (options.fanMenu) {
					this.listenTo(this.model, "change:FanMode", this.updateActive);
					this.collection = this._buildFanModeItemCollection();
				} else {
					// mode menu
					this.listenTo(this.model, "change:SystemMode", this.updateActive);
					this.collection = this._buildModeMenuItemCollectionThermostat();
				}
			},

			onRender: function () {
				var warningModeView = $(_.template(
					"<div class='padding-5 text-center temp-mode-menu-sizing'>" +
					"<p>This device is masquerading as a thermostat!</p>" +
					"<p>It does not have any real modes to set.</p>" +
					"</div>"
				)());

				if (!this.collection) {
					// TODO: this is bad when this happens
					this.$el.append(warningModeView);
				}

				if (this.options.holdMenu) {
					this.updateActive(this.model.get("HoldType"));
				} else if (this.options.fanMenu) {
					this.updateActive(this.model.get("FanMode"));
				} else {
					this.updateActive(this.model.get("SystemMode"));
				}
			},

			_handleRowClicked: function (childView) {
				if (this.options.holdMenu) {
					this._setHoldMode(childView);
				} else if (this.options.fanMenu) {
					this._setFanMode(childView);
				} else {
					this._setActiveMode(childView);
				}
			},

			updateActive: function (modelProp) {
				var val = modelProp ? modelProp.getProperty() : -1, modeId, isActive;

				this.children.each(function (childView) {
					modeId = childView.model.get("modeTypeId");
					isActive = val === modeId;

					childView.$el.toggleClass("active-mode", isActive).toggleClass("select-mode", !isActive);
				});
			},

			showSetFrostPointModal: function () {
				var that = this,
					frostPointModalView = new FrostPointModal({model: this.model}),
					salusModalView = new SalusModalView({
						contentView: frostPointModalView
					});

				this.listenToOnce(frostPointModalView, "modal:closed", function () {
					App.hideModal();

					that.showSpinner({
						textKey: constants.spinnerTextKeys.processing
					}, that.$el);

					that.model.setProperty("SystemMode", constants.thermostatModeTypes.OFF);
				});

				App.modalRegion.show(salusModalView);

				App.showModal();
			},

			_setHoldMode: function (childView) {
				var that = this, modelProp = this.model.get("HoldType"), modeTypeId = childView.model.get("modeTypeId");

				if (modelProp) {
					this.listenToOnce(modelProp, "propertiesSynced", function () {
						that.hideSpinner();
						that.trigger("close:menu");
					});

					this.listenToOnce(modelProp, "propertiesFailedSync", function () {
						that.hideSpinner();
						that.trigger("close:menu");
						App.Consumer.Controller.showDeviceSyncError(that.model);
					});

					this.showSpinner({
						textKey: constants.spinnerTextKeys.processing,
						greyBg: true
					});
					
					modelProp.setProperty(modeTypeId).then(function () {
                        that.temporaryHold = false;
                    });
				}
			},

			_setFanMode: function (childView) {
				var that = this, fanMode = this.model.get("FanMode");

				if (fanMode) {
					this.listenToOnce(fanMode, "propertiesSynced", function () {
						that.hideSpinner();
						that.trigger("close:menu");
					});

					this.listenToOnce(fanMode, "propertiesFailedSync", function () {
						that.hideSpinner();
						that.trigger("close:menu");
						App.Consumer.Controller.showDeviceSyncError(that.model);
					});

					this.showSpinner({
						textKey: constants.spinnerTextKeys.processing,
						greyBg: true
					});

					fanMode.setProperty(childView.model.get("modeTypeId"));
				}
			},

			_setActiveMode: function (childView) {
				var that = this, systemMode = this.model.get("SystemMode");

				if (systemMode) {
					this.listenToOnce(systemMode, "propertiesSynced", function () {
						that.hideSpinner();
						that.trigger("close:menu");
					});

					this.listenToOnce(systemMode, "propertiesFailedSync", function () {
						that.hideSpinner();
						that.trigger("close:menu");
						App.Consumer.Controller.showDeviceSyncError(that.model);
					});

					// Only show the set frost point model if the user hasn't permanently dismissed it and the mode
					// button that was clicked is the "Off" button
					if (!this.model.get("hide_set_frost_point_modal") && childView.model.get("modeTypeId") === constants.thermostatModeTypes.OFF) {
						this.showSetFrostPointModal();
					} else {
						this.showSpinner({
							textKey: constants.spinnerTextKeys.processing,
							greyBg: true
						});

						systemMode.setProperty(childView.model.get("modeTypeId"));
					}
				}
			},

			_buildModeMenuItemCollectionThermostat: function () {
				return new App.Consumer.Dashboard.Models.ModeMenuItemCollection([
					{
						modeName: App.translate("equipment.thermostat.menus.modeLabels.heating"),
						modeIconClass: "op-heating-icon-on",
						modeColorClass: "heating-color",
						modeTypeId: constants.thermostatModeTypes.HEAT
					},
					{
						modeName: App.translate("equipment.thermostat.menus.modeLabels.cooling"),
						modeIconClass: "op-cooling-icon-on",
						modeColorClass: "cooling-color",
						modeTypeId: constants.thermostatModeTypes.COOL
					}
				]);
			},

			_buildHoldModeMenuItemCollection: function () {
				var holdType = this.model.get("HoldType"), followOnMenu = {
					modeName: "",
					modeIconClass: "hold-auto-icon",
					modeColorClass: "hold-auto-color",
					modeTypeId: constants.fc600HoldTypes.FOLLOW
				};

				if (holdType && holdType.getProperty() === constants.fc600HoldTypes.OFF) {
					followOnMenu.modeName = App.translate("equipment.property.values.on"); // reusing a key
				} else {
					followOnMenu.modeName = App.translate("equipment.thermostat.menus.modeLabels.followSchedule");
				}

				return new App.Consumer.Dashboard.Models.ModeMenuItemCollection([
					followOnMenu,
//					{
//						modeName: App.translate("equipment.thermostat.menus.modeLabels.temporaryHold"),
//						modeIconClass: "hold-temporary-icon",
//						modeColorClass: "hold-temp-color",
//						modeTypeId: constants.fc600HoldTypes.TEMPHOLD
//					},
					{
						modeName: App.translate("equipment.thermostat.menus.modeLabels.permanentHold"),
						modeIconClass: "hold-permanent-icon",
						modeColorClass: "hold-perm-color",
						modeTypeId: constants.fc600HoldTypes.PERMHOLD
					},
					{
						modeName: App.translate("equipment.thermostat.menus.modeLabels.eco"),
						modeIconClass: "hold-eco-icon",
						modeColorClass: "hold-eco-color",
						modeTypeId: constants.fc600HoldTypes.ECO
					},
					{
						modeName: App.translate("equipment.thermostat.menus.modeLabels.off"),
						modeIconClass: "power-icon",
						modeColorClass: "power-color",
						modeTypeId: constants.fc600HoldTypes.OFF
					}
				]);
			},

			_updateHoldModeMenuCollection: function () {
				var holdType = this.model.get("HoldType"),
					optionToChange = this.collection.findWhere({modeTypeId: constants.fc600HoldTypes.FOLLOW});

				this.collection.remove(optionToChange);

				if (holdType && holdType.getProperty() === constants.fc600HoldTypes.OFF) {
					optionToChange.set("modeName", App.translate("equipment.property.values.on"));
				} else {
					optionToChange.set("modeName", App.translate("equipment.thermostat.menus.modeLabels.followSchedule"));
				}

				// put it back at top
				this.collection.unshift(optionToChange);

				this.updateActive(holdType);
			},

			_buildFanModeItemCollection: function () {
				return new App.Consumer.Dashboard.Models.ModeMenuItemCollection([
					{
						modeName: App.translate("equipment.thermostat.menus.modeLabels.auto"),
						modeIconClass: "fan-auto",
						modeColorClass: "auto-color",
						modeTypeId: constants.fanModeTypes.AUTO
					},
					{
						modeName: App.translate("equipment.thermostat.menus.modeLabels.fanSpeed3"),
						modeIconClass: "fan-speed3",
						modeColorClass: "fan-on-color",
						modeTypeId: constants.fanModeTypes.SPEED3
					},
					{
						modeName: App.translate("equipment.thermostat.menus.modeLabels.fanSpeed2"),
						modeIconClass: "fan-speed2",
						modeColorClass: "fan-on-color",
						modeTypeId: constants.fanModeTypes.SPEED2
					},
					{
						modeName: App.translate("equipment.thermostat.menus.modeLabels.fanSpeed1"),
						modeIconClass: "fan-speed1",
						modeColorClass: "fan-on-color",
						modeTypeId: constants.fanModeTypes.SPEED1
					},
					{
						modeName: App.translate("equipment.thermostat.menus.modeLabels.fanOff"),
						modeIconClass: "fan-off",
						modeColorClass: "power-color",
						modeTypeId: constants.fanModeTypes.OFF
					}
				]);
			}
		}).mixin([SalusView]);
	});

	return App.Consumer.Dashboard.Views.TileContentViews.ThermostatSpecific.ModeMenu;
});
