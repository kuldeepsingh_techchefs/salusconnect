"use strict";

define([
	"app",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/SalusDropdown.view",
	"consumer/models/SalusDropdownViewModel"
], function (App, consumerTemplates, SalusView) {

	App.module("Consumer.Dashboard.Views.TileContentViews.ThermostatSpecific", function (Views, App, B, Mn, $, _) {
		Views.FrostPointModal = Mn.ItemView.extend({
			template: consumerTemplates["dashboard/tile/thermostatFrostPointModal"],
			className: "bb-frost-point-modal frost-point-modal",
			ui: {
				degreesDropdown: ".bb-frost-degrees-dropdown",
				buttonTurnOffHVAC: ".bb-turn-off-hvac-btn",
				checkboxHideSetFrostPointModal: ".bb-hide-set-frost-point-modal-checkbox",
				radioOptionYesSetFrostPoint: "#optionYesSetFrostPoint",
				radioOptionNoSetFrostPoint: "#optionNoSetFrostPoint"
			},
			events: {
				'click @ui.buttonTurnOffHVAC': "_handleClickTurnOffHVACButton",
				'click @ui.radioOptionYesSetFrostPoint': "_handleRadioYesClick",
				'click @ui.radioOptionNoSetFrostPoint': "_handleRadioNoClick"
			},
			initialize: function () {
				_.bindAll(this, "_handleClickTurnOffHVACButton", "_getTemperatureMode");
			},
			onRender: function () {
				var degreesCollection = new App.Consumer.Models.DropdownItemViewCollection();

				for (var i = 5; i <= 17; i += 0.5) {
					degreesCollection.add(new App.Consumer.Models.DropdownItemViewModel({
						value: i,
						displayText: this._getTemperatureMode(i)
					}));
				}

				this.dropdownView = new App.Consumer.Views.SalusDropdownViewNoi18N({
					collection: degreesCollection,
					viewModel: new B.Model({
						id: "degreesDropdown",
						dropdownLabelKey: App.translate("equipment.thermostat.frostPointModal.labels.chooseFrostPoint")
					})
				});

				this.ui.degreesDropdown.append(this.dropdownView.render().$el);
			},

			onDestroy: function () {
				this.dropdownView.destroy();
			},

			_getTemperatureMode: function (i) {
				var temperatureMode = App.translate("equipment.thermostat.frostPointModal.tempModeC", {number: i}),
						temperatureDisplayMode = this.model.get("TemperatureDisplayMode");

				if (temperatureDisplayMode && temperatureDisplayMode.getProperty() === 1) {
					temperatureMode = App.translate("equipment.thermostat.frostPointModal.tempModeF", {number: i});
				}

				return temperatureMode;
			},

			_handleRadioYesClick: function () {
				this.ui.degreesDropdown.show();
			},
			_handleRadioNoClick: function () {
				this.ui.degreesDropdown.hide();
			},
			_handleClickTurnOffHVACButton: function () {
				var that = this,
					checkboxValue = this.ui.checkboxHideSetFrostPointModal.is(":checked"),
					modelCheckboxValue = this.model.get("hide_set_frost_point_modal"),
					shouldSaveModel = false;

				if (!modelCheckboxValue && checkboxValue) {
					shouldSaveModel = true;
					this.model.set("hide_set_frost_point_modal", checkboxValue);
				}

				// TODO Add same checks as above for saving the device's SetFrostPoint
				// TODO when we can actually set it

				// If one of our properties was updated, trigger a model save
				if (shouldSaveModel) {
					this.model.persist().then(function () {
						that.trigger("modal:closed", that);
					});
				} else {
					this.trigger("modal:closed", this);
				}
			}
		}).mixin([SalusView]);
	});

	return App.Consumer.Dashboard.Views.TileContentViews.ThermostatSpecific.FrostPointModal;
});