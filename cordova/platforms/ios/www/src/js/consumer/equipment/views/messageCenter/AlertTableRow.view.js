"use strict";

define([
	"momentWrapper",
	"app",
	"consumer/consumerTemplates",
	"consumer/equipment/views/messageCenter/AlertRowDetailSection.view",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/mixins/mixin.registeredRegions",
	"consumer/equipment/views/messageCenter/AlertSounding.view"
], function (moment, App, templates, AlertRowDetailSection, SalusViewMixin, RegisteredRegions) {

	App.module("Consumer.Equipment.Views", function (Views, App, B, Mn) {
		Views.AlertTableRowView = Mn.LayoutView.extend({
			template: templates["equipment/messageCenter/alertTableRow"],
			className: "row alert-table-row",

			events: {
				"click": "expandOrCollapseDetailView"
			},

			bindings: {
				".bb-alert-name": "name",
				".bb-alert-dateTime": {
					observe: "dateTime",
					onGet: function (value) {
						return moment(value).format("MMM hh:mma");
					}
				},
				".bb-alert-dev-name": {
					observe: "deviceIdRef",
					onGet: function (value) {
						var device = App.salusConnector.getDevice(value);

						return !device ? null : device.get("product_name");
					}
				},
				".bb-alert-dev-alias": {
					observe: "deviceIdRef",
					onGet: function (value) {
						var device = App.salusConnector.getDevice(value);

						return !device ? null : device.get("alias");
					}
				},
				".bb-alarm-sounding": {
					observe: 'alarmSounding',
					visible: function (val) {
						return val;
					}
				}
			},

			modelEvents: {
				"change:deviceIdRef": "_deviceChanged"
			},

			regions: {
				"detailRegion": ".bb-expanded-detail-region",
				"alertSoundingRegion": ".bb-alarm-sounding"
			},

			initialize: function () {
				this.detailView = new Views.AlertRowDetailView({ model: this.model });

				this.registerRegion("detailRegion", this.detailView);
				this.registerRegion("alertSoundingRegion", new Views.AlertSoundingView());

				this._deviceChanged();
			},

			expandOrCollapseDetailView: function () {
				var shouldCollapse = !this.detailView.isCollapsed();

				this.$el.toggleClass("expanded-bg", !shouldCollapse);
				this.detailView.collapseOrExpand(shouldCollapse);
			},

			_deviceChanged: function () {
				var that = this;

				if (this.currentDevice) {
					this.currentDevice.off();
				}

				this.currentDevice = App.salusConnector.getDevice(this.model.get("deviceIdRef"));

				if (this.currentDevice) {
					this.listenTo(this.currentDevice, "change:product_name", function () {
						that.render();
					});
				}
			}
		}).mixin([SalusViewMixin, RegisteredRegions]);
	});

	return App.Consumer.Equipment.Views.AlertTableRowView;
});