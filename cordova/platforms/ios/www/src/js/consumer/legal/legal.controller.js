"use strict";

define([
    "app",
    "consumer/consumer.controller",
    "consumer/legal/views/PrivacyPolicy.view",
    "consumer/legal/views/TermsAndConditions.view"
], function(App, ConsumerController) {
    App.module("Consumer.Legal", function(Legal) {
        Legal.Controller = {
            privacyPolicy: function() {
                var page = new App.Consumer.Legal.Views.PrivacyPolicy();
                if(App.salusConnector.isLoggedIn() || App.salusConnector.isLoggingIn()) {
                    ConsumerController.showLayout(page, true);
                } else {
                    ConsumerController.showLayout(page, false);
                }
            },
            
            termsAndConditions: function() {
                var page = new App.Consumer.Legal.Views.TermsAndConditions();
                if(App.salusConnector.isLoggedIn() || App.salusConnector.isLoggingIn()) {
                    ConsumerController.showLayout(page, true);
                } else {
                    ConsumerController.showLayout(page, false);
                }
            }
        };
    });
    return App.Consumer.Legal.Controller;
});