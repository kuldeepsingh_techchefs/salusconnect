"use strict";

define([
	"app",
	"application/Router",
	"consumer/myStatus/myStatus.controller",
	"common/model/salusWebServices/rules/RuleMakerManager"
], function (App, AppRouter, Controller) {

	App.module("Consumer.MyStatus", function (MyStatus, App) {
		MyStatus.Router = AppRouter.extend({
			// dont change the order of these routes, it can introduce bus
			appRoutes: {
				"myStatus(/)(:dsn)": "myStatus",
				"myStatus/(:dsn)/newStatus": "newStatus",
				"myStatus/(:dsn)/makeStatus": "makeStatus"
			}
		});

		App.addInitializer(function () {
			return new MyStatus.Router({
				controller: Controller
			});
		});
	});
});