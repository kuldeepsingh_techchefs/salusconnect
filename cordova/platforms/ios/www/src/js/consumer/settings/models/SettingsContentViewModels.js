"use strict";

define([
    "app",
    "bluebird",
    "i18next",
    "common/constants",
    "common/config",
    "common/util/utilities",
	"common/util/StringsFunctions",
	"common/util/CoreValidator"
], function (App, P, i18n, constants, config, utilities) {

	App.module("Consumer.Settings.Models", function (Models, App, B, Mn, $, _) {
		Models.EquipmentSettingsPanelModel = B.Model.extend({
			defaults: {
				deviceModel: null,
				modelCollection: null
			}
		});

		Models.SettingsContentRowViewModel = B.Model.extend({
			defaults: {
				type: "readonly",
				value: null,
				collection: null,
				propertyModel: null
			},
			initialize: function (/*data, options*/) {
				this.revert();
				this.listenTo(this.propertyModel, "change:value", this.revert);
			},
			isDirty: function () {
				return  this.get("value") !== this.get("propertyModel").get("value");
			},
			revert: function () {
				this.set("value", this.get("propertyModel").get("value"));
			},
			save: function () {
				if (this.isDirty()) {
					return this.get("propertyModel").setProperty(this.get("value"));
				}
			}
		});

		Models.SettingsContentRowViewCollection = B.Collection.extend({
			model: Models.SettingsContentRowViewModel,
			isDirty: function () {
				return this.some(function (model) {
					return model.isDirty();
				});
			},
			revert: function () {
				this.each(function (model) {
					model.revert();
				});
			},
            
            buildDeviceSettingForFC600: function(deviceModel) {
                var deviceSettingProperty = deviceModel.get("DeviceSetting"),
                    deviceSettingValue = deviceSettingProperty.get("value");
            
                if(!deviceSettingValue) {
                    deviceSettingValue = _.reduce(Models.SettingsContentFactory.propsFromDeviceSetting, function(memo, property) {
                        return memo + property.defaultValue;
                    }, "");
                }
                var newDeviceSettingValue = deviceSettingValue;
                this.each(function(model) {
                    var displayName = model.get("propertyModel").get("display_name"),
                        value = model.get("value"),
                        isDeviceSettingProperty = model.get("propertyModel").get("isDeviceSettingProperty");
                    if(isDeviceSettingProperty) {
                        var propConf = _.find(Models.SettingsContentFactory.propsFromDeviceSetting, function(property) {
                            return property.propName === displayName;
                        });
                        if(value) {
                            newDeviceSettingValue = deviceSettingValue.substring(0, propConf.propStart) + value + deviceSettingValue.substring(propConf.propStart + propConf.propLen);
                        }
                    }
                });
                return newDeviceSettingValue;
            },

			/**
			 * Saves all settings that have changed.
			 * @param devices optional list of devices to save properties on.
			 * @returns {*}
			 */
			save: function (devices, deviceModel) {
                var that = this, promises = [], dirtyModels = [];
                var networkSettings = [], settingLanModeToDynamic;
                
                var isFC600 = deviceModel.modelType === constants.modelTypes.FC600,
                    newDeviceSettingValue;
                if(isFC600) {
                    newDeviceSettingValue = this.buildDeviceSettingForFC600(deviceModel);
                }
                
                this.each(function(model) {
                    var isLanModeProp = model.get("isNetworkLanModeProperty");
                        settingLanModeToDynamic = settingLanModeToDynamic || (!!isLanModeProp && model.get("value") === 1);
                    if(model.isDirty()) {
                        dirtyModels.push(model);
                    }
                    if (model.get("isNetworkSetting")) {
                        networkSettings.push(model);
                    }
                });
                
                if(devices) {
                    var valid = _.every(dirtyModels, function(model) {
                        return !model.get("isNetworkSetting") && !model.get("isNetworkLanModeProperty");
                    });
                    
                    if(valid) {
                        _.each(devices, function (device) {
                            var promise = null;
                            _.each(dirtyModels, function(model) {
                                var pName = model.get("propertyModel").get("display_name");
                                var deviceProperty = null, 
                                deviceProperty = device.get(pName);
                                if (deviceProperty) {
                                    if(model.get("isWifiProperty")) {
                                        var newValue = utilities.encryptWithAES(model.get("value"), config.encryptParams.key , config.encryptParams.iv);
                                        promise = deviceProperty.setProperty(newValue);
                                    } else {
                                        promise = deviceProperty.setProperty(model.get("value"));
                                    }
                                    promises.push(promise);
                                }
                            });
                            
                            if(isFC600 && device.get("DeviceSetting") && device.get("DeviceSetting").get("value") !== newDeviceSettingValue) {
                                promise = device.get("DeviceSetting").setProperty(newDeviceSettingValue);
                                promises.push(promise);
                            }
                        });
                    }
                } else {
                    var promise = null;
                    _.each(dirtyModels, function(model) {
                        var pName = model.get("propertyModel").get("display_name");
                        var deviceProperty = null, 
                        deviceProperty = deviceModel.get(pName);
                        if (deviceProperty) {
                            promise = deviceProperty.setProperty(model.get("value"));
                            promises.push(promise);
                        }
                    });
                    if(isFC600 && deviceModel.get("DeviceSetting") && deviceModel.get("DeviceSetting").get("value") !== newDeviceSettingValue) {
                        promise = deviceModel.get("DeviceSetting").setProperty(newDeviceSettingValue);
                        promises.push(promise);
                    }
				}
                
                if(dirtyModels.length === 0){
                    return P.all(promises).then(function () {
                       that.trigger("noSaved:networkSettings");
                    });
                } else {
                    return P.all(promises).then(function () {
                        that.trigger("saved:networkSettings", settingLanModeToDynamic ? null : networkSettings, dirtyModels, promises);
                    });
                }
            }
        });

        var createRangeArray = function (multiple, min, max, increment) {
            var list = [],
                i = min;

            increment = (increment || 1);

            for (i; i <= max; i += increment) {
                list.push({
                    value: i * multiple,
                    text: App.translate("equipment.property.values.numberInCFormatted", {number: i.toString()})
                });
            }
            return list;
        };

        Models.SettingsContentFactory = {
            lockKeyArray: [
                {
                    key: "equipment.property.values.locked",
                    value: 1
                },
                {
                    key: "equipment.property.values.unlocked",
                    value: 0
                }
            ],
            temperatureDisplayModeArray: [
                {
                    key: "equipment.property.values.fahrenheit",
                    value: 1
                },
                {
                    key: "equipment.property.values.celsius",
                    value: 0
                }
            ],
            enabledDisabledArray: [
                {
                    key: "equipment.property.values.enabled",
                    value: 1
                },
                {
                    key: "equipment.property.values.disabled",
                    value: 0
                }
            ],
            yesNoArray: [
                {
                    key: "equipment.property.values.yes",
                    value: 1
                },
                {
                    key: "equipment.property.values.no",
                    value: 0
                }
            ],
            onOffArray: [
                {
                    key: "equipment.property.values.on",
                    value: 1
                },
                {
                    key: "equipment.property.values.off",
                    value: 0
                }
            ],
            systemModeArray: [
                {
                    key: "equipment.property.values.off",
                    value: 0
                },
                {
                    key: "equipment.property.values.auto",
                    value: 1
                },
                {
                    key: "equipment.property.values.cool",
                    value: 3
                },
                {
                    key: "equipment.property.values.heat",
                    value: 4
                },
                {
                    key: "equipment.property.values.emergencyHeating",
                    value: 5
                }
            ],
            FanCoilTypeArray: [
                {
                    key: "equipment.fancoil.property.values.system2",
                    value: 0
                },
                {
                    key: "equipment.fancoil.property.values.system4",
                    value: 1
                }
            ],
            S1ComTerminalsArray: [
                {
                    key: "equipment.fancoil.property.values.manual",
                    value: 0
                },
                {
                    key: "equipment.fancoil.property.values.switch",
                    value: 1
                },
                {
                    key: "equipment.fancoil.property.values.auto",
                    value: 2
                }
            ],
            S2ComTerminalsArray: [
                {
                    key: "equipment.fancoil.property.values.none",
                    value: 0
                },
                {
                    key: "equipment.fancoil.property.values.ocupancy",
                    value: 1
                },
                {
                    key: "equipment.fancoil.property.values.tempSensor",
                    value: 2
                }
            ],
            HeatCoolSelectionArray: [
                {
                    key: "equipment.fancoil.property.values.heatingCooling",
                    value: 0
                },
                {
                    key: "equipment.fancoil.property.values.heatingOnly",
                    value: 1
                },
                {
                    key: "equipment.fancoil.property.values.coolingOnly",
                    value: 2
                }
            ],
            deviceSetting: {
                keyLockFuncArray: [
                    {
                        key: "equipment.fancoil.property.values.manual",
                        value: "0"
                    },{
                        key: "equipment.fancoil.property.values.auto",
                        value: "1"
                    },{
                        key: "equipment.fancoil.property.values.unlocked",
                        value: "2"
                    }
                ],
                tpiOrSpanArray: [
                    {
                        key: "equipment.fancoil.property.values.tpi",
                        value: "0"
                    },{
                        key: "equipment.fancoil.property.values.spanControl",
                        value: "1"
                    }
                ],
                controlTypeArray: [
                    {
                        key: "equipment.fancoil.property.values.slowType",
                        value: "0"
                    },{
                        key: "equipment.fancoil.property.values.fastType",
                        value: "1"
                    }
                ],
                normalPositionInputS1Array: [
                    {
                        key: "equipment.fancoil.property.values.normalOpen",
                        value: "0"
                    },{
                        key: "equipment.fancoil.property.values.normalClose",
                        value: "1"
                    }
                ],
                normalPositionInputS2Array: [
                    {
                        key: "equipment.fancoil.property.values.normalOpen",
                        value: "0"
                    },{
                        key: "equipment.fancoil.property.values.normalClose",
                        value: "1"
                    }
                ],
                standbyOrECOArray: [
                    {
                        key: "equipment.fancoil.property.values.ecoMode",
                        value: "0"
                    },{
                        key: "equipment.fancoil.property.values.standbyMode",
                        value: "1"
                    }
                ],
                statusAfterPowerBreakdownArray: [
                    {
                        key: "equipment.fancoil.property.values.standby",
                        value: "0"
                    },{
                        key: "equipment.fancoil.property.values.lastConf",
                        value: "1"
                    }
                ]
            },
            
            propsFromDeviceSetting: [
                {
                    "propName": "keyLockFunc",
                    "propStart": 0,
                    "propLen": 1,
                    "type": "dropdown",
                    "defaultValue": "0"
                },
                {
                    "propName": "tpiOrSpan",
                    "propStart": 1,
                    "propLen": 1,
                    "type": "dropdown",
                    "defaultValue": "0"
                },
                {
                    "propName": "controlType",
                    "propStart": 2,
                    "propLen": 1,
                    "type": "dropdown",
                    "defaultValue": "1"
                },
                {
                    "propName": "spanForHeatingMode",
                    "propStart": 3,
                    "propLen": 2,
                    "type": "input",
                    "multiple": 10, 
                    "min": 0.1,
                    "max": 2.0,
                    "symbol": "kelvin",
                    "defaultValue": "03"
                },
                {
                    "propName": "spanForCoolingMode",
                    "propStart": 5,
                    "propLen": 2,
                    "type": "input",
                    "multiple": 10, 
                    "min": 0.1,
                    "max": 2.0,
                    "symbol": "kelvin",
                    "defaultValue": "05"
                },
                {
                    "propName": "timeForHeatingMode",
                    "propStart": 7,
                    "propLen": 3,
                    "type": "input",
                    "multiple": 1, 
                    "min": 10,
                    "max": 300,
                    "symbol": "seconds",
                    "defaultValue": "010"
                },
                {
                    "propName": "timeForCoolingMode",
                    "propStart": 10,
                    "propLen": 3,
                    "type": "input",
                    "multiple": 1, 
                    "min": 10,
                    "max": 300,
                    "symbol": "seconds",
                    "defaultValue": "010"
                },
                {
                    "propName": "tpiHeatControl",
                    "propStart": 13,
                    "propLen": 2,
                    "type": "dropdown",
                    "multiple": 1,
                    "min": 3,
                    "max": 12,
                    "increment": 1,
                    "symbol": "onOff",
                    "defaultValue": "06"
                },
                {
                    "propName": "tpiCoolControl",
                    "propStart": 15,
                    "propLen": 2,
                    "type": "dropdown",
                    "multiple": 1,
                    "min": 3,
                    "max": 12,
                    "increment": 1,
                    "symbol": "onOff",
                    "defaultValue": "03"
                },
                {
                    "propName": "deadZoneComfortMode",
                    "propStart": 17,
                    "propLen": 1,
                    "type": "dropdown",
                    "multiple": 1, 
                    "min": 1,
                    "max": 5,
                    "symbol": "kelvin",
                    "defaultValue": "2"
                },
                {
                    "propName": "swiPointCooling",
                    "propStart": 18,
                    "propLen": 4,
                    "type": "input",
                    "multiple": 100, 
                    "min": 10,
                    "max": 25,
                    "symbol": "celsius",
                    "defaultValue": "1600"
                },
                {
                    "propName": "swiPointHeating",
                    "propStart": 22,
                    "propLen": 4,
                    "type": "input",
                    "multiple": 100, 
                    "min": 27,
                    "max": 40,
                    "symbol": "celsius",
                    "defaultValue": "2800"
                },
                {
                    "propName": "normalPositionInputS1",
                    "propStart": 26,
                    "propLen": 1,
                    "type": "dropdown",
                    "defaultValue": "0"
                },
                {
                    "propName": "normalPositionInputS2",
                    "propStart": 27,
                    "propLen": 1,
                    "type": "dropdown",
                    "defaultValue": "0"
                },
                {
                    "propName": "serviceFilter",
                    "propStart": 28,
                    "propLen": 4,
                    "type": "input",
                    "multiple": 1000, 
                    "min": 0,
                    "max": 9.9,
                    "symbol": "hours",
                    "defaultValue": "0000"
                },
                {
                    "propName": "protectionHeatingSetpoint",
                    "propStart": 32,
                    "propLen": 4,
                    "type": "dropdown",
                    "multiple": 100,
                    "min": 5,
                    "max": 40,
                    "increment": 0.5,
                    "symbol": "celsius",
                    "defaultValue": "0800"
                },
                {
                    "propName": "protectionCoolingSetpoint",
                    "propStart": 36,
                    "propLen": 4,
                    "type": "dropdown",
                    "multiple": 100,
                    "min": 5,
                    "max": 40,
                    "increment": 0.5,
                    "symbol": "celsius",
                    "defaultValue": "0000"
                },
                {
                    "propName": "tempDisplayResolution",
                    "propStart": 40,
                    "propLen": 1,
                    "type": "dropdown",
                    "defaultValue": "1"
                },
                {
                    "propName": "coolingStartDelay",
                    "propStart": 41,
                    "propLen": 2,
                    "type": "dropdown",
                    "multiple": 1,
                    "min": 0,
                    "max": 15,
                    "increment": 1,
                    "symbol": "minutes",
                    "defaultValue": "00"
                },
                {
                    "propName": "standbyOrECO",
                    "propStart": 43,
                    "propLen": 1,
                    "type": "dropdown",
                    "defaultValue": "1"
                },
                {
                    "propName": "offsetOfSecondSensor",
                    "propStart": 44,
                    "propLen": 2,
                    "type": "dropdown",
                    "min": -3,
                    "max": 3,
                    "increment": 0.5,
                    "symbol": "celsius",
                    "defaultValue": "06"
                },
                {
                    "propName": "statusAfterPowerBreakdown",
                    "propStart": 46,
                    "propLen": 1,
                    "type": "dropdown",
                    "defaultValue": "1"
                }
            ],
            
            _getPropFromDeviceOrDevices: function (deviceObj, propName) {
                if (_.isArray(deviceObj)) {
                    return _.reduce(deviceObj, function (acc, device) {
                        if (acc) {
                            return acc;
                        } else if (device.get(propName)) {
                            return device.get(propName);
                        } else {
                            return false;
                        }
                    }, false);
                } else {
                    return deviceObj.get(propName);
                }
            },
            getPropFromDeviceOrDevices: function(deviceObj, propName){
                return this._getPropFromDeviceOrDevices(deviceObj, propName);
            },
            
            createViewCollectionUnValidate: function (deviceModel) {
                //List defined at https://salusinc.atlassian.net/wiki/display/SCS/7+-+Thermostat+Control
                var that = this,
                    propertyNames = [
                        "LockKey",
                        "ShutOffDisplay",
                        "FrostSetpoint_x100",
                        "TimeFormat24Hour"
                    ];

                var properties = _.compact(_.map(propertyNames, function (propertyName) {
                    var prop = that._getPropFromDeviceOrDevices(deviceModel, propertyName);

                    if (!prop) {
                        return prop;
                    }

                    return that._createViewModel(deviceModel, prop);

                }));

                return new Models.SettingsContentRowViewCollection(properties);
            },

            createViewCollection: function (deviceModel) {
                

                //List defined at https://salusinc.atlassian.net/wiki/display/SCS/7+-+Thermostat+Control
                var that = this,
                    propertyNames = [
                        "LockKey",
                        "ShutOffDisplay",
                        "KeyLock",
                        "FrostSetpoint_x100",
                        "TimeFormat24Hour",
                        "TemperatureDisplayMode",
                        "HeatingControl",
                        "TemperatureOffset",
                        "OUTSensorProbe",
                        "OUTSensorType",
                        "CylinderConnection",
                        "CoolingControl",
                        "ActuatorType",
                        "ValveProtection",
                        "MaxHeatSetpoint_x100",
                        "MinCoolSetpoint_x100",
                        "FloorCoolingMin_x100",
                        "FloorHeatingMax_x100",
                        "FloorHeatingMin_x100",
                        "LEDMode",
                        "NetworkLANMode",
//                        "NetworkSSID",
//                        "NetworkPassword",
                        "NetworkLANIP",
                        "NetworkLANSubnet",
                        "NetworkLANRouterAddr",
                        "NetworkPriDNS",
                        "NetworkSecDNS",
                        "AllowUnlockFromDevice"
                    ],
                    
                    propertyNameForFC = [
                        "FanCoilType",
                        "S1ComTerminals",
                        "S2ComTerminals",
                        "HeatCoolSelection",
                        "TempCalibration_x10",
                        "MinHeatSetpoint_x100",
                        "MaxCoolSetpoint_x100",
                        "UnocCoolingSetpoint_x100",
                        "UnocHeatingSetpoint_x100",
                        "DeviceSetting"
                    ];
                    
                if(!_.isArray(deviceModel) && deviceModel.get("oem_model") === constants.modelTypes.FC600) {
                    propertyNameForFC.splice(-1, 0, "MaxHeatSetpoint_x100", "MinCoolSetpoint_x100", "TimeFormat24Hour");
                    propertyNames = propertyNameForFC;
                }
                
                var properties = [];
                _.each(propertyNames, function(propertyName) {
                    var prop = that._getPropFromDeviceOrDevices(deviceModel, propertyName);

                    if (!prop) {
                        return prop;
                    }
                    
                    var propertyModel = that._createViewModel(deviceModel, prop);
                    if(_.isArray(propertyModel)) {
                        _.each(propertyModel, function(model) {
                            properties.push(model);
                        });
                    } else {
                        properties.push(propertyModel);
                    }
                });
                
                var fancoilTypeValue, s1ComTerminalsValue, s2ComTerminalsValue, tpiOrSpanProperty, tpiOrSpanValue,
                    arr =[];
                if(!_.isArray(deviceModel)) {
                    fancoilTypeValue = deviceModel.get("FanCoilType") ? deviceModel.get("FanCoilType").get("value") : null;
                    s1ComTerminalsValue = deviceModel.get("S1ComTerminals") ? deviceModel.get("S1ComTerminals").get("value") : null;
                    s2ComTerminalsValue = deviceModel.get("S2ComTerminals") ? deviceModel.get("S2ComTerminals").get("value") : null;
                    tpiOrSpanProperty = _.find(properties, function(property) {
                        return property.get("propertyModel").get("display_name") === "tpiOrSpan";
                    });
                    tpiOrSpanValue = tpiOrSpanProperty ? tpiOrSpanProperty.get("value") : null;
                }
                
                var propertiesOfDefaultSort = [
                    "FanCoilType",
                    "S1ComTerminals",
                    "S2ComTerminals",
                    "HeatCoolSelection",
                    "TempCalibration_x10",
                    "MaxHeatSetpoint_x100",
                    "MinHeatSetpoint_x100",
                    "MaxCoolSetpoint_x100",
                    "MinCoolSetpoint_x100",
                    "UnocCoolingSetpoint_x100",
                    "UnocHeatingSetpoint_x100",
                    "keyLockFunc",
                    "tpiOrSpan",
                    "controlType",
                    "spanForHeatingMode",
                    "spanForCoolingMode",
                    "timeForHeatingMode",
                    "timeForCoolingMode",
                    "tpiHeatControl",
                    "tpiCoolControl",
                    "deadZoneComfortMode",
                    "swiPointCooling",
                    "swiPointHeating",
                    "normalPositionInputS1",
                    "normalPositionInputS2",
                    "serviceFilter",
                    "protectionHeatingSetpoint",
                    "protectionCoolingSetpoint",
                    "tempDisplayResolution",
                    "coolingStartDelay",
                    "TimeFormat24Hour",
                    "standbyOrECO",
                    "offsetOfSecondSensor",
                    "statusAfterPowerBreakdown"
                ];
                var cloneProperties = [];
                if(!_.isArray(deviceModel) && deviceModel.get("oem_model") === constants.modelTypes.FC600) {
                    _.each(propertiesOfDefaultSort, function(propName) {
                        _.each(properties, function(property) {
                            if(propName === property.get("propertyModel").get("display_name")) {
                                cloneProperties.push(property);
                            }
                        });
                    });
                } else {
                    cloneProperties = properties;
                }
                
                
                _.each(cloneProperties, function(property) {
                    if(property.get("propertyModel").get("display_name") === "HeatCoolSelection") {
                        if(fancoilTypeValue === 0 && s1ComTerminalsValue === 0) {
                            arr.push(property);
                        }
                    } else if(property.get("propertyModel").get("display_name") === "controlType") {
                        if(tpiOrSpanValue === "0") {
                            arr.push(property);
                        }
                    } else if(property.get("propertyModel").get("display_name") === "spanForHeatingMode" ||
                            property.get("propertyModel").get("display_name") === "spanForCoolingMode") {
                        if(tpiOrSpanValue === "1") {
                            arr.push(property);
                        }
                    } else if(property.get("propertyModel").get("display_name") === "deadZoneComfortMode") {
                        if(fancoilTypeValue === 1) {
                            arr.push(property);
                        }
                    } else if(property.get("propertyModel").get("display_name") === "normalPositionInputS1" ||
                            property.get("propertyModel").get("display_name") === "normalPositionInputS2") {
                        if(s1ComTerminalsValue === 1) {
                            arr.push(property);
                        }
                    } else if(property.get("propertyModel").get("display_name") === "offsetOfSecondSensor") {
                        if(s2ComTerminalsValue === 2) {
                            arr.push(property);
                        }
                    } else {
                        arr.push(property);
                    }
                });

                return new Models.SettingsContentRowViewCollection(arr);
            },

            /**
             * Definitions at https://salusinc.atlassian.net/wiki/display/SCS/7+-+Thermostat+Control
             * @param prop
             * @returns {*}
             * @private
             */
            _createViewModel: function (deviceModel, prop) {
                var that = this;
                if (!prop) {
                    return null;
                }

                var propertyName = prop.get("display_name");
                if (!propertyName || propertyName.endsWith("_d") || propertyName.endsWith("_c")) {
                    return null;
                }

                var options = {type: "readonly"};

                if (propertyName === "CoolingSetpoint_x100") {
                    var list = createRangeArray(100, 5, 45, 0.5);
                    options = {
                        type: "dropdown",
                        collection: list.slice().reverse()
                    };
                } else if (propertyName === "HeatingControl") {
                    options = {
                        type: "dropdown",
                        collection: [
                            {
                                text: App.translate("equipment.property.values.shortCyle"),
                                //text: "PWM",
                                value: 0
                            },
                            {
                                text: App.translate("equipment.property.values.midCyle"),
                                value: 1
                            },
                            {
                                text: App.translate("equipment.property.values.longCycle"),
                                value: 2
                            }
                        ]
                    };
                } else if (propertyName === "TemperatureOffset") {
                    options = {
                        type: "dropdown",
                        collection: createRangeArray(100, -3, 3, 0.5)
                    };
                } else if (propertyName === "CylinderConnection") {
                    options = {
                        type: "dropdown",
                        collection: [
                            {
                                key: "equipment.property.values.cylinderNotConnected",
                                value: 0
                            },
                            {
                                key: "equipment.property.values.cylinderConnected",
                                value: 1
                            }
                        ]
                    };
                } else if (propertyName === "OUTSensorProbe") {
                    options = {
                        type: "dropdown",
                        collection: [
                            {
                                key: "equipment.property.values.sensorNotConnected",
                                value: 0
                            },
                            {
                                key: "equipment.property.values.sensorConnected",
                                value: 1
                            }
                        ]
                    };
                } else if (propertyName === "OUTSensorType") {
                    options = {
                        type: "dropdown",
                        collection: [
                            {
                                key: "equipment.property.values.sensorAir",
                                value: 0
                            },
                            {
                                key: "equipment.property.values.sensorFloor",
                                value: 1
                            }
                        ]
                    };
                } else if (propertyName === "CoolingControl") {
                    options = {
                        type: "dropdown",
                        collection: [
                            {
                                text: App.translate("equipment.property.values.midCyle"),
                                value: 1
                            },
                            {
                                text: App.translate("equipment.property.values.longCycle"),
                                value: 2
                            }
                        ]
                    };
                } else if (propertyName === "ActuatorType") {
                    options = {
                        type: "readonly",
                        collection: [
                            {
                                key: "equipment.property.values.actuatorOpen",
                                value: 0
                            },
                            {
                                key: "equipment.property.values.actuatorClosed",
                                value: 1
                            }
                        ]
                    };
                } else if (propertyName === "ValveProtection") {
                    options = {
                        type: "dropdown",
                        collection: Models.SettingsContentFactory.enabledDisabledArray
                    };
                } else if (propertyName === "FrostSetpoint_x100") {
                    options = {
                        type: "dropdown",
                        collection: createRangeArray(100, 5, 17, 0.5)
                    };
                } else if (propertyName === "FloorCoolingMax_x100") {
                    options = {
                        type: "numberX100",
                        validationMethod: function () {
                            //"This" will be the EquipmentSettingsPanelContentRow object.
                            //Range 6 - 45°C

                            var value = this.model.get("value");
                            return App.validate.intX100Range(value, 6, 45);
                        }
                    };
                } else if (propertyName === "FloorCoolingMin_x100") {
                    options = {
                        type: "numberX100",
                        validationMethod: function () {
                            //"This" will be the EquipmentSettingsPanelContentRow object.
                            //Range 6 - 45°C

                            var value = this.model.get("value");
                            return App.validate.intX100Range(value, 6, 45);
                        }
                    };
                } else if (propertyName === "FloorHeatingMax_x100") {
                    options = {
                        type: "numberX100",
                        validationMethod: function () {
                            //"This" will be the EquipmentSettingsPanelContentRow object.
                            //Range 11 - 45°C

                            var value = this.model.get("value");
                            return App.validate.intX100Range(value, 11, 45);
                        }
                    };
                } else if (propertyName === "FloorHeatingMin_x100") {
                    options = {
                        type: "numberX100",
                        validationMethod: function () {
                            //"This" will be the EquipmentSettingsPanelContentRow object.
                            //Range 6 - 40°C

                            var value = this.model.get("value");
                            return App.validate.intX100Range(value, 6, 40);
                        }
                    };
                } else if (propertyName === "HeatingSetpoint_x100") {
                    options = {
                        type: "dropdown",
                        collection: createRangeArray(100, 5, 45, 0.5)
                    };
                } else if (propertyName === "HoldDuration") {
                    options = {
                        type: "text",
                        validationMethod: function () {
                            //"This" will be the EquipmentSettingsPanelContentRow object.
                            //0 - 1440, 65535 (0xffff) means Permanent Hold

                            var value = this.model.get("value");

                            if (value === constants.thermostatHoldDuration) {
                                return {
                                    valid: true
                                };
                            }

                            return App.validate.intRange(value, 0, 1440);
                        }
                    };
                } else if (propertyName === "LockKey" || propertyName === "KeyLock") {
                    options = {
                        type: "dropdown",
                        collection: Models.SettingsContentFactory.lockKeyArray
                    };
                } else if ((propertyName === "MaxHeatSetpoint_x100" || 
                        propertyName === "MinCoolSetpoint_x100") && deviceModel.get("oem_model") !== constants.modelTypes.FC600) {
                    options = {
                        type: "numberX100",
                        step: 0.50,
                        validationMethod: function () {
                            //"This" will be the EquipmentSettingsPanelContentRow object.
                            //Range 5 - 35°C
                            
                            var value = this.model.get("value");
                            return App.validate.intX100Range(value, config.thermostatMinimumTemp, config.thermostatMaximumTemp);
                        }
                    };
                } else if((propertyName === "MaxHeatSetpoint_x100" ||
                        propertyName === "MinCoolSetpoint_x100" ||
                        propertyName === "MinHeatSetpoint_x100" ||
                        propertyName === "MaxCoolSetpoint_x100" ||
                        propertyName === "UnocCoolingSetpoint_x100" ||
                        propertyName === "UnocHeatingSetpoint_x100") && deviceModel.get("oem_model") === constants.modelTypes.FC600) {
                    options = {
                        type: "number_FC",
                        bindingOnSet: function (value) {
                            if (value) {
                                return Math.floor(parseFloat(value) * 100);
                            }
                            return null;
                        },
                        bindingOnGet: function (value) {
                            if (value) {
                                return (parseFloat(value) / 100).toFixed(1);
                            }
                            return null;
                        },
                        validationMethod: function () {
                            var value = this.model.get("value");
                            var property = {
                                min: -5,
                                max: 40,
                                multiple: 100
                            };
                            return that.validateMethod(value, property, "temp", 0.5);
                        }
                    };
                } else if (propertyName === "OnOff") {
                    options = {
                        type: "dropdown",
                        collection: Models.SettingsContentFactory.onOffArray
                    };
                } else if (propertyName === "ShutOffDisplay") {
                    options = {
                        type: "dropdown",
                        collection: [
                            {
                                key: "equipment.property.values.turnOn",
                                value: 0
                            },
                            {
                                key: "equipment.property.values.turnOff",
                                value: 1
                            }
                        ]
                    };
                } else if (propertyName === "SystemMode") {
                    options = {
                        type: "dropdown",
                        collection: Models.SettingsContentFactory.systemModeArray
                    };
                } else if (propertyName === "TemperatureDisplayMode") {
                    options = {
                        type: "dropdown",
                        collection: Models.SettingsContentFactory.temperatureDisplayModeArray
                    };
                } else if (propertyName === "TimeFormat24Hour") {
                    options = {
                        type: "dropdown",
                        collection: [
                            {
                                key: "equipment.property.values.hour12",
                                value: 0
                            },
                            {
                                key: "equipment.property.values.hour24",
                                value: 1
                            }
                        ]
                    };
                } else if (propertyName === "NetworkLANMode") {
                    options = {
                        type: "dropdown",
                        collection: [
                            {
                                key: "equipment.property.values.static",
                                value: 0
                            },
                            {
                                key: "equipment.property.values.dynamic",
                                value: 1
                            }
                        ],
                        isNetworkLanModeProperty: true
                    };
                } else if (propertyName === "NetworkLANIP" || propertyName === "NetworkLANSubnet" ||
                        propertyName === "NetworkLANRouterAddr" || propertyName === "NetworkPriDNS" || propertyName === "NetworkSecDNS") {
                    options = {
                        type: "text",
                        isNetworkSetting: true,
                        validationMethod: function () {
                            return App.validate.ipAddress(this.model.get("value"));
                        }
                    };
                }  else if (propertyName === "LEDMode") {
                    options = {
                        type: "dropdown",
                        collection: [
                            {
                                key: "equipment.property.values.alwaysOn",
                                value: 1
                            },
                            {
                                key: "equipment.property.values.offWhenIdle",
                                value: 2
                            }
                        ]
                    };
                }else if (propertyName === "AllowUnlockFromDevice") {
                    options = {
                        type: "dropdown",
                        collection: Models.SettingsContentFactory.yesNoArray
                    };
                }
//                else if(propertyName === "NetworkPassword") {
//                    options = {
//                        type: "password",
//                        isWifiProperty: true,
//                        validationMethod: function() {
////                            return App.validate.password(this.model.get("value"));
//                            return {
//                                valid: true
//                            };
//                        }
//                    };
//                } else if(propertyName === "NetworkSSID") {
//                    options = {
//                        type: "text_",
//                        isWifiProperty: true,
//                        validationMethod: function() {
//                            return {
//                                valid: true
//                            };
//                        }
//                    };
//                }

                else if(propertyName === "FanCoilType" || propertyName === "S1ComTerminals" 
                        || propertyName === "S2ComTerminals" || propertyName === "HeatCoolSelection") {
                    options = {
                        type: "text_FC",
                        readOnly: "readonly",
                        required: false,
                        bindingOnGet: function(value) {
                            if(value !== null && value !== "") {
                                var collection = Models.SettingsContentFactory[propertyName + "Array"];
                                return App.translate(collection[value].key);
                            }
                            return null;
                        }
                    };
                } else if(propertyName === "TempCalibration_x10") {
                    options = {
                        type: "dropdown",
                        collection: createRangeArray(10, -3, 3, 0.5)
                    };
                } else if(propertyName === "DeviceSetting") {
                    return this.createPropsWithDS(prop, options);
                }
                
                return new Models.SettingsContentRowViewModel(_.extend(options, {
                    propertyModel: prop
                }));
            },
            
            createPropsWithDS: function(prop, options) {
                var propValue = prop.get("value");
                var propertyArray = [];
                var that = this;
                _.each(Models.SettingsContentFactory.propsFromDeviceSetting, function(property) {
                    var value;
                    if(propValue) {
                        value = propValue.substr(property.propStart, property.propLen);
                    } else {
                        value = null;
                    }
                    var propModel = new B.Model({
                        display_name: property.propName,
                        value: value,
                        isDeviceSettingProperty: true
                    });
                    
                    if(property.type === "dropdown") {
                        var collection = Models.SettingsContentFactory.deviceSetting[property.propName + "Array"] ?
                            Models.SettingsContentFactory.deviceSetting[property.propName + "Array"] : that.createRangeArrayDS(property);
                        options = {
                            type: "dropdown",
                            collection: collection
                        };
                    } else if(property.type === "input") {
                        options = {
                            type: "number_FC",
                            symbol: property.symbol,
                            bindingOnSet: function(value) {
                                this.model.set("oldValue", value);
                                if(value) {
                                    if(parseFloat(value) > property.max || parseFloat(value) < property.min) {
                                        return parseFloat(value) * property.multiple;
                                    }
                                    return ("0000" + parseInt(value * property.multiple).toString()).substr(-property.propLen, property.propLen);
                                }
                                return null;
                            },
                            bindingOnGet: function(value) {
                                if(value) {
                                    this.model.set("oldValue", parseFloat(value) / property.multiple);
                                    if(property.propName === "swiPointCooling" || property.propName === "swiPointHeating") {
                                        return (parseFloat(value) / property.multiple).toFixed(1);
                                    } else {
                                        return parseFloat(value) / property.multiple;
                                    }
                                }
                                return null;
                            },
                            validationMethod: function() {
                                var step = null, type, value = this.model.get("value");
                                if(property.propName === "swiPointCooling" || property.propName === "swiPointHeating") {
                                    step = 0.5;
                                    type = "temp";
                                } else if(property.propName === "spanForHeatingMode" || property.propName === "spanForCoolingMode") {
                                    type = "span";
                                    value = this.model.get("oldValue");
                                } else if(property.propName === "timeForHeatingMode" || property.propName === "timeForCoolingMode") {
                                    type = "time";
                                    value = this.model.get("oldValue");
                                } else if(property.propName === "serviceFilter") {
                                    type = "serviceFilter";
                                    value = this.model.get("oldValue");
                                }
                                return that.validateMethod(value, property, type, step);
                            }
                        };
                    }

                    propertyArray.push(new Models.SettingsContentRowViewModel(_.extend(options, {
                        propertyModel: propModel
                    })));
                });
                return propertyArray;
            },
            
            validateMethod: function(value, property, type, step) {
                var min = property.min * property.multiple || 0,
                    max = property.max * property.multiple || 0,
                    num = parseFloat(value),
                    messageKey = "common.validation.numberRangeFormatted";
                
                if(isNaN(num)) {
                    num = min - 1;
                }
                if(type === "temp") {
                    messageKey = "equipment.fancoil.property.temperatureError";
                    if (min <= num && num <= max && step && num/property.multiple%step === 0) {
                        return {
                            valid: true
                        };
                    } else {                
                        return {
                            valid: false,
                            messageText: i18n.t(messageKey, {
                                min: property.min.toFixed(0),
                                max: property.max.toFixed(0)
                            })
                        };
                    }
                } else if(type === "span") {
                    messageKey = "equipment.fancoil.property.spanModeError";
                    if (property.min <= num && num <= property.max && new RegExp(/^([0-2]{1})(\.\d{1})?$/).test(num)) {
                        return {
                            valid: true
                        };
                    } else {                
                        return {
                            valid: false,
                            messageText: i18n.t(messageKey, {
                                min: property.min.toFixed(1),
                                max: property.max.toFixed(1)
                            })
                        };
                    }
                } else if(type === "time") {
                    messageKey = "common.validation.numberRangeFormatted";
                    if (property.min <= num && num <= property.max && new RegExp(/^\d{2,3}$/).test(num)) {
                        return {
                            valid: true
                        };
                    } else {
                        return {
                            valid: false,
                            messageText: i18n.t(messageKey, {
                                min: property.min.toFixed(0),
                                max: property.max.toFixed(0)
                            })
                        };
                    }
                } else if(type === "serviceFilter") {
                    messageKey = "common.validation.numberRangeFormatted";
                    if (property.min <= num && num <= property.max && new RegExp(/^(\d{1})(\.\d{1})?$/).test(num)) {
                        return {
                            valid: true
                        };
                    } else {
                        return {
                            valid: false,
                            messageText: i18n.t(messageKey, {
                                min: property.min.toFixed(1),
                                max: property.max.toFixed(1)
                            })
                        };
                    }
                } else {
                    if (min <= num && num <= max) {
                        return {
                            valid: true
                        };
                    } else {                
                        return {
                            valid: false,
                            messageText: i18n.t(messageKey, {
                                min: property.min.toFixed(2),
                                max: property.max.toFixed(2)
                            })
                        };
                    }
                }
            },
            
            createRangeArrayDS: function (property) {
                var list = [],
                    multiple = property.multiple,
                    i = property.min,
                    max = property.max,
                    increment = (property.increment || 1),
                    symbol = property.symbol,
                    len = property.propLen;

                var symbolText;
                
                if(property.propName === "tempDisplayResolution") {
                    list.push({
                        text: App.translate("equipment.property.values.numberInCFormatted", {number: "0.1"}),
                        value: "0"
                    },{
                        text: App.translate("equipment.property.values.numberInCFormatted", {number: "0.5"}),
                        value: "1"
                    });
                    return list;
                }
                
                if(symbol) {
                    symbolText = App.translate("equipment.fancoil.property.symbols." + symbol);
                }
                if(property.propName === "protectionHeatingSetpoint" || property.propName === "protectionCoolingSetpoint") {
                    list.push({
                        value: "0000",
                        text: "OFF"
                    });
                }
                if(property.propName === "offsetOfSecondSensor") {
                    var j = 0;
                    for (i; i <= max; i += increment) {
                        var value = ("0000" + j.toString()).substr(-len, len);
                        list.push({
                            value: value,
                            text: i.toString() + " " + symbolText
                        });
                        j++;
                    }
                    return list;
                }
                for (i; i <= max; i += increment) {
                    var value = ("0000" + (i * multiple).toString()).substr(-len, len);
                    list.push({
                        value: value,
                        text: i.toString() + " " + symbolText
                    });
                }
                return list;
            }
        };
    });

    return App.Consumer.Settings.Models;
});