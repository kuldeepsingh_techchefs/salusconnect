"use strict";

define([
	"app",
    "common/constants",
	"consumer/consumerTemplates",
    "consumer/views/SalusModal.view",
    "consumer/views/mixins/mixin.salusView",
	"consumer/models/SalusRadioViewModel",
	"consumer/views/SalusRadio.view",
    "consumer/models/SalusCheckboxViewModel",
	"consumer/views/SalusCheckbox.view",
	"consumer/views/SalusButtonPrimary.view",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/mixins/mixin.registeredRegions"
], function (App, constants, consumerTemplates, SalusModal, SalusView) {

	App.module("Consumer.SetupWizard.Fancoil.Views", function (Views, App, B, Mn, $, _) {

		Views.FancoilCopySetupLayout = Mn.LayoutView.extend({
			template: consumerTemplates["setupWizard/fancoil/copySetup"],
			className: "container",
			regions: {
                "fc600CollectionRegion": ".bb-fc600-collection-region",
				"nextButton": ".bb-next-button"
                
			},
           
			initialize: function () {
                var that = this;
				_.bindAll(this, "handleNextButtonClicked");
                
               
                this.fc600CollectionView = new Views.FC600CollectionView({
					model: this.model
				});

				this.nextButtonView = this.registerRegion("nextButton", new App.Consumer.Views.SalusButtonPrimaryView({
					id: "save-data-collection-btn",
					buttonTextKey: "common.labels.finished",
					classes: "center-relative padding-l-30 padding-r-30",
					clickedDelegate: this.handleNextButtonClicked
				}));
                
                this.listenTo(this.fc600CollectionView, "click:checkbox", function () {
					// check started updated/edit
					if (that.fc600CollectionView.anyChecked()) {
						that.nextButtonView.enable();
					} else {
						that.nextButtonView.disable();
					}
				});
			},
            
            onRender: function(){
            	this.nextButtonView.disable();
                this.fc600CollectionRegion.show(this.fc600CollectionView);
            },
			handleNextButtonClicked: function () {
                var that = this, checked = this.fc600CollectionView.getCheckedItems();
               // App.hideModal();
				this.nextButtonView.showSpinner();
                
                //copy setup to other FC600
                
                var setPropertyPromise = [];
                var fancoilType = that.model.getPropertyValue("FanCoilType");
                var s1ComTerminals = that.model.getPropertyValue("S1ComTerminals");
                var heatCoolSelection = that.model.getPropertyValue("HeatCoolSelection");
                var systeMode = that.model.getPropertyValue("SystemMode");
                var s2ComTerminals = that.model.getPropertyValue("S2ComTerminals");
                var timeFormat24Hour = that.model.getPropertyValue("TimeFormat24Hour");
                var deepCopy=that.model.getSchedules().deepCopy();
                
                _.each(checked,function(device){
                    setPropertyPromise.push(device.setProperty("FanCoilType", fancoilType));
                    setPropertyPromise.push(device.setProperty("S1ComTerminals", s1ComTerminals));
                    setPropertyPromise.push(device.setProperty("HeatCoolSelection", heatCoolSelection));
                    if(systeMode !== null){
                        setPropertyPromise.push(device.setProperty("SystemMode", systeMode));
                    }
                    
                    setPropertyPromise.push(device.setProperty("S2ComTerminals", s2ComTerminals));
                    setPropertyPromise.push(device.setProperty("TimeFormat24Hour", timeFormat24Hour));
                    setPropertyPromise.push(device.getSchedules().setFromCopy(deepCopy));
                }),
				
                
				P.all(setPropertyPromise).then(function(){
					App.hideModal();
                    that.model.trigger("click:save", that.model);
					
				});
                
//				
                
                
			}
            
            
		}).mixin([App.Consumer.Views.Mixins.SalusView, App.Consumer.Views.Mixins.RegisteredRegions]);
        
        Views.FC600EntryView = Mn.LayoutView.extend({
			className: "col-xs-12 trv-item",
			template: consumerTemplates["setupWizard/fancoil/facoilItem"],
			regions: {
				checkboxRegion: ".bb-checkbox"
			},
			
			initialize: function (/*options*/) {
                var that = this;
				this.checkboxView = new App.Consumer.Views.CheckboxView({
					model: new App.Consumer.Models.CheckboxViewModel({
						name: "equipmentCheckbox",
						nonKeyLabel: this.model.get("name"),
						secondaryIconClass: "",
						isChecked: false
					})
				});
                
                this.listenTo(this.checkboxView, "clicked:checkbox", function () {
					that.trigger("click:checkbox");
				});

			},
			onRender: function () {
				this.checkboxRegion.show(this.checkboxView);
			}
		}).mixin([SalusView]);
        
        Views.FC600CollectionView = Mn.CollectionView.extend({
			className: "trv-collection-view row",
			childView: Views.FC600EntryView,
			childEvents: {
				"click:checkbox": "handleCheckboxClick"
			},
			initialize: function () {
				var deviceModels = [];
                this.collection = new B.Collection(deviceModels);
                var that = this;
                
                this.gateway = App.getCurrentGateway();

				if (!this.gateway || this.gateway.pairingDevices.length === 0) {
					// get out
					return App.navigate("setup/pairing");
				}

				

				// get device models from salusConnector
				deviceModels = _.map(this.gateway ? this.gateway.pairingDevices : [], function (deviceObj) {
					return App.salusConnector.getDevice(deviceObj.dsn);
				});

				// set collection
                var deviceArray = [];
				for(var i = 0; i < deviceModels.length; i++){
                    //currentModel
                   // if(deviceModels[i] !== undefined){
					if(deviceModels[i] !== undefined && deviceModels[i].get("dsn") !== this.model.get("dsn") &&
                            deviceModels[i].get("oem_model") === constants.modelTypes.FC600){
						deviceArray.push(deviceModels[i]);
					}
				}
				this.collection.set(deviceArray);
			},
            getCheckedItems: function () {
				var array = [];
				if (!this.collection.isEmpty()) {
					this.children.each(function (childView) {
						if (childView.checkboxView.getIsChecked()) {
							array.push(childView.model);
						}
					});
				}

				return array;
			},
            anyChecked: function () {
				if (!this.collection || this.collection.isEmpty()) {
					return false;
				} else {
					return _.some(this.children._views, function (childView) {
						return childView.checkboxView.getIsChecked();
					});
				}
			},
			handleCheckboxClick: function () {
                
				this.trigger("click:checkbox");
			}
            
			
			
		}).mixin([SalusView]);
	});

	return App.Consumer.SetupWizard.Fancoil.Views.FancoilCopySetupLayout;
});