"use strict";

define([
	"app",
	"bluebird",
	"common/constants",
	"common/config",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/mixins/mixin.salusPage",
	"consumer/views/SalusModal.view",
	"consumer/views/SalusButtonPrimary.view",
	"consumer/setupWizard/views/provisioning/CustomizeEquipmentModal.view"
], function (App, P, constants, config, consumerTemplates, SalusView, SalusPage, SalusModal, SalusButtonPrimary) {

	App.module("Consumer.SetupWizard.Provisioning.Views", function (Views, App, B, Mn, $, _) {

		/**
		 * ProvisioningCustomizeEquipmentPageView
		 * navigated to after naming equipment in provisioning flow
		 */
		Views.ProvisioningCustomizeEquipmentPageView = Mn.LayoutView.extend({
			template: consumerTemplates["setupWizard/provisioning/provisioningCustomizeEquipmentPage"],
			className: "provision-customize-equipment-page",
			regions: {
				equipmentRegion: ".bb-equipment-region",
				finishButtonRegion: ".bb-finish-button",
				cancelButtonRegion: ".bb-cancel-button"
			},
			initialize: function () {
				_.bindAll(this, "handleDeviceClick");
                
                

				this.gateway = App.getCurrentGateway();

				if (!this.gateway || this.gateway.pairingDevices.length === 0) {
					// get out
					return App.navigate("setup/pairing");
				}
				this.equipmentCompositeView = new Views.ProvisioningCustomizeEquipmentCollectionView({
					model: this.gateway
				});

				this.finishButtonView = new SalusButtonPrimary({
					id: "finish-button",
					classes: "btn-primary width100 font-semi-bold text-size-16",
					buttonTextKey: "common.labels.finished",
					clickedDelegate: this.handleFinishClick
				});

				this.cancelButtonView = new SalusButtonPrimary({
					id: "cancel-button",
					className: "btn btn-default width100 font-semi-bold text-size-16",
					buttonTextKey: "common.labels.cancel",
					clickedDelegate: this.handleCancelClick
				});

				this.listenTo(this.equipmentCompositeView, "click:device", this.handleDeviceClick);
			},
			onRender: function () {
				this.equipmentRegion.show(this.equipmentCompositeView);
				this.finishButtonRegion.show(this.finishButtonView);
				this.cancelButtonRegion.show(this.cancelButtonView);
			},
			handleFinishClick: function () {
                
                App.navigate("dashboard");
			},
			handleCancelClick: function () {
				// back to root of pairing flow
				App.navigate("setup/pairing");
			},
			handleDeviceClick: function (deviceView) {
                var deviceType = deviceView.model.modelType;
                if(constants.isPinDevices(deviceType)){
                    var that = this;
                    this.deviceView = deviceView; // keep this around
                    this.modalView = new Views.CustomizeEquipmentModalView({
                        model: deviceView.model
                    });

//                    this.listenTo(this.modalView, "click:save", function (/*deviceModel*/) {
//                        that.deviceView.ui.checkMark.removeClass("hidden");
//                    });
                    this.listenTo(deviceView.model, "click:save", function (/*deviceModel*/) {
                        that.deviceView.ui.checkMark.removeClass("hidden");
                    });

                    App.modalRegion.show(new SalusModal({
                        contentView: this.modalView,
                        size: "modal-lg"
                    }));

                    App.showModal();   
                }

			},
			onDestroy: function () {
				// this is the last step, reset pairingDevices
				if (this.gateway) {
					this.gateway.pairingDevices = [];
				}
			}
		}).mixin([SalusPage], {
			analyticsSection: "setupWizard",
			analyticsPage: "provisioningCustomize"
		});

		/**
		 * ProvisioningCustomizeEquipmentDeviceView
		 * childView of list view
		 */
		Views.ProvisioningCustomizeEquipmentDeviceView = Mn.ItemView.extend({
			template: consumerTemplates["setupWizard/provisioning/provisioningCustomizeEquipmentDevice"],
			className: "customize-equipment-list-item",
			bindings: {
				".bb-equipment-name": "name"
			},
			ui: {
				icon: ".bb-equipment-icon",
				checkMark: ".bb-check-mark"
			},
			triggers: {
				"click": "click:device"
			},
			onRender: function () {
				this.ui.icon.css("background-image", "url(" + this.model.get("equipment_page_icon_url") + ")");
                //set pin and unpin
                var deviceType = this.model.modelType;
                if(constants.isPinDevices(deviceType)){
                    this.model.shouldPin = true;
                    App.salusConnector.pinToDashboard(this.model, "device");
                }
                else{
                    this.model.shouldPin = false;
                    App.salusConnector.unpinFromDashboard(this.model, "device");
                    this.ui.checkMark.removeClass("hidden");
                }
               
			}
		}).mixin([SalusView]);

		/**
		 * ProvisioningCustomizeEquipmentCollectionView
		 * display a list of devices that were just provisioned
		 */
		Views.ProvisioningCustomizeEquipmentCollectionView = Mn.CollectionView.extend({
			template: false,
			className: "customize-equipment-list",
			childView: Views.ProvisioningCustomizeEquipmentDeviceView,
			childEvents: {
				"click:device": function (childView) {
					// propagate the view up
					this.trigger("click:device", childView);
				}
			},
			initialize: function () {
				var deviceModels = [];

				this.collection = new B.Collection(deviceModels);
                var that = this;

				// get device models from salusConnector
				deviceModels = _.map(this.model ? this.model.pairingDevices : [], function (deviceObj) {
					return App.salusConnector.getDevice(deviceObj.dsn);
				});

				// set collection
                 //Dev_SCS-3282 start
                var deviceArray = [];
				for(var i = 0; i < deviceModels.length; i++){
					if(deviceModels[i] !== undefined){
						deviceArray.push(deviceModels[i]);
					}
				}
				this.collection.set(deviceArray);
                //Dev_SCS-3282 end
				//this.collection.set(deviceModels);
			}
		}).mixin([SalusView]);
	});

	return App.Consumer.SetupWizard.Provisioning.Views.ProvisioningCustomizeEquipmentPageView;
});
