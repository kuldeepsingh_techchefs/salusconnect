"use strict";

define([
	"app",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView"
], function (App, templates, SalusViewMixin) {

	App.module("Consumer.Views", function (Views, App, B, Mn /*, $*/) {

		var BreadcrumbView = Mn.ItemView.extend({
			tagName: "li",
			template: templates["base/breadcrumb"],
			events: {
				click: "_handleClick"
			},
			initialize: function () {
				var textKey = this.model.get("textKey"),
					text = this.model.get("text");

				if (!text && textKey) {
					this.model.set("text", App.translate(textKey));
				}
			},
			onRender: function () {
				//Adds the active class to this li element.
				if (this.model.get("active")) {
					this.$el.addClass("active");
				}
			},
			templateHelpers: function () {
				return {
					active: this.model.get("active") || false,
					href: this.model.get("href") || ""
				};
			},
			_handleClick: function (evt) {
				if (this.model.has("onClick")) {
					this.model.get("onClick")(evt);
				}
			}
		}).mixin([SalusViewMixin]);

		/**
		 *This view takes in an array of crumbs.
		 * A crumb has the following properties:
		 *	text: String Optional | The text of the crumb. overrides textKey.
		 *	textKey: String Optional | Language text key to be used as the text of the crumb.
		 *	href: String Optional | If present wraps the crumb text in an a tag with the given href.
		 *	active: boolean Optional [Default false] | Highlight the crumb.
		 *	onClick: function(evt) Optional | click event handler for the crumb.
		 */
		Views.BreadcrumbsView = Mn.CollectionView.extend({
			tagName: "ol",
			className: "breadcrumb hidden-xs",
			childView: BreadcrumbView,
			initialize: function (options) {
				options = options || { crumbs: [] };
				this.collection = new B.Collection(options.crumbs);
			}
		}).mixin([SalusViewMixin]);
	});

	return App.Consumer.Views;
});