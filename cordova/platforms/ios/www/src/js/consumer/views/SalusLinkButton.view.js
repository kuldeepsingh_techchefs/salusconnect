"use strict";

define([
	"app",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusButton"
], function (App, consumerTemplates, SalusButtonViewMixin) {

	App.module("Consumer.Views", function (Views, App, B, Mn) {
		Views.SalusLinkButtonView = Mn.ItemView.extend({
			tagName: "button",
			type: "button",
			template: consumerTemplates.SalusLinkButton,
			attributes: function () {
				return {
					type: "button"
				};
			},
			/**
			 * This is a work around for having the attributes function above.
			 * When we define the attributes, we can nolonger support having classNames be passed from the parent
			 * controller.  The work around is have the parent pass cssClass instead and we combine it here.
			 * @returns {string} css class string for the element.
			 */
			className: function () {
				var cssClass = "btn btn-link button-link";
				if (this.options.cssClass) {
					cssClass += " " + this.options.cssClass;
				}
				return cssClass;
			},
			initialize: function (options) {
				options = options || {};
				this.iconClass = options.iconClass || "";
			},
			onRender: function () {
				var text = this.buttonText || App.translate(this.buttonTextKey);

				if (this.iconClass) {
					this.$(".bb-logo-area").addClass(this.iconClass);
					this.$(".bb-link-area").text(text);
				} else {
					this.$el.text(text);
				}
			}
		}).mixin([SalusButtonViewMixin], {
			eventNameKey: "buttonTextKey",
			eventName: "click"
		});
	});

	return App.Consumer.Views.SalusLinkButtonView;
});