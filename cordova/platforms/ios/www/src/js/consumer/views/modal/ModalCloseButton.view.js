"use strict";

define([
	"app",
	"consumer/views/mixins/mixin.salusButton"
], function (App, SalusButtonMixin) {

	App.module("Consumer.Views", function (Views, App, B, Mn) {
		Views.ModalCloseButton = Mn.ItemView.extend({
			tagName: "button",
			className: "btn btn-default",
			attributes: {
				"type": "button",
				"data-dismiss": "modal"
			},
			template: false,

			onRender: function () {
				this.$el.text(App.translate(this.buttonTextKey));
			}
		}).mixin([SalusButtonMixin]);
	});

	return App.Consumer.Views.ModalCloseButton;
});
