"use strict";

define([
	"app",
	"bluebird",
	"momentWrapper",
	"common/model/ayla/mixin.AylaBacked",
	"common/AylaConfig"
], function (App, P, moment, AylaBackedMixin, AylaConfig) {

	App.module("Models", function (Models, App, B, Mn, $, _) {
		/**
		 * This represents an ayla model property that lives on ayla devices
		 */
		Models.DevicePropertyModel = B.Model.extend({
			isLinkedDeviceProperty: false,
			defaults: {
				key: null,
				name: null,
				display_name: null,
				created_at: null,
				value: null,
				device_key: null,
				data_updated_at: null,
				read_only: null,
				base_type: null
			},

			idAttribute: "key",

			initialize: function () {
				_.bindAll(this, "updateData");
			},

			getProperty: function () {
				return this.get("value");
			},

			setAttributes: function (data) {
				if (data.data_updated_at && this.get("data_updated_at") && moment(data.data_updated_at) <= moment(this.get("data_updated_at"))) {
					// short circuit because we are trying to set old data
					return false;
				}

				return this.set(data);
			},

			load: function (options) {
				var that = this;
				return App.salusConnector.makeAjaxCall(_.template(AylaConfig.endpoints.device.property.fetch)({ id: this.id }), null, "GET", "json", options).then(function (data) {
					that.setAttributes(data.property);
				});
			},

			getDatapoints: function (/*oldDate, newDate, customIdentifier*/) {
				//todo this is for getDatapoints by time range which will be implemented later
//				var payload = {
//					time_range_filter: [{
//						time_range_id: customIdentifier,
//						start_time: moment(oldDate).format("YYYY/MM/DD HH:mm:ss"),
//						end_time: moment(newDate).format("YYYY/MM/DD HH:mm:ss")
//					}]
//				};

				return App.salusConnector.makeAjaxCall(_.template(AylaConfig.endpoints.device.property.getDataPointTimeRange)(this.toJSON()), null /*JSON.stringify(payload)*/, "GET", "json").then(function (data) {
					//unwrapping datapoints
					return data.map(function (obj) {
						return obj.datapoint;
					});
				});
			},

			setProperty: function (value) {
				// TODO: making an exception to save ACPhase_c on energy meter
				if (this.get("read_only") && (this.get("display_name") !== "ACPhase_c") && (this.get("display_name") !== "AppData_c")) {
					return P.reject("Error: You cannot set a read-only property");
				}

				var that = this,
					promise,
					payload = {"datapoint": {
						"value": value
					}};

				if (App.isCordovaApiReady("createDatapoint")) {
					var deviceId = this.get("device_key");  //TODO: look into why this is null and why its not effecting things. (might be because we are not testing lan mode)
					var propId = this.get("key");
					var baseType = this.get("base_type");

					promise = App.salusConnector.makeCordovaCall("createDatapoint", [deviceId, propId, baseType, value]);
				} else {
					var url = _.template(AylaConfig.endpoints.device.property.setDataPoint)(this.toJSON());
					promise = App.salusConnector.makeAjaxCall(url, payload, "POST");
				}

				return promise.then(function (response) {
					var model = {
						value: response.datapoint.value,
						data_updated_at: response.datapoint.updated_at
					};

					that.set(model);
				});
			},
			getDisplayNameTranslated: function () {
				if (!this.translatedName) {
					var name = this.get("display_name") || "";
					var camelCasedName = name.substr(0, 1).toLowerCase() + name.substr(1);
					this.translatedName = App.translate("equipment.property.names." + camelCasedName);
				}

				return this.translatedName;
			},
			updateData: function (data) {
//				this.set(data);
				this.setAttributes(data);
			}

		}).mixin([AylaBackedMixin], {
			apiWrapperObjectName: "property"
		});
	});

	return App.Models.DevicePropertyModel;
});
