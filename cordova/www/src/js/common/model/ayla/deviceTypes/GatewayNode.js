"use strict";

define([
	"app",
	"common/constants",
	"common/AylaConfig",
	"common/config",
	"common/model/ayla/deviceTypes/mixin.BaseDevice"
], function (App, constants, AylaConfig, config, BaseAylaDevice) {

	App.module("Models", function (Models, App, B) {
		/**
		 * this is a wrapper for testing ayla base device. it allows us to construct one and run unit tests and such
		 */
		Models.GatewayNode = B.Model.extend({
			tileType: "wud",
			modelType: constants.modelTypes.GATEWAYNODE,
			defaults: {
			},
			initialize: function (/*data, options*/) {
				//TODO Setup
				this._setCategoryDefaults();
				this._setEquipmentPageDefaults();
			},
			_setCategoryDefaults: function () {
				this.set({
					device_category_id: 2,
					device_category_icon_url: App.rootPath("/images/icons/dashboard/icon_wud.svg"),
					device_category_type: constants.categoryTypes.WIRELESSUNCONTROLLABLEDEVICES,
					device_category_name_key: "equipment.myEquipment.categories.types.wirelessUncontrollableDevices"
				});
			},
			_setEquipmentPageDefaults: function () {
				this.set({
					equipment_page_icon_url: App.rootPath("/images/icons/myEquipment/icon_wud_grey.svg")
				});
			}
		}).mixin([BaseAylaDevice]);
	});

	return App.Models.GatewayNode;
});
