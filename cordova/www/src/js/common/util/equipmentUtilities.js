"use strict";

define([
	"common/constants",
	"underscore"
], function (constants, _) {
	var equipmentUtilities = {};

	/**
	 * return an object with percent and status
	 * can be used for door/window sensor
	 * CO/smoke detectors
	 */
	equipmentUtilities.sensorBatteryMapperForVoltage = function (voltage, modelType) {
		var batteryInfo = {}, modelTypes = constants.modelTypes;

		if (voltage > 0 && modelType) {
			if (modelType === modelTypes.WINDOWMONITOR) {
				if (voltage >= 2.6) {
					batteryInfo.percent = 100;
					batteryInfo.status = "full";
				} else if (voltage < 2.6 && voltage >= 2.3) {
					batteryInfo.percent = 50;
					batteryInfo.status = "half";
				} else if (voltage < 2.3 && voltage >= 2.1) {
					batteryInfo.percent = 25;
					batteryInfo.status = "low";
				} else if (voltage < 2.1) {
					batteryInfo.percent = 0;
					batteryInfo.status = "critical";
				}
			} else if (modelType === modelTypes.DOORMONITOR || modelType === modelTypes.CARBONMONOXIDEDETECTOR || modelType === modelTypes.SMOKEDETECTOR) {
				// TODO: this may not work for Smoke/CO - we need to see a physical device
				if (voltage >= 2.9) {
					batteryInfo.percent = 100;
					batteryInfo.status = "full";
				} else if (voltage < 2.9 && voltage >= 2.8) {
					batteryInfo.percent = 50;
					batteryInfo.status = "half";
				} else if (voltage < 2.8 && voltage >= 2.2) {
					batteryInfo.percent = 25;
					batteryInfo.status = "low";
				} else if (voltage < 2.2) {
					batteryInfo.percent = 0;
					batteryInfo.status = "critical";
				}
			} else if (modelType === modelTypes.ENERGYMETER) {
				if (voltage >= 5.2) {
					batteryInfo.percent = 100;
					batteryInfo.status = "full";
				} else if (voltage < 5.2 && voltage >= 4.6) {
					batteryInfo.percent = 50;
					batteryInfo.status = "half";
				} else if (voltage < 4.6 && voltage >= 4.2) {
					batteryInfo.percent = 25;
					batteryInfo.status = "low";
				} else if (voltage < 4.2) {
					batteryInfo.percent = 0;
					batteryInfo.status = "critical";
				}
			}
		}

		return batteryInfo;
	};

	/**
	 * these next two functions useful for TRV associations
	 */
	equipmentUtilities.integerTo64BitHex = function (number) {
		if (_.isNumber(number)) {
			var hex = number.toString(16).toUpperCase();

			// make 64 bit
			for (var i = hex.length; i < 4; i++) {
				hex = "0" + hex;
			}

			return hex;
		} else {
			return number;
		}
	};

	equipmentUtilities.hexToInt = function (hex) {
		if (_.isString(hex)) {
			return parseInt(hex, 16);
		} else {
			return hex;
		}
	};

	/**
	 * equipment page, equipment settings page
	 * preserve category name case for these model types on the delete button
	 * @param device
	 * @returns {boolean}
	 */
	equipmentUtilities.shouldPreserveCategoryCase = function (device) {
		if (!device) {
			return false;
		}

		var which = [ // basically just IT600 family
			constants.modelTypes.IT600BOILERRECEIVER,
			constants.modelTypes.IT600THERMOSTAT,
			constants.modelTypes.IT600TRV,
			constants.modelTypes.IT600WIRINGCENTER,
            constants.modelTypes.SMARTPLUG,
            constants.modelTypes.MINISMARTPLUG,
            constants.modelTypes.FC600
            
		];

		return _.contains(which, device.modelType);
	};

	return equipmentUtilities;
});