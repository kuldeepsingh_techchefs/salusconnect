"use strict";

define([
	"app",
	"common/constants",
	"common/commonTemplates",
	"consumer/views/mixins/mixin.salusView"
], function (App, constants, commonTemplates, SalusView) {

	App.module("Consumer.Views", function (Views, App, B, Mn) {
		Views.SpinnerPageView = Mn.ItemView.extend({
			className: "spinner-view",
			template: commonTemplates.spinnerPage,
			ui: {
				wrapper: ".bb-spinner-wrapper",
				graphic: ".bb-spinner",
				text: ".bb-spinner-text"
			},
			initialize: function (options) {
				this.textKey = options.textKey || constants.spinnerTextKeys.loading;
				this.isLight = !!options.isLight;
				this.greyBg = !!options.greyBg;
			},
			onRender: function () {
				if (this.textKey === "none" || !this.textKey) {
					this.ui.text.addClass("hidden");
				} else {
					this.ui.text.text(App.translate(this.textKey));
				}

				this.ui.graphic.toggleClass("light", this.isLight);
				this.ui.wrapper.toggleClass("grey", this.greyBg);
			}
		}).mixin([SalusView]);
	});

	return App.Consumer.Views.SpinnerPageView;
});