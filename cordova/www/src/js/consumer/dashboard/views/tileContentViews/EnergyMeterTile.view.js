"use strict";

define([
	"app",
	"common/util/equipmentUtilities",
	"consumer/consumerTemplates",
	"consumer/dashboard/views/tileContentViews/mixin.TileContentView",
	"consumer/views/mixins/mixin.batteryLevel"
], function (App, equipUtils, consumerTemplates, TileContentViewMixin, BatteryLevel) {

	App.module("Consumer.Dashboard.Views.TileContentViews", function (Views, App, B, Mn) {
		Views.EnergyMeterTile = {};

		Views.EnergyMeterTile.FrontView = Mn.ItemView.extend({
			className: "energy-meter-tile",
			template: consumerTemplates["dashboard/tile/equipmentTileFront"],
			attributes: {
				role: "button"
			},
            initialize: function () {
				this.model.getDevicePropertiesPromise().then (function () {
					
				});

			},
			templateHelpers: function () {
				return {
					isLargeTile: this.isLargeTile
				};
			}
            
		}).mixin([TileContentViewMixin]);

		Views.EnergyMeterTile.BackView = Mn.ItemView.extend({
			className: "energy-meter-tile",
			template: consumerTemplates["dashboard/tile/energyMeterTileBack"],
			ui: {
				batteryWrapper: ".bb-battery-wrapper",
				viewUsage: ".bb-view-usage"
			},
			events: {
				"click @ui.viewUsage": "goToUsage"
			},
            initialize: function () {
				this.model.getDevicePropertiesPromise().then (function () {
					
				});

			},
			goToUsage: function (/*evt*/) {
				var dsn = this.model.get("dsn");

				App.Consumer.shouldScrollTo = "#bb-graph-region";
				App.navigate("equipment/myEquipment/" + dsn);
			}
		}).mixin([TileContentViewMixin, BatteryLevel]);
	});

	return App.Consumer.Dashboard.Views.EnergyMeterTile;
});
