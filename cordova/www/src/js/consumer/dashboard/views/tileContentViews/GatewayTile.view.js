"use strict";

define([
	"app",
	"common/constants",
	"consumer/consumerTemplates",
	"consumer/dashboard/views/tileContentViews/mixin.TileContentView",
	"consumer/dashboard/views/tileContentViews/EquipmentTile.view"
], function (App, constants, consumerTemplates, TileContentViewMixin) {

	App.module("Consumer.Dashboard.Views.TileContentViews", function (Views, App, B, Mn, $, _) {
		Views.GatewayTile = {};

		Views.GatewayTile.FrontView = Mn.ItemView.extend({
			className: "gateway-tile",
			template: consumerTemplates["dashboard/tile/gatewayTileFront"],
			attributes: {
				role: "button"
			},
			ui: {
				activeStatus: ".bb-active-status"
			},
			bindings: {
				":el": {
					observe: "connection_status",
					update: function ($el, connection) {
						$el.toggleClass("offline", connection !== "Online");
						$el.toggleClass("disconnected", connection !== "Online");
					}
				}
			},
			initialize: function () {
				_.bindAll(this, "setActiveGatewayUI");

				this.listenTo(App.salusConnector, "change:currentGateway", this.setActiveGatewayUI);
			},
			onRender: function () {
				this.setActiveGatewayUI();
			},
			setActiveGatewayUI: function () {
				var isActive = this.model.get("dsn") === App.getCurrentGatewayDSN();

				this.ui.activeStatus.toggleClass("active", isActive);
			},
			templateHelpers: function () {
				return {
					isLargeTile: this.isLargeTile
				};
			}
		}).mixin([TileContentViewMixin]);

		Views.GatewayTile.BackView = Mn.ItemView.extend({
			className: "gateway-tile",
			template: consumerTemplates["dashboard/tile/tileBack"],
			ui: {
				backIcon: ".bb-equipment-icon"
			},
			bindings: {
				".bb-equipment-icon": {
					observe: "connection_status",
					update: function ($el, connection) {
						$el.toggleClass("offline", connection !== "Online");
					}
				}
			},
			onRender: function () {
				this.ui.backIcon.removeClass("hidden");
			}
		}).mixin([TileContentViewMixin]);
	});

	return App.Consumer.Dashboard.Views.GatewayTile;
});
