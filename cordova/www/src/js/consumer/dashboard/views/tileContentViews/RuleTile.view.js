"use strict";

define([
	"app",
	"bluebird",
	"common/constants",
	"consumer/consumerTemplates",
	"consumer/dashboard/views/tileContentViews/mixin.TileContentView",
	"common/model/salusWebServices/rules/Rule.model"
], function (App, P, constants, consumerTemplates, TileContentViewMixin) {

	App.module("Consumer.Dashboard.Views.TileContentViews", function (Views, App, B, Mn, $, _) {
		Views.RuleTile = {};

		var emptyRule;

		Views.RuleTile.FrontView = Mn.ItemView.extend({
			className: "rule-tile",
			template: consumerTemplates["dashboard/tile/equipmentTileFront"],
			attributes: {
				role: "button"
			},
			initialize: function () {
				_.bindAll(this, "_getRuleModel");

				if (this.model.get("referenceId")) {
					this.ruleRef = this.model.get("referenceId");

					this.model = new App.Models.RuleModel({
						dsn: App.getCurrentGatewayDSN(),
						name: App.translate("equipment.oneTouch.automations.title"), // OneTouch
						key: this.ruleRef
					});

					emptyRule = this.model;

					this.listenTo(App.salusConnector, "loaded:ruleCollection", this._getRuleModel);
				}
			},
			_getRuleModel: function () {
				if (this.ruleRef) {
					var getRule = App.salusConnector.getRule(this.ruleRef);

					if (getRule) {
						this.model.set(getRule.toJSON());
						this.render();
					}
				}
			},
            onRender: function() {
                if (this.model) {
                    if(this.model.get("active")) {
                        this.$el.removeClass("disconnected");
                    } else {
                        this.$el.addClass("disconnected");
                    }
				}
            },
			templateHelpers: function () {
				return {
					isLargeTile: true
				};
			}
		}).mixin([TileContentViewMixin]);

		Views.RuleTile.BackView = Mn.ItemView.extend({
			className: "rule-tile",
			template: consumerTemplates["dashboard/tile/tileBack"],
			ui: {
				titleLink: "a.bb-title-text",
				icon: ".bb-equipment-icon"
			},
			attributes: {
				role: "button"
			},
			events: {
				"mousedown .bb-equipment-icon": "_handleMouseDown",
				"mouseout .bb-equipment-icon": "_handleMouseUp",
				"mouseup .bb-equipment-icon": "_handleMouseUp",
				"click .bb-equipment-icon": "_handleButtonClick",
				"touchstart .bb-equipment-icon": "_handleMouseDown",
				"touchend .bb-equipment-icon": "_handleMouseUp",
				"touchcancel .bb-equipment-icon": "_handleMouseUp"
			},
			initialize: function () {
				_.bindAll(this, "_getRuleModel", "_handleButtonClick", "_handleMouseUp", "_handleMouseDown");

				if (this.model.get("referenceId")) {
					this.ruleRef = this.model.get("referenceId");

					this.model = emptyRule;

					this.listenTo(App.salusConnector, "loaded:ruleCollection", this._getRuleModel);
				}
			},
			_getRuleModel: function () {
				if (this.ruleRef) {
					var getRule = App.salusConnector.getRule(this.ruleRef);

					// if we can't get then the assumption is the rule was deleted and we will unpin it
					if (getRule) {
						this.model = getRule;
						this.render();
					} else {
						// using this instead of unpinFromDashboard() because this is just an empty Rule
						App.salusConnector.unpinFromDashboard(this.model, "rule");
						this.trigger("unpinned");
					}
				}
			},
			onRender: function () {
				if (this.model.get("key")) {
					// changing link from equipment page to rule page
					var path = "/oneTouch/" + App.getCurrentGatewayDSN() + "/" + encodeURIComponent(this.model.get("key"));
					this.ui.titleLink.attr("href", path);
				} else {
					this.ui.titleLink.attr("href", "#");
				}

				this.ui.icon.addClass("one-touch").removeClass("hidden");
			},
			_handleMouseDown: function () {
				this.ui.icon.addClass("pressed");
			},
			_handleMouseUp: function () {
				this.ui.icon.removeClass("pressed");
			},
			_handleButtonClick: function (/*event*/) {
				if (!this.spinning && this.model.includesRunNow()) {
					var that = this, triggerRuleProp, gatewayNode = App.getCurrentGateway().getGatewayNode();

					if (gatewayNode) {
						this.showSpinner(this.tileSpinnerOptions);

						gatewayNode.getDevicePropertiesPromise().then(function () {
							if ((triggerRuleProp = gatewayNode.get("TriggerRule"))) {
								that.listenToOnce(triggerRuleProp, "propertiesSynced", that.hideSpinner);
								that.listenToOnce(triggerRuleProp, "propertiesFailedSync", that.hideSpinner);

								return App.salusConnector.triggerRule(that.model);
							} else {
								that.hideSpinner();
							}
						}).catch(function (err) {
							that.hideSpinner();

							return P.reject(err);
						});
					}
				}
			}
		}).mixin([TileContentViewMixin]);
	});

	return App.Consumer.Dashboard.Views.RuleTile;
});
