"use strict";

define([
	"app",
	"jquery",
	"underscore",
	"backbone"
], function (App, $, _, B) {

	App.module("Consumer.Models", function (Models) {
		Models.AlertModalViewModel = B.Model.extend({
			defaults: {
				iconClass: "", // classes should be specified in _icons.scss
				primaryLabelText: "",
				secondaryLabelText: "",
				leftButton: null, // ex. new App.Consumer.Views.SalusButtonPrimaryView
				rightButton: null // ex. new App.Consumer.Views.ModalCloseButton
			}
		});
	});

	return App.Consumer.Models.AlertModalViewModel;
});
