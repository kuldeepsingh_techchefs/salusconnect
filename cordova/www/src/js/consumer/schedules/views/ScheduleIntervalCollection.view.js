"use strict";

define([
	"app",
	"momentWrapper",
	"common/util/utilities",
	"common/constants",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/mixins/mixin.salusTabContent",
	"consumer/schedules/views/AddNewInterval.view",
	"consumer/schedules/views/ScheduleInterval.view",
	"consumer/settings/views/EmptyList.view"
], function (App, moment, utilities, constants, consumerTemplates, SalusViewMixin, SalusTabContent) {

	App.module("Consumer.Schedules.Views", function (Views, App, B, Mn) {
		Views.ScheduleIntervalCollectionView = Mn.CollectionView.extend({
			className: "interval-collection",
			childEvents: {
				"enabled:editMode": "render",
				"click:close": "render",
                "intervalChange":"intervalChange"
			},
			emptyView: App.Consumer.Settings.Views.EmptyListView,
			emptyViewOptions: {
				tagName: "div",
				i18nTextKey: "schedules.schedule.noIntervals"
			},
			getChildView: function (model) {
				if (!model.isEditMode) {
					return App.Consumer.Schedules.Views.ScheduleIntervalView;
				} else {
					return App.Consumer.Schedules.Views.AddNewIntervalView;
				}
			},
			childViewOptions: function () {
				return {
					onOffMode: !(this.device.isThermostat() || this.device.isFC600()),
					timeFormat24Hour:this.timeFormat24Hour,
                    device: this.device,
                    isValidTime:this.isValidTime
				};
			},

			initialize: function (options) {
                var that=this;
                
				this.device = options.device;
                this.collection=options.collection;
				this.timeFormat24Hour=false;
                this.isValidTime=function (time , key){
                    //当新增未保存的,会存在无key
                    var res=_.filter(that.collection.models,function (item){
                        if(key){
                            return item.get("time")==time && item.get("key")!=key;
                        } else {
                            return item.get("time")==time;
                        }
                        
                    });
                    
                    //有key,则正常会是没有的,无key获取到的res是包含自己的
                    if(key){
                        return res.length==0;
                    } else {
                        return res.length==1;
                    }
                    
                }

				if(this.device.modelType==constants.modelTypes.MINISMARTPLUG || this.device.modelType==constants.modelTypes.SMARTPLUG || this.device.get("oem_model")=="ST880ZB" || this.device.get("oem_model")=="ST898ZB"){
				    this.timeFormat24Hour=true;
				}else if(this.device.modelType==constants.modelTypes.IT600THERMOSTAT || this.device.modelType==constants.modelTypes.WATERHEATER){
				    if(this.device.get("TimeFormat24Hour") && this.device.get("TimeFormat24Hour").getProperty()=="1"){
				        this.timeFormat24Hour=true;
				    }
				}
			},
            
            intervalChange:function (){
                this.trigger("intervalChange");
            },

			viewComparator: function (modelA, modelB) {
				var timeA = moment(modelA.get("time"), "HH:mm:ss").format("HH:mm"),
						timeB = moment(modelB.get("time"), "HH:mm:ss").format("HH:mm");

				if (timeA < timeB) {
					return -1;
				} else if (timeA > timeB) {
					return 1;
				} else if (timeA === timeB){
					return 0;
				}
			}
		}).mixin([SalusViewMixin, SalusTabContent]);
	});

	return App.Consumer.Schedules.Views.ScheduleIntervalCollectionView;
});

