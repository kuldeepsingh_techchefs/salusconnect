"use strict";

define([
    "app",
    "bluebird",
    "common/constants",
    "consumer/consumerTemplates",
    "consumer/views/mixins/mixin.salusPage",
    "consumer/equipment/views/tsc/ThermostatSingleControlPage.view",
    "consumer/schedules/views/ScheduleCollection.view"
], function (App, P, constants, consumerTemplates, SalusPageMixin) {

    App.module("Consumer.Schedules.Views", function (Views, App, B, Mn) {

        Views.SchedulePage = Mn.LayoutView.extend({
            id: "thermostat-schedule",
            className: "container",
            template: consumerTemplates["schedules/schedulePage"],
            regions: {
                scheduleCollectionRegion: ".bb-schedule-collection",
                thermostatIndividualRegion: ".bb-thermostat-individual-collection"
            },
            ui: {
                title: ".bb-title"
            },
            initialize: function () {
                var categories = constants.categoryTypes;

                App.Consumer.Schedules.Controller.idIndex = 0;


                this.isThermostat = this.options.deviceCategory === categories.THERMOSTATS;

                if (this.options.key) {
                    var type;
                    var singleControl;
                    //判断是否是thermostat,如果是则需要判断是否是single control
                    if (this.options.deviceCategory === categories.THERMOSTATS) {
                        var tscCollection = App.salusConnector.getTSCCollection();
                        var device = App.salusConnector.getDeviceByKey(this.options.key);
                        singleControl = tscCollection.getSingleControlByThermostat(device);
                        if (singleControl) {
                            type = constants.scheduleTypes.thermostats.singleControl;
                            var defaultDevice = App.salusConnector.getDeviceByEUID(singleControl.get("defaultDevice"));
                            this.options.key = defaultDevice.get("key");
                        } else {
                            type = constants.scheduleTypes.thermostats.individualControl;
                        }
                    }
                    this.deviceScheduleCollectionView = new App.Consumer.Schedules.Views.ScheduleCollectionView({
                        deviceCategory: this.options.deviceCategory,
                        key: this.options.key,
                        type: type,
                        singleControl: singleControl
                    });
                } else if (this.options.deviceCategory === categories.THERMOSTATS) {
                    this.deviceScheduleCollectionView = new App.Consumer.Schedules.Views.ScheduleCollectionView({
                        deviceCategory: this.options.deviceCategory,
                        type: constants.scheduleTypes.thermostats.singleControl
                    });
                    this.thermostatScheduleIndividualCollectionView = new App.Consumer.Schedules.Views.ScheduleCollectionView({
                        deviceCategory: this.options.deviceCategory,
                        type: constants.scheduleTypes.thermostats.individualControl
                    });
                } else {
                    this.deviceScheduleCollectionView = new App.Consumer.Schedules.Views.ScheduleCollectionView({
                        deviceCategory: this.options.deviceCategory
                    });
                }



            },
            onRender: function () {
                var that = this, devCollec;

                this.showSpinner({
                    textKey: constants.spinnerTextKeys.loading
                });

                //原本以下代码是要等到property加载完后才会执行,目前暂时注释,看下是否能够正常运行
//                App.salusConnector.getFullDeviceCollection().getPropertiesPromise().then(function () {
                    if (that.options.deviceCategory === constants.categoryTypes.THERMOSTATS) {
                        devCollec = App.salusConnector.getThermostatCollection();
                    } else if (that.options.deviceCategory === constants.categoryTypes.WATERHEATERS) {
                        devCollec = App.salusConnector.getWaterHeaterCollection();
                    } else if (that.options.deviceCategory === constants.categoryTypes.SMARTPLUGS) {
                        devCollec = App.salusConnector.getSmartPlugCollection();
                    }
                    
                    this._getTitle();
                    
                    var reloadDevices=[];

                    return P.all(devCollec.map(function (device) {
                        if (device.hasLoadedSchedules()) {
                            reloadDevices.push(device);
                        } else {
                            return device.loadSchedules();
                        }
                    })).then(function () {
                        
                            that.hideSpinner();
                            that.scheduleCollectionRegion.show(that.deviceScheduleCollectionView);

                            if (!that.options.key && that.isThermostat) {
                                that.thermostatIndividualRegion.show(that.thermostatScheduleIndividualCollectionView);
                            }

                            //Get how many devices are going to be in the collection. If deviceCount = 0 show empty view
                            App.salusConnector.getDataLoadPromise(["tscs"]).then(function () {
                                var deviceCount = 0, categories = constants.categoryTypes;
                                
                                if(!this.options.key){
                                    switch (that.options.deviceCategory) {
                                        case categories.THERMOSTATS:
                                            deviceCount = App.salusConnector.getThermostatCollection();
                                            break;
                                        case categories.WATERHEATERS:
                                            deviceCount += App.salusConnector.getWaterHeaterCollection().length;
                                            break;
                                        case categories.SMARTPLUGS:
                                            deviceCount += App.salusConnector.getSmartPlugCollection().length;
                                            break;
                                        default:
                                            throw new Error("Cannot retrieve invalid device type");
                                    }

                                    if (deviceCount === 0) {
                                        that.scheduleCollectionRegion.show(new App.Consumer.Schedules.Views.ScheduleEmptyLayoutView({
                                            deviceCategory: that.options.deviceCategory
                                        }));
                                    }
                                }
                            });
                        
                    }).then(function (){
                         reloadDevices.map(function (device){
                            device.loadSchedules(); 
                        });
                        
                    }).catch(function () {
                        that.hideSpinner();
                    });
//                });

               
            },
            _getTitle: function () {
                var categories = constants.categoryTypes;

                switch (this.options.deviceCategory) {
                    case categories.THERMOSTATS:
                        this.ui.title.text(App.translate("schedules.thermostat.title"));
                        break;
                    case categories.WATERHEATERS:
                        this.ui.title.text(App.translate("schedules.waterHeater.title"));
                        break;
                    case categories.SMARTPLUGS:
                        this.ui.title.text(App.translate("schedules.smartPlug.title"));
                        break;
                }
            }
        }).mixin([SalusPageMixin], {
            analyticsSection: "schedule",
            analyticsPage: "list",
            controller: App.Consumer.Schedules.Controller
        });
    });

    return App.Consumer.Schedules.Views.SchedulePage;
});
