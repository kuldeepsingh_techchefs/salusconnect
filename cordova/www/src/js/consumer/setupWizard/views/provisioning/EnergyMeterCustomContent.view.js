"use strict";

define([
	"app",
	"common/constants",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/SalusDropdown.view",
	"consumer/models/SalusDropdownViewModel",
	"consumer/views/SalusCheckbox.view",
	"consumer/models/SalusCheckboxViewModel",
	"consumer/views/FormTextInput.view"
], function (App, constants, consumerTemplates, SalusView) {

	App.module("Consumer.SetupWizard.Provisioning.Views", function (Views, App, B, Mn, $, _) {

		var voltages = [230, 220, 110],
				phases = [1, 3];

		/**
		 * EnergyMeterCustomContentView
		 * base view for energy meter customization
		 * this is an itemView that appends some dropdown and checkbox views
		 * this could probably be done cleaner too
		 */
		Views.EnergyMeterCustomContentView = Mn.ItemView.extend({
			className: "energy-meter-custom-content row",
			template: consumerTemplates["setupWizard/provisioning/energyMeterCustomContent"],
			ui: {
				voltageDropdown: ".bb-voltage-dropdown",
				phasesDropdown: ".bb-phases-dropdown",
				clamps: ".bb-clamps-anchor",
				clampsRow: ".bb-clamps-row",
				clampsError: ".bb-clamps-error",
                clamps1Error: ".bb-clamps1-error",
				clampsNameError: ".bb-clamps-name-error"
			},
			initialize: function () {
				var that = this;
				_.bindAll(this, "handleVoltageChange", "showError", "getCheckedClamps", "showClamps1Error","hideClamps1Error", "hideClampsError", "showErrorIfAlreadyShown");

				this.voltageDropdownCollection = new App.Consumer.Models.DropdownItemViewCollection();
				this.phasesDropdownCollection = new App.Consumer.Models.DropdownItemViewCollection();

				for (var i = 0; i < Math.max(voltages.length, phases.length); i++) {
					if (voltages.length > i) {
						// add to voltages
						this.voltageDropdownCollection.add(new App.Consumer.Models.DropdownItemViewModel({
							value: voltages[i],
							text: voltages[i]
						}));
					}

					if (phases.length > i) {
						// add phases
						this.phasesDropdownCollection.add(new App.Consumer.Models.DropdownItemViewModel({
							value: phases[i],
							text: phases[i]
						}));
					}
				}
				this.voltageDropdownView = new App.Consumer.Views.SalusDropdownWithValidation({
					collection: this.voltageDropdownCollection,
					viewModel: new App.Consumer.Models.DropdownViewModel({
						id: "voltageDropdown",
                        dropdownLabelText: "230"
					}),
					model: new B.Model({value: null}),
					boundAttribute: "value",
					required: true
				});
                

				this.phasesDropdownView = new App.Consumer.Views.SalusDropdownWithValidation({
					collection: this.phasesDropdownCollection,
					viewModel: new App.Consumer.Models.DropdownViewModel({
						id: "phasesDropdown",
						dropdownLabelText: "common.labels.select"
					}),
					model: new B.Model({value: null}),
					boundAttribute: "value",
					required: true
				});

				this.model.getDevicePropertiesPromise().then(function () {
					if (!that.model.isDestroyed) {
						var phase = that.model.get("ACPhase_c").anyEqual(3) ? 3 : 1,
								voltage = that.model.get("SetMeasuredVoltage_x10").first().getProperty();
						that.phasesDropdownView.model.set("value", phase);

						if (_.isNumber(phase)) {
							that.ui.clampsRow.show();
						}

						that.voltageDropdownView.model.set("value", voltage / 10 );
                        if(_.isNull(voltage)){
                            that.model.shouldSetVoltage = 230*10;
                        }
                        else{
                            that.model.shouldSetVoltage = voltage;
                        }
						
						that.phasesDropdownView.model.set("value", phase);
						that.model.shouldSetPhases = phase;

						if (phase === 1) {
							that._setOnePhase();
						} else if (phase === 3) {
							that._setThreePhase();
						}
					}
				});

				this.clampsView = new Views.EnergyMeterClampsCollectionView({
					model: this.model,
                    phaseModel: this.phasesDropdownView
				});

				this.listenTo(this.voltageDropdownView, "value:change", this.handleVoltageChange);
				this.listenTo(this.phasesDropdownView, "value:change", this.handlePhasesChange);
				this.listenTo(this.clampsView, "valid:clamps", this.hideClampsError);
                this.listenTo(this.clampsView, "valid:clamps1", this.hideClamps1Error);
                this.listenTo(this.clampsView, "invalid:clamps1", this.showClamps1Error);
				this.listenTo(this.clampsView, "invalid:clamps", this.showClampsError);
			},
			onRender: function () {
				this.ui.voltageDropdown.append(this.voltageDropdownView.render().$el);
				this.ui.phasesDropdown.append(this.phasesDropdownView.render().$el);
				this.ui.clamps.append(this.clampsView.render().$el);
				this.hideClampsError();
                this.hideClamps1Error();
				this.ui.clampsRow.hide();
			},
			onDestroy: function () {
				this.voltageDropdownView.destroy();
				this.phasesDropdownView.destroy();
				this.clampsView.destroy();
			},
			handleVoltageChange: function (dropdownItem) {
				// keep track
				this.model.shouldSetVoltage = dropdownItem.selectedValue * 10;
			},
			handlePhasesChange: function (dropdownItem) {
				// keep track
				this.model.shouldSetPhases = dropdownItem.selectedValue;
				if (!this.ui.clampsRow.is(":visible")) {
					this.ui.clampsRow.show();
				}
                this._initAllCheckboxs();  
                this.hideErrors();
			},

			hideErrors: function () {
				this.hideClampsError();
                this.hideClamps1Error();
				this.hideClampsNameError();
			},

			showError: function () {
				this.voltageDropdownView.validateDropdownField();
				this.phasesDropdownView.validateDropdownField();
			},
			getCheckedClamps: function () {
				return this.clampsView.getCheckedClamps();
			},
           

			hideClampsError: function () {
				this.ui.clampsError.addClass("vis-hidden");
			},
            hideClamps1Error: function () {
				this.ui.clamps1Error.addClass("vis-hidden");
			},

			hideClampsNameError: function () {
				this.ui.clampsNameError.addClass("vis-hidden");
			},

			showErrorIfAlreadyShown: function () {
				if (this.hasShowClampsError) {
					this.showClampsError();
				}
			},
            
            showClamps1Error: function () {
				this.ui.clamps1Error.removeClass("vis-hidden");
				this.hasShowClampsError = true;
			},

			showClampsError: function () {
				this.ui.clampsError.removeClass("vis-hidden");
				this.hasShowClampsError = true;
			},

			showClampsNameError: function () {
				this.ui.clampsNameError.removeClass("vis-hidden");
				this.hasShowClampsError = true;
			},

			_setThreePhase: function () {
				this.clampsView.showCheckBoxes();
			},

			_setOnePhase: function () {
				//this.clampsView.setOnePhase();
                this.clampsView.showCheckBoxes();
			},
            _initAllCheckboxs: function () {
                var that = this;
                var phase = this.model.shouldSetPhases;
                if(phase === 3){
                    var threePhaseClamps = _.filter(that.getCheckedClamps(), function (clamp) {
						return clamp.phase !== 0;
					});
                    if(threePhaseClamps.length === 3){
                        this.clampsView.hideNonFirstCheckedTextBoxes();
                    }
                }
                else{
                    this.clampsView.showTextBoxes();
                }
               
				//this.clampsView.unCheckAll();
               
			}
		}).mixin([SalusView]);

		Views.EnergyMeterClampsView = Mn.ItemView.extend({
			className: "col-xs-12 margin-b-15",
			template: consumerTemplates["setupWizard/provisioning/energyMeterClampsItem"],
			ui: {
				checkbox: ".bb-clamp-checkbox",
				textbox: ".bb-clamp-textbox"
			},
			initialize: function () {
				_.bindAll(this, "handleCheckboxClick");

				this.checkboxView = new App.Consumer.Views.CheckboxView({
					model: new App.Consumer.Models.CheckboxViewModel({
						name: "clampCheckbox",
						nonKeyLabel: this.model.get("clamp"),
						secondaryIconClass: "",
						isChecked: this.model.get("phase") === 3 || this.model.get("phase") === 1
					})
				});

				this.textboxView = new App.Consumer.Views.FormTextInput({
					labelText: "setupWizard.provisioning.customize.clampLabel",
					model: this.model,
					modelProperty: "name"
				});

				this.listenTo(this.checkboxView, "clicked:checkbox", this.handleCheckboxClick);
			},
			onRender: function () {
				this.ui.checkbox.append(this.checkboxView.render().$el);
				this.ui.textbox.append(this.textboxView.render().$el);

				// not needed and screwing with vertical spacing
				this.$(".bb-error").detach();
				this.$(".bb-form-group").removeClass("form-group has-success");

				this.$(".salus-checkbox").toggleClass("checked", this.checkboxView.getIsChecked()); // style different when checked
			},
			onDestroy: function () {
				this.checkboxView.destroy();
				this.textboxView.destroy();
			},
			hideCheckBox: function () {
				this.checkboxView.hideCheckBox();
			},
			showCheckBox: function () {
				this.checkboxView.showCheckBox();
			},
			hideTextBox: function () {
				this.ui.textbox.hide();
			},
			showTextBox: function () {
				this.ui.textbox.show();
			},
			isChecked: function () {
				return this.checkboxView.getIsChecked();
			},
			handleCheckboxClick: function (checkboxModel) {
				var isChecked = checkboxModel.get("isChecked");
                //this.model.set("phase", isChecked ? 3 : 1);
                var phase = $("#phasesDropdown .pull-left").text();
                if(isChecked){
                    this.model.set("phase", phase === "3" ? 3 : 1);
                }
                else{
                    this.model.set("phase",0);
                }
				

				this.$(".salus-checkbox").toggleClass("checked", isChecked); // style different when checked

				this.trigger("click:checkbox");
			},
			unCheck: function () {
				if (this.checkboxView.getIsChecked()) {
					this.ui.checkbox.find("input").click();
				}
			}
		}).mixin([SalusView]);

		Views.EnergyMeterClampsCollectionView = Mn.CollectionView.extend({
			className: "clamps-view row",
			template: false,
			childView: Views.EnergyMeterClampsView,
			childEvents: {
				"switch:mainLine": "_setMainLinesFalse",
				"click:checkbox": "_onCheckboxClicked"
			},
			initialize: function () {
				var that = this;
				_.bindAll(this, "getCheckedClamps", "_onCheckboxClicked");

				this.collection = new B.Collection([
					{clamp: 1, name: "", phase: 0},
					{clamp: 2, name: "", phase: 0},
					{clamp: 3, name: "", phase: 0},
					{clamp: 4, name: "", phase: 0}
				]);

				if (!this.model.get("ClampsUsed")) {
					this.model.fetchMetaData().then(function () {
						var clampsUsed = that.model.get("ClampsUsed");
						if (!that.isDestroyed && clampsUsed) {
							if (clampsUsed.length > 0 && _.isNumber(clampsUsed[0])) {
								// short circuit and use the default;
								return false;
							}
							that.collection.reset(clampsUsed);
						}
					});
				} else {
					var clampsUsed = this.model.get("ClampsUsed");
					if (clampsUsed.length > 0 && !_.isObject(clampsUsed[0])) {
						// short circuit and use the default;
						return false;
					}
                    else if(clampsUsed.length === 0){
                        return false;
                    }
                    
					this.collection.reset(clampsUsed);
				}
			},
			getCheckedClamps: function () {
				return this.collection.toJSON();
			},
			onRender: function () {
                var clampsUsed = this.model.get("ClampsUsed");
                var phaseClamps = _.filter(clampsUsed, function (clamp) {
					return clamp.phase === 3;
				});
                if(phaseClamps.length > 0){
                    this.hideNonFirstCheckedTextBoxes();
                }
               
			},
			_onCheckboxClicked: function () {
                var phase = $("#phasesDropdown .pull-left").text();
                if(phase === "3"){
                    var threePhaseClamps = _.filter(this.getCheckedClamps(), function (clamp) {
							return clamp.phase === 3;
					});
                    if(threePhaseClamps.length > 0){
                        this.hideNonFirstCheckedTextBoxes();
                    }
                    if (threePhaseClamps.length !== 3) {
                        this.trigger("invalid:clamps");
                    } else{
                        this.trigger("valid:clamps");
                    }

                   
                }else{
                     var hasCheckedPhaseClamps = _.filter(this.getCheckedClamps(), function (clamp) {
							return clamp.phase === 1;
					 });
                    if (hasCheckedPhaseClamps.length < 1) {
                        this.trigger("invalid:clamps1");
                    }
                    else{
                        this.trigger("valid:clamps1");
                    }
                }
			},
           
			unCheckAll: function () {
				this.children.each(function (childView) {
					childView.unCheck();
				});
			},
			setOnePhase: function () {
				//this.unCheckAll();
				this.hideCheckBoxes();
			},
			hideNonFirstCheckedTextBoxes: function () {
				var hasSeenChecked = false;
				this.children.each(function (childView) {
					if (childView.isChecked()) {
						if (!hasSeenChecked ) {
							hasSeenChecked = true;
							childView.showTextBox();
						} else {
							childView.hideTextBox();
						}
					} else {
						childView.showTextBox();
					}
				});
			},
			hideCheckBoxes: function () {
				this.children.each(function (childView) {
					childView.hideCheckBox();
				});
			},
			showCheckBoxes: function () {
				this.children.each(function (childView) {
					childView.showCheckBox();
				});
			},
            showTextBoxes: function () {
				this.children.each(function (childView) {
					childView.showTextBox();
				});
			}
		}).mixin([SalusView]);
	});

	return App.Consumer.SetupWizard.Provisioning.Views.EnergyMeterCustomContentView;
});