//Dev_SCS-3282 start
"use strict";

define([
	"app",
    "common/constants",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
    "consumer/views/SalusButtonPrimary.view"
], function (App, constants, consumerTemplates, SalusViewMixin, SalusPrimaryButtonView) {

	App.module("Consumer.SetupWizard.Provisioning.Views", function (Views, App, B, Mn, $, _) {
		Views.ProvisioningNameEquipmentModalView = Mn.LayoutView.extend({
            className: "modal-body",
			template: consumerTemplates["setupWizard/provisioning/provisioningNameEquipmentModal"],
			regions: {
				centerBtnRegion: "#bb-center-btn-area"
			},
            ui: {
				spinner: ".bb-spinner"
			},
			events: {
				"keydown": "handleEnterButtonPress"
			},
			initialize: function () {
				_.bindAll(this, "handleBackClick", "propertyPoll", "propertyPollTimeout", "startPropertyPoll", 
                "clearPropertyPollTimeout", "getAllBoilerReceiver", "checkBoilerReceiver");

				this.provisionBackBtn = new SalusPrimaryButtonView({
					id: "bb-provision-back-btn",
					classes: "width100",
					buttonTextKey: "common.labels.ok",
					clickedDelegate: this.handleBackClick
				});
			},
			handleBackClick: function () {
				App.hideModal();
                if(this.successDevice.length === 0){
                    window.history.back();
                    window.history.back();
                }
				
			},
			handleEnterButtonPress: function (event) {
				if (event.which && event.which === 13) {
					this.handleBackClick();
				}
			},
			onRender: function () {
               // $(".icon-large-close").addClass("hidden");
				this.$("#bb-content-label").text(App.translate("setupWizard.provisioning.naming.waiting"));
                
                //check euid for ervery device
                this.deviceModels = _.map(this.model ? this.model.pairingDevices : [], function (deviceObj) {
					return App.salusConnector.getDevice(deviceObj.dsn);
				});
                
                this.startPropertyPoll();
                
                
			},
            /**
			 * stop the spinner
			 */
			stopSpinner: function () {
				this.ui.spinner.detach();
			},
            
            propertyPoll: function(){
                var that = this;
                var device = this.deviceModels[this.count];
                var condition = null;
                
                device.getDeviceProperties(true).then(function () {
                    if(that.pollingInterval === null){
                        return;
                    }
                    condition = that.getEUIDValue(device);
                    
                    if (condition) {
                        if(device.getPropertyValue("EUID") === that.deviceModels[that.count].getPropertyValue("EUID")){
                            that.checkBoilerReceiver(device);
                            that.successDevice.push(device);
                            that.count ++;
                            if(that.count === that.deviceModels.length){
                                
                                that.clearPropertyPollTimeout();
                                                           
                            }
                        }

                    } 
                });
            },
            getAllBoilerReceiver: function(){
                var that = this;
                that.boilerArray = [];
                var devices = App.salusConnector.getDeviceCollection().models;
                _.each(devices, function (deviceModel) {
                    if(deviceModel.modelType === constants.modelTypes.IT600BOILERRECEIVER){
                        that.boilerArray.push(deviceModel);
                    }
                });
            },
            checkBoilerReceiver: function(singleDevice){
                var that = this;
                _.each(this.boilerArray, function (boiler) {
                    //var deviceModel = App.salusConnector.getDevice(singleDevice.device.key);
                    if(boiler.getPropertyValue("DeviceIndex") === singleDevice.getPropertyValue("DeviceIndex") &&
                        boiler.getEUID() !== singleDevice.getEUID()){
                        that.boilerRepeat = true;
                        //提示并且执行删除之前的Receiver的操作
                        boiler.unregister().then(function(){
                            that.boilerRepeat = true;
                            //"Please note this Boiler Receiver has replace old one, the old Boiler Receiver has been removed.";
                        });

                    }
                });
            },
            
            getEUIDValue: function(device){
                var condition = null;
                if(device.modelType === constants.modelTypes.ENERGYMETER){
                    condition = device && device.get("LeaveNetwork") && device.getPropertyValue("LeaveNetwork") !== 1 && 
                            device.get("ManufactureName") && device.getPropertyValue("ManufactureName").length === 4 &&
                            device.getPropertyValue("ManufactureName")[0] !== null && device.getPropertyValue("ManufactureName")[1] !== null &&
                            device.getPropertyValue("ManufactureName")[2] !== null && device.getPropertyValue("ManufactureName")[3] !== null;
                }
                else if(device.modelType === constants.modelTypes.FC600){
                    condition = device && device.get("EUID") && device.getPropertyValue("EUID") !== null &&
                            device.get("LeaveNetwork") && device.getPropertyValue("LeaveNetwork") !== 1 &&
                            device.get("CompleteSetup") && device.getPropertyValue("CompleteSetup") !== null;
                }
                else if(device.modelType === constants.modelTypes.IT600BOILERRECEIVER){
                    condition = device && device.get("EUID") && device.getPropertyValue("EUID") !== null &&
                            device.get("LeaveNetwork") && device.getPropertyValue("LeaveNetwork") !== 1 &&
                            device.get("DeviceIndex") && device.getPropertyValue("DeviceIndex") !== null;
                }
                else{
                    condition = device && device.get("EUID") && device.getPropertyValue("EUID") !== null &&
                            device.get("LeaveNetwork") && device.getPropertyValue("LeaveNetwork") !== 1;
                }
                return condition;
            },
            
            propertyPollTimeout: function(){
                var that = this;
                this.successDevice = [];
                this.boilerArray = [];
                this.boilerRepeat = false;
                //check every device euid,if the value is null then delete it from user;
                
                for(var i = 0; i < this.deviceModels.length; i ++){
                    var model = this.deviceModels[i];
                    var condition = this.getEUIDValue(model);
                    if(!condition){
                        model._deleteDevice().then(function(){
                        });
                    }else{
                        this.checkBoilerReceiver(model);
                        this.successDevice.push(model);
                    }
                }
                this.clearPropertyPollTimeout();
               
            },
            
            
			startPropertyPoll: function () {
                this.getAllBoilerReceiver();
                this.boilerRepeat = false;
                this.successDevice = [];
                this.count = 0;
				this.pollingInterval = setInterval(this.propertyPoll, 500);
                this.pollingTimeout = setTimeout(this.propertyPollTimeout, 1000 * 60 * 10);
			},
			clearPropertyPollTimeout: function () {
				clearInterval(this.pollingInterval);
                clearTimeout(this.pollingTimeout);
                this.pollingInterval = null;
                if(this.boilerRepeat){
                    //弹出提示框
                    this.centerBtnRegion.show(this.provisionBackBtn);
                    $(".bb-spinner").addClass("hidden");
                    $("#bb-content-label").text("Please note this Boiler Receiver has replace old one, the old Boiler Receiver has been removed.");
                }else if(this.successDevice.length === 0){
                    this.centerBtnRegion.show(this.provisionBackBtn);
                    $(".bb-spinner").addClass("hidden");
                    $("#bb-content-label").text(App.translate("setupWizard.provisioning.naming.failed"));
                }else{
                    App.hideModal();
                }
			},
            onBeforeDestroy: function () {
                this.clearPropertyPollTimeout();
            }
            
		}).mixin([SalusViewMixin]);
	});
	return App.Consumer.SetupWizard.Provisioning.Views.ProvisioningNameEquipmentModalView;
});

//Dev_SCS-3282 end