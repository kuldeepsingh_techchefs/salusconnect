"use strict";

define([
	"app",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView"
], function (App, consumerTemplates, SalusView) {

	App.module("Consumer.Views", function (Views, App, B, Mn, $) {

		Views.MainLayout = Mn.LayoutView.extend({
			template: consumerTemplates["base/mainLayout"],
			regions:{
				header: ".bb-region-header",
				body: ".bb-region-body",
				footer: ".bb-region-footer"
			},
			initialize: function () {
				// When iOS keyboard is open, change navbar to position:absolute to fix broken positioning
				// Reference: http://getbootstrap.com/getting-started/#support-fixed-position-keyboards
				if ('ontouchstart' in window) {
					var $body = $('body');

					$(document)
							.on('focus', 'input', function() {
								$body.addClass('keyboard-open');
							})
							.on('blur', 'input', function() {
								$body.removeClass('keyboard-open');
							});
				}
			}
		}).mixin([SalusView]);
	});

	return App.Consumer.Views.MainLayout;
});