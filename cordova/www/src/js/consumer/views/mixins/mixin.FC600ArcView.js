"use strict";

/**
 * The Thermostat Arc view mixin holds all of the common view logic for the temperature slider arc, thermostat mode menu,
 * frost point modal and hold modal.
 * Any view that's needs to show the LARGE version of the arc with these modals and menus should use this mixin.
 * intended to be mixed-in to the marionette view object and has a few basic responsibilities:
 * - hold similar thermostat arc level view code
 * - manage related menus and modals
 * - manage setting and getting and displaying Ayla properties for the thermostat
 */
define([
	"app",
	"bluebird",
	"common/config",
	"common/constants",
	"consumer/views/SalusModal.view",
	"consumer/dashboard/views/tileContentViews/thermostatSpecific/FC600ModeMenu.view",
	"consumer/equipment/views/HoldTempPointModal.view",
    "consumer/setupWizard/views/fancoil/FancoilSystem.view"
], function (App, P, config, constants, SalusModalView, ModeMenuModal, HoldTempPointModal) {

	App.module("Consumer.Views.Mixins", function (Mixins, App, B, Mn, $, _) {
		Mixins.FC600ArcView = function (/*options*/) {
			this.addToObj({
				events: {
					"click .bb-main-mode-icon": "toggleModeMenu",
					"click .bb-hold-mode-icon": "toggleHoldMenu",
					"click .bb-fan-mode-icon": "toggleFanMenu"
				},
				ui: {
					tstatArc: ".bb-tstat-arc",
					tstatDestinationTemp: ".bb-arc-set-point-temp",
					tstatCurrentTemp: ".bb-arc-current-temp",
					mainModeIcon: ".bb-main-mode-icon",
					holdModeIcon: ".bb-hold-mode-icon",
					fanModeIcon: ".bb-fan-mode-icon",
					ocupancyIcon: "bb-ocupancy-icon"
				}
			});

			// This needs to run after initialize because we can't overwrite the "initialize" method in setDefaults.
			this.after('initialize', function () {
				var that = this;

				// so many binds, but better safe than sorry
				_.bindAll(this,
					"handleDomLoadComplete",
					"toggleModeMenu",
					"toggleFanMenu",
					"toggleHoldMenu",
					"openModeMenu",
					"openFanMenu",
					"openHoldMenu",
					"closeModeMenu",
					"closeFanMenu",
					"closeHoldMenu",
					"_handleTempSliderDragContinuous",
					"_handleTempSliderDragEnd",
					"_getWhichSetPointToRead",
					"_showSetHoldPointModal",
					"_setTemperatureWithPollTimeOut"
				);

				this.setTemperatureIdlePollTimeout = null;
				this.model.allowChange = true;

				this.listenTo(this.model, "change:connection_status", function () {
					that.ui.tstatArc.roundSlider(that.model.isOnline() && !that.model.isLeaveNetwork() ? "enable": "disable");
				});
				
				// TODO update this for the non-flip case of equipment widget view
				this.listenTo(this, "tile:flipToFront", function () {
					that.closeModeMenu();
					that.closeFanMenu();
					that.closeHoldMenu();
				});

				// TODO update this for the non-flip case of equipment widget view
				this.listenTo(this, "tile:flipToBack", function () {
					if (!that.backTstatArcOK) {
						that.handleDomLoadComplete();
					}
				});
			});

			this.before("destroy", function () {
				//this.clearSetpointTimeout();
				if (!!this.ui.tstatArc  && this.ui.tstatArc.roundSlider) {
					this.ui.tstatArc.roundSlider("destroy");
				}
				
				this.ui.tstatArc.off();
				if (this.modeMenuView) {
					this.modeMenuView.destroy();
				}

				if (this.fanModeMenuView) {
					this.fanModeMenuView.destroy();
				}

				if (this.holdMenuView) {
					this.holdMenuView.destroy();
				}
			});

			this.after("render", function () {
				var that = this;
				
//				if (!this.model.isIT600) { // if optima
//					this.ui.holdModeIcon.detach();
//				} else {
//					this.ui.fanModeIcon.detach();
//					this.ui.mainModeIcon.addClass("default-cursor");
//				}

				this.addBinding(null, ".bb-arc-set-point-temp", {
					observe: "temporaryTemperatureValue", // This is the value we set temporarily as the user slides the arc slider before sending it off to the cooling or heating setpoint endpoint
					onGet: function (value /*, options*/) {
						var backRoundSliderInstance;

						value = value || config.thermostatMinimumTemp;

						if (!!this.ui.tstatArc) {
							backRoundSliderInstance = this.ui.tstatArc.data("roundSlider");
						}

						if (backRoundSliderInstance) {
							backRoundSliderInstance.setValue(value);
						}
						
						//99 is a special value, must be fix to MaxCoolSetpoint_x100.
						if (value === 99) {
							value = that.model.getPropertyValue("MaxCoolSetpoint_x100") / 100;
						}

						return value;
					}
				});

				this.addBinding(null, ".bb-arc-current-temp", {
					observe: "LocalTemperature_x100",
					onGet: function (value) {
						if (value) {
							return value.getProperty("value") ? value.getProperty("value") / 100 : config.thermostatMinimumTemp;
						}

						return config.thermostatMinimumTemp;
					}
				});

				this.addBinding(null, ".bb-main-mode-icon", {
					observe: ["SystemMode", "RunningMode"],
					onGet: function (props) {
						return [props[0] ? props[0].getProperty() : null, props[1] ? props[1].getProperty() : null];
					},
					update: function ($el, vals) {
						var modeClassName,
							classNameArray = [
								"power-icon",
								"auto-icon",
								"cooling-icon-on",
								"heating-icon-on",
								"cooling-icon-off",
								"heating-icon-off"
							];

						$el.removeClass(classNameArray.join(" "));

						var systemMode = vals[0],
							runningMode = vals[1];
						
						if (_.isNull(systemMode) || _.isNull(runningMode)) {
							modeClassName = classNameArray[0];
						} else {
							switch (systemMode) {
								case constants.thermostatModeTypes.OFF:
									modeClassName = classNameArray[0];
									break;
								case constants.thermostatModeTypes.AUTO:
									modeClassName = classNameArray[1];
									break;
								case constants.thermostatModeTypes.COOL:
									if (runningMode === constants.runningModeTypes.OFF) {
										modeClassName = classNameArray[4];
									} else if (runningMode === constants.runningModeTypes.COOL) {
										modeClassName = classNameArray[2];
									}
									break;
								case constants.thermostatModeTypes.HEAT:
								case constants.thermostatModeTypes.EMERGENCYHEATING:
									if (runningMode === constants.runningModeTypes.OFF) {
										modeClassName = classNameArray[5];
									} else if (runningMode === constants.runningModeTypes.HEAT) {
										modeClassName = classNameArray[3];
									}
									break;
								default:
									modeClassName = classNameArray[0];
									break;
							}
							
							var arcMin = that.ui.tstatArc.roundSlider("option", "min");
							var arcMax = that.ui.tstatArc.roundSlider("option", "max");
							var arcMin1 = arcMin;
							var arcMax1 = arcMax;
							if (systemMode === constants.thermostatModeTypes.COOL) {
								arcMin1 = that.model.getPropertyValue("MinCoolSetpoint_x100") / 100;
								arcMax1 = that.model.getPropertyValue("MaxCoolSetpoint_x100") / 100;
							} else if (systemMode === constants.thermostatModeTypes.HEAT) {
								arcMin1 = that.model.getPropertyValue("MinHeatSetpoint_x100") / 100;
								arcMax1 = that.model.getPropertyValue("MaxHeatSetpoint_x100") / 100;
							}
							if (arcMin !== arcMin1) {
								that.ui.tstatArc.roundSlider("option", "min", arcMin1);
							}
							if (arcMax !== arcMax1) {
								that.ui.tstatArc.roundSlider("option", "max", arcMax1);
							}
						}

						$el.addClass(modeClassName);
						
						if (modeClassName === "power-icon") {
							that.ui.tstatArc.roundSlider("disable");
						} else {
							that.ui.tstatArc.roundSlider("enable");
						}
					}
				});
				
				this.addBinding(null, ".bb-hold-mode-icon", {
					observe: "HoldType",
					onGet: function (prop) {
						return prop ? prop.getProperty() : null;
					},
					update: function ($el, val) {
						var modeClassName,
							classNameArray = [
								"hold-auto-icon", // follow schedule
								"hold-temporary-icon",
								"hold-permanent-icon",
								"hold-eco-icon",
								"power-icon"
							];

						$el.removeClass(classNameArray.join(" "));

						if (_.isNull(val)) {
							modeClassName = classNameArray[0];
						} else {
							switch (val) {
								case constants.fc600HoldTypes.FOLLOW:
									modeClassName = classNameArray[0];
									break;
								case constants.fc600HoldTypes.TEMPHOLD:
									modeClassName = classNameArray[1];
									break;
								case constants.fc600HoldTypes.PERMHOLD:
									modeClassName = classNameArray[2];
									break;
								case constants.fc600HoldTypes.ECO:
									modeClassName = classNameArray[3];
									break;
								case constants.fc600HoldTypes.OFF:
									modeClassName = classNameArray[4];
									break;
								default:
									modeClassName = classNameArray[0];
									break;
							}
						}

						$el.addClass(modeClassName);
						
						if (val === constants.fc600HoldTypes.OFF) {
							that.ui.tstatArc.roundSlider("disable");
							that.ui.mainModeIcon.css("visibility", "hidden");
							that.ui.fanModeIcon.css("visibility", "hidden");
						} else {
							if (val === constants.fc600HoldTypes.ECO) {
								that.ui.tstatArc.roundSlider("disable");
							} else {
								that.ui.tstatArc.roundSlider("enable");
							}
							that.ui.mainModeIcon.css("visibility", "visible");
							that.ui.fanModeIcon.css("visibility", "visible");
						}
					}
				});

				this.addBinding(null, ".bb-fan-mode-icon", {
					observe: "FanMode",
					onGet: function (prop) {
						return prop ? prop.getProperty() : null;
					},
					update: function ($el, val) {
						var modeClassName,
							classNameArray = [
								"fan-auto",
								"fan-speed3",
								"fan-speed2",
								"fan-speed1",
								"fan-off"
							];

						$el.removeClass(classNameArray.join(" "));

						if (_.isNull(val)) {
							modeClassName = classNameArray[0];
						} else {
							switch (val) {
								case constants.fanModeTypes.AUTO:
									modeClassName = classNameArray[0];
									break;
								case constants.fanModeTypes.SPEED3:
									modeClassName = classNameArray[1];
									break;
								case constants.fanModeTypes.SPEED2:
									modeClassName = classNameArray[2];
									break;
								case constants.fanModeTypes.SPEED1:
									modeClassName = classNameArray[3];
									break;
								case constants.fanModeTypes.OFF:
									modeClassName = classNameArray[4];
									break;
								default:
									modeClassName = classNameArray[0];
									break;
							}
						}

						$el.addClass(modeClassName);
					}
				});
				
				this.addBinding(null, ".bb-ocupancy-icon", {
					observe: ["S2ComTerminals", "Ocupancy"],
					onGet: function (props) {
						return [props[0] ? props[0].getProperty() : null, props[1] ? props[1].getProperty() : null];
					},
					update: function ($el, vals) {
						var modeClassName,
							classNameArray = [
								"occupied",
								"unoccupied"
							];

						$el.removeClass(classNameArray.join(" "));
						
						var s2ComTerminals = vals[0],
							ocupancy = vals[1];
						
						if (_.isNull(s2ComTerminals) || _.isNull(ocupancy)) {
							modeClassName = classNameArray[0];
						} else {
							switch (ocupancy) {
								case constants.ocupancy.OCCUPIED:
									modeClassName = classNameArray[0];
									break;
								case constants.ocupancy.UNOCCUPIED:
									modeClassName = classNameArray[1];
									break;
								default:
									modeClassName = classNameArray[0];
									break;
							}
						}
						$el.addClass(modeClassName);
						
						if (s2ComTerminals === 1) {
							$el.css("visibility", "visible");
						} else {
							$el.css("visibility", "hidden");
						}
					}
				});
					
			});

			this.setDefaults({
				/**
				 * gets called when the DOM load is complete
				 */
				handleDomLoadComplete: function () {
					var that = this,
						arcLocalTempToDisplay,
						tstatOuterWidth;

					this.model.getDevicePropertiesPromise().then(function () {
						if (!that.isDestroyed) {
							var whichSetPoint = that._getWhichSetPointToRead();

							// temporaryTemperatureValue is local only, we don't need to getProperty() it
							if (whichSetPoint.indexOf("temporary") > -1) {
								arcLocalTempToDisplay = that.model.get(whichSetPoint);
							} else if (that.model.get(whichSetPoint)) {
								arcLocalTempToDisplay = that.model.getPropertyValue(that._getWhichSetPointToRead()) / 100;
								that.model.set("temporaryTemperatureValue", arcLocalTempToDisplay || config.thermostatMinimumTemp);
							}

							if (!!that.ui.tstatArc) {
								tstatOuterWidth = that.ui.tstatArc.innerWidth();

								that.ui.tstatArc.roundSlider({
									sliderType: "min-range",
									handleShape: "round",
									circleShape: "pie",
									handleSize: "+20",
									radius: tstatOuterWidth || 1,
									min: that.model.getPropertyValue("MinCoolSetpoint_x100") / 100 || config.thermostatMinimumTemp,
									max: that.model.getPropertyValue("MaxHeatSetpoint_x100") / 100 || config.thermostatMaximumTemp,
									step: 0.5,
									value: arcLocalTempToDisplay || config.thermostatMinimumTemp,
									startAngle: 315,
									width: 10, // Value came from measuring the thermostat arc specs
									showTooltip: false,
									animation: true
								});
								
								that.backTstatArcOK = true;
								
								if (!that.model.isOnline() || that.model.isLeaveNetwork() || 
									that.model.getPropertyValue("HoldType") === constants.fc600HoldTypes.OFF ||
									that.model.getPropertyValue("HoldType") === constants.fc600HoldTypes.ECO)
								{
									that.ui.tstatArc.roundSlider("disable");
								} else {
									that.ui.tstatArc.roundSlider("enable");
								}
                                
								that.ui.tstatArc.on("start", function (event) {
                                    if(that.model.get("oem_model") === constants.modelTypes.FC600 &&
                                        that.model.getPropertyValue("CompleteSetup") !== 15){
                                        that.showSetupModal = true;
                                        var nextView = new App.Consumer.SetupWizard.Fancoil.Views.FancoilSystemLayout({
                                            model: that.model
                                        });
                                        App.modalRegion.show(new SalusModalView({
                                             contentView: nextView,
                                             size: "modal-lg"
                                        }));

                                        App.showModal();  
                                    }else{
                                        that._dragging = true;
                                        that._handleTempSliderDragContinuous(event);
                                    }
									
								});

								// Update the set point value during a slider drag or change (change occurs when tapping anywhere on the arc)
								that.ui.tstatArc.on("drag change", function (event) {
                                    if(!that.showSetupModal){
                                        if(that.model.get("oem_model") === constants.modelTypes.FC600 &&
                                            that.model.getPropertyValue("CompleteSetup") !== 15){
                                            var nextView = new App.Consumer.SetupWizard.Fancoil.Views.FancoilSystemLayout({
                                                model: that.model
                                            });
                                            App.modalRegion.show(new SalusModalView({
                                                 contentView: nextView,
                                                 size: "modal-lg"
                                            }));

                                            App.showModal();  
                                        }else{
                                            if(that.dragging)
                                            that._handleTempSliderDragContinuous(event);

                                            // if we've clicked the dial act like we dragged it.
                                            if (event.type === "change" && !that.dragging) {
                                                that._handleTempSliderDragEnd();
                                            }
                                        }
                                    }
                                    
								});

								// Remove the animation class applied to the set point font to make it grow
								that.ui.tstatArc.on("stop", function () {
                                    if(!(that.model.get("oem_model") === constants.modelTypes.FC600 &&
                                            that.model.getPropertyValue("CompleteSetup") !== 15)){
                                        that._dragging = false;
                                        that._handleTempSliderDragEnd();
                                    }
									
								});
							}

							that.modeMenuView = new ModeMenuModal.MenuContainer({
								model: that.model
							});

							that.listenTo(that.modeMenuView, "close:menu", that.closeModeMenu);
							that.$el.append(that.modeMenuView.render().$el);
							that.closeModeMenu();
							
							that.holdMenuView = new ModeMenuModal.MenuContainer({
								model: that.model,
								holdMenu: true
							});
							that.listenTo(that.holdMenuView, "close:menu", that.closeHoldMenu);
							that.$el.append(that.holdMenuView.render().$el);
							that.closeHoldMenu();

							that.fanModeMenuView = new ModeMenuModal.MenuContainer({
								model: that.model,
								fanMenu: true
							});
							that.listenTo(that.fanModeMenuView, "close:menu", that.closeFanMenu);
							that.$el.append(that.fanModeMenuView.render().$el);
							that.closeFanMenu();
						}

					});
				},

				toggleModeMenu: function () {
					if (!this.noMixinEvents && this.model.isOnline() && !this.model.isLeaveNetwork()) {
                        if(this.model.get("oem_model") === constants.modelTypes.FC600 &&
                            this.model.getPropertyValue("CompleteSetup") !== 15){
                            var nextView = new App.Consumer.SetupWizard.Fancoil.Views.FancoilSystemLayout({
                                model: this.model
                            });
                            App.modalRegion.show(new SalusModalView({
                                 contentView: nextView,
                                 size: "modal-lg"
                            }));

                            App.showModal();  

                         }else{
                            if ((this.model.getPropertyValue("FanCoilType") === 0 && 
                                this.model.getPropertyValue("S1ComTerminals") === 0 &&
                                this.model.getPropertyValue("HeatCoolSelection") === 0) ||
                                (this.model.getPropertyValue("FanCoilType") === 1 && 
                                this.model.getPropertyValue("S1ComTerminals") === 0)) {
                                //close others
                                this.closeFanMenu();
                                this.closeHoldMenu();

                                if (this.modeMenuView.$el.is(":visible")) {
                                    this.closeModeMenu();
                                } else {
                                    this.openModeMenu();
                                }
                            }
                        }
                    }
				},

				closeModeMenu: function () {
					if (this.modeMenuView) {
						this.modeMenuView.$el.toggle(false);
					}
				},

				openModeMenu: function () {
					if (this.modeMenuView) {
						this.modeMenuView.$el.toggle(true);
					}
				},

				toggleFanMenu: function () {
					if (!this.noMixinEvents && this.model.isOnline() && !this.model.isLeaveNetwork()) {
                        if(this.model.get("oem_model") === constants.modelTypes.FC600 &&
                            this.model.getPropertyValue("CompleteSetup") !== 15){
                            var nextView = new App.Consumer.SetupWizard.Fancoil.Views.FancoilSystemLayout({
                                model: this.model
                            });
                            App.modalRegion.show(new SalusModalView({
                                 contentView: nextView,
                                 size: "modal-lg"
                            }));

                            App.showModal();  

                         }else{ 
						// close others
                            this.closeModeMenu();
                            this.closeHoldMenu();

                            if (this.fanModeMenuView.$el.is(":visible")) {
                                this.closeFanMenu();
                            } else {
                                this.openFanMenu();
                            }
                        }
                    }
				},

				closeFanMenu: function () {
					if (this.fanModeMenuView) {
						this.fanModeMenuView.$el.toggle(false);
					}
				},

				openFanMenu: function () {
					if (this.fanModeMenuView) {
						this.fanModeMenuView.$el.toggle(true);
					}
				},

				toggleHoldMenu: function () {
                    if (!this.noMixinEvents && this.model.isOnline() && !this.model.isLeaveNetwork()) {
                        if(this.model.get("oem_model") === constants.modelTypes.FC600 &&
                            this.model.getPropertyValue("CompleteSetup") !== 15){
                            var nextView = new App.Consumer.SetupWizard.Fancoil.Views.FancoilSystemLayout({
                                model: this.model
                            });
                            App.modalRegion.show(new SalusModalView({
                                 contentView: nextView,
                                 size: "modal-lg"
                            }));

                            App.showModal();  

                         }else{
                            this.closeModeMenu();
                            this.closeFanMenu();

                            if (this.holdMenuView.$el.is(":visible")) {
                                this.closeHoldMenu();
                            } else {
								if (this.model.getPropertyValue("S2ComTerminals") === 1) {
									this.$el.find(".hold-eco-color").addClass("disabled");
								} else {
									this.$el.find(".hold-eco-color").removeClass("disabled");
								}
                                this.openHoldMenu();
                            }
                        }
                    }
                   
					
				},

				closeHoldMenu: function () {
					if (this.holdMenuView) {
						this.holdMenuView.$el.toggle(false);
					}
				},

				openHoldMenu: function () {
					if (this.holdMenuView) {
						this.holdMenuView.$el.toggle(true);
					}
				},

				/**
				 * This updates the set point value as the slider is dragged
				 * @param event
				 * @private
				 */
				_handleTempSliderDragContinuous: function (event) {
					// Add an animation class to the set point font so it grows when this starts updating
                    this.ui.tstatDestinationTemp.addClass("updating");

                    // close any menus
                    this.closeFanMenu();
                    this.closeHoldMenu();
                    this.closeModeMenu();

                    if (event.value) {
                        this.model.set("temporaryTemperatureValue", event.value);
                    }
                    
				},
				_handleTempSliderDragEnd: function () {
                    
                    var $arcSpinnerElement = _.template("<div class='bb-arc-spinner spinner-dynamic-arc-interior'></div>")();

                    this.ui.tstatDestinationTemp.removeClass("updating");
                    // TODO wire this so it blocks the action of sending data to Ayla until the user submits or cancels this modal, like frost points modal
                    // check if 'dont show this again' is checked
                    // TODO Commenting out, tasked to remove later, SCS-1261
                    //if (!this.model.get("hide_set_hold_point_modal")) {
                    //	this._showSetHoldPointModal();
                    //}

                    if (this.ui.tstatArc.find(".bb-arc-spinner").length === 0) {
                        this.ui.tstatArc.find("span.rs-block").prepend($arcSpinnerElement);
                        //this.showSmallTileSpinner();
                    }

                    this._setTemperatureWithPollTimeOut();
                    
                    
					
				},
				_setTemperatureWithPollTimeOut: function () {
					var modelPropString = this._getWhichSetPointToRead(),
							modelProp = this.model.get(modelPropString);

					if (modelProp) {
						var that = this;
						// If the user keeps dragging the thermostat arc without waiting the three seconds, then keep resetting
						// the set thermostat timeout so we don't write the value to Ayla until the user is really done.
						// The Optima zigbee thermostat manual states the hardware waits 3 seconds after the user changes the
						// temperature before it writes it to Ayla. We are replicating this functionality.
						if (this.setTemperatureIdlePollTimeout) {
							clearTimeout(this.setTemperatureIdlePollTimeout);
							this.setTemperatureIdlePollTimeout = null;
						}

						that.model.allowChange = false;
						this.setTemperatureIdlePollTimeout = setTimeout(function () {
							// Once the property is propagated to the getter property, remove the loading spinner
							that.listenToOnce(modelProp, "propertiesSynced", function () {
								that.model.allowChange = true;
								//that.hideSmallTileSpinner();
							});

							that.listenToOnce(modelProp, "propertiesFailedSync", function () {
								that.model.allowChange = true;
								that.ui.tstatArc.find(".bb-arc-spinner").remove();
								App.Consumer.Controller.showDeviceSyncError(that.model);
							});

							if (modelProp) {
								modelProp.setProperty(that.model.get("temporaryTemperatureValue") * 100).then(function () {
									that.ui.tstatArc.find(".bb-arc-spinner").remove();
								});
							} else {
								// TODO
								App.warn("TODO: figure out which temp property to set");
							}
						}, config.thermostatSetPointDelay);
					}
				},
				/**
				 * Figure out which setpoint value to read based on the currently set mode, cooling or heating
				 * @returns {*}
				 * @private
				 */
				_getWhichSetPointToRead: function () {
					var temperatureEndpoint, systemMode = this.model.get("SystemMode");

					if (systemMode) {
						switch (systemMode.getProperty()) {
							case constants.thermostatModeTypes.EMERGENCYHEATING:
							case constants.thermostatModeTypes.HEAT:
								temperatureEndpoint = "HeatingSetpoint_x100";
								break;
							case constants.thermostatModeTypes.COOL:
								temperatureEndpoint = "CoolingSetpoint_x100";
								break;
							default:
								temperatureEndpoint = "temporaryTemperatureValue";
								break;
						}
					} else {
						temperatureEndpoint = "temporaryTemperatureValue";
					}

					return temperatureEndpoint;
				},
				_showSetHoldPointModal: function () {
					var holdTempPointModalView = new HoldTempPointModal({model: this.model}),
						salusModalView = new SalusModalView({
							contentView: holdTempPointModalView
						});

					this.listenToOnce(holdTempPointModalView, "modal:closed", function () {
						App.hideModal();
					});

					App.modalRegion.show(salusModalView);
					App.showModal();
				}
			});
		};
	});

	return App.Consumer.Views.Mixins.FC600ArcView;
});