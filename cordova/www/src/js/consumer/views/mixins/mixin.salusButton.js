"use strict";

/**
 * The salus button is the is for creating user controls (we may need some other kinds of user controls at some point).
 * This will allow for standard hooks around user interactions.
 * - hold boilerplate code
 * - manage analytics integration
 */
define([
	"app",
	"underscore",
	"consumer/views/mixins/mixin.salusView",
	"common/util/AnalyticsWrapper"
], function (App, _, SalusView, AnalyticsWrapper) {

	App.module("Consumer.Views.Mixins", function (Mixins) {
		Mixins.SalusButton = function (options) {
			this.mixin([
				SalusView // by mixing in SalusButton we also mixin SalusView
			], options);

			this.after("initialize", function (viewOptions) {
				_.bindAll(this, "clicked");

				viewOptions = viewOptions || {};
				this.$el.addClass(viewOptions.classes || "");
				this.buttonText = viewOptions.buttonText || "";
				this.buttonTextKey = viewOptions.buttonTextKey || "missingKey"; //What to default this too.
				this.clickedDelegate = viewOptions.clickedDelegate;

				this._eventName = this[options.eventNameKey];
				this._triggerName = options.eventName;

				this.listenTo(this, this._triggerName, this._triggerAnalyticsEvent);
			});

			this.addToObj({
				events: {
					'click': 'clicked'
				}
			});

			this.setDefaults({
				clicked: function (e) {
					if (this.isEnabled()) {
						this.trigger("click", this);
						if (this.clickedDelegate) {
							this.clickedDelegate(e, this);
						}
					}
				},

				isEnabled: function () {
					return !this.$el.hasClass("disabled");
				},

				enable: function () {
					this.$el.removeClass("disabled");
				},

				disable: function () {
					this.$el.addClass("disabled");
				},

				_triggerAnalyticsEvent: function () {
					var pgAnalitycs = App.Consumer.Controller.getPageAnalytics();

					if (pgAnalitycs) {
						AnalyticsWrapper.onInteraction(pgAnalitycs.section, pgAnalitycs.page, this._eventName, this.buttonTextKey || "");
					}
				}
			});
		};
	});

	return App.Consumer.Views.Mixins.SalusButton;
});
