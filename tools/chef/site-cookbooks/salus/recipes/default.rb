cookbookName = File.basename(File.dirname(File.dirname(__FILE__)),"/")
recipeName = File.basename(__FILE__, ".rb")
log "Running #{cookbookName} #{recipeName} customizations!"

log "Apt-get update....."
execute 'Apt-get update' do
  command 'sudo apt-get update'
  not_if("which vim")
end

log "Installing vim."
execute 'Install vim' do
  command 'sudo apt-get -y install vim'
  not_if("which vim")
end

log "Configuring Node modules....."

log "gulp"
nodejs_npm "gulp" do
  version "3.9.0"
end
log "gulp-util"
nodejs_npm "gulp-util"

log "Done Configuring Node modules."

#Needed for nginx
execute 'Installing nginx.' do
  command 'apt-get install -y nginx'
  not_if("which nginx")
end

directory "/var/log" do
  group "vagrant"
  mode "775"
  action :create
end

directory "/var/log/nginx" do
  group "vagrant"
  mode "775"
  action :create
end

directory "/etc/nginx/ssl" do
  group "vagrant"
  mode "775"
  action :create
end

file "/etc/nginx/sites-enabled/default" do
  action :delete
end

file "/etc/nginx/sites-available/localhost-salus-connected.digitalfoundry.com" do
  owner 'root'
  group 'root'
  mode 0755
  content ::File.open("/vagrant/server/nginx/localhost-salus-connected.digitalfoundry.com").read
  action :create
end

link "/etc/nginx/sites-enabled/localhost-salus-connected.digitalfoundry.com" do
  to "/etc/nginx/sites-available/localhost-salus-connected.digitalfoundry.com"
  action :create
end

service "nginx" do
	action :restart
end

#Adding /vagrant/node_modules/.bin to path
execute 'Creating .bash_profile' do
  command 'echo "source ~/.profile\nsource ~/.bashrc\ncd /vagrant\n\n" > /home/vagrant/.bash_profile'
  #if [ -d \"/vagrant/node_modules/.bin\" ] ; then\nPATH=\"/vagrant/node_modules/.bin:\$PATH\"\nfi\n" > /home/vagrant/.bash_profile'
  not_if { File.exists?("/home/vagrant/.bash_profile") }
end

