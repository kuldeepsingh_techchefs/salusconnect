#!/usr/bin/python

import os, hashlib, shutil, glob, fileinput, re, sys

from subprocess import Popen

def _getDirs(base):
	return [x for x in glob.iglob(os.path.join( base, '*')) if os.path.isdir(x) ]

#Glob function that will find all files with a pattern in all subdirectories
def rglob(base, pattern):
	flist = []

	flist.extend(glob.glob(os.path.join(base, pattern)))
	dirs = _getDirs(base)

	if len(dirs):
		for d in dirs:
			subglob = rglob(d, pattern)
			if len(subglob):
				flist.extend(subglob)

	return flist

#Generates hashes for a list of files and replaces the reference to that file
def generateHashForFiles(files, filesToReplace):
	for file in files:
		hashedFilename =  hashFileName(file)

		#Only rename and replace refeourences if the filename has changed
		if hashedFilename != os.path.basename(file):
			newPath = os.path.dirname(file) + "/" + hashedFilename
			print file + " > " + newPath
			shutil.move(file, newPath)

			for fileToReplace in filesToReplace:
				findAndReplaceHashReferences(file.replace('web/output', ''), newPath.replace('web/output', ''), rglob('web/output', fileToReplace))

#Find references to the original filename and replaces it with the hashedFilename in a list of files
def findAndReplaceHashReferences(originalFilepath, hashedFilepath, files):
	for file in files:
		for line in fileinput.input(file, inplace=1):
			if originalFilepath in line:
				line = line.replace('"'+originalFilepath+'"', '"'+hashedFilepath+'"')
				line = line.replace("'"+originalFilepath+"'", "'"+hashedFilepath+"'")
				line = line.replace("("+originalFilepath+")", "("+hashedFilepath+")")
				line = line.replace("="+originalFilepath, "="+hashedFilepath)
			sys.stdout.write(line)

#Generates a hash for a specific file and returns the new filename
def hashFileName(file):
	hasher = hashlib.md5()
	with open(file, 'rb') as hashFile:
		buf = hashFile.read()
		hasher.update(buf)

	hashFile.close()

	fileExt = os.path.splitext(file)[1]
	hashedFilename = hasher.hexdigest() + fileExt

	return os.path.splitext(os.path.basename(file))[0] + "_" + hashedFilename

#Hash all files in output subdirectories and replace references in all html and css files
generateHashForFiles(rglob('web/output/css', '*.*'), ['*.html', '*.css', '*.js'])
generateHashForFiles(rglob('web/output/images', '*.*'), ['*.html', '*.css', '*.js'])
generateHashForFiles(rglob('web/output/fonts', '*.*'), ['*.html', '*.css', '*.js'])
generateHashForFiles(rglob('web/output/js', '*.*'), ['*.html', '*.css', '*.js'])

#Running a second time to rehash modified css and js
#generateHashForFiles(rglob('output/*', '*.*'), ['*.html', '*.css', '*.js'])