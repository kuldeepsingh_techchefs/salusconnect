"use strict";

define([
	"app",
	"jquery",
	"underscore",
	"backbone.marionette",
	"consumer/views/AuthenticatingPage.view"
], function (App, $, _, Mn) {

	var unauthenticatedRoutingControllers = [
		"login",
		"newUser",
		"pinpad",
        "privacyPolicy",
        "termsAndConditions",
        "aboutApp"
	];

	var isUnauthenticatedRoutingController = function (controllerName) {
		return _.contains(unauthenticatedRoutingControllers, controllerName);
	};

	var Router = Mn.AppRouter.extend({
		execute: function (callback, args, route) {
			var isUnauth = isUnauthenticatedRoutingController(route);

			if ((App.getDataCollectionOff() && route !== "dataCollection" && App.salusConnector.isLoggedIn())) {
				App.navigate("settings/dataCollection");
			} else {

				if (App.salusConnector.isLoggedIn()) {
					if (isUnauth && route !== "privacyPolicy" && route !== "termsAndConditions" && route !== "aboutApp") {
						App.navigate("dashboard", { replace: true });
					} else {
						if (callback) {
							callback.apply(this, args);
						}
					}
				} else if (!App.salusConnector.hasStoredToken()) {
					if (isUnauth) {
						if (callback) {
							callback.apply(this, args);
						}
					} else {
						App.navigate("login", { replace: true });
						return false;
					}
				} else if (callback) {
					App.mainRegion.show(new App.Consumer.Views.AuthenticatingPageView({
						router: this,
						routeCallBack: callback,
						routeArgs: args,
						isUnauthenticatedRoute: isUnauth
					}));
				}
			}
		}
	});

	return Router;
});