"use strict";

define([
	"app"
], function (App) {


	App.module("Models", function (Models, App, B) {
		var AlertFakeModel = B.Model.extend({
			defaults: {
				alerting_criteria: null,
				status: [],
				details: [],
				name: null,
				deviceIdRef: null,
				dateTime: null,
				alarmSounding: false
			},

			initialize: function () {
				// TODO remove when have the concept of alerts from either ayla or mike
				this.set("status", [
					{
						iconClass: "status-red",
						statusText: "Alarm Sounding"
					},
					{
						iconClass: "status-red",
						statusText: "Window Open"
					}
				]);

				this.set("details", [
					{
						dateTime: "2:45pm",
						detailText: "Sensor detected window open; alarm fired"
					},
					{
						dateTime: "2:45pm",
						detailText: "Sent 415-555-4589 text regarding lost communication"
					}
				]);
			}
		});

		Models.AylaAlertCollection = B.Collection.extend({
			model: AlertFakeModel
		});
	});

	return App.Models.AylaAlertCollection;
});