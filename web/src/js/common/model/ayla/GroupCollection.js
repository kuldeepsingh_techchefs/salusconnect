"use strict";

define([
    "app",
    "bluebird",
    "common/constants",
    "common/AylaConfig",
	"common/util/utilities",
    "common/model/ayla/mixin.AylaBacked",
    "common/model/ayla/Group.model"
], function (App, P, constants, AylaConfig, utilities, AylaBackedMixin, GroupModel) {

    App.module("Models", function (Models, App, B, Mn, $, _) {
        Models.GroupCollection = B.Collection.extend({
            model: GroupModel,
            initialize: function (data, options) {
                _.bindAll(this, "_loadEachGroup", "filterByCurrentGateway", "_onGroupAdd");

                options = options || {};

                this.gatewayDSN = options.gatwayDSN;

                if (options.gatewayDSN) {
                    this.listenTo(App.salusConnector.getFullGroupCollection(), "add", this._onGroupAdd);
                }
            },
            load: function (isLowPriority) {
                var that = this;

                // TODO figure out the underlying problem with using backbone fetch; see SCS-993
                return App.salusConnector.makeAjaxCall(AylaConfig.endpoints.groups.list, null, "GET", null, {isLowPriority: isLowPriority}).then(function (data) {
                    var groupsMd5New = utilities.md5HashString(JSON.stringify(data));
					App.salusConnector.getSessionUser().set("groupsMd5New", groupsMd5New);
					if (_.isNull(App.salusConnector.getSessionUser().get("groupsMd5"))) {
						App.salusConnector.getSessionUser().set("groupsMd5", groupsMd5New);
					}
					
					var mappedData = _.map(data, function (groupData) {
                        return groupData.group;
                    });
                    that.set(mappedData, {parse: true});
                }).then(function () {
                    return that._loadEachGroup(isLowPriority);
                }).then(this._getResolvedPromise);
            },
            refresh: function (isLowPriority) {
                var promise = this.load(isLowPriority);
                App.salusConnector.setDataLoadPromise("groups", promise);

                return promise;
            },
            isLoaded: function () {
                return !!this.length; //todo
            },
            /**
             * returns a collection containing devices not found in the argument collection
             * @param devCollec deviceCollection to compare with groups
             * @returns {Backbone.Collection}
             */
            getDevicesWithoutGroupFrom: function (devCollec) {
                var outArray = [],
                        groupArray = this.toArray();
                devCollec.each(function (device) {
                    var isFound = false;

                    for (var i = 0; i < groupArray.length && !isFound; i++) {
                        if (groupArray[i].hasDevice(device.get('key'))) {
                            isFound = true;
                        }
                    }

                    if (!isFound) {
                        outArray.push(device);
                    }
                });

                return new B.Collection(outArray);
            },
            /**
             * filter the group collection by looking into the first device's gateway dsn
             * @returns {Models.GroupCollection}
             */
            filterByCurrentGateway: function (shouldReturnArray) {
                var filteredArray = this.filter(function (group) {
                    var devices = group.get("devices"),
                            firstDevice = App.salusConnector.getDevice(devices[0]);

                    // ambiguous group
                    if (!firstDevice) {
                        return true;
                    }

                    // first one might be a gateway
                    if (firstDevice.modelType === constants.modelTypes.GATEWAY) {
                        return firstDevice.get("dsn") === App.getCurrentGatewayDSN();
                    } else {
                        return firstDevice.get("gateway_dsn") === App.getCurrentGatewayDSN();
                    }
                });

                if (shouldReturnArray) {
                    return filteredArray;
                }

                return new Models.GroupCollection(filteredArray, {gatewayDSN: App.getCurrentGatewayDSN()});
            },
			filterByNotTileOrderGroup: function() {
				var that = this;

                var toKeep = this.filter(function(group) {
                  if (group && group.get("name").indexOf(constants.tileOrderGroupName) === -1) {
                        return group;
                    }
                });

                this.reset(toKeep);
				return this;
			},
			getTileOrderGroup: function() {
				var tileOrderGroup = null;
				this.map(function (group) {
					if (group.get("name").indexOf(constants.tileOrderGroupName) !== -1) {
						tileOrderGroup = group;
					}
                });
				
				return tileOrderGroup;
			},
			createTileOrderGroup: function() {
				var deviceKeys = [];
				var devices = App.salusConnector.getDeviceCollection(true);
				devices.map(function (device) {
					deviceKeys.push(device.get("key")); 
                });
				
				if (deviceKeys.length === 0) {
					return P.Promise.resolve();
				}
				
				var device = App.salusConnector.getDevice(deviceKeys[0]);
				var dsn = device.get("dsn") || device.get("gateway_dsn");
				var name = constants.tileOrderGroupName + dsn;
				App.salusConnector.getFullGroupCollection().map(function(group) {
					if (name === group.get("name")) {
						group.unregister().then(function () {
							group.destroy();
						});
					}
				});
				
				var groupModel = new App.Models.GroupModel({
					name: name,
					devices: deviceKeys
				});

				return groupModel.add();
			},
			loadTileOrderGroupDatapoints: function() {
				var groupId = App.showGroupId;
				if (groupId === null) {
					var tileOrderGroup = App.salusConnector.getGroupCollection().getTileOrderGroup();
					if (tileOrderGroup === null) {
						return App.salusConnector.getDeviceCollection(true).loadDetails(true);
					}
					groupId = tileOrderGroup.get("key");
					if (tileOrderGroup.get("device_count") === 0) {
						return P.Promise.resolve();
					}
				}
				
				var query_string_params = "";
				_.each(constants.tileOrderGroupProperties, function(property) {
					if (query_string_params === "") {
						query_string_params += "?property_names[]=" + property;
					} else {
						query_string_params += "&property_names[]=" + property;
					}
				});
				var url = _.template(AylaConfig.endpoints.groups.getDatapoints)({key: groupId, query_string_params: query_string_params});
				
				return App.salusConnector.makeAjaxGet(url, null).then(function (data) {
					data.datapoints.devices.device.map(function(device) {
						if (device.id !== App.showDeviceId) {
							var dev = App.salusConnector.getDevice(device.id);
							device.properties.property.map(function(property) {
								var propertyName = property.name.split(":")[2];
								var identifier = property.name.split(":")[0];
								identifier.replace("ep_", "");
								var prop = dev.get(propertyName);
								if (prop && prop.getProperty() !== property.value) {
									if (prop.length === 1) {
										prop.first().set("value", property.value);
									} else {
										prop.set("value", property.value);
									}
								}
							});
						}
					});
				}).catch(function() {
                    return P.Promise.resolve();
                });
				
			},
            _buildFetchArray: function (isLowPriority) {
                return this.map(function (group) {
                    return group.refresh(isLowPriority);
                });
            },
            _loadEachGroup: function (isLowPriority) {
                return P.Promise.all(this._buildFetchArray(isLowPriority));
            },
            _onGroupAdd: function (device) {
                if (device.get("gateway_dsn") === this.gatewayDSN || device.isGateway() && device.get("dsn") === this.gatewayDSN) {
                    this.add(device);
                }
            },
            getGroupsByDevice: function (device) {
                var key = parseInt(device.get("key"));
                var groups = [];
                this.some(function (group) {
                    var res = _.contains(group.get("devices"), key);
                    if (res) {
                        groups.push(group);
                    }
                });
                return groups;
            }

        }).mixin([AylaBackedMixin], {
            apiWrapperObjectName: "group"
        });
    });

    return App.Models.GroupCollection;
});