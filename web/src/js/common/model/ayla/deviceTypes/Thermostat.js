"use strict";

define([
	"app",
	"common/constants",
	"common/AylaConfig",
	"common/model/ayla/deviceTypes/mixin.BaseDevice",
	"common/model/ayla/mixin.AylaSchedule",
	"common/model/ayla/deviceTypes/mixin.thermostats"
], function (App, constants, AylaConfig, BaseAylaDevice) {

	App.module("Models", function (Models, App, B) {
		/**
		 * this is a wrapper for testing ayla base device. it allows us to construct one and run unit tests and such
		 */
		Models.Thermostat = B.Model.extend({
			tileType: "thermostat",
			modelType: constants.modelTypes.THERMOSTAT,
			_metaDataProperties: [
				"hide_set_frost_point_modal",
				"hide_set_hold_point_modal"
			],
			_unsupportedProps: [ // per confluence https://salusinc.atlassian.net/wiki/display/CI/Thermostat
				"MainsVoltage_x10", "BatteryVoltage_x10", "ErrorPowerSLowBattery", "MeasuredValue_x100",
				"MeasuredValue_x100", "Hold", "HoldDuration", "OperationMode", "RunningState",
				"ErrorTherSTempSensor", "ErrorTherSCompressor"
			],
			defaults: {
				// get properties
				AutoCoolingSetpoint_x100: null, // integer, Temp = data / 100, in Celsius
				AutoHeatingSetpoint_x100: null, // integer, Temp = data / 100, in Celsius
				CoolingFanDelay: null,
				CoolingSetpoint_x100: null, // integer, Temp = data / 100, in Celsius
				DeviceType: null, //integer 769: Thermostat
				ErrorTherSCompressor: null, // boolean, compressor failure, not supported by iT530
				ErrorTherSTempSensor: null, // boolean, RoomTemperatureSensor Failure,  not supported by iT530
				FanMode: null, // integer, 4 = On, 5 = Auto, not supported by iT530
				FirmwareVersion: null, //string
				HeatingSetpoint_x100: null, //integer Temp = data / 100, in Celsius
				Hold: null, // boolean, hold status
				HoldDuration: null, // integer, 0 - 1440, 65535 (0xffff) means Permanent Hold (in minutes)
				KeyPadLockout: null, // integer, 0: No lockout, 1 - 5: Level 1 to 5
				LocalTemperature_x100: null, //integer
				MACAddress: null, //string length 16
				ManufactureName: null, //string length 32
				MaxCoolSetPoint_x100: null, // integer, Temp = data / 100, in Celsius
				MaxHeatSetPoint_x100: null, // integer, Temp = data / 100, in Celsius
				MeasuredValue_x100: null, // integer, Temp = data / 100, in Celsius (not used)
				MinCoolSetPoint_x100: null, // integer, Temp = data / 100, in Celsius
				MinHeatSetPoint_x100: null, // integer, Temp = data / 100, in Celsius
				ModelIdentifier: null, //string length 32
				// integer enum ->
				//	Bit 2: 0 Economy Off, 1 Economy On
				//	Bit 1: 0 Autorecovery Off, 1 Auto recovery On
				//	Bit 0: non-schedule, 1 - Schedule
				//	(iT530 not support)
				OperationMode: null,
				PowerSource: null, // integer, 1: AC Mains, 3: Battery
				RunningMode: null, // integer, 0: Off, 3: Cool, 4: Heat
				// integer enum ->
				// Bit 7: Heat 3rd Stage State On
				// Bit 4: Cool 2nd Stage State
				// Bit 3: Heat 2nd Stage State On
				// Bit 2: Fan State On
				// Bit 1: Cool State On
				// Bit 0: Heat State On
				// (iT530 not support)
				RunningState: null, // integer, Current HVAC system running status
				SetIndicator: null, // set device indicator with duration in seconds, no linked getter
				ShortCycleProtection: null,
				SubDeviceName_c: null, //string - not used _c naming is as defined on device wiki
				// integer enum ->
				// 0: Off
				// 1: Auto
				// 3: Cool
				// 4: Heat
				// 5: Emeregncy Heating
				// (iT530 only supports 3 and 4)
				SystemMode: null, //integer
				TemperatureDisplayMode: null, // integer, Temperature display mode (C or F), 0: C, 1: F
				TempCalibration_x10: null,  // integer, Temp = data / 10, in Celsius
				
				// Metadata properties
				hide_set_frost_point_modal: false,
				hide_set_hold_point_modal: false
			},
			initialize: function (/*data, options*/) {
				//TODO Setup
				this._setCategoryDefaults();
				this._setEquipmentPageDefaults();
			},
			processScheduleType: function (propName) {
				var lowerCaseName = propName.toLowerCase();
				if (lowerCaseName.indexOf("mode") >= 0) {
					return "OFF";
				} else if (lowerCaseName.indexOf("cool") >= 0) {
					return "COOL";
				} else {
					return "HEAT";
				}
			},
			_setCategoryDefaults: function () {
				this.set({
					device_category_id: 769,
					device_category_icon_url: App.rootPath("/images/icons/dashboard/icon_temp_idle.svg"),
					device_category_type: constants.categoryTypes.THERMOSTATS,
					device_category_name_key: "equipment.myEquipment.categories.types.thermostats"
				});
			},
			_setEquipmentPageDefaults: function () {
				this.set({
					equipment_page_icon_url: App.rootPath("/images/icons/myEquipment/icon_temp_grey.svg")
				});
			},
			getBaseScheduleProps: function (scheduleId) {
				return [{
					name: this.getSetterPropNameIfPossible("AutoHeatingSetpoint_x100"),
					base_type: "integer",
					schedule_id: scheduleId
				},{
					name: this.getSetterPropNameIfPossible("AutoCoolingSetpoint_x100"),
					base_type: "integer",
					schedule_id: scheduleId
				}];
			},
			isHoldOn: function () {
				var holdProp = this.get("HoldDuration");
				return holdProp && holdProp.get("value") === constants.thermostatHoldDuration;
			},
			turnHoldOn: function () {
				var holdProp = this.get("HoldDuration");

				if (holdProp) {
					holdProp.setProperty(constants.thermostatHoldDuration);
				}
			},
			turnHoldOff: function () {
				var holdProp = this.get("HoldDuration");

				if (holdProp) {
					holdProp.setProperty(0);
				}
			},

			getHoldProp: function () {
				return this.get("HoldDuration");
			},

			isSetToCurrentSchedule: function () {
				// TODO when more information on it600 and schedules is available update
				return false;
			}
		}).mixin([BaseAylaDevice, Models.AylaScheduleMixin, Models.ThermostatModelMixin]);
	});

	return App.Models.Thermostat;
});
