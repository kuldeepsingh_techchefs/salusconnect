"use strict";

define([
	"app",
], function (App) {

	App.module("Models", function (Models, App, B) {
		/**
		 *  This represents the temperature for the gateway device with id <deviceId>
		 */

		var WeatherMixin = function () {
			this.clobber("parse", function (data) {
				if (data.weather && data.weather.length > 0) {
					return {
						conditionId: data.weather[0].id, //weather condition id
						temp: data.main.temp, //kelvin temp
						deviceTimeUnix_utc: data.dt, //device time
						sunrise: data.sys.sunrise, //sunrise time
						sunset: data.sys.sunset, //sunset time
						country: data.sys.country //location based on coords
					};
				}
			});
		};

		Models.DeviceWeather = B.Model.extend({
			defaults: {
				deviceId: null,
				conditionId: null,
				temp: null,
				icon: null,
				deviceTimeUnix_utc: null,
				deviceTimeLocal: null, // rendered in Weather.view
				sunrise: null,
				sunset: null,
				country: null
			},

			initialize: function () {
				this.valid = false;
			},

			getTempInCelsius: function () {
				if (this.get("temp")) {
					return this.get("temp") - 273.15;
				}
			},

			getTempInFahrenheit: function () {
				var c = this.getTempInCelsius();
				if (c) {
					return ((9/5) * c) + 32;
				}
			},

			getWeatherIcon: function () {
				if (!this.get("icon") || !this.valid) {
					this._assignWeatherIcon();
				}

				return this.get("icon");
			},

			_assignWeatherIcon: function () {
				var condition = this.get("conditionId"),
						dayOrNight;

				if (this.get("deviceTimeUnix_utc") < this.get("sunrise") || this.get("deviceTimeUnix_utc") > this.get("sunset")) {
					dayOrNight = "night";
				} else {
					dayOrNight = "day";
				}

				switch (condition) {
					case 210:
					case 211:
					case 212:
					case 221:
						this.set("icon", App.rootPath("/images/icons/weather/thunder.svg"));
						break;
					case 200:
					case 201:
					case 202:
					case 230:
					case 231:
					case 232:
						this.set("icon", App.rootPath("/images/icons/weather/thunder_with_rain.svg"));
						break;
					case 300:
					case 301:
					case 302:
					case 310:
					case 311:
					case 312:
					case 313:
					case 314:
					case 321:
						this.set("icon", App.rootPath("/images/icons/weather/drizzle.svg"));
						break;
					case 500:
					case 501:
					case 502:
					case 503:
					case 504:
					case 511:
					case 520:
					case 521:
					case 522:
					case 531:
						this.set("icon", App.rootPath("/images/icons/weather/rain.svg"));
						break;
					case 600:
					case 601:
					case 602:
					case 611:
					case 612:
					case 615:
					case 616:
					case 620:
					case 621:
					case 622:
						this.set("icon", App.rootPath("/images/icons/weather/snow.svg"));
						break;
					case 701:
					case 711:
					case 721:
					case 731:
					case 741:
					case 751:
					case 761:
					case 762:
						this.set("icon", App.rootPath("/images/icons/weather/fog.svg"));
						break;
					case 771:
					case 781:
					case 900:
					case 901:
					case 902:
					case 905:
					case 954:
					case 955:
					case 956:
					case 957:
					case 958:
					case 959:
					case 960:
					case 961:
					case 962:
						this.set("icon", App.rootPath("/images/icons/weather/wind.svg"));
						break;
					case 800:
					case 903:
					case 904:
					case 951:
					case 952:
					case 953:
						switch (dayOrNight) {
							case "day":
								this.set("icon", App.rootPath("/images/icons/weather/sun.svg"));
								break;
							case "night":
								this.set("icon", App.rootPath("/images/icons/weather/moon.svg"));
								break;
						}
						break;
					case 801:
					case 802:
					case 803:
					case 804:
						switch (dayOrNight) {
							case "day":
								this.set("icon", App.rootPath("/images/icons/weather/overcast_day.svg"));
								break;
							case "night":
								this.set("icon", App.rootPath("/images/icons/weather/overcast_night.svg"));
								break;
						}
						break;
					case 906:
						this.set("icon", App.rootPath("/images/icons/weather/hail.svg"));
						break;
					default:
						this.set("icon", App.rootPath("/images/icons/weather/overcast_night.svg"));
						App.warn("Came up with unaccounted weather conditionId " + condition);
						break;
				}
			}
		}).mixin([WeatherMixin]);
	});

	return App.Models.DeviceWeather;
});
