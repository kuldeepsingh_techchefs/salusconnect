"use strict";

define([
	"app",
	"common/constants",
	"common/model/salusWebServices/rules/Rule.model"
], function (App, constants) {

	App.module("Models", function (Models, App, B, Mn, $, _) {
		Models.OneTouchSubMenuManager = B.Model.extend({
			getEquipmentMenuForRule: function (type) {
				var that = this, devices, filtered;

				// get devices for 'State of Equipment'. it is different for conditions vs actions
				if (type === constants.oneTouchMenuTypes.condition) {
					devices = App.salusConnector.getInteractableDevices();
				} else if (type === constants.oneTouchMenuTypes.action || type === constants.oneTouchMenuTypes.delayedAction) {
					devices = App.salusConnector.getActionableDevices();
				}

				// TODO: should not have to filter like this. All devices SHOULD have euid in the future
				filtered = devices.filter(function (device) {
					return device.get("EUID") && device.getEUID();
				});

				return _.map(filtered, function (device) {
					var childMenu;
					if (type === constants.oneTouchMenuTypes.condition) {
						childMenu = that._getConditionChildMenuForDevice(device);
					} else if (type === constants.oneTouchMenuTypes.action) {
						childMenu = that._getActionChildMenuForDevice(device);
					} else if (type === constants.oneTouchMenuTypes.delayedAction) {
						childMenu = that._getDelayedActionChildMenuForDevice(device);
					}

					return new Models.RuleMenuItemModel({
						displayText: device.getDisplayName(),
						condition: device,
						image: device.get("equipment_page_icon_url"),
						device: device
					}, {childMenu: childMenu});
				});
			},
			/**
			 * Common Menu item templates
			 */
			_getSmartPlugOnOffMenu: function (type, onDisplayTextKey, offDisplayTextKey) {
				return new B.Collection([
					new Models.RuleMenuItemModel({
						displayTextKey: onDisplayTextKey,
						type: type,
						isLeafMenu: true, // no unique view here
						condition: "OnOff",
						propertyValue: 1
					}),
					new Models.RuleMenuItemModel({
						displayTextKey: offDisplayTextKey,
						type: type,
						isLeafMenu: true, // no unique view here
						condition: "OnOff",
						propertyValue: 0
					})
				]);
			},
			_getWaterHeaterMenu: function (type) {
                if (type === constants.oneTouchMenuTypes.condition) {
                    return this._getConditionWaterHeaterMenu();
                }else{
                    return this._getActionWaterHeaterMenu();
                }
			},
            _getConditionWaterHeaterMenu: function (){
                var keyPrefix="equipment.oneTouch.menus.when.stateOfEquipment.waterHeater.";
                var type=constants.oneTouchMenuTypes.condition;

				return new B.Collection([
//                    new Models.RuleMenuItemModel({
//						displayTextKey: keyPrefix + "on",
//						type: type,
//						isLeafMenu: true, // no unique view here
//						condition: "RunningMode",
//						propertyValue: constants.runningModeTypes.HEAT
//					}),
//                    new Models.RuleMenuItemModel({
//						displayTextKey: keyPrefix + "off",
//						type: type,
//						isLeafMenu: true, // no unique view here
//						condition: "RunningMode",
//						propertyValue: constants.runningModeTypes.OFF
//					}),
//					new Models.RuleMenuItemModel({
//						displayTextKey: keyPrefix + "schedule",
//						type: type,
//						isLeafMenu: true, // no unique view here
//						condition: "HoldType",
//						propertyValue: constants.waterHeaterHoldTypes.SCHEDULE
//					}),
					new Models.RuleMenuItemModel({
                        displayTextKey: "equipment.oneTouch.menus.when.stateOfEquipment.thermostats.isOffMode",
						type: type,
						isLeafMenu: true, // no unique view here
						condition: "HoldType",
						propertyValue: constants.waterHeaterHoldTypes.OFF
					}),
//					new Models.RuleMenuItemModel({
//						displayTextKey: keyPrefix + "boost",
//						type: type,
//						isLeafMenu: true, // no unique view here
//						condition: "HoldType",
//						propertyValue: constants.waterHeaterHoldTypes.BOOST
//					}),
//					new Models.RuleMenuItemModel({
//						displayTextKey: keyPrefix + "continuous",
//						type: type,
//						isLeafMenu: true, // no unique view here
//						condition: "HoldType",
//						propertyValue: constants.waterHeaterHoldTypes.CONTINUOUS
//					}),
                    
				]);
            },
            _getActionWaterHeaterMenu: function (){
                var keyPrefix = "equipment.oneTouch.menus.doThis.propertyChange.waterHeater.";
                var type=constants.oneTouchMenuTypes.action;
                
                return new B.Collection([
					new Models.RuleMenuItemModel({
						displayTextKey: keyPrefix + "schedule",
						type: type,
						isLeafMenu: true, // no unique view here
						condition: "HoldType",
						propertyValue: constants.waterHeaterHoldTypes.SCHEDULE
					}),
					new Models.RuleMenuItemModel({
                        displayTextKey: keyPrefix + "off",
						type: type,
						isLeafMenu: true, // no unique view here
						condition: "HoldType",
						propertyValue: constants.waterHeaterHoldTypes.OFF
					}),
					new Models.RuleMenuItemModel({
						displayTextKey: keyPrefix + "boost",
						type: type,
						isLeafMenu: true, // no unique view here
						condition: "HoldType",
						propertyValue: constants.waterHeaterHoldTypes.BOOST
					}),
					new Models.RuleMenuItemModel({
						displayTextKey: keyPrefix + "continuous",
						type: type,
						isLeafMenu: true, // no unique view here
						condition: "HoldType",
						propertyValue: constants.waterHeaterHoldTypes.CONTINUOUS
					}),
                    this._thermostatMenuItem({
						displayTextKey: "equipment.oneTouch.menus.doThis.propertyChange.changeLockSettings",
						type: constants.oneTouchMenuTypes.action,
						isLeafMenu: false,
						condition: "LockKey",
						propertyValue: 0,
						childMenu: this._thermostatModeChild({
							type: constants.oneTouchMenuTypes.action,
							subType: "lock-key",
							uniqueView: "thermostat-mode-picker",
							uniqueViewType: "thermostat-mode-picker"
						})
					}),
				]);
            },
			_thermostatModeChild: function (modelObj) {
				if (modelObj.childMenuCollection) {
					return {
						menuModel: new B.Model({
							uniqueView: modelObj.uniqueView,
							type: modelObj.type,
							isLeafMenu: false,
							subType: modelObj.subType
						}),
						uniqueViewType: modelObj.uniqueViewType,
						childMenuCollection: modelObj.childMenuCollection
					};
				} else {
					return {
						menuModel: new B.Model({
							uniqueView: modelObj.uniqueView,
							type: modelObj.type,
							isLeafMenu: true,
							subType: modelObj.subType
						}),
						uniqueViewType: modelObj.uniqueViewType
					};
				}
			},
			_timerMenuChild: function (type) {
				return {
					menuModel: new B.Model({
						uniqueView: "timer",
						type: type,
						isLeafMenu: true
					}),
					uniqueViewType: "timer"
				};
			},
			_thermostatMenuItem: function (menuObj) {
				return new Models.RuleMenuItemModel({
					displayTextKey: menuObj.displayTextKey,
					type: menuObj.type,
					isLeafMenu: menuObj.isLeafMenu,
					condition: menuObj.condition,
					propertyValue: menuObj.propVal // to fill
				}, {
					childMenu: menuObj.childMenu
				});
			},
			_getNewEquipmentMenu: function (type) {
				return {
					menuModel: new B.Model({
						uniqueView: false,
						type: type,
						isLeafMenu: false
					}),
					childMenuCollection: null // to be filled
				};
			},
			/**
			 * get the condition menu for a particular type of device
			 * @param device
			 * @returns {{menuModel: Backbone.Model, childMenuCollection: Backbone.Collection}}
			 * @private
			 */
			_getConditionChildMenuForDevice: function (device) {
				var getThermostatView = function (type, device) {
					return {
						menuModel: new B.Model({
							uniqueView: "thermostat-select-view",
							type: constants.oneTouchMenuTypes.condition,
							isLeafMenu: true,
                            device: device
						}),
						uniqueViewType: "thermostat-select-view",
						type: type
					};
				};

				var smartPlugOnOffMenu = this._getSmartPlugOnOffMenu(constants.oneTouchMenuTypes.condition,
					"equipment.oneTouch.menus.when.stateOfEquipment.smartPlug.on",
					"equipment.oneTouch.menus.when.stateOfEquipment.smartPlug.off");

				var aboveBelowBetweenMenu = new B.Collection([
					new Models.RuleMenuItemModel({
						displayTextKey: "equipment.oneTouch.menus.when.stateOfEquipment.thermostats.above",
						type: constants.oneTouchMenuTypes.condition,
						isLeafMenu: false
					}, {childMenu: getThermostatView("above", device)}),
					new Models.RuleMenuItemModel({
						displayTextKey: "equipment.oneTouch.menus.when.stateOfEquipment.thermostats.below",
						type: constants.oneTouchMenuTypes.condition,
						isLeafMenu: false
					}, {childMenu: getThermostatView("below", device)}),
//					new Models.RuleMenuItemModel({
//						displayTextKey: "equipment.oneTouch.menus.when.stateOfEquipment.thermostats.between",
//						type: constants.oneTouchMenuTypes.condition,
//						isLeafMenu: false
//					}, {childMenu: getThermostatView("between", device)})
				]);
                
                var runningModeProperty=device.get("RunningMode");
                var runningModePropertyName='';
                if(runningModeProperty){
                    runningModePropertyName=runningModeProperty.get("name");
                }
                
                var holdTypeProperty=device.get("HoldType");
                var holdTypePropertyName='';
                if(holdTypeProperty){
                	holdTypePropertyName=holdTypeProperty.get("name");
                }
                
                var aboveBelowBetweenMenuIt600Thermostat = new B.Collection([
					new Models.RuleMenuItemModel({
						displayTextKey: "equipment.oneTouch.menus.when.stateOfEquipment.thermostats.above",
						type: constants.oneTouchMenuTypes.condition,
						isLeafMenu: false
					}, {childMenu: getThermostatView("above", device)}),
					new Models.RuleMenuItemModel({
						displayTextKey: "equipment.oneTouch.menus.when.stateOfEquipment.thermostats.below",
						type: constants.oneTouchMenuTypes.condition,
						isLeafMenu: false
					}, {childMenu: getThermostatView("below", device)}),
//					new Models.RuleMenuItemModel({
//						displayTextKey: "equipment.oneTouch.menus.when.stateOfEquipment.thermostats.between",
//						type: constants.oneTouchMenuTypes.condition,
//						isLeafMenu: false
//					}, {childMenu: getThermostatView("between", device)}),
                    new Models.RuleMenuItemModel({
						displayTextKey: "equipment.oneTouch.menus.when.stateOfEquipment.thermostats.isHeating",
						type: constants.oneTouchMenuTypes.condition,
						isLeafMenu: true,
                        ruleData:{
                            type: "propVal",
                            eq: 4,
                            newVal: null,
                            propName: runningModePropertyName,
                            device: device,
                            EUID: device.getEUID()
                        }
					}),
//                    new Models.RuleMenuItemModel({
//						displayTextKey: "equipment.oneTouch.menus.when.stateOfEquipment.thermostats.isCooling",
//						type: constants.oneTouchMenuTypes.condition,
//						isLeafMenu: true,
//                        ruleData:{
//                            type: "propVal",
//                            eq: 3,
//                            newVal: null,
//                            propName: runningModePropertyName,
//                            device: device,
//                            EUID: device.getEUID()
//                        }
//					}),
                    new Models.RuleMenuItemModel({
						displayTextKey: "equipment.oneTouch.menus.when.stateOfEquipment.thermostats.isOff",
						type: constants.oneTouchMenuTypes.condition,
						isLeafMenu: true,
                        ruleData:{
                            type: "propVal",
                            eq: 0,
                            newVal: null,
                            propName: runningModePropertyName,
                            device: device,
                            EUID: device.getEUID()
                        }
					}),
//                    new Models.RuleMenuItemModel({
//						displayTextKey: "equipment.oneTouch.menus.when.stateOfEquipment.waterHeater.schedule",
//						type: constants.oneTouchMenuTypes.condition,
//						isLeafMenu: true,
//                        ruleData:{
//                            type: "propVal",
//                            eq: 0,
//                            newVal: null,
//                            propName: holdTypePropertyName,
//                            device: device,
//                            EUID: device.getEUID()
//                        }
//					}),
//                    new Models.RuleMenuItemModel({
//						displayTextKey: "equipment.oneTouch.menus.when.stateOfEquipment.thermostats.isTemporaryHold",
//						type: constants.oneTouchMenuTypes.condition,
//						isLeafMenu: true,
//                        ruleData:{
//                            type: "propVal",
//                            eq: 1,
//                            newVal: null,
//                            propName: holdTypePropertyName,
//                            device: device,
//                            EUID: device.getEUID()
//                        }
//					}),
//                    new Models.RuleMenuItemModel({
//						displayTextKey: "equipment.oneTouch.menus.when.stateOfEquipment.thermostats.isPermanentHold",
//						type: constants.oneTouchMenuTypes.condition,
//						isLeafMenu: true,
//                        ruleData:{
//                            type: "propVal",
//                            eq: 2,
//                            newVal: null,
//                            propName: holdTypePropertyName,
//                            device: device,
//                            EUID: device.getEUID()
//                        }
//					}),
//                    new Models.RuleMenuItemModel({
//						displayTextKey: "equipment.oneTouch.menus.when.stateOfEquipment.thermostats.isOffMode",
//						type: constants.oneTouchMenuTypes.condition,
//						isLeafMenu: true,
//                        ruleData:{
//                            type: "propVal",
//                            eq: 7,
//                            newVal: null,
//                            propName: holdTypePropertyName,
//                            device: device,
//                            EUID: device.getEUID()
//                        }
//					}),
				]);
                
                var aboveBelowBetweenMenuFC600 = new B.Collection([
					new Models.RuleMenuItemModel({
						displayTextKey: "equipment.oneTouch.menus.when.stateOfEquipment.thermostats.above",
						type: constants.oneTouchMenuTypes.condition,
						isLeafMenu: false
					}, {childMenu: getThermostatView("above", device)}),
					new Models.RuleMenuItemModel({
						displayTextKey: "equipment.oneTouch.menus.when.stateOfEquipment.thermostats.below",
						type: constants.oneTouchMenuTypes.condition,
						isLeafMenu: false
					}, {childMenu: getThermostatView("below", device)}),
//					new Models.RuleMenuItemModel({
//						displayTextKey: "equipment.oneTouch.menus.when.stateOfEquipment.thermostats.between",
//						type: constants.oneTouchMenuTypes.condition,
//						isLeafMenu: false
//					}, {childMenu: getThermostatView("between", device)}),
                    new Models.RuleMenuItemModel({
						displayTextKey: "equipment.oneTouch.menus.when.stateOfEquipment.thermostats.isHeating",
						type: constants.oneTouchMenuTypes.condition,
						isLeafMenu: true,
                        ruleData:{
                            type: "propVal",
                            eq: 4,
                            newVal: null,
                            propName: runningModePropertyName,
                            device: device,
                            EUID: device.getEUID()
                        }
					}),
                    new Models.RuleMenuItemModel({
						displayTextKey: "equipment.oneTouch.menus.when.stateOfEquipment.thermostats.isCooling",
						type: constants.oneTouchMenuTypes.condition,
						isLeafMenu: true,
                        ruleData:{
                            type: "propVal",
                            eq: 3,
                            newVal: null,
                            propName: runningModePropertyName,
                            device: device,
                            EUID: device.getEUID()
                        }
					}),
                    new Models.RuleMenuItemModel({
						displayTextKey: "equipment.oneTouch.menus.when.stateOfEquipment.thermostats.isOff",
						type: constants.oneTouchMenuTypes.condition,
						isLeafMenu: true,
                        ruleData:{
                            type: "propVal",
                            eq: 0,
                            newVal: null,
                            propName: runningModePropertyName,
                            device: device,
                            EUID: device.getEUID()
                        }
					}),
//                    new Models.RuleMenuItemModel({
//						displayTextKey: "equipment.oneTouch.menus.when.stateOfEquipment.waterHeater.schedule",
//						type: constants.oneTouchMenuTypes.condition,
//						isLeafMenu: true,
//                        ruleData:{
//                            type: "propVal",
//                            eq: 0,
//                            newVal: null,
//                            propName: holdTypePropertyName,
//                            device: device,
//                            EUID: device.getEUID()
//                        }
//					}),
//                    new Models.RuleMenuItemModel({
//						displayTextKey: "equipment.oneTouch.menus.when.stateOfEquipment.thermostats.isTemporaryHold",
//						type: constants.oneTouchMenuTypes.condition,
//						isLeafMenu: true,
//                        ruleData:{
//                            type: "propVal",
//                            eq: 1,
//                            newVal: null,
//                            propName: holdTypePropertyName,
//                            device: device,
//                            EUID: device.getEUID()
//                        }
//					}),
//                    new Models.RuleMenuItemModel({
//						displayTextKey: "equipment.oneTouch.menus.when.stateOfEquipment.thermostats.isPermanentHold",
//						type: constants.oneTouchMenuTypes.condition,
//						isLeafMenu: true,
//                        ruleData:{
//                            type: "propVal",
//                            eq: 2,
//                            newVal: null,
//                            propName: holdTypePropertyName,
//                            device: device,
//                            EUID: device.getEUID()
//                        }
//					}),
//                    new Models.RuleMenuItemModel({
//						displayTextKey: "equipment.oneTouch.menus.when.stateOfEquipment.thermostats.isOffMode",
//						type: constants.oneTouchMenuTypes.condition,
//						isLeafMenu: true,
//                        ruleData:{
//                            type: "propVal",
//                            eq: 7,
//                            newVal: null,
//                            propName: holdTypePropertyName,
//                            device: device,
//                            EUID: device.getEUID()
//                        }
//					}),
				]);

				var equipmentMenu = this._getNewEquipmentMenu(constants.oneTouchMenuTypes.condition);

				var doorWindowOpenMenu = new B.Collection([
					new Models.RuleMenuItemModel({
						displayTextKey: "equipment.oneTouch.menus.when.stateOfEquipment.doorWindow.open",
						type: constants.oneTouchMenuTypes.condition,
						isLeafMenu: true, // no unique view here
                        // Bug_SCS-3288 Rule error for Window sensor
						condition: "ErrorIASZSAlarmed1",
						propertyValue: 1
					}),
					new Models.RuleMenuItemModel({
						displayTextKey: "equipment.oneTouch.menus.when.stateOfEquipment.doorWindow.closed",
						type: constants.oneTouchMenuTypes.condition,
						isLeafMenu: true, // no unique view here
                        // Bug_SCS-3288 Rule error for Window sensor
						condition: "ErrorIASZSAlarmed1",
						propertyValue: 0
					})
				]);

				var sensorIsAlertingMenu = new B.Collection([
					new Models.RuleMenuItemModel({
						displayTextKey: "equipment.oneTouch.menus.when.stateOfEquipment.sensor.alert",
						type: constants.oneTouchMenuTypes.condition,
						isLeafMenu: true, // no unique view here
                        // Bug_SCS-3288 Rule error for Window sensor
						condition: "ErrorIASZSAlarmed1",
						propertyValue: 1
					})
				]);

				var waterHeaterMenu = this._getWaterHeaterMenu(constants.oneTouchMenuTypes.condition);

				switch (device.modelType) {
					case constants.modelTypes.THERMOSTAT:
                        equipmentMenu.childMenuCollection = aboveBelowBetweenMenu;
                        break;
					case constants.modelTypes.IT600THERMOSTAT:
						equipmentMenu.childMenuCollection = aboveBelowBetweenMenuIt600Thermostat;
						break;
                    case constants.modelTypes.FC600:
                        equipmentMenu.childMenuCollection = aboveBelowBetweenMenuFC600;
                        break;
					case constants.modelTypes.WATERHEATER:
						equipmentMenu.childMenuCollection = waterHeaterMenu;
						equipmentMenu.menuModel.set("isLeafMenu", true);
						break;
					case constants.modelTypes.SMARTPLUG:
					case constants.modelTypes.MINISMARTPLUG:
						equipmentMenu.childMenuCollection = smartPlugOnOffMenu;
						equipmentMenu.menuModel.set("isLeafMenu", true); // smart plug ends on OnOff
						break;
					case constants.modelTypes.DOORMONITOR:
					case constants.modelTypes.WINDOWMONITOR:
						equipmentMenu.childMenuCollection = doorWindowOpenMenu;
						equipmentMenu.menuModel.set("isLeafMenu", true);
						break;
					case constants.modelTypes.CARBONMONOXIDEDETECTOR:
					case constants.modelTypes.SMOKEDETECTOR:
						equipmentMenu.childMenuCollection = sensorIsAlertingMenu;
						equipmentMenu.menuModel.set("isLeafMenu", true); // sensor is alerting
						break;
					default:
						/*equipmentMenu.childMenuCollection = aboveBelowBetweenMenu; // todo: more*/
						break;
				}

				return equipmentMenu;
			},
			/**
			 * get the action menu for a particular device type
			 * @param device
			 * @returns {{menuModel: Backbone.Model, childMenuCollection: Backbone.Collection}}
			 * @private
			 */
			_getActionChildMenuForDevice: function (device) {
				var smartPlugOnOffMenu = this._getSmartPlugOnOffMenu(constants.oneTouchMenuTypes.action,
					"equipment.oneTouch.menus.doThis.propertyChange.smartPlugOn",
					"equipment.oneTouch.menus.doThis.propertyChange.smartPlugOff");

				var thermostatMenuOptions = new B.Collection([
					this._thermostatMenuItem({
						displayTextKey: "equipment.oneTouch.menus.doThis.propertyChange.changeMode",
						type: constants.oneTouchMenuTypes.action,
						isLeafMenu: false,
						condition: "SystemMode",
						propertyValue: 0,
						childMenu: this._thermostatModeChild({
							type: constants.oneTouchMenuTypes.action,
							subType: "system-mode",
							uniqueView: "thermostat-mode-picker",
							uniqueViewType: "thermostat-mode-picker"
						})
					}),
					this._thermostatMenuItem({
						displayTextKey: "equipment.oneTouch.menus.doThis.propertyChange.changeFanMode",
						type: constants.oneTouchMenuTypes.action,
						isLeafMenu: false,
						condition: "FanMode",
						propertyValue: 0,
						childMenu: this._thermostatModeChild(
							{
								type: constants.oneTouchMenuTypes.action,
								subType: "fan-mode",
								uniqueView: "thermostat-mode-picker",
								uniqueViewType: "thermostat-mode-picker"
							}
						)
					}),
					this._thermostatMenuItem({
						displayTextKey: "equipment.oneTouch.menus.doThis.propertyChange.changeSetTemp",
						type: constants.oneTouchMenuTypes.action,
						isLeafMenu: false,
						condition: "LocalTemperature_x100",
						propertyValue: 0,
						childMenu: this._thermostatModeChild({
							type: constants.oneTouchMenuTypes.action,
							subType: "exact-temp",
							uniqueView: "thermostat-select-view",
							uniqueViewType: "thermostat-select-view"
						})
					})
				]);


				var it600ThermostatMenuOptions = new B.Collection([
					this._thermostatMenuItem({
						displayTextKey: "equipment.oneTouch.menus.doThis.propertyChange.changeSetTemp",
						type: constants.oneTouchMenuTypes.action,
						isLeafMenu: false,
						condition: "LocalTemperature_x100",
						propertyValue: 0,
						childMenu: this._thermostatModeChild({
							type: constants.oneTouchMenuTypes.action,
							subType: "exact-temp",
							uniqueView: "thermostat-select-view",
							uniqueViewType: "thermostat-select-view"
						})
					}),
                    this._thermostatMenuItem({
						displayTextKey: "equipment.oneTouch.menus.doThis.propertyChange.changeMode",
						type: constants.oneTouchMenuTypes.action,
						isLeafMenu: false,
						condition: "HoldType",
						propertyValue: 0,
						childMenu: this._thermostatModeChild({
							type: constants.oneTouchMenuTypes.action,
							subType: "hold-type",
							uniqueView: "thermostat-mode-picker",
							uniqueViewType: "thermostat-mode-picker"
						})
					}),
                    this._thermostatMenuItem({
						displayTextKey: "equipment.oneTouch.menus.doThis.propertyChange.changeLockSettings",
						type: constants.oneTouchMenuTypes.action,
						isLeafMenu: false,
						condition: "LockKey",
						propertyValue: 0,
						childMenu: this._thermostatModeChild({
							type: constants.oneTouchMenuTypes.action,
							subType: "lock-key",
							uniqueView: "thermostat-mode-picker",
							uniqueViewType: "thermostat-mode-picker"
						})
					}),
				]);
                
                var fc600MenuOptions = new B.Collection([
					this._thermostatMenuItem({
						displayTextKey: "equipment.oneTouch.menus.doThis.propertyChange.changeSetTemp",
						type: constants.oneTouchMenuTypes.action,
						isLeafMenu: false,
						condition: "LocalTemperature_x100",
						propertyValue: 0,
						childMenu: this._thermostatModeChild({
							type: constants.oneTouchMenuTypes.action,
							subType: "exact-temp",
							uniqueView: "thermostat-select-view",
							uniqueViewType: "thermostat-select-view"
						})
					}),
                    this._thermostatMenuItem({
						displayTextKey: "equipment.oneTouch.menus.doThis.propertyChange.changeMode",
						type: constants.oneTouchMenuTypes.action,
						isLeafMenu: false,
						condition: "HoldType",
						propertyValue: 0,
						childMenu: this._thermostatModeChild({
							type: constants.oneTouchMenuTypes.action,
							subType: "hold-type",
							uniqueView: "thermostat-mode-picker",
							uniqueViewType: "thermostat-mode-picker"
						})
					}),
                    this._thermostatMenuItem({
						displayTextKey: "equipment.oneTouch.menus.doThis.propertyChange.changeLockSettings",
						type: constants.oneTouchMenuTypes.action,
						isLeafMenu: false,
						condition: "LockKey",
						propertyValue: 0,
						childMenu: this._thermostatModeChild({
							type: constants.oneTouchMenuTypes.action,
							subType: "lock-key",
							uniqueView: "thermostat-mode-picker",
							uniqueViewType: "thermostat-mode-picker"
						})
					}),
				]);

				var waterHeaterMenu = this._getWaterHeaterMenu(constants.oneTouchMenuTypes.action);

				var equipmentMenu = this._getNewEquipmentMenu(constants.oneTouchMenuTypes.action);

				switch (device.modelType) {
					case constants.modelTypes.THERMOSTAT:
						equipmentMenu.childMenuCollection = thermostatMenuOptions;
						break;
					case constants.modelTypes.IT600THERMOSTAT:
						equipmentMenu.childMenuCollection = it600ThermostatMenuOptions;
						break;
                    case constants.modelTypes.FC600:
                        equipmentMenu.childMenuCollection = fc600MenuOptions;
                        break;
					case constants.modelTypes.WATERHEATER:
						equipmentMenu.childMenuCollection = waterHeaterMenu;
						equipmentMenu.menuModel.set("isLeafMenu", true); // final menu
						break;
					case constants.modelTypes.SMARTPLUG:
					case constants.modelTypes.MINISMARTPLUG:
						equipmentMenu.childMenuCollection = smartPlugOnOffMenu;
						equipmentMenu.menuModel.set("isLeafMenu", true); // smart plug ends on OnOff
						break;
					default:
						/*equipmentMenu.childMenuCollection = smartPlugOnOffMenu; // todo: more*/
						break;
				}

				return equipmentMenu;
			},
			/**
			 * get the delayed action menu for a particular device type
			 * @param device
			 * @returns {{menuModel: Backbone.Model, childMenuCollection: Backbone.Collection}}
			 * @private
			 */
			_getDelayedActionChildMenuForDevice: function (device) {
				var delayedActionSmartPlugMenu = new B.Collection([
					new Models.RuleMenuItemModel({
						displayTextKey: "equipment.oneTouch.menus.doThis.propertyChange.smartPlugOn",
						type: constants.oneTouchMenuTypes.delayedAction,
						isLeafMenu: false,
						condition: "OnOff",
						propertyValue: 1,
						saveData: true,
						isTimed: true
					}, {
						childMenu: this._timerMenuChild(constants.oneTouchMenuTypes.delayedAction)
					}),
					new Models.RuleMenuItemModel({
						displayTextKey: "equipment.oneTouch.menus.doThis.propertyChange.smartPlugOff",
						type: constants.oneTouchMenuTypes.delayedAction,
						isLeafMenu: false, // no unique view here
						condition: "OnOff",
						propertyValue: 0,
						isTimed: true
					}, {
						childMenu: this._timerMenuChild(constants.oneTouchMenuTypes.delayedAction)
					})
				]);

				var delayedWaterHeaterMenu = new B.Collection([
					new Models.RuleMenuItemModel({
						displayTextKey: "equipment.oneTouch.menus.doThis.propertyChange.waterHeater.schedule",
						type: constants.oneTouchMenuTypes.delayedAction,
						isLeafMenu: false,
						condition: "HoldType",
						propertyValue: constants.waterHeaterHoldTypes.SCHEDULE,
						saveData: true,
						isTimed: true
					}, {
						childMenu: this._timerMenuChild(constants.oneTouchMenuTypes.delayedAction)
					}),
					new Models.RuleMenuItemModel({
						displayTextKey: "equipment.oneTouch.menus.doThis.propertyChange.waterHeater.off",
						type: constants.oneTouchMenuTypes.delayedAction,
						isLeafMenu: false, // no unique view here
						condition: "HoldType",
						propertyValue: constants.waterHeaterHoldTypes.OFF,
						isTimed: true
					}, {
						childMenu: this._timerMenuChild(constants.oneTouchMenuTypes.delayedAction)
					}),
					new Models.RuleMenuItemModel({
						displayTextKey: "equipment.oneTouch.menus.doThis.propertyChange.waterHeater.boost",
						type: constants.oneTouchMenuTypes.delayedAction,
						isLeafMenu: false, // no unique view here
						condition: "HoldType",
						propertyValue: constants.waterHeaterHoldTypes.BOOST,
						isTimed: true
					}, {
						childMenu: this._timerMenuChild(constants.oneTouchMenuTypes.delayedAction)
					}),
					new Models.RuleMenuItemModel({
						displayTextKey: "equipment.oneTouch.menus.doThis.propertyChange.waterHeater.continuous",
						type: constants.oneTouchMenuTypes.delayedAction,
						isLeafMenu: false, // no unique view here
						condition: "HoldType",
						propertyValue: constants.waterHeaterHoldTypes.CONTINUOUS,
						isTimed: true
					}, {
						childMenu: this._timerMenuChild(constants.oneTouchMenuTypes.delayedAction)
					}),
                    this._thermostatMenuItem({
						displayTextKey: "equipment.oneTouch.menus.doThis.propertyChange.changeLockSettings",
						type: constants.oneTouchMenuTypes.delayedAction,
						isLeafMenu: false,
						condition: "LockKey",
						propertyValue: 0,
						childMenu: this._thermostatModeChild({
							type: constants.oneTouchMenuTypes.delayedAction,
							subType: "lock-key",
							uniqueView: "thermostat-mode-picker",
							uniqueViewType: "thermostat-mode-picker",
                            childMenuCollection: new B.Collection([
									this._timerMenuChild(constants.oneTouchMenuTypes.delayedAction)
								])
						})
					}),
				]);

				var thermostatMenuOptions = new B.Collection([
					this._thermostatMenuItem({
						displayTextKey: "equipment.oneTouch.menus.doThis.propertyChange.changeMode",
						type: constants.oneTouchMenuTypes.delayedAction,
						isLeafMenu: false,
						condition: "SystemMode",
						propertyValue: 0,
						childMenu: this._thermostatModeChild(
							{
								type: constants.oneTouchMenuTypes.delayedAction,
								subType: "system-mode",
								uniqueView: "thermostat-mode-picker",
								uniqueViewType: "thermostat-mode-picker",
								childMenuCollection: new B.Collection([
									this._timerMenuChild(constants.oneTouchMenuTypes.delayedAction)
								])
							})
					}),
					this._thermostatMenuItem({
						displayTextKey: "equipment.oneTouch.menus.doThis.propertyChange.changeFanMode",
						type: constants.oneTouchMenuTypes.delayedAction,
						isLeafMenu: false,
						condition: "FanMode",
						propertyValue: 0,
						childMenu: this._thermostatModeChild(
							{
								type: constants.oneTouchMenuTypes.delayedAction,
								subType: "fan-mode",
								uniqueView: "thermostat-mode-picker",
								uniqueViewType: "thermostat-mode-picker",
								childMenuCollection: new B.Collection([
									this._timerMenuChild(constants.oneTouchMenuTypes.delayedAction)
								])
							}
						)
					}),
					this._thermostatMenuItem({
						displayTextKey: "equipment.oneTouch.menus.doThis.propertyChange.changeSetTemp",
						type: constants.oneTouchMenuTypes.action,
						isLeafMenu: false,
						condition: "LocalTemperature_x100",
						propertyValue: 0,
						childMenu: this._thermostatModeChild({
							type: constants.oneTouchMenuTypes.delayedAction,
							subType: "exact-temp",
							uniqueView: "thermostat-select-view",
							uniqueViewType: "thermostat-select-view",
							childMenuCollection: new B.Collection([
								this._timerMenuChild(constants.oneTouchMenuTypes.delayedAction)
							])
						})
					})
				]);

				var it600ThermostatMenuOptions = new B.Collection([
					this._thermostatMenuItem({
						displayTextKey: "equipment.oneTouch.menus.doThis.propertyChange.changeSetTemp",
						type: constants.oneTouchMenuTypes.action,
						isLeafMenu: false,
						condition: "LocalTemperature_x100",
						propertyValue: 0,
						childMenu: this._thermostatModeChild({
							type: constants.oneTouchMenuTypes.delayedAction,
							subType: "exact-temp",
							uniqueView: "thermostat-select-view",
							uniqueViewType: "thermostat-select-view",
							childMenuCollection: new B.Collection([
								this._timerMenuChild(constants.oneTouchMenuTypes.delayedAction)
							])
						})
					}),
                    this._thermostatMenuItem({
						displayTextKey: "equipment.oneTouch.menus.doThis.propertyChange.changeMode",
						type: constants.oneTouchMenuTypes.action,
						isLeafMenu: false,
						condition: "HoldType",
						propertyValue: 0,
						childMenu: this._thermostatModeChild({
							type: constants.oneTouchMenuTypes.delayedAction,
							subType: "hold-type",
							uniqueView: "thermostat-mode-picker",
							uniqueViewType: "thermostat-mode-picker",
                            childMenuCollection: new B.Collection([
								this._timerMenuChild(constants.oneTouchMenuTypes.delayedAction)
							])
						})
					}),
                    this._thermostatMenuItem({
						displayTextKey: "equipment.oneTouch.menus.doThis.propertyChange.changeLockSettings",
						type: constants.oneTouchMenuTypes.action,
						isLeafMenu: false,
						condition: "LockKey",
						propertyValue: 0,
						childMenu: this._thermostatModeChild({
							type: constants.oneTouchMenuTypes.delayedAction,
							subType: "lock-key",
							uniqueView: "thermostat-mode-picker",
							uniqueViewType: "thermostat-mode-picker",
                            childMenuCollection: new B.Collection([
								this._timerMenuChild(constants.oneTouchMenuTypes.delayedAction)
							])
						})
					}),
				]);
                
                var fc600MenuOptions = new B.Collection([
					this._thermostatMenuItem({
						displayTextKey: "equipment.oneTouch.menus.doThis.propertyChange.changeSetTemp",
						type: constants.oneTouchMenuTypes.action,
						isLeafMenu: false,
						condition: "LocalTemperature_x100",
						propertyValue: 0,
						childMenu: this._thermostatModeChild({
							type: constants.oneTouchMenuTypes.delayedAction,
							subType: "exact-temp",
							uniqueView: "thermostat-select-view",
							uniqueViewType: "thermostat-select-view",
							childMenuCollection: new B.Collection([
								this._timerMenuChild(constants.oneTouchMenuTypes.delayedAction)
							])
						})
					}),
                    this._thermostatMenuItem({
						displayTextKey: "equipment.oneTouch.menus.doThis.propertyChange.changeMode",
						type: constants.oneTouchMenuTypes.action,
						isLeafMenu: false,
						condition: "HoldType",
						propertyValue: 0,
						childMenu: this._thermostatModeChild({
							type: constants.oneTouchMenuTypes.delayedAction,
							subType: "hold-type",
							uniqueView: "thermostat-mode-picker",
							uniqueViewType: "thermostat-mode-picker",
                            childMenuCollection: new B.Collection([
								this._timerMenuChild(constants.oneTouchMenuTypes.delayedAction)
							])
						})
					}),
                    this._thermostatMenuItem({
						displayTextKey: "equipment.oneTouch.menus.doThis.propertyChange.changeLockSettings",
						type: constants.oneTouchMenuTypes.action,
						isLeafMenu: false,
						condition: "LockKey",
						propertyValue: 0,
						childMenu: this._thermostatModeChild({
							type: constants.oneTouchMenuTypes.delayedAction,
							subType: "lock-key",
							uniqueView: "thermostat-mode-picker",
							uniqueViewType: "thermostat-mode-picker",
                            childMenuCollection: new B.Collection([
								this._timerMenuChild(constants.oneTouchMenuTypes.delayedAction)
							])
						})
					}),
				]);

				var equipmentMenu = this._getNewEquipmentMenu(constants.oneTouchMenuTypes.delayedAction);

				switch (device.modelType) {
					case constants.modelTypes.THERMOSTAT:
						equipmentMenu.childMenuCollection = thermostatMenuOptions;
						break;
					case constants.modelTypes.IT600THERMOSTAT:
						equipmentMenu.childMenuCollection = it600ThermostatMenuOptions;
						break;
                    case constants.modelTypes.FC600:
						equipmentMenu.childMenuCollection = fc600MenuOptions;
						break;
					case constants.modelTypes.WATERHEATER:
						equipmentMenu.childMenuCollection = delayedWaterHeaterMenu;
						break;
					case constants.modelTypes.SMARTPLUG:
					case constants.modelTypes.MINISMARTPLUG:
						equipmentMenu.childMenuCollection = delayedActionSmartPlugMenu;
						break;
					default:
						break;
				}

				return equipmentMenu;
			}
		});
	});

	return App.Models.OneTouchSubMenuManager;
});