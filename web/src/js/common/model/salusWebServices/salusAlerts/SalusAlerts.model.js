"use strict";

define([
	"app",
	"bluebird",
	"common/AylaConfig",
	"common/model/ayla/mixin.AylaBacked"
], function (App, P, AylaConfig, AylaBackedMixin) {

	App.module("Models", function (Models, App, B, Mn, $, _) {
		Models.SalusAlertsModel = B.Model.extend({
			defaults: {
                dsn: null,
				properties: null //array
			},
            
            idAttribute: "dsn",

			initialize: function (data, options) {
				if (options && options.dsn) {
					this.set("dsn", options.dsn);
				}
			},

			load: function (isLowPriority) {
				var that = this,
                    fetchURLModel = {dsn: this.dsn},
					url = _.template(AylaConfig.endpoints.salusAlerts.fetch)(fetchURLModel);

				return App.salusConnector.makeAjaxCall(url, null, "GET", "json", null, {isLowPriority: isLowPriority}).then(function (data) {
					that.set(that.aParse(data.devices[0]));
				});
			},

			aParse: function (data) {
				return {
					dsn: data.dsn,
					properties: data.properties
				};
			}
		}).mixin([AylaBackedMixin], {
			apiWrapperObjectName: "devices"
		});
	});

	return App.Models.SalusAlertsModel;
});

