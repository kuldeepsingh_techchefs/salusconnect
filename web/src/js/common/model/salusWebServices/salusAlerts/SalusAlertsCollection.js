"use strict";

define([
	"app",
	"bluebird",
	"common/AylaConfig",
	"common/model/ayla/mixin.AylaBacked",
	"common/model/salusWebServices/salusAlerts/SalusAlerts.model"
], function (App, P, AylaConfig, AylaBackedMixin) {

	App.module("Models", function (Models, App, B, Mn, $, _) {
		Models.SalusAlertsCollection = B.Collection.extend({
			model: Models.SalusAlertsModel,

			initialize: function (data, options) {
                options = options || {};
			},

			refresh: function (isLowPriority) {
				var promise = this.load(isLowPriority);
				App.salusConnector.setDataLoadPromise("salusAlerts", promise);

				return promise;
			},

			load: function (isLowPriority) {
				var that = this;

				return App.salusConnector.makeAjaxCall(_.template(AylaConfig.endpoints.salusAlerts.list)(this.toJSON()), null, "GET", null, {isLowPriority: isLowPriority}).then(function (data) {
					var filteredArray = _.filter(data.devices, function (device) {
                        return _.isArray(device["properties"]);
                    });
                    that.reset(filteredArray, {parse: true});
				});
			},
            
            loadWhereDate: function (startDate, endDate) {
				var that = this,
					fetchURLModel = {
                        start_date: startDate,
                        end_date: endDate
                    };

				return App.salusConnector.makeAjaxCall(_.template(AylaConfig.endpoints.salusAlerts.listWhereDate)(fetchURLModel), null, "GET").then(function (data) {
					var filteredArray = _.filter(data.devices, function (device) {
                        return _.isArray(device["properties"]);
                    });
                    that.reset(filteredArray, {parse: true});
				});
			},
            
            markDatapoint: function (ids) {
				var url = _.template(AylaConfig.endpoints.salusAlerts.markDatapoint)(this.toJSON());
                
				var payload = _.map(ids, function(id) {
                    return {id: id};
                });

				return App.salusConnector.makeAjaxCall(url, payload, "PUT", "json");
			},
            
			isLoaded: function () {
				return !!this.length; //todo
			},
            
            filterByHasError: function () {
                var donotShowProperties = ["ErrorIASZSAlarmed1"];
                var filteredArray = this.filter(function (obj) {
                    var properties = obj.get("properties");
                    var showProperties = [];
                    var hasError = 0;
                    _.each(properties, function(property) {
                        if (property.value === "1") {
                            hasError = 1;
                        }
                        var propertyName = property.name.split(":")[2];
                        if (_.indexOf(donotShowProperties, propertyName) === -1) {
                            showProperties.push(property);
                        }
                    });
                    obj.set("properties", showProperties);
                    
                    return hasError === 1 && showProperties.length > 0;
                });
                
                App.salusConnector.getFullDeviceCollection().forEach(function(device) {
                    if (device.get("connection_status") === "Offline") {
                        filteredArray.push(new Models.SalusAlertsModel(null, {dsn: device.get("dsn")}));
                    }
                });
                
                var groupObj = _.groupBy(filteredArray, function(model){
                    var device = App.salusConnector.getDeviceByDSN(model.get("dsn"));
                    return device.get("gateway_dsn") || device.get("dsn"); 
                });
                
                var newArray = [];
                _.each(groupObj, function(arr) {
                    _.each(arr, function(obj) {
                        newArray.push(obj);
                    });
                });
                
                return new Models.SalusAlertsCollection(newArray);
            },
            
            hasSalusAlerts: function() {
                if (this.filterByHasError().length > 0) {
                    $(".bb-salus-alerts-icon").removeClass("hidden");
                    App.hasSalusAlerts = 1;
                } else {
                    $(".bb-salus-alerts-icon").addClass("hidden");
                    App.hasSalusAlerts = 0;
                }
            },
            
			saveAlertsStatus: function (key) {
                var alertsStatusArray = [];
                this.forEach(function (obj) {
                    var properties = obj.get("properties");
                    _.each(properties, function(property) {
                        if (property.key === key) {
                            property.status = 1;
                        }
                        if (property.status === 1) {
                            alertsStatusArray.push(property.key);
                        }
                    });
                });
                
				var that = this, payload = {
					datum: {
						value: JSON.stringify(alertsStatusArray)
					}
				};

				return App.salusConnector.makeAjaxCall(AylaConfig.endpoints.salusAlerts.alertsStatus.save, payload, "PUT", null, null, [404])
                    .catch(function (err) {
                        if (err.status === 404) {
                            App.warn("Making New Alerts Status");
                            return that._makeAlertsStatusProp(alertsStatusArray);
                        }

                        throw err;
                    });
			},

			fetchAlertsStatus: function () {
				var that = this;

				return App.salusConnector.makeAjaxCall(AylaConfig.endpoints.salusAlerts.alertsStatus.fetch, null, "GET", null, null, [404])
					.then(function (data) {
						var alertsStatusParse = JSON.parse(data.datum.value);

						if (!_.isArray(alertsStatusParse)) {
							return that.deleteAlertsStatus().then(function () {
								App.warn("Making New Alerts Status");
								return that._makeAlertsStatusProp([]);
							});
						}
                        
                        that._withAlertsStatus(alertsStatusParse);

						return alertsStatusParse;
					}).catch(function (err) {
						if (err.status === 404) {
							App.warn("Making New Alerts Status");
							return that._makeAlertsStatusProp([]);
						}

						throw err;
					});
			},
            
            deleteAlertsStatus: function () {
				return App.salusConnector.makeAjaxCall(AylaConfig.endpoints.salusAlerts.alertsStatus.delete, null, "DELETE", "text");
			},
            
            _withAlertsStatus: function(alertsStatusArray) {
                this.forEach(function (obj) {
                    var properties = obj.get("properties");
                    _.each(properties, function(property) {
                        if (_.indexOf(alertsStatusArray, property.key) !== -1) {
                            property.status = 1;
                        } else {
                            property.status = 0;
                        }
                    });
                });
            },

			_makeAlertsStatusProp: function (alertsStatusArray) {
				var payload = {
					datum: {
						key: "alertsStatus",
						value: JSON.stringify(alertsStatusArray)
					}
				};

				return App.salusConnector.makeAjaxCall(AylaConfig.endpoints.salusAlerts.alertsStatus.create, payload, "POST").then(function (data) {
					return JSON.parse(data.datum.value);
				});
			}
		}).mixin([AylaBackedMixin], {
			apiWrapperObjectName: "devices"
		});
	});

	return App.Models.SalusAlertsCollection;
});

