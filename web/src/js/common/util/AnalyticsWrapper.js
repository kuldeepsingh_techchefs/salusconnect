"use strict";

define(["app"], function (App) {
	var AnalyticsWrapper = {};

	/**
	 * trigger a page loaded analytics event
	 * @param section
	 * @param page
	 */
	AnalyticsWrapper.onPageLoaded = function (section, page) {
		AnalyticsWrapper.dispatchEvent("pageLoad", section, page);
	};

	/**
	 * trigger an onInteraction (eg: click) analytics event
	 * @param section
	 * @param page
	 * @param widget
	 */
	AnalyticsWrapper.onInteraction = function (section, page, interaction, interactionKey) {
		AnalyticsWrapper.dispatchEvent("interaction", section, page, interaction, interactionKey);
	};

	/**
	 * fire of the event itself
	 * @param type
	 * @param section
	 * @param page
	 * @param widget
	 */
	AnalyticsWrapper.dispatchEvent = function (type, section, page, interaction, interactionKey) {
		var eventObj = {
			type: "analytics",
			analyticsType: type,
			section: section,
			page: page
		};

		if (interaction) {
			eventObj.interaction = interaction;
		}

		if (interactionKey) {
			eventObj.interactionKey = interactionKey;
		}

		if (!App.isDevOrInt()) {
			//try {
			//	App.salusConnector.log(eventObj).catch(function () {
					//Eat network errors. They have all ready been logged to console.
					//This will pevent an infiite loop as we also logged unhandled Rejections.
			//	});
			//} catch (e) {
				//Use console not app.error to prevent infinite loop.
			//	console.error(e);
			//}
		} else {
			console.log("Analytics Event:\n" + JSON.stringify(eventObj, null, 2));
		}
	};

	return AnalyticsWrapper;

});
