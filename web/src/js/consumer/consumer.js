"use strict";

define([
	"app",
	"consumer/models/DashboardManager"
], function (App) {

	App.module("Consumer", function (Consumer, App, B, Mn, $, _) {

		App.addInitializer(function () {
			Consumer.shouldScrollTo = "";
			Consumer.dashboardManager = new Consumer.Models.DashboardManager();
		});

		Consumer.fillDropdownCollection = function (list, prefix, collection) {
			require(["consumer/models/SalusDropdownViewModel"], function () {
				_.each(list, function (key) {
					collection.add(new App.Consumer.Models.DropdownItemViewModel({
						value: key,
						displayText: prefix + "." + key
					}));
				});
			});
		};
	});

	return App.Consumer;
});