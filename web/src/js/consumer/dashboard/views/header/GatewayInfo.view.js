"use strict";

define([
	"app",
	"consumer/consumerTemplates",
	"common/config",
	"momentWrapper",
	"consumer/views/mixins/mixin.salusView"
], function (App, templates, config, moment, SalusView) {

	App.module("Consumer.Dashboard.Views", function (Views, App, B, Mn) {
		Views.GatewayInfoView = Mn.ItemView.extend({
			template: templates["dashboard/header/gatewayInfo"],
			className: "gateway-info",
			bindings: {
				".bb-gateway-address": {
					observe: "address",
					onGet: function (address) {
						if (address && address.get("street")) {
							return address.get("street");
						}

						return "";
					}
				},
				".bb-gateway-connection": {
					observe: "connection_status",
					update: function ($el, status) {
						$el.toggleClass("online", status === "Online");
					}
				}
			},
			initialize: function (/*options*/) {
				var that = this;

				if (this.model) {
					this.listenTo(App.Consumer.dashboardManager, "change:lastScannedDate", function (scannedGateway) {
						if (scannedGateway === that.model) {
							that.render();
						}
					});

					// we want to re-render the view every minute so the last scanned date gets updated
					this.renderTimer = window.setInterval(this.render, config.dashboardHeaderRenderInterval);
				}
			},

			onDestroy: function () {
				// remove the re-render call when the view is destroyed
				window.clearInterval(this.renderTimer);
			},

			templateHelpers: function () {
				var lastScanned = App.translate("dashboard.header.scanned") + " " +
					moment(App.Consumer.dashboardManager.getLastScanned(this.model), undefined, this.model).fromNow();

				return {
					lastScanned: lastScanned
				};
			}
		}).mixin([SalusView]);
	});

	return App.Consumer.Dashboard.Views.GatewayInfoView;
});