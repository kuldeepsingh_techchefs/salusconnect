"use strict";

define([
	"app",
	"bluebird",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/dashboard/views/header/GatewayInfo.view",
	"consumer/dashboard/views/header/Weather.view",
	"consumer/dashboard/views/header/SettingsPanel.view"
], function (App, P, templates, SalusView) {


	App.module("Consumer.Dashboard.Views", function (Views, App, B, Mn) {
		Views.Overview = Mn.LayoutView.extend({
			template: templates["dashboard/header/overview"],
			regions: {
				weather: ".bb-weather",
				gatewayInfo: ".bb-gateway-info"
			},
			triggers: {
				"click .bb-settings-button": "openSettings"
			},
			initialize: function (options) {
				this.gateway = options.gateway;
			},

			onRender: function () {
				var that = this;

				this.gateway.get("address").getFetchPromise().then(function () {
					that.gatewayInfo.show(new Views.GatewayInfoView({
						model: that.gateway
					}));

//					if (!!that.gateway) {
//						that.gateway.getWeather(true).then(function (model) {
//							if (!that.isDestroyed && model) {
//								that.weather.show(new Views.WeatherView({
//									model: model,
//									gateway: that.gateway
//								}));
//							}
//						});
//					}
				});
			},
			handleScanClick: function () {
				this.weather.currentView.$el.hide();
			},
			onBeforeDestroy: function () {
				this.gateway = null;
			}
		}).mixin([SalusView]);
	});

	return App.Consumer.Dashboard.Views.Overview;
});