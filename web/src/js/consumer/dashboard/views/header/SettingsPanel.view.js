"use strict";

define([
	"app",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/mixins/mixin.salusButton"
], function (App, templates, SalusView, SalusButton) {

	App.module("Consumer.Dashboard.Views", function (Views, App, B, Mn, $, _) {

		Views.SettingsButtonView = Mn.ItemView.extend({
			template: templates["dashboard/header/settingsButton"],
			className: "settings-menu-button",
			ui: {
				icon: ".bb-icon",
				buttonText: ".bb-button-text"
			},

			initialize: function (options) {
				this.$el.addClass(options.classes || "");
				this.iconClass = options.iconClass || "";
				this.translationKey = options.translationKey || "";
			},

			onRender: function () {
				this.ui.icon.addClass(this.iconClass);
				this.ui.buttonText.text(App.translate(this.translationKey));
			}
		}).mixin([SalusButton]);

		//TODO abstract the SettingsButtonView props/functionality into a mixin.
		Views.OpenFolderButtonView = Mn.ItemView.extend({
			template: templates["dashboard/header/openFolderButton"],
			className: "settings-menu-button",
			ui: {
				icon: ".bb-icon",
				buttonText: ".bb-button-text",
				"fileForm": "#fileinfo",
				"fileInput": "#file"
			},

			initialize: function (options) {
				this.$el.addClass(options.classes || "");
				this.iconClass = options.iconClass || "";
				this.translationKey = options.translationKey || "";
			},

			onRender: function () {
				this.ui.icon.addClass(this.iconClass);
				this.ui.buttonText.text(App.translate(this.translationKey));

				var that = this;
				this.ui.fileInput.change(function () {
					if (that.ui.fileInput.val()) {
						that.trigger("spinner:start");
						App.Consumer.dashboardManager.saveDesktopPhotoInGateway(that.ui.fileForm).then(function () {
							that.trigger("spinner:end");
							that.trigger("fileUploadDone");
						}).catch(function (error) {
							App.error(error);

							if (error.statusText.indexOf("Too Large") !== -1) {
								App.warn("Image was too large. Couldn't upload.");

									that.errorModal = new App.Consumer.Views.SalusAlertModalView({
										staticBackdrop: true,
										model: new App.Consumer.Models.AlertModalViewModel({
											iconClass: "icon-warning",
											primaryLabelText: App.translate("errors.imageUpload"),
											leftButton: new App.Consumer.Views.ModalCloseButton({
												classes: "width100",
												buttonTextKey: "common.labels.close",
												clickedDelegate: function() {
													App.hideModal();
												}
											 })
										})
									});
									App.modalRegion.show(that.errorModal);
									App.showModal();
							}

							that.trigger("spinner:end");
						});
					}
				});
			},

			onBeforeDestroy: function () {
				this.ui.fileInput.off();
			}

		}).mixin([SalusButton]);

		Views.SettingsPanelView = Mn.LayoutView.extend({
			template: templates["dashboard/header/settingsPanel"],
			className: "settings-panel",
			regions: {
				topButtonRegion: "#bb-top-button-region",
				bottomButtonRegion: "#bb-bottom-button-region"
			},
			events: {
				"click #bb-settings-close-button,.settings-close-cover": "handleClose"
			},
			initialize: function () {
				_.bindAll(this, "handleScanClick", "handlePhotoClick", "handleCapturePhotoClick", "handlePhotoLibraryClick", "handleClose");

				this.mode = "general";
			},

			onRender: function () {
				var that = this;

				if (this.mode === "general") {
					// top button, scan my home
					this.scanButton = new Views.SettingsButtonView({
						classes: "scan-button no-bg-button",
						translationKey: "dashboard.header.scanMyHome",
						iconClass: "icon-scan",
						clickedDelegate: this.handleScanClick
					});

					if (App.onMobile()) {
						// bottom button, mobile, take a photo, brings up submenu
						this.photoButton = new Views.SettingsButtonView({
							classes: "photo-button",
							translationKey: "dashboard.header.addAPhoto",
							iconClass: "icon-camera",
							clickedDelegate: this.handlePhotoClick
						});
					} else {
						// bottom button, desktop, browse for photo
						this.photoButton = new Views.OpenFolderButtonView({
							classes: "open-folder-button no-bg-button",
							translationKey: "dashboard.header.choosePhoto",
							iconClass: "icon-folder"
						});

						this.listenTo(this.photoButton, "fileUploadDone", function () {
							that.trigger("close");
						});
					}

					this.listenTo(this.photoButton, "spinner:start", this.showSpinner);
					this.listenTo(this.photoButton, "spinner:end", this.hideSpinner);
					this.topButtonRegion.show(this.scanButton);
					this.bottomButtonRegion.show(this.photoButton);

				} else if (this.mode === "photo") {
					// top button, photo menu, capture a photo from device
					this.capturePhotoBtn = new Views.SettingsButtonView({
						classes: "capture-photo-button",
						translationKey: "dashboard.header.takeAPhoto",
						iconClass: "icon-camera",
						clickedDelegate: this.handleCapturePhotoClick
					});

					// bottom button, photo menu, choose photo from photo library
					this.photoLibraryBtn = new Views.SettingsButtonView({
						classes: "photo-library-button",
						translationKey: "dashboard.header.choosePhoto",
						iconClass: "icon-folder",
						clickedDelegate: this.handlePhotoLibraryClick
					});
					
					this.listenTo(this.capturePhotoBtn, "spinner:start", this.showSpinner);
					this.listenTo(this.capturePhotoBtn, "spinner:end", this.hideSpinner);

					this.topButtonRegion.show(this.capturePhotoBtn);
					this.bottomButtonRegion.show(this.photoLibraryBtn);
				}
			},

			handleScanClick: function () {
				this.trigger("scan");
				this.trigger("close");
			},

			handlePhotoClick: function () {
				this.mode = "photo";
				this.render();
			},

			handleCapturePhotoClick: function () {
				this.trigger("capturePhotoClick");
			},

			handlePhotoLibraryClick: function () {
				this.trigger("photoLibraryClick");
			},

			handleClose: function () {
				if (this.mode === "photo") {
					this.mode = "general";
					this.render();
				} else {
					this.trigger("close");
				}
			}
		}).mixin([SalusView]);
	});

	return App.Consumer.Dashboard.Views.SettingsPanelView;
});