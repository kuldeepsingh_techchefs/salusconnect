"use strict";

define([
	"app",
	"common/constants",
	"common/config",
	"consumer/consumerTemplates",
	"consumer/dashboard/views/tileContentViews/mixin.TileContentView",
	"consumer/views/mixins/mixin.FC600ArcView",
	"consumer/dashboard/views/tileContentViews/thermostatSpecific/ModeMenu.view",
	"roundslider"
], function (App, constants, config, consumerTemplates, TileContentViewMixin, FC600ArcViewMixin) {

	App.module("Consumer.Dashboard.Views.TileContentViews", function (Views, App, B, Mn, $, _) {
		Views.FC600Tile = {};

		Views.FC600Tile.FrontView = Mn.ItemView.extend({
			className: "thermostat-tile fc600-tile",
			template: consumerTemplates["dashboard/tile/fc600TileFront"],
			ui: {
				tstatArc: ".bb-tstat-arc",
				mainModeIcon: ".bb-main-mode-icon",
				holdModeIcon: ".bb-hold-mode-icon",
				fanModeIcon: ".bb-fan-mode-icon",
				ocupancyIcon: ".bb-ocupancy-icon"
			},
			attributes: {
				role: "button"
			},
			bindings: {
				":el": {
					observe: "RunningMode",
					onGet: function (prop) {
						return prop ? prop.getProperty() : null;
					},
					update: function ($el, val) {
						var modeClassName,
							classNameArray = [
								"idle",
								"cooling",
								"heating"
							];

						$el.removeClass(classNameArray.join(" "));

						if (_.isNull(val)) {
							modeClassName = classNameArray[0];
						} else {
							switch (val) {
								case constants.runningModeTypes.OFF:
									modeClassName = classNameArray[0];
									break;
								case constants.runningModeTypes.COOL:
									modeClassName = classNameArray[1];
									break;
								case constants.runningModeTypes.HEAT:
									modeClassName = classNameArray[2];
									break;
								default:
									modeClassName = classNameArray[0];
									break;
							}
						}

						$el.addClass(modeClassName);
					}
				},
				".bb-main-mode-icon": {
					observe: ["SystemMode", "RunningMode"],
					onGet: function (props) {
						return [props[0] ? props[0].getProperty() : null, props[1] ? props[1].getProperty() : null];
					},
					update: function ($el, vals) {
						var modeClassName,
							classNameArray = [
								"power-icon", //0
								"auto-icon", //1
								"cooling-icon-on", //2
								"heating-icon-on", //3
								"cooling-icon-off", //4
								"heating-icon-off", //5
                                "follow-cooling-icon-on", //6
								"follow-heating-icon-on", //7
								"follow-cooling-icon-off", //8
								"follow-heating-icon-off" //9
							];

						$el.removeClass(classNameArray.join(" "));

						var systemMode = vals[0],
							runningMode = vals[1];
                    
                        var S1ComTerminals = _.isObject(this.model.get("S1ComTerminals")) ? this.model.getPropertyValue("S1ComTerminals") : "";
						
						if (_.isNull(systemMode) || _.isNull(runningMode)) {
							modeClassName = classNameArray[0];
						} else {
							switch (systemMode) {
								case constants.thermostatModeTypes.OFF:
									modeClassName = classNameArray[0];
									break;
								case constants.thermostatModeTypes.AUTO:
									modeClassName = classNameArray[1];
									break;
								case constants.thermostatModeTypes.COOL:
									if (runningMode === constants.runningModeTypes.OFF) {
										modeClassName = S1ComTerminals === 0 ? classNameArray[4] : classNameArray[8];
									} else if (runningMode === constants.runningModeTypes.COOL) {
										modeClassName = S1ComTerminals === 0 ? classNameArray[2] : classNameArray[6];
									}
									break;
								case constants.thermostatModeTypes.HEAT:
								case constants.thermostatModeTypes.EMERGENCYHEATING:
									if (runningMode === constants.runningModeTypes.OFF) {
										modeClassName = S1ComTerminals === 0 ? classNameArray[5] : classNameArray[9];
									} else if (runningMode === constants.runningModeTypes.HEAT) {
										modeClassName = S1ComTerminals === 0 ? classNameArray[3] : classNameArray[7];
									}
									break;
								default:
									modeClassName = classNameArray[3];
									break;
							}
						}

						$el.addClass(modeClassName);
					}
				},
				".bb-hold-mode-icon": {
					observe: "HoldType",
					onGet: function (prop) {
						return prop ? prop.getProperty() : null;
					},
					update: function ($el, val) {
						var modeClassName,
							classNameArray = [
								"hold-schedule-icon", // follow schedule
								"hold-temporary-icon",
								"hold-permanent-icon",
								"hold-eco-icon",
								"power-icon"
							];

						$el.removeClass(classNameArray.join(" "));

						if (_.isNull(val)) {
							modeClassName = classNameArray[0];
						} else {
							switch (val) {
								case constants.fc600HoldTypes.FOLLOW:
									modeClassName = classNameArray[0];
									break;
								case constants.fc600HoldTypes.TEMPHOLD:
									modeClassName = classNameArray[1];
									break;
								case constants.fc600HoldTypes.PERMHOLD:
									modeClassName = classNameArray[2];
									break;
								case constants.fc600HoldTypes.ECO:
									modeClassName = classNameArray[3];
									break;
								case constants.fc600HoldTypes.OFF:
									modeClassName = classNameArray[4];
									break;
								default:
									modeClassName = classNameArray[0];
									break;
							}
						}

						$el.addClass(modeClassName);
						
						if (val === constants.fc600HoldTypes.OFF) {
							this.ui.mainModeIcon.css("visibility", "hidden");
							this.ui.fanModeIcon.css("visibility", "hidden");
						} else {
							this.ui.mainModeIcon.css("visibility", "visible");
							this.ui.fanModeIcon.css("visibility", "visible");
						}
					}
				},
				".bb-fan-mode-icon": {
					observe: "FanMode",
					onGet: function (prop) {
						return prop ? prop.getProperty() : null;
					},
					update: function ($el, val) {
						var modeClassName,
							classNameArray = [
								"fan-auto",
								"fan-speed3",
								"fan-speed2",
								"fan-speed1",
								"fan-off"
							];

						$el.removeClass(classNameArray.join(" "));

						if (_.isNull(val)) {
							modeClassName = classNameArray[0];
						} else {
							switch (val) {
								case constants.fanModeTypes.AUTO:
									modeClassName = classNameArray[0];
									break;
								case constants.fanModeTypes.SPEED3:
									modeClassName = classNameArray[1];
									break;
								case constants.fanModeTypes.SPEED2:
									modeClassName = classNameArray[2];
									break;
								case constants.fanModeTypes.SPEED1:
									modeClassName = classNameArray[3];
									break;
								case constants.fanModeTypes.OFF:
									modeClassName = classNameArray[4];
									break;
								default:
									modeClassName = classNameArray[0];
									break;
							}
						}

						$el.addClass(modeClassName);
					}
				},
				".bb-ocupancy-icon": {
					observe: ["S2ComTerminals", "Ocupancy"],
					onGet: function (props) {
						return [props[0] ? props[0].getProperty() : null, props[1] ? props[1].getProperty() : null];
					},
					update: function ($el, vals) {
						var modeClassName,
							classNameArray = [
								"occupied",
								"unoccupied"
							];

						$el.removeClass(classNameArray.join(" "));
						
						var s2ComTerminals = vals[0],
							ocupancy = vals[1];
						
						if (_.isNull(s2ComTerminals) || _.isNull(ocupancy)) {
							modeClassName = classNameArray[0];
						} else {
							switch (ocupancy) {
								case constants.ocupancy.OCCUPIED:
									modeClassName = classNameArray[0];
									break;
								case constants.ocupancy.UNOCCUPIED:
									modeClassName = classNameArray[1];
									break;
								default:
									modeClassName = classNameArray[0];
									break;
							}
						}
						$el.addClass(modeClassName);
						
						if (s2ComTerminals === 1) {
							$el.css("visibility", "visible");
						} else {
							$el.css("visibility", "hidden");
						}
					}
				},
				".rs-tooltip": {
					observe: "LocalTemperature_x100",
					onGet: function (value, options) {
						if (_.isObject(value)) {
							var temp = _.isNumber(value.getProperty("value")) ? value.getProperty("value") : config.thermostatMinimumTemp * 100;
							if (temp > 32767) {
								temp = (temp - 65536) / 100;
								return temp.toFixed(0);
							}
							
							var deviceSetting = options.view.model.getPropertyValue("DeviceSetting");
							if (_.isNull(deviceSetting)) {
								return temp /= 100;
							}
							
							if (deviceSetting[40] === "0") {
								temp /= 100;
								var index = temp.toString().indexOf(".");
								if (index === -1) {
									return temp;
								} else {
									return temp.toString().substring(0, index + 2);
								}
							} else {
								var a = temp % 50;
								var b = (a >= 25) ? 50 : 0;
								return (temp - a + b) / 100;
							}
						} else {
							value = 20;
						}

						return value;
					}
				}
			},
			initialize: function () {
				this.model.getDevicePropertiesPromise();
			}
		}).mixin([TileContentViewMixin]);

		Views.FC600Tile.BackView = Mn.ItemView.extend({
			className: "thermostat-tile fc600-tile",
			template: consumerTemplates["dashboard/tile/fc600TileBack"]
		}).mixin([TileContentViewMixin, FC600ArcViewMixin]);
	});

	return App.Consumer.Dashboard.Views.FC600Tile;
});
