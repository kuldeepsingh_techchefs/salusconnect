"use strict";

define([
	"app",
	"common/constants",
	"common/config",
	"consumer/consumerTemplates",
	"consumer/dashboard/views/tileContentViews/mixin.TileContentView",
	"consumer/views/mixins/mixin.thermostatArcView",
	"consumer/dashboard/views/tileContentViews/thermostatSpecific/ModeMenu.view",
	"roundslider"
], function (App, constants, config, consumerTemplates, TileContentViewMixin, ThermostatArcViewMixin) {

	App.module("Consumer.Dashboard.Views.TileContentViews", function (Views, App, B, Mn, $, _) {
		Views.ThermostatTile = {};

		Views.ThermostatTile.FrontView = Mn.ItemView.extend({
			className: "thermostat-tile",
			template: consumerTemplates["dashboard/tile/thermostatTileFront"],
			ui: {
				tstatArc: ".bb-tstat-arc",
				fanModeIcon: ".bb-fan-mode-icon",
				holdModeIcon: ".bb-hold-mode-icon"
			},
			attributes: {
				role: "button"
			},
			bindings: {
				":el": {
					observe: "RunningMode",
					onGet: function (prop) {
						return prop ? prop.getProperty() : null;
					},
					update: function ($el, val) {
						var modeClassName,
							classNameArray = [
								"idle",
								"cooling",
								"heating"
							];

						$el.removeClass(classNameArray.join(" "));

						if (_.isNull(val)) {
							modeClassName = classNameArray[0];
						} else {
							switch (val) {
								case constants.runningModeTypes.OFF:
									modeClassName = classNameArray[0];
									break;
								case constants.runningModeTypes.COOL:
									modeClassName = classNameArray[1];
									break;
								case constants.runningModeTypes.HEAT:
									modeClassName = classNameArray[2];
									break;
								default:
									modeClassName = classNameArray[0];
									break;
							}
						}

						$el.addClass(modeClassName);
					}
				},
				".bb-main-mode-icon": {
					observe: ["SystemMode", "RunningMode"],
					onGet: function (props) {
						return [props[0] ? props[0].getProperty() : null, props[1] ? props[1].getProperty() : null];
					},
					update: function ($el, vals) {
						var modeClassName,
							classNameArray = [
								"power-icon",
								"auto-icon",
								"cooling-icon-on",
								"heating-icon-on",
								"cooling-icon-off",
								"heating-icon-off"
							];

						$el.removeClass(classNameArray.join(" "));

						var systemMode = vals[0],
							runningMode = vals[1];
						
						if (_.isNull(systemMode) || _.isNull(runningMode)) {
							modeClassName = classNameArray[0];
						} else {
							switch (systemMode) {
								case constants.thermostatModeTypes.OFF:
									modeClassName = classNameArray[0];
									break;
								case constants.thermostatModeTypes.AUTO:
									modeClassName = classNameArray[1];
									break;
								case constants.thermostatModeTypes.COOL:
									if (runningMode === constants.runningModeTypes.OFF) {
										modeClassName = classNameArray[4];
									} else if (runningMode === constants.runningModeTypes.COOL) {
										modeClassName = classNameArray[2];
									}
									break;
								case constants.thermostatModeTypes.HEAT:
								case constants.thermostatModeTypes.EMERGENCYHEATING:
									if (runningMode === constants.runningModeTypes.OFF) {
										modeClassName = classNameArray[5];
									} else if (runningMode === constants.runningModeTypes.HEAT) {
										modeClassName = classNameArray[3];
									}
									break;
								default:
									modeClassName = classNameArray[3];
									break;
							}
						}

						$el.addClass(modeClassName);
					}
				},
				".rs-tooltip": {
					observe: "LocalTemperature_x100",
					onGet: function (value, options) {
						if (_.isObject(value)) {
							var temp = _.isNumber(value.getProperty("value")) ? value.getProperty("value") : config.thermostatMinimumTemp * 100;
							if (temp > 32767) {
								temp -= 65536;
							}
							
							temp /= 100;
							if (options.view.model.isIT600) {
                                if (temp === 45.5) {
                                    return "Lo";
                                } else if (temp === 46) {
                                    return "Hi";
                                } else if (temp > 46) {
                                    return "--";
                                }
                                
								temp = temp.toFixed(1);
								var index = temp.indexOf(".");
								if (temp[index + 1] < 5) {
									return temp.substring(0, index);
								} else {
									return temp.substring(0, index + 1) + "5";
								}
							} else {
								return temp.toFixed(0);
							}
						} else {
							value = 20;
						}

						return value;
					}
				}
			},
			initialize: function () {
				_.bindAll(this, "handleTstatArc", "_addOptimaBindings", "_addIt600Bindings");
				this.model.getDevicePropertiesPromise();
			},
			onRender: function () {
				if (!this.model.isIT600) { // if optima
					this.ui.holdModeIcon.detach();
					this._addOptimaBindings();
				} else {
					this.ui.fanModeIcon.detach();
					this._addIt600Bindings();
				}
			},
			/**
			 * Place any tasks that we need to handle once the DOM is loaded for a tile
			 * (E.G. kick off thermostat round slider render)
			 */
			handleTstatArc: function () {
				var that = this,
					$innerCircleGroup,
					tstatOuterWidth = this.ui.tstatArc.innerWidth(),
					$innerCircleDropShadowEl = $(_.template("<div class='bb-inner-shadow inner-shadow'></div>")());


				this.model.getDevicePropertiesPromise().then(function () {
					if (!that.isDestroyed) {
						that.ui.tstatArc.roundSlider({
							sliderType: "min-range",
							handleSize: "+10",
							handleShape: "round",
							circleShape: "pie",
							radius: tstatOuterWidth,
							min: config.thermostatMinimumTemp,
							max: config.thermostatMaximumTemp,
							step: 0.5,
							value: 18,
							startAngle: 315,
							width: 5, // width of spec arc * actual width of tile on screen / spec tile width
							showTooltip: true,
							editableTooltip: false,
							animation: false,
							readOnly: true
						});

						that.stickit();

						// In order to set an inner drop shadow on the circle, we need to create
						// and append a div to the auto-created roundSlider arc and apply a shadow to it
						$innerCircleGroup = that.ui.tstatArc.find(".rs-block.rs-outer.rs-border.rs-split");

						// Only apply the shadow if we don't already have one
						if ($innerCircleGroup && $innerCircleGroup.find(".bb-inner-shadow").length === 0) {
							$innerCircleGroup.append($innerCircleDropShadowEl);
						}
					}
				});
			},
			_addOptimaBindings: function () {
				this.addBinding(null, ".bb-fan-mode-icon", {
					observe: "FanMode",
					onGet: function (prop) {
						return prop ? prop.getProperty() : null;
					},
					update: function ($el, val) {
						var modeClassName,
							classNameArray = [
								"fan-auto",
								"fan-on"
							];

						$el.removeClass(classNameArray.join(" "));

						if (_.isNull(val)) {
							modeClassName = classNameArray[0];
						} else {
							switch (val) {
								case constants.fanModeTypes.AUTO:
									modeClassName = classNameArray[0];
									break;
								case constants.fanModeTypes.ON:
									modeClassName = classNameArray[1];
									break;
								default:
									modeClassName = classNameArray[0];
									break;
							}
						}

						$el.addClass(modeClassName);
					}
				});
			},
			_addIt600Bindings: function () {
				this.addBinding(null, ".bb-hold-mode-icon", {
					observe: "HoldType",
					onGet: function (prop) {
						return prop ? prop.getProperty() : null;
					},
					update: function ($el, val) {
						var modeClassName,
							classNameArray = [
								"hold-auto-icon", // follow schedule
								"hold-temporary-icon",
								"hold-permanent-icon",
								"power-icon"
							];

						$el.removeClass(classNameArray.join(" "));

						if (_.isNull(val)) {
							modeClassName = classNameArray[0];
						} else {
							switch (val) {
								case constants.it600HoldTypes.FOLLOW:
									modeClassName = classNameArray[0];
									break;
								case constants.it600HoldTypes.TEMPHOLD:
									modeClassName = classNameArray[1];
									break;
								case constants.it600HoldTypes.PERMHOLD:
									modeClassName = classNameArray[2];
									break;
								case constants.it600HoldTypes.OFF:
									modeClassName = classNameArray[3];
									break;
								default:
									modeClassName = classNameArray[0];
									break;
							}
						}

						$el.addClass(modeClassName);
					}
				});
			},
			onBeforeDestroy: function () {
				if (this.ui.tstatArc.roundSlider) {
					this.ui.tstatArc.roundSlider("destroy");
				}
			}
		}).mixin([TileContentViewMixin]);

		Views.ThermostatTile.BackView = Mn.ItemView.extend({
			className: "thermostat-tile",
			template: consumerTemplates["dashboard/tile/thermostatTileBack"]
		}).mixin([TileContentViewMixin, ThermostatArcViewMixin]);
	});

	return App.Consumer.Dashboard.Views.ThermostatTile;
});
