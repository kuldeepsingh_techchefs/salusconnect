"use strict";

define([
	"app",
	"underscore",
	"common/util/utilities",
	"common/config",
	"common/constants",
	"consumer/views/mixins/mixin.salusView"
], function (App, _, utilities, config, constants, SalusView) {

	App.module("Consumer.Dashboard.Views.TileContentViews", function (Views) {
		Views.TileContentViewMixin = function (options) {
			this.mixin([
				SalusView // by mixing in TileContentView we also mixin SalusView
			], options);

			this.addToObj({
				events: {
					"click .bb-pin-icon": "unpinFromDashboard"
				},
				bindings: {
					".bb-front-tile-title": {
						observe: "name",
						onGet: function (name) {
							return utilities.shortenText(name, config.tiles.frontTitleMaxLength);
						}
					},
					".bb-back-tile-title": {
						observe: "name",
						onGet: function (name) {
							return utilities.shortenText(name, config.tiles.backTitleMaxLength);
						}
					},
					".bb-pin-icon": {
						observe: "isInDashboard",
						update: function ($el, val, model) {
	                        var deviceType = model.modelType;
                            if(constants.isPinDevices(deviceType)){
                                var modelVal = model.get("isInDashboard");
                                if (modelVal === null || _.isUndefined(modelVal)) {
                                    $el.removeClass("pin-icon").removeClass("unpin-icon").addClass("hidden");
                                } else if (modelVal === false) {
                                    $el.removeClass("hidden").removeClass("pin-icon").addClass("unpin-icon");
                                } else if (modelVal === true) {
                                    $el.removeClass("hidden").removeClass("unpin-icon").addClass("pin-icon");
                                }
	                        }
	                        else{
	                            $el.removeClass("pin-icon").removeClass("unpin-icon").addClass("hidden");
	                        }
						}
					}					
				}
			});

			this.setDefaults({
				initiateRefreshPoll: function () {
					this.devicePropsPollingInterval = setInterval(this.refreshPoll, config.devicePollingInformation.devicePropsPollingInterval);
				},
				clearRefreshPollTimeout: function () {
					clearInterval(this.devicePropsPollingInterval);
				},
				refreshPoll: function () {
					this.model.getDeviceProperties(true);
				},
				
				unpinFromDashboard: function ($el) {
					var ele = $($el.currentTarget);
					if (this.model.get("isInDashboard")) {
						var refType = "device";
						if (this.model.get("conditions")) { // chosen arbitrarily to identify rule model
							refType = "rule";
						}
						App.salusConnector.unpinFromDashboard(this.model, refType);
						if (ele.closest(".dashboard-modal-container").length) {
							ele.closest(".bb-tile-container").remove();							
						}
						if(App.flippedTile) {
							App.flippedTile = null;
						}																
					}					
				},
				showSmallTileSpinner: utilities.noOp,
				//		function () {
				//	this.trigger("toggle:smallSpinner", true);
				//},

				hideSmallTileSpinner: utilities.noOp
				//		function () {
				//	this.trigger("toggle:smallSpinner", false);
				//}
			});

			this.before("initialize", function (options) {
				this.isLargeTile = !!options.isLargeTile;

				if (options.frontView) {
					this.frontView = options.frontView;
				}
				
				this.$el.addClass("tile-content-view");
				
				this.tileSpinnerOptions = {
					textKey: constants.spinnerTextKeys.processing,
					greyBg: true
				};

				_.bindAll(this, "unpinFromDashboard");
			});
			
			this.after('initialize', function () {
				var that = this;

				// so many binds, but better safe than sorry
				_.bindAll(this,
					"initiateRefreshPoll",
					"clearRefreshPollTimeout",
					"refreshPoll"
				);

				// TODO update this for the non-flip case of equipment widget view
				this.listenTo(this, "tile:flipToFront", function () {
					that.clearRefreshPollTimeout();
				});

				// TODO update this for the non-flip case of equipment widget view
				this.listenTo(this, "tile:flipToBack", function () {
					that.initiateRefreshPoll();
				});
			});
			
			this.before("destroy", function () {
				this.clearRefreshPollTimeout();
			});
		};
	});

	return App.Consumer.Dashboard.Views.TileContentViews.TileContentViewMixin;
});
