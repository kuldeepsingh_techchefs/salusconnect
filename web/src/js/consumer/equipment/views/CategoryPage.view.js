"use strict";

define([
	"app",
	"bluebird",
	"common/constants",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusPage",
	"consumer/views/Breadcrumbs.view",
	"consumer/equipment/views/myEquipment/GroupsTab.view",
	"consumer/equipment/views/EquipmentTileList.view",
	"consumer/equipment/views/ThermostatCategory.view"
], function (App, P, constants, templates, SalusPageMixin, BreadcrumbsView) {

	App.module("Consumer.Equipment.Views", function (Views, App, B, Mn, $, _) {

		Views.CategoryPageView = Mn.LayoutView.extend({
			id: "bb-category-page",
			className: "category-page container",
			template: templates["equipment/myEquipment/categoryPage"],
			regions: {
				breadcrumbs: ".bb-breadcrumbs",
				equipmentList: ".bb-equipment-list"
			},
			initialize: function (options) {
				//TODO: Actually grab the correct cagegory.
				//TODO: redirect if category type is not correct.
				this.categoryType = options.type;
			},
			onRender: function () {

				localStorage.setItem("subCategory","true");
				var that = this, dependencies = ["devices"];
                
                var href="/settings/categories/" + that.categoryType;
                        
                //暂时都取消掉
                href="";

				this.breadcrumbs.show(new BreadcrumbsView.BreadcrumbsView(
					{ crumbs: [
						{
							textKey: "equipment.myEquipment.title",
							href: "/equipment"
						},
						{
							text: App.translate("equipment.myEquipment.categories.types." + that.categoryType),
							active: true,
							href: href
						}
					]}
				));

				if (this.categoryType === constants.categoryTypes.THERMOSTATS) {
					dependencies.push("tscs");
				}

				App.salusConnector.getDataLoadPromise(dependencies).then(function (/*arrayOfData*/) {
					var allEquipment = App.salusConnector.getUserDeviceCollection();

					var equipmentInCategory = allEquipment.filter(function (device) {
							return device.get("device_category_type") === that.categoryType;
						});

					if (that.categoryType !== constants.categoryTypes.THERMOSTATS) {
						that.equipmentList.show(new Views.EquipmentTileListView({
							collection: new B.Collection(equipmentInCategory),
							type: constants.equipmentLists.category,
							categoryType: that.categoryType
						}));
					} else {
						//tsc needs EUID prop
						var propertyPromises = _.map(equipmentInCategory, function (device) {
							return device.getDevicePropertiesPromise();
						});

						P.all(propertyPromises).then(function () {
							that.equipmentList.show(new Views.ThermostatCategoryView());
						});
					}
				});
			}
		}).mixin([SalusPageMixin], {
			analyticsSection: "equipment",
			analyticsPage: "category" //TODO: This should be a function so the Category Id is recorded.
		});
	});

	return App.Consumer.Equipment.Views.CategoryPageView;
});