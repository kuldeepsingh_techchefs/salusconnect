"use strict";

define([
    "app",
    "consumer/consumerTemplates",
    "consumer/views/mixins/mixin.salusView",
    "consumer/views/FormTextInput.view",
    "consumer/views/SalusButtonPrimary.view",
	"consumer/views/modal/ModalCloseButton.view"
], function (App, consumerTemplates, SalusView) {

    App.module("Consumer.Equipment.Views", function (Views, App, B, Mn, $, _) {
        Views.EquipmentAliasEdit = Mn.LayoutView.extend({
            getTemplate: function () {
                if (this.options.page === "settings") {
                    return consumerTemplates["settings/equipment/settingsAliasEdit"];
                } else if (this.options.page === "equipment") {
                    return consumerTemplates["equipment/widgets/equipmentAliasEdit"];
                }
            },
            id: "bb-equip-alias-edit",
            className: "equip-alias-edit",
            regions: {
                aliasRegion: ".bb-alias-textbox",
                cancelRegion: ".bb-alias-cancel",
                saveRegion: ".bb-alias-save"
            },
            initialize: function (/*options*/) {
                _.bindAll(this, "handleCancelClick", "handleSaveClick");
            },
            onRender: function () {
                this.aliasRegion.show(new App.Consumer.Views.FormTextInput({
                    required: true,
                    value: this.model.get("name") || "",
                    labelText: "equipment.status.alias"
                }));

                this.cancelRegion.show(new App.Consumer.Views.SalusButtonPrimaryView({
                    className: "btn btn-default width100",
                    buttonTextKey: "common.labels.cancel",
                    clickedDelegate: this.handleCancelClick
                }));

                this.saveRegion.show(new App.Consumer.Views.SalusButtonPrimaryView({
                    classes: "width100",
                    buttonTextKey: "common.labels.save",
                    clickedDelegate: this.handleSaveClick
                }));
            },
            handleCancelClick: function () {
                this.trigger("close");
            },
            handleSaveClick: function () {
                var that = this,
                        newAlias = this.aliasRegion.currentView.getValue();
                
                // Dev_SCS-3284 Start
                if(App.getIsDemo()) {
                    this.trigger("close");
                    return;
                }
                // Dev_SCS-3284 End
                
                 //gateway name can't be duplicate
                var gateways = App.salusConnector.getGatewayCollection().models;
                var newGateways = [];
                _.each(gateways,function(gateway){
                    if(gateway.get("key") !== that.model.get("key")){
                        newGateways.push(gateway);
                    }
                });
                var names = _.map(newGateways, function (obj) {
                    return obj.get("name");
                });
                names.push(newAlias);
               
                
                var hasDuplicateNames = _.uniq(names).length !== names.length;
                
                if(hasDuplicateNames){
                     this.errorModal = new App.Consumer.Views.SalusAlertModalView({
                        staticBackdrop: true,
                        model: new App.Consumer.Models.AlertModalViewModel({
                            iconClass: "icon-warning",
                            primaryLabelText: App.translate("equipment.error.renameError"),
                            rightButton: new App.Consumer.Views.ModalCloseButton({
                                classes: "btn-danger width100",
                                buttonTextKey: "common.labels.ok",
                                clickedDelegate: App.hideModal()
                            }),
                            leftButton: new App.Consumer.Views.ModalCloseButton({
                                 classes: "width100",
                                 buttonTextKey: "common.labels.cancel",
                                 clickedDelegate: App.hideModal()
                             })
                        })
                     });
                     App.modalRegion.show(this.errorModal);
                     App.showModal();
                    return;
                }
                
                this.aliasRegion.currentView.hideErrors();
                if(_.isString(newAlias) && newAlias.trim().length === 0) {
                    this.aliasRegion.currentView.showErrors(App.translate("common.nameError"));
                    return false;
                }
                
                var valid = _.some(App.salusConnector.getDeviceCollection().models, function(model) {
                    return model.get("name") === newAlias && model.get("dsn") !== that.model.get("dsn");
                });
                if(valid) {
                    this.aliasRegion.currentView.showErrors(App.translate("equipment.error.renameError"));
                    return false;
                }
                
                // allow set empty string
                if (_.isString(newAlias)) {
                    this.model.set("alias", newAlias);
                    this.model.set("product_name", newAlias);
                    this.model.saveProductName();
                    this.model.persist().then(function () {
                        that.trigger("saved");
                        that.trigger("click:saveName",that);
                    });
                }

                this.trigger("close");
            }
        }).mixin([SalusView]);
    });

    return App.Consumer.Equipment.Views.EquipmentAliasEdit;
});