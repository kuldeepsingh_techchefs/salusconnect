"use strict";

define([
	"app",
    "bluebird",
	"momentWrapper",
	"common/constants",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView"
], function (App, P, moment, constants, consumerTemplates, SalusView) {

	App.module("Consumer.Equipment.Views", function (Views, App, B, Mn, _) {

		Views.EquipmentInfoWidgetRow = Mn.ItemView.extend({
			template: consumerTemplates["equipment/widgets/equipmentInfoWidgetRow"],
			className: "information-content-row row",
			bindings: function () {
				return {
					".bb-info-content": {
						observe: this.observedProp,
						onGet: this.onGetter
					}
				};
			},
			ui: {
				title: ".bb-info-title"
			},
			initialize: function () {
				this.type = this.options.type;

				this.observedProp = this.options.observedProp;
				this.onGetter = this.options.onGetter;
			},
			onRender: function () {
				var titleKey = "equipment.information.headers." + this.type;

				this.ui.title.text(App.translate(titleKey));
			}
		}).mixin([SalusView]);

		Views.EquipmentInfoWidgetContentView = Mn.CollectionView.extend({
			template: false,
			className: "info-widget-content col-xs-12",
			childView: Views.EquipmentInfoWidgetRow,
			childViewOptions: function (model) {
				var type = model.get("type"),
					stickitInfo = this.getStickitInfo(type);

				return {
					model: this.model,
					type: type,
					observedProp: stickitInfo[0],
					onGetter: stickitInfo[1]
				};
			},
            
			
		
			initialize: function () {
                var that = this;
                
                    
                this.collection = new B.Collection([ // todo: does this need to be done better
                        {type: "connection"},
                        {type: "oem"},
                        {type: "dsn"},
                        {type: "mac"}
                    ]);
                   // if (this.model.isGateway() && this.model.get("version")) {
                    //if (this.model.isGateway() && this.model.get("version") && this.options.isSettingsPage) {
                        
//                    } else if (!this.model.isGateway()) {
//                        this.collection.add({type: "firmware"});
//                    }
                    if(this.model.isGateway()){
                        this.devices = App.salusConnector.getGatewayAndDelegates();
                        var object = App.Consumer.Settings.Models.SettingsContentFactory.getPropFromDeviceOrDevices(this.devices,"FirmwareVersion");
                        if(!object || object.get("value") === null){
                            var promise = P.all(this.devices.map(function (device) {
                               return device.getDeviceProperties();
                            }));
                            promise.then(function () {
                                that.collection.add({type: "version"});
                                that.collection.add({type: "coordinate"});

                            });
                        }else{
                             this.collection.add({type: "version"});
                             this.collection.add({type: "coordinate"});

                        }
                        
                    }else{
                        this.collection.add({type: "firmware"});
                    }

				
			},
           
          
			/**
			 * get stickit Binding information
			 * @param type
			 * @returns {Array}
			 * index 0 will be observed property, index 1 will be onGet function
			 */
			getStickitInfo: function (type) {
				var stickitVals = [];

				if (type === "connection") {
					// gateway shows connected date, others show online/offline
					stickitVals.push(["connection_status", "OnlineStatus_i", "LeaveNetwork"]);
					stickitVals.push(function (connectionArray) {
						var connectionStatus = connectionArray[0], 
							onlineStatus_i = connectionArray[1], 
							leaveNetwork = connectionArray[2], 
							connection;

                        if (connectionStatus === "Online") {
                            connection = App.translate("equipment.status.online");
                        } else if (connectionStatus === "Offline") {
                            connection = App.translate("equipment.status.offline");
                        } else if (onlineStatus_i === 1) {
                            connection = App.translate("equipment.status.online");
                        } else {
                            connection = App.translate("equipment.status.offline");
                        }
                        //TODO: leaveNetwork
                        if (leaveNetwork && leaveNetwork.getProperty() === 1) {
                            connection = App.translate("equipment.status.leftNetwork");
                        }

						return connection || "-";
					});

				} else if (type === "oem") {
                    stickitVals.push("localOem");
					stickitVals.push(function (dsn) {
						return dsn || "-";
					});

				} else if (type === "dsn") {
					stickitVals.push("dsn");
					stickitVals.push(function (dsn) {
						return dsn || "-";
					});

				} else if (type === "mac") {
					stickitVals.push("EUID");
					stickitVals.push(function (EUID) {
						// gateways kept mac property, all others EUID
						var mac, formatted;

						if (!EUID && this.model.get("mac")) {
							mac = this.model.get("mac") || "-";
						} else {
							mac = EUID ? EUID.getProperty() : "-";
						}

						// try to make it look like a mac address instead of just a string of numbers
						if (mac && mac !== "-" && mac.length !== 0) {
							// find groups of 1 or 2 numbers and put into array
							formatted = mac.match(/.{1,2}/g);

							// prepend 00 if needed, n times
							for (var i = formatted.length; i < 6; i++) {
								formatted.unshift("00");
							}

							formatted = formatted.join(":").toUpperCase();
						}

						return formatted || "-";
					});

				} else if (type === "version") {//GW version
                    var object = App.Consumer.Settings.Models.SettingsContentFactory.getPropFromDeviceOrDevices(this.devices,"GatewaySoftwareVersion");
                    var  software = object ? object.getProperty() : "-";
					stickitVals.push("version");
					stickitVals.push(function (version) {
                        return software ? software : "-";
					});
				} else if (type === "firmware") {
					stickitVals.push("FirmwareVersion");
					stickitVals.push(function (firmware) {
						return firmware ? firmware.getProperty() : "-";
					});
                }
                else if (type === "coordinate") {//ZC version
                    
                    var object = App.Consumer.Settings.Models.SettingsContentFactory.getPropFromDeviceOrDevices(this.devices,"FirmwareVersion");
                    
                    var software = object ? object.getProperty() : "-";
                    stickitVals.push("coordinate");
					stickitVals.push(function (coordinate){
                        
                    return software ? software : "-";

						//return coordinate ? software : "-";
					});
				}

				return stickitVals;
			}

		}).mixin([SalusView]);
	});

	return App.Consumer.Equipment.Views.EquipmentInfoWidgetContentView;
});