"use strict";

define([
	"app",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/SalusButtonPrimary.view",
	"consumer/models/SalusButtonPrimary"
], function (App, consumerTemplates, SalusViewMixin, SalusBtnPrimView) {

	App.module("Consumer.Login.Views", function (Views, App, B, Mn, $, _) {
		Views.WelcomeWellView = Mn.ItemView.extend({
			id: "login-welcome-well",
			template: consumerTemplates["login/WelcomeWell"],
			initialize: function () {
                // Dev_SCS-3284
                // _.bindAll(this, "handleButtonClicked");
				_.bindAll(this, "handleButtonClicked", "demoSignIn");

				this.signInButton = new SalusBtnPrimView({
					id: "sign-up-btn",
					buttonTextKey: "login.welcome.accountLink",
					clickedDelegate: this.handleButtonClicked
				});
                // Dev_SCS-3284 Start
                this.demoSignInButton = new App.Consumer.Views.SalusButtonPrimaryView({
                    id: "demo-sign-in-btn",
                    buttonTextKey: "login.welcome.demoButton",
                    clickedDelegate: this.demoSignIn
                });
                // Dev_SCS-3284 End
			},

			onRender: function () {
				this.$("#sign-in-btn-region").append(this.signInButton.render().$el);
                // Dev_SCS-3284
                this.$("#demo-sign-in-btn-region").append(this.demoSignInButton.render().$el);
				return this;
			},

			onDestroy: function () {
				this.signInButton.destroy();
			},

			handleButtonClicked: function () {
				App.navigate("login/signIn");
			},
            
            // Dev_SCS-3284 Start
            demoSignIn: function() {
                var that = this, sc = App.salusConnector;
                this.demoSignInButton.showSpinner();
                var loginPromise = sc.loginWithUsername("SalusUGdemo@gmail.com", "Salus123");
                var errorsEl = this.$(".bb-error"), errorMessage;
                errorsEl.addClass("invisible");
                loginPromise.then(function () {
                    window.sessionStorage.setItem("isDemo", true);
                    sc.getDataLoadPromise(["devices"]).then(function () {
                        sc.firstTimeLoginRouteCheck();
                        that.demoSignInButton.hideSpinner();
                    });
                }).catch(function (e) {
                    that.demoSignInButton.hideSpinner();
                    if (e.status === 401) {
                        if (e.responseText.indexOf("Invalid email or password.") > -1) {
                            errorMessage = App.translate("login.login.error.invalidUsernamePassword");
                        } else if (e.responseText.indexOf("Your account is locked.") > -1) {
                            errorMessage = App.translate("login.login.error.accountLocked");
                        }
                    } else {
                        errorMessage = App.translate("login.login.error.otherInvalidError", {statusCode: e.status});
                    }
                    errorsEl.html(errorMessage);
                    errorsEl.removeClass("invisible");

                });
            }
            // Dev_SCS-3284 End
		}).mixin([SalusViewMixin]);
	});


	return App.Consumer.Login.Views.WelcomeWellView;
});
