"use strict";

define([
	"app",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/FormTextInput.view",
	"consumer/views/SalusButtonPrimary.view"
], function (App, consumerTemplates, SalusView, FormTextInput, SalusButton) {

	App.module("Consumer.MyStatus.Views", function (Views, App, B, Mn, $, _) {
		Views.MyStatusNameEdit = Mn.LayoutView.extend({
			id: "myStatus-name-edit",
			template: consumerTemplates["myStatus/makeStatusNameEdit"],
			regions: {
				inputNameRegion: ".bb-input-name",
				cancelButtonRegion: ".bb-cancel-button",
				saveButtonRegion: ".bb-save-button"
			},
			ui: {
				name: ".bb-current-name"
			},
			initialize: function (ruleGroup) {
				this.ruleGroup = ruleGroup;

				_.bindAll(this, "handleCancelClick", "handleSaveClick");

				this.inputNameField = new FormTextInput({
					value: ruleGroup.get("name"),
					labelText: "myStatus.makeNewStatus.editName.name"
				});

				this.cancelButton = new SalusButton({
					className: "btn btn-default my-status-edit-name-button center-horizontal-absolute",
					buttonTextKey: "common.labels.cancel",
					clickedDelegate: this.handleCancelClick
				});

				this.saveButton = new SalusButton({
					classes: "my-status-edit-name-button center-horizontal-absolute",
					buttonTextKey: "common.labels.save",
					clickedDelegate: this.handleSaveClick
				});
			},
			onRender: function () {
				this.ui.name.text(this.ruleGroup.get("name"));
				this.inputNameRegion.show(this.inputNameField);
				this.cancelButtonRegion.show(this.cancelButton);
				this.saveButtonRegion.show(this.saveButton);
			},
			handleCancelClick: function () {
				this.trigger("click:close");
			},
			handleSaveClick: function () {
                var string = this.inputNameRegion.currentView.getValue(),
                    ruleGroupCollec = App.salusConnector.getRuleGroupCollection();
                this.inputNameRegion.currentView.hideErrors();
                var valid = _.some(ruleGroupCollec.models, function(model) {
                    return model.get("name") === string;
                });
                if(valid) {
                    this.inputNameRegion.currentView.showErrors(App.translate("equipment.error.renameError"));
                    return false;
                }
                
				if (string.trim().length > 0) {
					this.ruleGroup.set("name", string);
					this.trigger("click:save", this);
				} else {
                    this.inputNameRegion.currentView.showErrors(App.translate("common.nameError"));
                    return false;
                }
			}
		}).mixin([SalusView]);
	});

	return App.Consumer.MyStatus.Views.MyStatusNameEdit;
});
