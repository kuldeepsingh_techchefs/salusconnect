"use strict";
var newUserLocalData;
define([
	"app",
	"bluebird",
	"common/config",
    "common/constants",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/mixins/mixins",
	"consumer/models/SalusButtonPrimary",
	"consumer/models/SalusDropdownViewModel",
	"consumer/models/SalusRadioViewModel",
	"consumer/views/FormTextInput.view",
	"consumer/views/SalusButtonPrimary.view",
	"consumer/views/SalusDropdown.view",
	"consumer/views/SalusRadio.view",
	"consumer/views/SalusCheckbox.view",
	"consumer/models/SalusCheckboxViewModel",
	"consumer/views/SalusTooltip.view",
	"consumer/views/SalusModal.view",
	"consumer/newUser/views/EUCookieModal.view"
], function (App, P, config, constants, consumerTemplates, SalusViewMixin, ConsumerMixins) {

	App.module("Consumer.NewUser.Views", function (Views, App, B, Mn, $, _) {
		Views.NewUserForm = Mn.LayoutView.extend({
			template: consumerTemplates["newUser/newUserForm"],
			regions: {
				"firstNameRegion": ".bb-first-name",
				"lastNameRegion": ".bb-last-name",
				"emailRegion": ".bb-email",
				"emailConfirmRegion": ".bb-email-confirm",
				"passwordRegion": ".bb-password",
				"passwordConfirmRegion": ".bb-password-confirm",
				"countryRegion": ".bb-country",
                "countryCodeRegion": ".bb-country-code",
                "phoneRegion": ".bb-phone",
				"submitButtonRegion": ".bb-submit-button",
				"cancelButtonRegion": ".bb-cancel-button"
			},
			ui: {
				"termsConditionsCheckbox": ".bb-toc-checkbox",
				"privacyPolicyCheckbox": ".bb-pp-checkbox"
			},
			modelEvents: {
				"change:country": "onCountryChange"
			},
			events: {
				"click .bb-toc-checkbox": "validateForm",
				"click .bb-pp-checkbox": "validateForm",
				"blur .bb-first-name": "saveLocalDate",
				"blur .bb-last-name": "saveLocalDate",
				"blur .bb-email": "saveLocalDate",
				"blur .bb-email-confirm": "saveLocalDate",
				"blur .bb-country": "saveLocalDate",
				"blur .bb-country-code": "saveLocalDate",
				"blur .bb-phone": "saveLocalDate",
				"click .bb-toc-checkbox": "saveLocalDate",
				"click .bb-pp-checkbox": "saveLocalDate",
				"mouseover .bb-submit-button": "validateForm"
			},
			initialize: function () {
				this.registeredRegions = {};

				newUserLocalData = JSON.parse(window.localStorage.getItem("newUserData")) || {};

				_.bindAll(this,
						"_validatePasswordConfirm",
						"_validateEmailConfirm",
						"handleSubmitClicked",
						"gotoConfirmationPage",
						"validateForm",
						"checkShouldShowEUModal"
				);

				if(!$.isEmptyObject(newUserLocalData)){
					this.model.attributes = newUserLocalData.attributes;
				}

				var countriesCollection = new App.Consumer.Models.DropdownItemViewCollection();

				this._fillDropdownCollection(config.countries, config.countryPrefix, countriesCollection);

				this.firstNameField = this.registerRegion("firstNameRegion", new App.Consumer.Views.FormTextInput({
					labelText: "setupWizard.newUserForm.formFields.firstName",
					required: true,
					model: this.model,
					modelProperty: "firstname",
					value: this.model.get("firstname")
				}));

				this.lastNameField = this.registerRegion("lastNameRegion", new App.Consumer.Views.FormTextInput({
					labelText: "setupWizard.newUserForm.formFields.lastName",
					required: true,
					model: this.model,
					modelProperty: "lastname",
					value: this.model.get("lastname")

				}));

				this.emailField = this.registerRegion("emailRegion", new App.Consumer.Views.FormTextInput({
					labelText: "setupWizard.newUserForm.formFields.email",
					required: true,
					type: "email",
					model: this.model,
					modelProperty: "email",
					value: this.model.get("email")

				}));

				this.emailConfirmField = this.registerRegion("emailConfirmRegion", new App.Consumer.Views.FormTextInput({
					labelText: "setupWizard.newUserForm.formFields.emailConfirm",
					validationMethod: this._validateEmailConfirm,
					required: true,
					type: "email",
					model: this.model,
					showFeedback: true,
					preventTrigger: true, // textbox that is 2nd pass confirmation should have this
					value: newUserLocalData.confirmEmail || ''
				}));

				this.passwordField = this.registerRegion("passwordRegion", new App.Consumer.Views.FormTextInput({
					labelText: "setupWizard.newUserForm.formFields.password",
					type: "password",
					required: true,
					model: this.model
				}));

				this.passwordConfirmField = this.registerRegion("passwordConfirmRegion", new App.Consumer.Views.FormTextInput({
					labelText: "setupWizard.newUserForm.formFields.passwordConfirm",
					type: "password",
					validationMethod: this._validatePasswordConfirm,
					required: true,
					model: this.model,
					showFeedback: true,
					preventTrigger: true // textbox that is 2nd pass confirmation should have this
				}));

				this.countryDropdown = this.registerRegion("countryRegion", new App.Consumer.Views.SalusDropdownWithValidation({
					collection: countriesCollection,
					viewModel: new App.Consumer.Models.DropdownViewModel({
						id: "countrySelect",
						dropdownLabelKey: "setupWizard.newUserForm.formFields.countrySelect"
					}),
					required: true,
					defaultSelectionValue: this.model.get("country"),
					boundAttribute: "country",
					model: this.model
				}));

                this.countryCodeField = this.registerRegion("countryCodeRegion", new App.Consumer.Views.FormTextInput({
					labelText: "equipment.oneTouch.menus.doThis.notifyMe.contactMenu.countryCode",
					readonly: true,
					value: this.model.get("phone_country_code") ?  "+" + this.model.get("phone_country_code") : this.model.get("phone_country_code")
                }));
                
                this.phoneField = this.registerRegion("phoneRegion", new App.Consumer.Views.FormTextInput({
                    labelText: "setupWizard.newUserForm.formFields.phone",
                    type: "tel",
					required: false,
					model: this.model,
                    modelProperty: "phone",
                    validationMethod: function() {
                        if(this.getValue() && this.getValue() !== "") {
                            return App.validate.mobileNumber(this.getValue());
                        } else {
                            return {
                                valid: true
                            };
                        }
                    },
					value: this.model.get("phone")
				}));

				this.submitButton = this.registerRegion("submitButtonRegion", new App.Consumer.Views.SalusButtonPrimaryView({
					buttonTextKey: "setupWizard.newUserForm.formFields.submitButton",
					classes: "width100 disabled",
					clickedDelegate: this.handleSubmitClicked
				}));

				this.cancelButton = this.registerRegion("cancelButtonRegion", new App.Consumer.Views.SalusButtonPrimaryView({
					buttonTextKey: "common.labels.cancel",
					classes: "col-xs-6",
					className: "width100 btn btn-default",
					clickedDelegate: this.handleCancelClicked
				}));

				this.tocCheckboxView = new App.Consumer.Views.CheckboxView({
					model: new App.Consumer.Models.CheckboxViewModel({
						id: "bb-terms-conditions-checkbox",
						labelTextKey: "setupWizard.newUserForm.formFields.termsAndConditionsLabel",
						secondaryIconClass: "",
						isChecked: newUserLocalData.toc || false
					})
				});

				this.ppCheckboxView =  new App.Consumer.Views.CheckboxView({
					model: new App.Consumer.Models.CheckboxViewModel({
						id: "bb-privacy-policy-checkbox",
						labelTextKey: "setupWizard.newUserForm.formFields.privacyPolicyLabel",
						secondaryIconClass: "",
						isChecked: newUserLocalData.pp || false
					})
				});

				this.validationNeeded.push(this.firstNameField, this.lastNameField, this.emailField,
					this.emailConfirmField, this.passwordField, this.passwordConfirmField, this.countryDropdown, this.phoneField);

				this.attachValidationListener(); // to validationNeeded items

				
			},
			onRender: function () {
				var that = this;

				this.ui.termsConditionsCheckbox.prepend(this.tocCheckboxView.render().$el);
				this.ui.privacyPolicyCheckbox.prepend(this.ppCheckboxView.render().$el);
				
				if(!$.isEmptyObject(newUserLocalData)){

					that.firstNameRegion.show(that.firstNameField);
					that.lastNameRegion.show(that.lastNameField);
					that.emailRegion.show(that.emailField);
					that.emailConfirmRegion.show(that.emailConfirmField);
					that.submitButtonRegion.show(that.submitButton);
					that.cancelButtonRegion.show(that.cancelButton);
                    that.countryRegion.show(that.countryDropdown);
                    that.countryCodeRegion.show(that.countryCodeField);
                    that.phoneRegion.show(that.phoneField);
                    that.model.get("email") && that.emailField._handleInputBlur();
                    newUserLocalData.confirmEmail && that.emailConfirmField._handleInputBlur();
                }
				
			},
			onDestroy: function () {
				this.tocCheckboxView.destroy();
				this.ppCheckboxView.destroy();
			},
			_fillDropdownCollection: function (list, prefix, collection) {
				_.each(list, function (key) {
					collection.add(new App.Consumer.Models.DropdownItemViewModel({
						value: key,
						displayText: prefix + "." + key
					}));
				});
			},
			_validatePasswordConfirm: function () {
				var pass1 = this.passwordRegion.currentView.getValue(),
					pass2 = this.passwordConfirmRegion.currentView.getValue();

				return this.model.validatePassword(pass1, pass2);
			},
			_validateEmailConfirm: function () {
				var email1 = this.emailRegion.currentView.getValue(),
					email2 = this.emailConfirmRegion.currentView.getValue();

				return this.model.validateEmail(email1, email2);
			},
			saveLocalDate: function () {
				var localData = {
									attributes : this.model.attributes
								};
				localData.toc = this.tocCheckboxView.model.get('isChecked');
				localData.pp = this.ppCheckboxView.model.get('isChecked');
				localData.confirmEmail = this.emailConfirmRegion.currentView.getValue();
				
				window.localStorage.setItem("newUserData",JSON.stringify(localData));
			},
            onCountryChange: function() {
                var country = this.model.get("country"),
                    phoneCode = constants.countryPhoneCodes[country];
                this.model.set("phone_country_code", phoneCode);

                this.$(".bb-country-code").empty();
                var countryCodeField = new App.Consumer.Views.FormTextInput({
                    labelText: "equipment.oneTouch.menus.doThis.notifyMe.contactMenu.countryCode",
                    value: "+" + phoneCode,
                    readonly: true
                });
                this.$(".bb-country-code").append(countryCodeField.render().$el);
            },
			checkShouldShowEUModal: function () {
				var pass1, pass2, that = this,
                    country = this.model.get("country");
            
                pass1 = that.passwordRegion.currentView.getValue();
                pass2 = that.passwordConfirmRegion.currentView.getValue();
                
				// don't need to show for US/RU or if privacy policy is checked already
				if (country === "us") {
                    that.submitButtonRegion.currentView.showSpinner();

                    // save new user
                    that.model.persist(pass1, pass2).then(function () {
                        that.submitButtonRegion.currentView.hideSpinner();
                        window.localStorage.removeItem("newUserData");
                        that.gotoConfirmationPage();
                    }).catch(function (err) {
                        that.submitButtonRegion.currentView.hideSpinner();
                        return P.reject(err);
                    });
					return;
				}

				App.modalRegion.show(new App.Consumer.Views.SalusModalView({
					contentView: new Views.EUCookieModalView({
						acceptClickDelegate: function () {
							App.hideModal();

                            that.submitButtonRegion.currentView.showSpinner();

                            // save new user
                            that.model.persist(pass1, pass2).then(function () {
                                that.submitButtonRegion.currentView.hideSpinner();
                                window.localStorage.removeItem("newUserData");
                                that.gotoConfirmationPage();
                            }).catch(function (err) {
                                that.submitButtonRegion.currentView.hideSpinner();
                                return P.reject(err);
                            });
						}
					})
				}));

				App.showModal();

				// make span the full width of the screen
				// we don't need to clean this up because the modalRegion destroys when it hides, cleaning DOM
				$(".modal-dialog").addClass("full-width");
			},           
			validateForm: function () {
				var valid = this.isValid() &&
					this.tocCheckboxView.getIsChecked() &&
					this.ppCheckboxView.getIsChecked();
				
				if (!valid) {
					this.submitButtonRegion.currentView.disable();
				} else {
					this.submitButtonRegion.currentView.enable();
				}

				return valid;
			},
			gotoConfirmationPage: function () {
				App.navigate("login/confirm");
			},
			handleSubmitClicked: function () {
				var that = this;

				// make sure the form validates before we submit
				if (!this.validateForm()) {
					return;
				}

                this.checkShouldShowEUModal();
			},
			handleCancelClicked: function () {
				App.navigate(""); //empty string, goes back to login welcome screen.
			}
		}).mixin([SalusViewMixin, ConsumerMixins.RegisteredRegions, ConsumerMixins.FormWithValidation]);
	});

	return App.Consumer.NewUser.Views.NewUserForm;
});