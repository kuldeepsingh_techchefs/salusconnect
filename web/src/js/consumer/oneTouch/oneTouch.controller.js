"use strict";

define([
	"app",
	"common/constants",
	"consumer/consumer.controller",
	"consumer/oneTouch/views/OneTouchPage.view",
	"consumer/oneTouch/views/CreateOneTouchPage.view",
	"consumer/oneTouch/views/OneTouchDetailPage.view",
	"consumer/oneTouch/views/OneTouchApplyPage.view",
	"consumer/oneTouch/views/OneTouchPinPage.view",
    "consumer/oneTouch/views/DeviceWindowSensor.view"
], function (App, constants, ConsumerController) {

	App.module("Consumer.OneTouch", function (OneTouch) {
		OneTouch.Controller = {
			oneTouch: function oneTouch(dsn) {
				var page = new App.Consumer.OneTouch.Views.OneTouchPageView();

				if (!dsn) {
					dsn = App.getCurrentGatewayDSN();

					if (dsn !== constants.noGateway) {
						App.changeRoute("oneTouch/" + dsn);
					}
				}
				// if (ConsumerController.changeGatewayIfNeeded(dsn, "oneTouch")) {
					ConsumerController.showLayout(page, true, ["devices"]);
				// }
			},

			createOneTouch: function createOneTouch(dsn) {
				var page = new App.Consumer.OneTouch.Views.CreateOneTouchPageView();

				// if (ConsumerController.changeGatewayIfNeeded(dsn, "oneTouch")) {
					ConsumerController.showLayout(page, true, ["devices", "rules"]);
				// }
			},

			editOneTouch: function editOneTouch(dsn, OTKey) {
				var page = new App.Consumer.OneTouch.Views.CreateOneTouchPageView({
					key: OTKey,
					edit: true
				});

				// if (ConsumerController.changeGatewayIfNeeded(dsn, "oneTouch")) {
					ConsumerController.showLayout(page, true, ["devices", "rules"]);
				// }
			},

			oneTouchDetail: function oneTouchDetail(dsn, OTKey) {
				var page = new App.Consumer.OneTouch.Views.OneTouchDetailPage({
					key: OTKey
				});

				// if (ConsumerController.changeGatewayIfNeeded(dsn, "oneTouch")) {
					ConsumerController.showLayout(page, true, ["devices", "rules"]);
				// }
			},

			applyOneTouch: function (dsn) {
				var page = new App.Consumer.OneTouch.Views.OneTouchApplyPageView();

				// if (ConsumerController.changeGatewayIfNeeded(dsn, "oneTouch")) {
					ConsumerController.showLayout(page, true, ["rules", "ruleGroups"]);
				// }
			},

			pinOneTouch: function (dsn) {
				var page = new App.Consumer.OneTouch.Views.OneTouchPinPageView();

				// if (ConsumerController.changeGatewayIfNeeded(dsn, "oneTouch")) {
					ConsumerController.showLayout(page, true, ["rules"]);
				// }
			},
            
            deviceWindowSensor: function (key){
                var page = new App.Consumer.OneTouch.Views.DeviceWindowSensor({key:key});


				ConsumerController.showLayout(page, true, ["rules"]);

            },
            
            backOfficeOneTouch: function (dsn) {
                var page = new App.Consumer.OneTouch.Views.BackOfficePageView();
				// if (ConsumerController.changeGatewayIfNeeded(dsn, "oneTouch")) {
					ConsumerController.showLayout(page, true, ["rules"]);
				// }
            }
		};
	});

	return App.Consumer.OneTouch.Controller;
});
