"use strict";

define([
	"app",
	"common/constants",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/FormTextInput.view",
	"consumer/views/SalusCheckbox.view",
	"consumer/models/SalusCheckboxViewModel",
	"consumer/views/SalusDropdown.view",
	"consumer/models/SalusDropdownViewModel"
], function (App, constants, consumerTemplates, SalusView, FormTextInput, CheckboxView, CheckboxModel) {

	App.module("Consumer.OneTouch.Views", function (Views, App, B, Mn, $, _) {

		// layout
		// has sections and ui
		Views.ContactInfoView = Mn.LayoutView.extend({
			className: "contact-info-view",
			template: consumerTemplates["oneTouch/contactInfoBase"],
			regions: {
				emailRegion: ".bb-email-region",
				smsRegion: ".bb-sms-region",
				inputRegion: ".bb-input-region"
			},
			bindings: {
				".bb-input-region input": "inputMessage"
			},
			initialize: function (/*options*/) {
				_.bindAll(this, "getRuleData");
                
                this.userData = App.salusConnector.getSessionUser();

				this.model = new B.Model({
					"inputMessage": ""
				});

				this.type = this.options.menu.menuModel.get("contactType");

				// we could have one for push notifications too
				this.emailSectionView = new Views.ContactInfoItemView({
                    userData: this.userData,
					type: "email"
				});

				this.smsSectionView = new Views.ContactInfoItemView({
                    userData: this.userData,
					type: "sms"
				});
                
				this.inputSectionView = new App.Consumer.Views.SalusTextBox({
					wrapperClassName: "alert-input",
					inputPlaceholder: App.translate("equipment.oneTouch.menus.doThis.notifyMe.messagePlaceHolder"),
					inputType: "input",
					overlayedUnits: "",
					hideErrorTriangle: true,
					hideArrows: true
				});
			},
			onRender: function () {
				this.emailRegion.show(this.emailSectionView);
				this.smsRegion.show(this.smsSectionView);
				this.inputRegion.show(this.inputSectionView);
			},
			/**
			 * return the rule data for sms or for email
			 * @returns {*}
			 */
			getRuleData: function () {
				// hijacking submit if we are showing a form
                var emailChoices = this.emailSectionView.getCheckedItems(),
                    smsChoices = this.smsSectionView.getCheckedItems(),
                    messageText = this.model.get("inputMessage"),
                    ruleData = [];

                if (emailChoices.length > 0) {
                    ruleData.push({
                        type: "email",
                        recipients: emailChoices, // array
                        message: messageText
                    });
                }

                if (smsChoices.length > 0) {
                    smsChoices = this._cleanAndBuildSMS(smsChoices);

                    ruleData.push({
                        type: "sms",
                        recipients: smsChoices, // array
                        message: messageText
                    });
                }

                return ruleData.length > 0 ? ruleData : false;
			},
			_cleanAndBuildSMS: function (smsChoices) {
				// map the smsChoices to another array, with the country code and digits split as an object
				return _.map(smsChoices, function (phoneNum) {
					var split = phoneNum.split(" ");

					return {
						countryCode: parseInt(split[0]),
						phoneNumber: parseInt(split[1])
					};
				});
			}
		}).mixin([SalusView]);

		Views.ContactInfoItemView = Mn.ItemView.extend({
            template: consumerTemplates["oneTouch/contactInfoComposite"],
			className: "contact-composite",
            ui: {
                contactCheckbox: ".bb-contact-checkbox",
                type: ".bb-contact-type",
				check: ".bb-green-check"
            },
            events: {
				"click .bb-contact-checkbox": "_handleCheckboxClick"
			},
			initialize: function () {
                _.bindAll(this, "_handleCheckboxClick", "_anyChecked");
                this.type = this.options.type;
                
                var that = this, displayText;
                if(this.type === "email") {
                    displayText = this.options.userData.get("email");
                } else {
                    displayText = "+" + this.options.userData.get("phone_country_code") + " " + this.options.userData.get("phone");
                }
				this.checkboxView = new CheckboxView({
					model: new CheckboxModel({
						name: "contactCheckbox",
						isChecked: false,
						secondaryIconClass: "",
						nonKeyLabel: displayText
					})
				});

				this.listenTo(this.checkboxView, "clicked:checkbox", function () {
					that.trigger("clicked:checkbox");
				});
			},
			onRender: function () {
                this.ui.check.toggleClass("green-check", this._anyChecked());
                if((this.type === "sms" && this.options.userData.get("phone")) || ( this.type === "email" && this.options.userData.get("email"))) {
                    this.ui.contactCheckbox.append(this.checkboxView.render().$el);
                }
                
                if (this.type === "sms") {
					this.ui.type.text(App.translate("equipment.oneTouch.menus.doThis.notifyMe.smsHeader"));
				} else if (this.type === "email") {
					this.ui.type.text(App.translate("equipment.oneTouch.menus.doThis.notifyMe.emailHeader"));
				}
			},
            _anyChecked: function () {
                return this.checkboxView.getIsChecked();
			},
            _handleCheckboxClick: function () {
				this.ui.check.toggleClass("green-check", this._anyChecked());
			},
            getCheckedItems: function () {
				var array = [];
                if (this.checkboxView.getIsChecked()) {
                    array.push(this.checkboxView.model.get("nonKeyLabel"));
                }
				return array;
			},
			onDestroy: function () {
				this.checkboxView.destroy();
			}
		}).mixin([SalusView]);
	});

	return App.Consumer.OneTouch.Views.TimeOfDayPickerView;
});