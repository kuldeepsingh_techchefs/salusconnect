"use strict";

define([
	"app",
	"common/config",
	"common/constants",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusPage",
	"consumer/views/mixins/mixin.salusView",
	"common/util/VendorfyCssForJs",
	"consumer/oneTouch/views/RecommendedRulesLayout.view",
	"consumer/equipment/views/myEquipment/AddNewTile.view",
	"common/model/salusWebServices/rules/Rule.model"
], function (App, config, constants, consumerTemplates, SalusPage, SalusView, VendorfyCss) {

	App.module("Consumer.OneTouch.Views", function (Views, App, B, Mn, $, _) {
		Views.OneTouchPageView = Mn.LayoutView.extend({
			className: "container",
			id: "one-touch-page",
			template: consumerTemplates["oneTouch/oneTouchPage"],
			ui: {
				noGatewayText: ".bb-no-gateway-text"
			},
			regions: {
				automationsRegion: ".bb-automations-region",
				recommendedRegion: ".bb-recommended-region"
			},
			initialize: function () {
				_.bindAll(this,
					"initiateRefreshPoll",
					"clearRefreshPollTimeout",
					"refreshPoll"
				);

				this.pollingInterval = null;
			},
			onBeforeDestroy: function() {
				this.clearRefreshPollTimeout();
			},
			onRender: function () {
				var that = this;

				App.salusConnector.getDataLoadPromise(["devices", "rules"]).then(function (/*arrayOfData*/) {
					var ruleCollection = App.salusConnector.getRuleCollection();

					if (ruleCollection) {
						ruleCollection.refresh();
					}

					if (!App.hasCurrentGateway()) {
						that.ui.noGatewayText.removeClass("hidden");
					}
                    
                    var collection_uxhint = new App.Models.RuleCollection();
                    var collection_un_uxhint = new App.Models.RuleCollection();
                    
                    if(ruleCollection) {
                        _.each(App.salusConnector.getDisplayRuleCollection().models, function(rule) {
                            if(!!rule.uxhint) {
                                collection_uxhint.push(rule);
                            } else {
                                collection_un_uxhint.push(rule);
                            }
                        });
                    }
					that.automationsRegion.show(new Views.OneTouchCollectionView({
                        collection: collection_un_uxhint,
                        collection_BO: collection_uxhint
					}));

					if (App.salusConnector.shouldListRecommendedRules()) {
						that.recommendedRegion.show(new Views.RecommendedRulesLayoutView());
					}
				});
				
				this.initiateRefreshPoll();
			},
			initiateRefreshPoll: function () {
				this.pollingInterval = setInterval(this.refreshPoll, config.devicePollingInformation.oneTouchsPollingInterval);
			},
			clearRefreshPollTimeout: function () {
				clearInterval(this.pollingInterval);
			},
			refreshPoll: function () {
				var ruleCollection = App.salusConnector.getRuleCollection();

				if (ruleCollection) {
					ruleCollection.refresh();
				}
			}
		}).mixin([SalusPage], {
			analyticsSection: "oneTouch",
			analyticsPage: "list"
		});

		Views.OneTouchTileView = Mn.ItemView.extend({
			template: consumerTemplates["oneTouch/oneTouchTile"],
			className: "one-touch-tile col-xs-3 col-sm-2",
			attributes: {
				role: "button"
			},
			events: {
				"click": "_handleClick"
			},
			ui: {
				tileBackground: ".bb-one-touch-tile-background"
			},
			initialize: function () {
				_.bindAll(this, "_handleClick", "_navigateDetailPage");
			},
			onRender: function () {
				VendorfyCss.formatBgGradientWithImage(
					this.ui.tileBackground,
					"center 52% / 40%",
					"no-repeat",
					App.rootPath("/images/icons/dashboard/icon_one_touch.svg"),
					"145deg",
					constants.tileGradients.TILE_GREEN_GRADIENT_TOP,
					"0%",
					constants.tileGradients.TILE_GREEN_GRADIENT_BOTTOM,
					"100%"
				);
			},
			_handleClick: function () {
				var uriKey = encodeURIComponent(this.model.get("key"));

				this._navigateDetailPage(uriKey);
			},
			_navigateDetailPage: function (OTKey) {
				App.navigate("oneTouch/" + App.getCurrentGatewayDSN() + "/" + OTKey);
			}
		}).mixin([SalusView]);

		Views.OneTouchCollectionView = Mn.CollectionView.extend({
			className: "row one-touch-collection",
			template: false,
			childView: Views.OneTouchTileView,
			initialize: function () {
				_.bindAll(this, "updateCollection");

				this.listenTo(App.salusConnector.getRuleCollection(), "update", this.updateCollection);
			},
			onRender: function () {
				if (!this.addNewAutomationTile) {
					var key = "equipment.oneTouch.automations.addNew",
						clickDestination = "oneTouch/" + App.getCurrentGatewayDSN() + "/create";

					if (!App.getCurrentGateway()) {
						key = "myStatus.addNewGateway";
						clickDestination = "setup/gateway";
					}

					this.addNewAutomationTile = new App.Consumer.Equipment.Views.AddNewTileView({
						i18nTextKey: key,
						size: "small",
						classes: "one-touch-tile",
						clickDestination: clickDestination
					});
				}
                if(this.options.collection_BO && this.options.collection_BO.length > 0) {
                    this.backOfficeRuleTile = new Views.BackOfficeRuleView({
                        count: this.options.collection_BO.length
                    });
                    this.$el.prepend(this.backOfficeRuleTile.render().$el);
                }
				this.$el.prepend(this.addNewAutomationTile.render().$el);
			},
			onDestroy: function () {
				if (this.addNewAutomationTile) {
					this.addNewAutomationTile.destroy();
				}
                if (this.backOfficeRuleTile) {
                    this.backOfficeRuleTile.destroy();
                }
			},
			updateCollection: function () {
				var that = this;

				App.salusConnector.getDataLoadPromise("rules").then(function () {
					that.collection.set(App.salusConnector.getDisplayRuleCollection().toArray());
				});
			}
		}).mixin([SalusView]);
        
        Views.BackOfficeRuleView = Mn.ItemView.extend({
            template: consumerTemplates["oneTouch/oneTouchGroupTile"],
			attributes: {
				role: "button",
                class: "group-tile col-xs-3 col-sm-2"
			},
			ui: {
                text: ".bb-one-touch-name",
				gradientWithIcon: ".bb-rules-tile-background",
                count: ".bb-rule-count"
			},
			events: {
				click: "_handleClick"
			},
			initialize: function () {
				_.bindAll(this, "_handleClick");
                this.rule_count = this.options.count;
			},
			onRender: function () {
                VendorfyCss.formatBgGradientWithImage(
                    this.ui.gradientWithIcon,
                    "center 50%/48% 48%",
                    "no-repeat",
                    App.rootPath("/images/icons/dashboard/icon_groups.svg"),
                    "145deg",
                    constants.tileGradients.TILE_GREEN_GRADIENT_TOP,
                    "0%",
                    constants.tileGradients.TILE_GREEN_GRADIENT_BOTTOM,
                    "100%"
				);
        
                this.ui.text.text(App.translate("equipment.oneTouch.automations.backOffice"));
                this.ui.count.text(this.rule_count);
			},
			_handleClick: function () {
                App.navigate("oneTouch/" + App.getCurrentGatewayDSN() + "/backOffice");
			}
		}).mixin([SalusView]);
        
        Views.BackOfficePageView = Mn.LayoutView.extend({
			className: "container",
			id: "one-touch-page",
			template: consumerTemplates["oneTouch/oneTouchPage"],
			regions: {
				automationsRegion: ".bb-automations-region"
			},
			initialize: function () {
				_.bindAll(this, "initiateRefreshPoll", "clearRefreshPollTimeout", "refreshPoll");
				this.pollingInterval = null;
			},
			onBeforeDestroy: function() {
				this.clearRefreshPollTimeout();
			},
			onRender: function () {
				var that = this;
                var collection_uxhint = new App.Models.RuleCollection();

                _.each(App.salusConnector.getDisplayRuleCollection().models, function(rule) {
                    if(!!rule.uxhint) {
                        collection_uxhint.push(rule);
                    }
                });

                that.automationsRegion.show(new Views.BackOfficeCollectionView({
                    collection: collection_uxhint
                }));
				
                this.$("h1").text(App.translate("equipment.oneTouch.automations.backOffice"));
                this.$("h1").addClass("text-capitalize");
				this.initiateRefreshPoll();
			},
			initiateRefreshPoll: function () {
				this.pollingInterval = setInterval(this.refreshPoll, config.devicePollingInformation.oneTouchsPollingInterval);
			},
			clearRefreshPollTimeout: function () {
				clearInterval(this.pollingInterval);
			},
			refreshPoll: function () {
				var ruleCollection = App.salusConnector.getRuleCollection();

				if (ruleCollection) {
					ruleCollection.refresh();
				}
			}
		}).mixin([SalusPage], {
			analyticsSection: "oneTouch",
			analyticsPage: "list"
		});
        
        Views.BackOfficeCollectionView = Mn.CollectionView.extend({
			className: "row one-touch-collection",
			template: false,
			childView: Views.OneTouchTileView,
			initialize: function () {
				_.bindAll(this, "updateCollection");

				this.listenTo(App.salusConnector.getRuleCollection(), "update", this.updateCollection);
			},
			updateCollection: function () {
				var that = this;

				App.salusConnector.getDataLoadPromise("rules").then(function () {
                    var collection_uxhint = new App.Models.RuleCollection();

                    _.each(App.salusConnector.getDisplayRuleCollection().models, function(rule) {
                        if(!!rule.uxhint) {
                            collection_uxhint.push(rule);
                        }
                    });
					that.collection.set(collection_uxhint.toArray());
				});
			}
		}).mixin([SalusView]);
	});

	return App.Consumer.OneTouch.Views.OneTouchPageView;
});