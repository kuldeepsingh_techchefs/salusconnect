"use strict";

define([
    "app",
    "application/Router",
    "consumer/schedules/schedules.controller"
], function (App, AppRouter, Controller) {

    App.module("Consumer.Schedules", function (Schedules, App, B, Mn, $, _) {
        Schedules.Router = AppRouter.extend({
            // dont change the order of these routes, it can introduce bus
            appRoutes: {
                "schedules/presets": "presets",
                "schedules/thermostat(/:key)": "thermostatSchedule",
                "schedules/waterHeater(/:key)": "waterHeaterSchedule",
                "schedules/smartPlug(/:key)": "smartPlugSchedule",
//				"schedule/currentDevice/(:key)": "currentDevice"
            }
        });

        App.addInitializer(function () {
            Schedules.PageEventController = {};
            _.extend(Schedules.PageEventController, B.Events);

            return new Schedules.Router({
                controller: Controller
            });
        });
    });
});