"use strict";

define([
	"app",
	"momentWrapper",
	"common/config",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusPage",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/mixins/mixins"
], function (App, moment, config, consumerTemplates, SalusPage) {

	App.module("Consumer.Settings.Views", function (Views, App, B, Mn) {
		Views.AboutAppPage = Mn.LayoutView.extend({
			template: consumerTemplates["settings/aboutThisApp"],
			id: "about-page",
			className: "container",
			ui: {
				"companyName": ".bb-company-name-text",
				"companyUrl": ".bb-company-url-text",
				"buildDateText": ".bb-build-date-text",
				"releasedDateText": ".bb-released-date-text",
				"versionText": ".bb-version-text",
				"copyrightText": ".bb-copyright-text"
			},
			onRender: function () {
				var releaseDate = window.build.releaseDate ? window.build.releaseDate : window.build.buildDate;

				//TODO - check if user is EU or US and set url appropriately
				this.ui.companyUrl.text("www.salus-tech.com"); //http://eu.salusconnect.io/
				this.ui.buildDateText.text(moment(new Date(window.build.buildDate)).utc().format("LL"));
				this.ui.releasedDateText.text(moment(new Date(releaseDate)).utc().format("LL"));
				this.ui.versionText.text(window.build.versionNumber + " " + window.build.buildNumber);

				this.ui.copyrightText.text(App.translate("settings.about.copyrightFormatted", {
					"currentYear": new Date().getFullYear()
				}));
			}
		}).mixin([SalusPage], {
			analyticsSection: "settings",
			analyticsPage: "about"
		});
	});


	return App.Consumer.Settings.Views.AboutAppPage;
});

