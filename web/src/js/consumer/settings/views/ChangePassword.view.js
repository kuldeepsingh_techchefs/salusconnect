"use strict";

define([
	"app",
	"common/config",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusPage",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/mixins/mixins",
	"consumer/models/SalusButtonPrimary",
	"consumer/models/SalusRadioViewModel",
	"consumer/views/FormTextInput.view",
	"consumer/views/SalusButtonPrimary.view",
], function (App, config, consumerTemplates,
			 SalusPage, SalusViewMixin, ConsumerMixins) {

	App.module("Consumer.Settings.Views", function (Views, App, B, Mn, $, _) {
		Views.ChangePasswordPage = Mn.LayoutView.extend({
			template: consumerTemplates["settings/changePassword"],
			regions: {
				"oldPasswordRegion": ".bb-old-password",
				"newPasswordRegion": ".bb-new-password",
				"passwordConfirmRegion":".bb-confirm-password",
				"submitButtonRegion": ".bb-submit-button",
				"cancelButtonRegion": ".bb-cancel-button"
			},
			events: {
				"mouseover .bb-submit-button": "validateForm"
			},
			initialize: function() {
				_.bindAll(this,
						"_validatePasswordConfirm",
						"handleSubmitClicked",
						"validateForm",
						"handleCancelClicked");

				this.oldPasswordField = new App.Consumer.Views.FormTextInput({
					labelText: "settings.profile.changePassword.formFields.oldPassword",
					type: "password",
					required: true,
					model: this.model
				});

				this.newPasswordField = new App.Consumer.Views.FormTextInput({
					labelText: "settings.profile.changePassword.formFields.newPassword",
					type: "password",
					required: true,
					model: this.model
				});

				this.passwordConfirmField = new App.Consumer.Views.FormTextInput({
					labelText: "settings.profile.changePassword.formFields.confirmPassword",
					type: "password",
					validationMethod: this._validatePasswordConfirm,
					required: true,
					model: this.model,
					showFeedback: true,
					preventTrigger: true // textbox that is 2nd pass confirmation should have this
				});

				this.submitButton = new App.Consumer.Views.SalusButtonPrimaryView({
					buttonTextKey: "settings.profile.formFields.submitButton",
					classes: "width100 disabled",
					clickedDelegate: this.handleSubmitClicked
				});

				this.cancelButton = new App.Consumer.Views.SalusButtonPrimaryView({
					buttonTextKey: "common.labels.cancel",
					classes: "",
					className: "width100 btn btn-default",
					clickedDelegate: this.handleCancelClicked
				});

				this.validationNeeded.push(this.oldPasswordField, this.passwordConfirmField, this.newPasswordField);

				this.attachValidationListener(); // to validationNeeded items
			},

			onRender: function() {
				this.oldPasswordRegion.show(this.oldPasswordField);
				this.newPasswordRegion.show(this.newPasswordField);
				this.passwordConfirmRegion.show(this.passwordConfirmField);
				this.submitButtonRegion.show(this.submitButton);
				this.cancelButtonRegion.show(this.cancelButton);
			},
			handleCancelClicked: function () {
				window.history.back();
			},
			_validatePasswordConfirm: function () {
				var pass1 = this.newPasswordRegion.currentView.getValue(),
						pass2 = this.passwordConfirmRegion.currentView.getValue();

				return this.model.validatePassword(pass1, pass2);
			},
			validateForm: function () {
				var valid = this.isValid();

				if (!valid) {
					this.submitButtonRegion.currentView.disable();
				} else {
					this.submitButtonRegion.currentView.enable();
				}

				return valid;
			},
			changePassword: function() {
				var oldPass = this.oldPasswordRegion.currentView.getValue(),
						newPass = this.newPasswordRegion.currentView.getValue();

				return this.model.changePassword(newPass, oldPass);
			},
			handleSubmitClicked: function () {
				var that = this;
				// make sure the form validates before we submit
				if (!this.validateForm()) {
					return;
				}
                
                var oldPass = this.oldPasswordRegion.currentView.getValue(),
                    newPass = this.newPasswordRegion.currentView.getValue(),
                    errorMessage = "";
            
                if(newPass === oldPass){
                    errorMessage = App.translate("settings.profile.changePassword.newPasswordIsSameWithOldPassword.");
					this._showSalusAlertModalView(errorMessage);
					return;
                }

				this.submitButton.showSpinner();

				return this.changePassword().then(function () {
					that.submitButton.hideSpinner();

                    //App.salusConnector.setAylaUserPassword(newPass);
					App.navigate("/dashboard");
				}).catch(function (data) {
					if (data.responseJSON.errors) {
						errorMessage = App.translate("settings.profile.changePassword.oldPasswordIncorrect");
						that._showSalusAlertModalView(errorMessage);
					}
					that.submitButton.hideSpinner();
				});
			},
			_showSalusAlertModalView: function(msg) {
				this.errorModal = new App.Consumer.Views.SalusAlertModalView({
					model: new App.Consumer.Models.AlertModalViewModel({
						iconClass: "icon-warning",
						primaryLabelText: msg,
						rightButton: new App.Consumer.Views.ModalCloseButton({
							classes: "btn-danger width100",
							buttonTextKey: "common.labels.ok",
							clickedDelegate: App.hideModal()
						}),
						leftButton: new App.Consumer.Views.ModalCloseButton({
							classes: "width100",
							buttonTextKey: "common.labels.cancel",
							clickedDelegate: App.hideModal()
						})
					})
				});

				App.modalRegion.show(this.errorModal);
				App.showModal();
			}
			
		}).mixin([SalusPage, ConsumerMixins.FormWithValidation], {
			analyticsSection: "login",
			analyticsPage: "changePassword" //TODO: This should be a function so the equipment Id is recorded.
		});
	});

	return App.Consumer.Settings.Views.ChangePasswordPage;
});