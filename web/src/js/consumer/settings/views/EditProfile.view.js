"use strict";

define([
	"app",
	"common/config",
	"common/constants",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusPage",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/mixins/mixins",
	"consumer/views/SalusLinkButton.view",
	"consumer/views/SalusModal.view",
	"consumer/settings/views/DeleteUserProfileModal.view",
	"consumer/models/SalusButtonPrimary",
	"consumer/views/FormTextInput.view",
	"consumer/views/SalusButtonPrimary.view",
	"consumer/views/SalusDropdown.view"
], function (App, config, constants, consumerTemplates,SalusPage, SalusViewMixin,
			 ConsumerMixin, SalusLinkButtonView, SalusModalView) {

	App.module("Consumer.Settings.Views", function (Views, App, B, Mn, $, _) {
        var cloneModel;
		Views.EditProfilePage = Mn.LayoutView.extend({
			template: consumerTemplates["settings/profile/editProfile"],
			id: "edit-profile",
			regions: {
				"firstNameRegion": ".bb-first-name",
				"lastNameRegion": ".bb-last-name",
				"emailRegion": ".bb-email",
				"languageRegion": ".bb-language",
				"submitButtonRegion": ".bb-submit-button",
				"cancelButtonRegion": ".bb-cancel-button",
				"deleteProfileRegion": ".bb-delete-profile",
				//"changePasswordRegion": ".bb-password",
                "countryRegion": ".bb-country",
                "countryCodeRegion": ".bb-country-code",
                "phoneRegion": ".bb-phone"
			},
			initialize: function () {
				_.bindAll(this, "handleSubmitClicked", "validateForm", "handleCancelClicked",  "onCountryChanged", "onLanguageChanged");
                
				var languagesCollection = new App.Consumer.Models.DropdownItemViewCollection();
                
                var countriesCollection = new App.Consumer.Models.DropdownItemViewCollection();
				this._fillDropdownCollection(config.countries, config.countryPrefix, countriesCollection);

				this._fillDropdownCollection(this.getLanguageList(), config.languagePrefix, languagesCollection);
                
                cloneModel = new App.Models.AylaUser({
                    firstname: this.model.get("firstname"),
                    lastname: this.model.get("lastname"),
                    email: this.model.get("email"),
                    country: this.model.get("country"),
                    phone_country_code: this.model.get("phone_country_code"),
                    phone: this.model.get("phone")
                });
                
                this.listenTo(cloneModel, "change:country", this.onCountryChanged);
                this.listenTo(this.model, "change:language", this.onLanguageChanged);

				this.firstNameField =  new App.Consumer.Views.FormTextInput({
					labelText: "settings.profile.formFields.firstName",
                    required: true,
                    value: cloneModel.get("firstname"),
                    model: cloneModel,
                    modelProperty: "firstname"
				});

				this.lastNameField = new App.Consumer.Views.FormTextInput({
					labelText: "settings.profile.formFields.lastName",
					required: true,
                    value: cloneModel.get("lastname"),
                    model: cloneModel,
                    modelProperty: "lastname"
				});

				this.emailField = new App.Consumer.Views.FormTextInput({
					labelText: "settings.profile.formFields.email",
					value: cloneModel.get("email"),
					readonly: true,
                    model: cloneModel,
                    modelProperty: "email"
				});
                
                this.countryField = new App.Consumer.Views.SalusDropdownWithValidation({
					collection: countriesCollection,
					viewModel: new App.Consumer.Models.DropdownViewModel({
						id: "countrySelect",
						dropdownLabelKey: "setupWizard.newUserForm.formFields.countrySelect"
					}),
					required: true,
                    defaultSelectionValue: cloneModel.get("country"),
					boundAttribute: "country",
					model: cloneModel
				});
                
                this.countryCodeField = new App.Consumer.Views.FormTextInput({
					labelText: "equipment.oneTouch.menus.doThis.notifyMe.contactMenu.countryCode",
					value: cloneModel.get("phone_country_code") ? "+" + cloneModel.get("phone_country_code") : "",
					readonly: true,
                    model: cloneModel,
                    modelProperty: "phone_country_code"
				});
                
                this.phoneField = new App.Consumer.Views.FormTextInput({
                    type: "tel",
                    labelText: "setupWizard.newUserForm.formFields.phone",
                    required: false,
                    value: cloneModel.get("phone"),
                    model: cloneModel,
                    modelProperty: "phone",
                    validationMethod: function() {
                        if(this.getValue() && this.getValue() !== "") {
                            return App.validate.mobileNumber(this.getValue());
                        } else {
                            return {
                                valid: true
                            };
                        }
                    }
                });

				this.languageDropdown = new App.Consumer.Views.SalusDropdownWithValidation({
					collection: languagesCollection,
					validationMethod: this._validateLanguageConfirm,
					viewModel: new App.Consumer.Models.DropdownViewModel({
						id: "languageSelect",
						dropdownLabelKey: "settings.profile.formFields.languageSelect"
					}),
					defaultSelectionValue: this.model.get("language"),
					boundAttribute: "language",
					model: this.model
				});

				this.submitButton = new App.Consumer.Views.SalusButtonPrimaryView({
					buttonTextKey: "settings.profile.formFields.submitButton",
					classes: "width100 disabled",
					clickedDelegate: this.handleSubmitClicked
				});

				this.cancelButton = new App.Consumer.Views.SalusButtonPrimaryView({
					buttonTextKey: "common.labels.cancel",
					classes: "col-xs-6",
					className: "width100 btn btn-default",
					clickedDelegate: this.handleCancelClicked
				});

				this.deleteProfile = new SalusLinkButtonView({
					buttonText: App.translate("settings.profile.deleteProfile"),
					className: "delete-button btn btn-link margin-r-10",
					clickedDelegate: this.handleRemoveClick
				});

				/*this.changePasswordLink = new SalusLinkButtonView({
					buttonTextKey: "settings.profile.changePassword.title",
					className: "btn btn-link",
					clickedDelegate: this.handleChangePasswordClick
				});*/
              

				this.validationNeeded.push(this.firstNameField, this.lastNameField, this.phoneField);

				this.attachValidationListener();

				this.userMetaPromise = App.salusConnector.getSessionUser().fetchMetaData();
			},
			onRender: function () {
				var that = this;

				this.showSpinner({textKey: constants.spinnerTextKeys.loading}, this.$el.find(".profile-info"));
                
                App.salusConnector.getSessionUser().fetchMetaData().then(function () {
					that.hideSpinner();
                    
                    cloneModel.set("firstname", that.model.get("firstname"));
                    cloneModel.set("lastname", that.model.get("lastname"));
                    cloneModel.set("email", that.model.get("email"));
                    cloneModel.set("country", that.model.get("country"));
                    cloneModel.set("phone_country_code", that.model.get("phone_country_code") ? "+" + that.model.get("phone_country_code") : "");
                    cloneModel.set("phone", that.model.get("phone"));
                    
					that.firstNameRegion.show(that.firstNameField);
					that.lastNameRegion.show(that.lastNameField);
					that.emailRegion.show(that.emailField);
					that.languageRegion.show(that.languageDropdown);
					that.submitButtonRegion.show(that.submitButton);
					that.cancelButtonRegion.show(that.cancelButton);
					that.deleteProfileRegion.show(that.deleteProfile);
					//that.changePasswordRegion.show(that.changePasswordLink);
                    that.countryRegion.show(that.countryField);
                    that.countryCodeRegion.show(that.countryCodeField);
                    that.phoneRegion.show(that.phoneField);
                    
				});
			},
            
            onCountryChanged: function() {
                if(!this.countryRegion.currentView) {
                    return ;
                }
                var country = cloneModel.get("country");
                var phoneCode = constants.countryPhoneCodes[country];
                cloneModel.set("phone_country_code", phoneCode);
                
                if (this.isValid()) {
					this.submitButtonRegion.currentView.enable();
				}
                
                this.$(".bb-country-code").empty();
                var countryCodeField = new App.Consumer.Views.FormTextInput({
					labelText: "equipment.oneTouch.menus.doThis.notifyMe.contactMenu.countryCode",
					value: "+" + phoneCode,
					readonly: true,
                    model: cloneModel,
                    modelProperty: "phone_country_code"
				});
                this.$(".bb-country-code").append(countryCodeField.render().$el);
            },
            
            onLanguageChanged: function() {
                if(!this.countryRegion.currentView) {
                    return ;
                }
                if (this.isValid()) {
					this.submitButtonRegion.currentView.enable();
				}
            },
            
            getLanguageList: function(){
               var list = config.languages;
               var lan = this.model.get("language");
               if(lan === "da"){
                   list = config.languages_da;
               }
               else if(lan === "nl"){
                   list = config.languages_nl;
               }
                else if(lan === "fr"){
                   list = config.languages_fr;
               }
                else if(lan === "de"){
                   list = config.languages_de;
               }
                else if(lan === "no"){
                   list = config.languages_no;
               }
                else if(lan === "pl"){
                   list = config.languages_pl;
               }
                else if(lan === "ro"){
                   list = config.languages_ro;
               }
                else if(lan === "ru"){
                   list = config.languages_ru;
               }
                else if(lan === "sv"){
                   list = config.languages_sv;
               }
               return list;
               
            },
			_fillDropdownCollection: function (list, prefix, collection) {
				_.each(list, function (key) {
					collection.add(new App.Consumer.Models.DropdownItemViewModel({
						value: key,
						displayText: prefix + "." + key
					}));
				});
			},
			validateForm: function () {
				var valid = this.isValid();

				if (!valid) {
					this.submitButtonRegion.currentView.disable();
				} else {
					this.submitButtonRegion.currentView.enable();
				}

				return valid;
			},
			saveData: function () {
				var firstName, lastName, phone;

				firstName = this.firstNameRegion.currentView.getValue();
				this.model.set("firstname", firstName);

				lastName = this.lastNameRegion.currentView.getValue();
				this.model.set("lastname", lastName);
                
                phone = this.phoneRegion.currentView.getValue();
                this.model.set("phone", phone);
                
                this.model.set("country", cloneModel.get("country"));
                this.model.set("phone_country_code", cloneModel.get("phone_country_code").replace("+", ""));

				return this.model.persist();
			},
			handleSubmitClicked: function () {
				var that = this;

				// make sure the form validates before we submit
				if (!this.validateForm()) {
					return;
				}

				this.submitButton.showSpinner();

				this.saveData().then(function () {
					that.submitButton.hideSpinner();
					var language = that.model.get("language");
					App.changeLanguage(language).catch(function (err) {
						//ingnore lanugage change error. will default to dev/english
						App.error("Language file " + language + " : " + err);
					}).then(function () {
						App.navigate("");
					});
				}).catch(function () {
					that.submitButton.hideSpinner();
				});

			},
			handleCancelClicked: function () {
				window.history.back();
			},
			handleRemoveClick: function (){
				App.modalRegion.show(new SalusModalView({
					contentView: new App.Consumer.Settings.Views.DeleteUserProfileModalView()
				}));
				App.showModal();
			}
			/*,
			handleChangePasswordClick: function () {
				App.navigate("/settings/profile/changePassword");
			}*/
            
		}).mixin([SalusPage, ConsumerMixin.FormWithValidation], {
			analyticsSection: "settings",
			analyticsPage: "profile" //TODO: This should be a function so the equipment Id is recorded.
		});
	});


	return App.Consumer.Settings.Views.EditProfilePage;
});

