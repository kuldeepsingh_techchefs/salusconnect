"use strict";

define([
	"app",
	"bluebird",
	"common/constants",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/setupWizard/views/provisioning/CustomizeEquipmentModal.view",
	"consumer/setupWizard/views/provisioning/EnergyMeterCustomContent.view"
], function (App, P, constants, templates, SalusView) {
	App.module("Consumer.Settings.Views", function(Views, App, B, Mn, $, _) {
		Views.EnergyMeterSettingsContentView = Mn.LayoutView.extend({
			id: "bb-energy-meter-settings-content",
			className: "energy-meter-settings-content",
			template: templates["settings/equipment/energyMeterSettingsContent"],
			regions: {
				customContentRegion: "#bb-custom-content-region",
				saveButton: ".bb-button-save",
                cancelButton: ".bb-button-cancel"
			},
			ui: {
				savedLabel: ".saved-label"
			},
			initialize: function () {
				_.bindAll(this, "handleSubmitClicked");

				this.energyMeterCustomContentView = new App.Consumer.SetupWizard.Provisioning.Views.EnergyMeterCustomContentView({
					model: this.model,
					isEdit: true
				});

				this.saveButtonView = new App.Consumer.Views.SalusButtonPrimaryView({
					buttonTextKey: "common.labels.save",
					classes: "min-width-btn",
					clickedDelegate: this.handleSubmitClicked
				});
                this.cancelButtonView = new App.Consumer.Views.SalusButtonPrimaryView({
                    buttonTextKey: "common.labels.cancel",
                    className: "btn btn-default min-width-btn pull-right",
                    clickedDelegate: this.handleCancelClick
                });
			},
			onRender: function () {
				this.customContentRegion.show(this.energyMeterCustomContentView);
				this.saveButton.show(this.saveButtonView);
                this.cancelButton.show(this.cancelButtonView);
			},
            handleCancelClick: function () {
                window.history.back();
            },
            handleSubmitClicked: function () {
				var that = this;
				var threePhaseClamps,hasCheckedPhaseClamps,hasDuplicateNames, hasEmptyNames,
						hasError = false, promises = [],
						checkedClamps = this.energyMeterCustomContentView.getCheckedClamps();

				this.energyMeterCustomContentView.hideErrors();

				if (this.model.shouldSetPhases && this.model.shouldSetVoltage) {
					if (this.model.shouldSetPhases === 3) {
                        var phase = this.model.shouldSetPhases;
                        _.each(checkedClamps, function (clamp) {
                            if (clamp.phase !== 0) {
                                clamp.phase = 3;
                            }
                        });
                        
						threePhaseClamps = _.filter(checkedClamps, function (clamp) {
							return clamp.phase === 3;
						});

						if (threePhaseClamps.length !== 3) {
							this.energyMeterCustomContentView.showClampsError();
							hasError = true;
						}
                       
                        var threePhaseName = threePhaseClamps[0].name;
                        if(_.isEmpty(threePhaseName)){
                            hasEmptyNames = true;
                        }
						if (hasEmptyNames) {
							this.energyMeterCustomContentView.showClampsNameError();
							hasError = true;
						}

						// short circuit if we have an error
						if (hasError) {
							return false;
						}

						_.each(checkedClamps, function (clamp) {
							if (clamp.phase === 3) {
                                if(_.isEmpty(clamp.name)){
                                    clamp.name = threePhaseName;
                                }
								
							}
						});
					}
                    else {
                        _.each(checkedClamps, function (clamp) {
                            if (clamp.phase !== 0) {
                                clamp.phase = 1;
                            }
                        });
                        
                        hasCheckedPhaseClamps = _.filter(checkedClamps, function (clamp) {
							return clamp.phase === 1;
						});
                        if (hasCheckedPhaseClamps.length < 1) {
							this.energyMeterCustomContentView.showClamps1Error();
							hasError = true;
						}
						var names = _.map(hasCheckedPhaseClamps, function (obj) {
							return obj.name;
						});

						hasDuplicateNames = _.uniq(names).length !== names.length;
						hasEmptyNames = _.some(names, function (name) {
							return !name;
						});

						if (hasDuplicateNames || hasEmptyNames) {
							this.energyMeterCustomContentView.showClampsNameError();
							return false;
						}
                        	// short circuit if we have an error
						if (hasError) {
							return false;
						}
					}
				}
                else {
					this.energyMeterCustomContentView.showError();
					if (this.model.shouldSetPhases === 3 && checkedClamps.length !== 3) {
						this.energyMeterCustomContentView.showClampsError();
					}
					return false;
				}

				promises.push(this.model.setProperty("SetMeasuredVoltage_x10", this.model.shouldSetVoltage));

				// correspond to properties on the device ie. setting 2nd clamp sets 2nd property
				var indexesToSet = _.map(threePhaseClamps || [], function (clampObj) {
					return clampObj.clamp;
				});

				// the last one is still set to be single phase
                
                promises.push(this.model.get("ACPhase_c").setProperty(this.model.shouldSetPhases, null, indexesToSet));
                promises.push(this.model.get("ACPhase_c").setProperty(0, null, _.difference([1, 2, 3 , 4], indexesToSet)));

                
                    
				
				// save as meta
				this.model.set("ClampsUsed", checkedClamps);
				promises.push(this.model.persist());

				this.saveButtonView.showSpinner();
				return P.all(promises).then(function () {
					that._showSaveMessage();
					that.saveButtonView.hideSpinner();
				});
			},
			_showSaveMessage: function () {
				var that = this;
				this.ui.savedLabel.removeClass("invisible");
				window.setTimeout(function () {
					if (!that.isDestroyed) {
						that.ui.savedLabel.addClass("invisible");
					}
				}, 3000);
			}
		}).mixin([SalusView]);
	});

	return App.Consumer.Settings.Views.EnergyMeterSettingsContentView;
});