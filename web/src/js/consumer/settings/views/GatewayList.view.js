"use strict";

define([
	"app",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/mixins/mixin.registeredRegions",
	"consumer/equipment/views/GatewayForm.view",
	"common/model/ayla/deviceTypes/GatewayAylaDevice",
	"consumer/settings/views/GatewayListRow.view",
	"consumer/settings/views/EmptyList.view"
], function (App, consumerTemplates, SalusView, RegisteredRegions, GatewayView, GatewayModel) {
    
	App.module("Consumer.Settings.Views", function (Views, App, B, Mn) {
		Views.GatewayListView = Mn.CompositeView.extend({
			template: consumerTemplates["settings/manageDevices/gatewayList"],
			ui: {
				newGatewayForm: "#bb-new-gateway",
                addGateway: ".bb-add-gateway"
			},
			childViewContainer: "#bb-gateway-table",
			childView: Views.GatewayListRowView,
			childViewOptions: {
				isEdit: true
			},
			emptyView: Views.EmptyListView,
			emptyViewOptions: {
				i18nTextKey: "settings.manageDevices.gateway.empty"
			},
			events: {
				"click .bb-add-gateway": "handleAddGatewayClick"
			},
			initialize: function () {
				this.collection = new B.Collection();
			},
			onRender: function () {
				var that = this;

				App.salusConnector.getDataLoadPromise(["devices"]).then(function () {
					that.collection.set(App.salusConnector.getGatewayCollection().toArray());
                });
			},
			handleAddGatewayClick: function () {
				var that = this;
				// checks that we are only showing one gateway form
				if (!this.ui.newGatewayForm.children().length) {
                    this.ui.addGateway.hide();
					var formView = this.newFormView = new GatewayView({
						model: new GatewayModel(),
						startExpanded: true,
						mode: "add"
					});

					this.ui.newGatewayForm.append(formView.render().$el);
                    
                    this.listenTo(formView, "destroy", function() {
                        if(that.newFormView) {
                            clearInterval(that.newFormView.refreshInterval);
                            clearTimeout(that.newFormView.refreshPoll);
                        }
                    });

					this.listenTo(formView, "clicked:cancel", function () {
                        that.ui.addGateway.show();
						formView.destroy();
					});

					this.listenTo(formView, "added:device", function () {
						// TODO make soft refresh which resolves when devices.json resolves and makes properties/metadata in background
						App.salusConnector.getFullDeviceCollection().load().then(function () {
							formView.model.set("product_name", formView.model.get("alias"));
							formView.model.set("name", formView.model.get("alias"));
							formView.model.saveProductName();
							that.render();
						});
						formView.destroy();
					});
				}else{
                    this.ui.addGateway.show();
                }
			},
            
            onDestroy: function() {
                if(this.newFormView) {
                    clearInterval(this.newFormView.refreshInterval);
                    clearTimeout(this.newFormView.refreshPoll);
                }
            }
		}).mixin([SalusView, RegisteredRegions], {});
	});

	return App.Consumer.Settings.Views.GatewayListView;
});
