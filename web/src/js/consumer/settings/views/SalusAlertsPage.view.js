"use strict";

define([
	"app",
    "common/config",
	"common/constants",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusPage",
    "consumer/views/mixins/mixin.salusView"
], function (App, config, constants, consumerTemplates, SalusPageMixin, SalusView) {

	App.module("Consumer.Settings.Views", function (Views, App, B, Mn) {
        
        Views.SalusAlertsPropertiesView = Mn.ItemView.extend({
			template: consumerTemplates["settings/salusAlerts/salusAlertsProperties"],
			className: "salus-alerts-properties row center-contents-row",
            ui: {
				propertyName: ".bb-property-name"
			},
            events: {
				"click @ui.propertyName": "unreadDatapointClick"
			},
            initialize: function () {
                _.bindAll(this, "unreadDatapointClick");
            },
            onRender: function () {
                var propertyName = this.model.get("name").split(":")[2];
                // TODO: In addition to iT600 equipment and other equipment error to translation
                propertyName = App.translate("equipment.it600Warnings." + propertyName);
                this.ui.propertyName.text(propertyName);
                
                if (this.model.get("status") !== 1) {
                    this.ui.propertyName.addClass("unread");
                }
            },
            unreadDatapointClick: function (/*event*/) {
				var that = this;
				if (this.ui.propertyName.hasClass("unread")) {
					this.showSpinner();
                    
                    this.model.set("status", 1);
                    
                    App.salusConnector.getSalusAlertsCollection().saveAlertsStatus(this.model.get("key")).then(function() {
						that.hideSpinner();
                        that.ui.propertyName.removeClass("unread");
					}).error(function() {
                        that.hideSpinner();
                    });
				}
			}
		}).mixin([SalusView]);

		Views.SalusAlertsDevicesView = Mn.CompositeView.extend({
			template: consumerTemplates["settings/salusAlerts/salusAlertsDevices"],
			childView: Views.SalusAlertsPropertiesView,
			className: "salus-alerts-devices col-xs-12",
			initialize: function () {
                this.collection = new B.Collection(this.model.get("properties"));
			},
            onRender: function () {
				var device = App.salusConnector.getDeviceByDSN(this.model.get("dsn"));
                var connectionStatus = device.get("connection_status");
                var onlineStatus_i = device.get("OnlineStatus_i") ? device.getPropertyValue("OnlineStatus_i") : "";
                var leaveNetwork = device.get("LeaveNetwork") ? device.getPropertyValue("LeaveNetwork") : "";
                var connectivityMsg = this.$(".bb-device-msg-top");
                if (connectionStatus === "Offline") {
                    connectivityMsg.text(App.translate("equipment.status.notConnected"));
                } else if (onlineStatus_i === 0) {
                    connectivityMsg.text(App.translate("equipment.status.notConnected"));
                }
                if (leaveNetwork === 1) {
                    connectivityMsg.text(App.translate("equipment.status.leftNetwork"));
                }
			},
			templateHelpers: function () {
                var deviceName = "";
                var device = App.salusConnector.getDeviceByDSN(this.model.get("dsn"));
                if (device.isGateway()) {
                    deviceName = device.get("name");
                } else {
                    var gateway = App.salusConnector.getDeviceByDSN(device.get("gateway_dsn"));
                    deviceName = gateway.get("name") + " - " + device.get("name");
                }
                
				return {
					deviceName: deviceName
				};
			}
		}).mixin([SalusView]);

		Views.SalusAlertsCollectionView = Mn.CollectionView.extend({
			className: "salus-alerts row",
			childView: Views.SalusAlertsDevicesView,
			initialize: function (options) {
 				options = options || {};

                var salusAlertsCollection = App.salusConnector.getSalusAlertsCollection().filterByHasError();
                
                this.collection = salusAlertsCollection;
			}
		}).mixin([SalusView]);
        
		Views.SalusAlertsPage = Mn.LayoutView.extend({
            className: "container",
            id: "salus-alerts-page",
			template: consumerTemplates["settings/salusAlerts/salusAlertsPage"],
			regions: {
                "salusAlerts": ".bb-salus-alerts-region"
			},
			initialize: function (options) {
                options = options || {};
			},
			onRender: function () {
				var that = this;

				App.salusConnector.getDataLoadPromise(["devices", "salusAlerts"]).then(function (/*arrayOfData*/) {
                    that.salusAlertsCollectionView = new Views.SalusAlertsCollectionView();
                    that.salusAlerts.show(that.salusAlertsCollectionView);
				});
			}
		}).mixin([SalusPageMixin], {
			analyticsSection: "settings",
			analyticsPage: "salusAlerts"
        });
	});

	return App.Consumer.Settings.Views.SalusAlertsPage;
});