"use strict";

define([
	"app",
	"application/Router",
	"consumer/setupWizard/setupWizard.controller"
], function (App, AppRouter, Controller) {

	App.module("Consumer.SetupWizard", function (SetupWizard, App) {
		SetupWizard.Router = AppRouter.extend({
			appRoutes: {
				"setup": "setupLanding",
				"setup/gateway": "setupGateway",
				"setup/pairing": "setupPairing",
				"setup/provision": "provisionEquipment",
				"setup/naming": "provisionNaming",
				"setup/customize": "provisionCustomize"
			}
		});

		App.addInitializer(function () {
			return new SetupWizard.Router({
				controller: Controller
			});
		});
	});
});