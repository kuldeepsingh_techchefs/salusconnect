"use strict";

define([
	"app",
	"bluebird",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusPage",
	"consumer/views/SalusButtonPrimary.view",
	"consumer/views/SalusModal.view",
	"consumer/views/SalusAlertModal.view",
	"consumer/views/SalusButtonPrimary.view",
	"consumer/views/modal/ModalCloseButton.view",
	"consumer/models/SalusAlertModalModel"
], function (App, P, consumerTemplates, SalusPageMixin) {

	App.module("Consumer.SetupWizard.Views", function (Views, App, B, Mn, $, _) {
		Views.SetupPairingPage = Mn.LayoutView.extend({
			template: consumerTemplates["setupWizard/pairing"],
			className: "pairing-page",
			regions: {
				buttonRegion: ".bb-pairing-button"
			},
			initialize: function () {
				_.bindAll(this, "handleConnectClick");

				this.buttonView = new App.Consumer.Views.SalusButtonPrimaryView({
					classes: "btn-warning preserve-case",
					clickedDelegate: this.handleConnectClick,
					buttonTextKey: "setupWizard.pairing.buttonText"
				});

				// todo : could be optimized by just getting properties for the coordinator
				this.deviceDependencies = App.salusConnector.getGatewayAndDelegates().map(function (device) {
					return device.getDeviceProperties();
				});
			},
			handleConnectClick: function () {
				var that = this;
				this.buttonView.showSpinner();
				App.getCurrentGateway().initiatePairingMode().then(function () {
					that.buttonView.hideSpinner();
					App.navigate("setup/provision");
				}).catch(function (err) {
					//that.buttonView.hideSpinner();
					if (err === "No coordinator to initiate pairing") {
                      //  var that = this;

                        if (that.isDestroyed) {
                            return;
                        }
                        that.addNodeToGateway().then(function () {
                            that.buttonView.hideSpinner();
                            App.navigate("setup/provision");

                        }).catch(function (err) {
                            that.buttonView.hideSpinner();
                            App.modalRegion.show(new App.Consumer.Views.SalusAlertModalView({
                                model: new App.Consumer.Models.AlertModalViewModel({
                                    iconClass: "icon-warning",
                                    primaryLabelText: App.translate("setupWizard.pairing.noCoordinator"),
                                    secondaryLabelText: App.translate("setupWizard.pairing.requiresCoordinator"),
                                    leftButton: new App.Consumer.Views.ModalCloseButton({
                                        id: "ok-close-btn",
                                        classes: "width100 preserve-case",
                                        buttonTextKey: "common.labels.ok"
                                    })
                                })
                            }));

                            App.showModal();
                        });
			
                     }
				});
			},
 			onRender: function () {
				var that = this;

				P.all(this.deviceDependencies).then(function () {
                    if(App.salusConnector.getGatewayAndDelegates().length === 3){
                        return that.writeRefreshToNode();
                    }
				}).then(function() {
                    that.$("#bb-pairing-instruction-text").html(App.translate("setupWizard.pairing.paragraphHtml"));
					that.buttonRegion.show(that.buttonView);
                });
			},
            writeRefreshToNode: function(){
                return  App.salusConnector.getFullDeviceCollection().load().then(function () {
                    var addPromises = _.map(App.salusConnector.getGatewayAndDelegates(), function (provisionDevice) {
                        return provisionDevice.getDevicePropertiesPromise();
                    });
                    
                    return P.all(addPromises).then(function(){
                        var gatewayNode = App.getCurrentGateway().getGatewayNode(), 
                        coordinator = App.getCurrentGateway().getCoordinator(), refreshPromises = [];

                        if (gatewayNode && gatewayNode.getPropertyValue("GatewaySoftwareVersion") === null) {
                            refreshPromises.push(gatewayNode.setProperty("SetRefresh", 1));
                        }

                        if (coordinator && coordinator.getPropertyValue("FirmwareVersion") === null) {
                            refreshPromises.push(coordinator.setProperty("SetRefresh_d", 1));
                        }

                        return P.all(refreshPromises);
                    });
                        
                });
                
            },
            addNodeToGateway: function(){
                var deviceArray = [];
                return  App.salusConnector.getFullDeviceCollection().load().then(function(){
                        return App.getCurrentGateway().getDevicesToPair().then(function (devicesToPair) {
                            _.each(devicesToPair, function (device) {
                                if(device.oem_model === "SAU2AG1-GW" ||
                                        device.oem_model === "SAU2AG1-ZC"){
                                    deviceArray.push(device);
                                }

                            });
                            return P.resolve(deviceArray);

                        });
                    }).then(function(){
                        var promiseArray = [];
                        _.each(deviceArray, function (device) {
                            // add the device to gateway
                            promiseArray.push(App.getCurrentGateway().pairDevice(device.dsn));
                        });

                        return P.all(promiseArray);
                    }).then(function(){                        
                        return  App.salusConnector.getFullDeviceCollection().load().then(function () {
                        	var addPromises = _.map(App.salusConnector.getGatewayAndDelegates(), function (provisionDevice) {
                                return provisionDevice.getDevicePropertiesPromise();
                                
							});

							return P.all(addPromises);
                        
                      })
                    });
            }
		}).mixin([SalusPageMixin], {
			analyticsSection: "setupWizard",
			analyticsPage: "pairing"
		});
	});

	return App.Consumer.SetupWizard.Views.SetupPairingPage;
});