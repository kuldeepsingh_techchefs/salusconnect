"use strict";

define([
	"app",
	"consumer/consumerTemplates",
    "consumer/views/SalusModal.view",
	"consumer/models/SalusRadioViewModel",
	"consumer/views/SalusRadio.view",
	"consumer/views/SalusButtonPrimary.view",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/mixins/mixin.registeredRegions",
    "consumer/setupWizard/views/fancoil/FancoilSystemMode.view",
    "consumer/setupWizard/views/fancoil/FancoilScheduleNHC.view"
], function (App, consumerTemplates, SalusModal) {

	App.module("Consumer.SetupWizard.Fancoil.Views", function (Views, App, B, Mn, $, _) {

		Views.FancoilHCSelectionLayout = Mn.LayoutView.extend({
			template: consumerTemplates["setupWizard/fancoil/heatCoolSelection"],
			className: "container",
			regions: {
				"radioHeatingCooling": ".bb-heating-cooling",
				"radioHeatingOnly": ".bb-heating-only",
                "radioCoolingOnly": ".bb-cooling-only",
				"nextButton": ".bb-next-button"
			},
			
			events: {
				"change input[name=hcSelection]": "handleRadioChange"
			},
			initialize: function () {
				_.bindAll(this, "handleRadioChange", "handleNextButtonClicked");

				var value = this.model.getPropertyValue("HeatCoolSelection");
                var heatingCooling = false, heating = false, cooling = false;
                switch(value){
                    case 0:
                        this.hcSelection = 0;
                        heatingCooling = true;
                        break;
                    case 1:
                        this.hcSelection = 1;
                        cooling = true;
                        break;
                    case 2:
                        this.hcSelection = 2;
                        heating = true;
                        break;
                    default:
                        this.hcSelection = 0;
                        heatingCooling = true;
                        break;
                }
				this.registerRegion("radioHeatingCooling", new App.Consumer.Views.RadioView({
					model: new App.Consumer.Models.RadioViewModel({
						value: 0,
						name: "hcSelection",
						isChecked: heatingCooling,
						classes: "pull-right data-collection-radio"
					})
				}));

				this.registerRegion("radioHeatingOnly", new App.Consumer.Views.RadioView({
					model: new App.Consumer.Models.RadioViewModel({
						value: 2,
						name: "hcSelection",
						isChecked: heating,
						classes: "pull-right data-collection-radio"
					})
				}));
                this.registerRegion("radioCoolingOnly", new App.Consumer.Views.RadioView({
					model: new App.Consumer.Models.RadioViewModel({
						value: 1,
						name: "hcSelection",
						isChecked: cooling,
						classes: "pull-right data-collection-radio"
					})
				}));

				this.nextButtonView = this.registerRegion("nextButton", new App.Consumer.Views.SalusButtonPrimaryView({
					id: "save-data-collection-btn",
					buttonTextKey: "common.labels.next",
					classes: "center-relative padding-l-30 padding-r-30",
					clickedDelegate: this.handleNextButtonClicked
				}));
			},

			handleRadioChange: function () {
                //(0: Heat/Cool, 1: Cooling, 2: Heating)
				var selected = this.$("input[name=hcSelection]").filter(":checked").val();
				this.hcSelection = parseInt(selected);
				
			},
			handleNextButtonClicked: function () {
                var that = this;
				var setPropertyPromise,device = this.model;
				setPropertyPromise = device.setProperty("HeatCoolSelection", this.hcSelection);
				setPropertyPromise.then(function(){
					App.hideModal();
					$(".modal-backdrop").remove();
                    var nextView = null;
                    //if choose heating&cooling, go to page SystemModePage, otherwise go to SchedulePage.
                    if(that.hcSelection === 0){
                        nextView = new App.Consumer.SetupWizard.Fancoil.Views.FancoilSystemModeLayout({
                            model: that.model
                         });
                    }else{
                        nextView = new App.Consumer.SetupWizard.Fancoil.Views.FancoilScheduleNHCLayout({
                            model: that.model
                        });
                    }
					
					App.modalRegion.show(new SalusModal({
                        staticBackdrop: true,
						contentView: nextView,
						size: "modal-lg"
					}));

					App.showModal(); 
				});
				
			}
		}).mixin([App.Consumer.Views.Mixins.SalusView, App.Consumer.Views.Mixins.RegisteredRegions]);
	});

	return App.Consumer.SetupWizard.Fancoil.Views.FancoilHCSelectionLayout;
});