"use strict";

define([
	"app",
    "common/constants",
	"consumer/consumerTemplates",
    "consumer/views/SalusModal.view",
	"consumer/models/SalusRadioViewModel",
	"consumer/views/SalusRadio.view",
	"consumer/views/SalusButtonPrimary.view",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/mixins/mixin.registeredRegions",
    "consumer/setupWizard/views/fancoil/FancoilSuccessSetup.view",
    "consumer/setupWizard/views/fancoil/FancoilSystem.view"
], function (App, constants, consumerTemplates, SalusModal) {

	App.module("Consumer.SetupWizard.Fancoil.Views", function (Views, App, B, Mn, $, _) {

		Views.FancoilHourFormatLayout = Mn.LayoutView.extend({
			template: consumerTemplates["setupWizard/fancoil/hourFormat"],
			className: "container",
			regions: {
				"radio24": ".bb-24",
				"radio12": ".bb-12",
				"nextButton": ".bb-next-button"
			},
			
			events: {
				"change input[name=fancoilHourFormat]": "handleRadioChange"
			},
			initialize: function () {
				_.bindAll(this, "handleRadioChange", "handleNextButtonClicked", "startPropertyPoll", 
                    "clearPropertyPollTimeout", "propertyPollTimeout", "propertyPoll");

				var hourFormatChecked = false;
                this.hourFormat = 0;
                if(this.model.getPropertyValue("TimeFormat24Hour") === 1){
                    hourFormatChecked = true;
                    this.hourFormat = 1;
                }
				this.registerRegion("radio24", new App.Consumer.Views.RadioView({
					model: new App.Consumer.Models.RadioViewModel({
						value: 1,
						name: "fancoilHourFormat",
						isChecked: hourFormatChecked,
						classes: "pull-right data-collection-radio"
					})
				}));

				this.registerRegion("radio12", new App.Consumer.Views.RadioView({
					model: new App.Consumer.Models.RadioViewModel({
						value: 0,
						name: "fancoilHourFormat",
						isChecked: !hourFormatChecked,
						classes: "pull-right data-collection-radio"
					})
				}));
                
               

				this.nextButtonView = this.registerRegion("nextButton", new App.Consumer.Views.SalusButtonPrimaryView({
					id: "save-data-collection-btn",
					buttonTextKey: "setupWizard.provisioning.customize.completeSetup",
					classes: "center-relative padding-l-30 padding-r-30",
					clickedDelegate: this.handleNextButtonClicked
				}));
			},

			handleRadioChange: function () {
				var selected = this.$("input[name=fancoilHourFormat]").filter(":checked").val();
				this.hourFormat = parseInt(selected);
				
			},

			

			handleNextButtonClicked: function () {
				var that = this;
				var setPropertyPromise,device = this.model;
				setPropertyPromise = device.setProperty("TimeFormat24Hour", this.hourFormat);
				
				this.gateway = App.getCurrentGateway();
				var deviceModels = [];
				// get device models from salusConnector
				deviceModels = _.map(this.gateway ? this.gateway.pairingDevices : [], function (deviceObj) {
					return App.salusConnector.getDevice(deviceObj.dsn);
				});

				// set collection
                var deviceArray = [];
				for(var i = 0; i < deviceModels.length; i++){
                    //currentModel
                   // if(deviceModels[i] !== undefined){
					if(deviceModels[i] !== undefined && deviceModels[i].get("dsn") !== this.model.get("dsn") &&
                            deviceModels[i].get("oem_model") === constants.modelTypes.FC600){
						deviceArray.push(deviceModels[i]);
					}
				}
				if(deviceArray.length > 0){
					setPropertyPromise.then(function(){
						App.hideModal();
						$(".modal-backdrop").remove();
						var nextView = new App.Consumer.SetupWizard.Fancoil.Views.FancoilSuccessSetupLayout({
							model: that.model
						});


						 App.modalRegion.show(new SalusModal({
							 contentView: nextView,
                             staticBackdrop: true,
							 size: "modal-lg"
						 }));

						 App.showModal(); 
					});
				}else{
					setPropertyPromise.then(function(){
                        //that.startPropertyPoll();
                        App.hideModal();
                        that.model.trigger("click:save", that.model);
					});
				}
				

				
			},
            startPropertyPoll: function () {                
				this.pollingInterval = setInterval(this.propertyPoll, 500);
                this.pollingTimeout = setTimeout(this.propertyPollTimeout, 1000 * 60 * 10);
			},
            clearPropertyPollTimeout: function () {
                this.pollingInterval = null;
				clearInterval(this.pollingInterval);
                clearTimeout(this.pollingTimeout);
                
			},
            propertyPollTimeout: function(){
                this.clearPropertyPollTimeout();
                this.showSetupPage();
                
            },
            propertyPoll: function(){
                var that = this;
                
                this.model.getDeviceProperties(true).then(function () {
                    if(that.pollingInterval !== null && that.model.getPropertyValue("CompleteSetup") === 15){
                        that.clearPropertyPollTimeout();
                        that.model.trigger("click:save", that.model);
						App.hideModal();
                    }
                       
                });
            },
            showSetupPage: function(){
                App.hideModal();
                $(".modal-backdrop").remove();
                var nextView = new App.Consumer.SetupWizard.Fancoil.Views.FancoilSystemLayout({
                    model: this.model
                 });
                 App.modalRegion.show(new SalusModal({
                     contentView: nextView,
                     staticBackdrop: true,
                     size: "modal-lg"
                 }));

                 App.showModal();  
            }
            
		}).mixin([App.Consumer.Views.Mixins.SalusView, App.Consumer.Views.Mixins.RegisteredRegions]);
	});

	return App.Consumer.SetupWizard.Fancoil.Views.FancoilHourFormatLayout;
});