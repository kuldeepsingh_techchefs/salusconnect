"use strict";

define([
	"app",
    "common/constants",
	"consumer/consumerTemplates",
    "consumer/views/SalusModal.view",
	"consumer/models/SalusRadioViewModel",
	"consumer/views/SalusRadio.view",
	"consumer/views/SalusButtonPrimary.view",
    "consumer/models/SalusCheckboxViewModel",
	"consumer/views/SalusCheckbox.view",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/mixins/mixin.registeredRegions",
    "consumer/setupWizard/views/fancoil/FancoilS2Terminal.view"
], function (App,constants, consumerTemplates, SalusModal) {

	App.module("Consumer.SetupWizard.Fancoil.Views", function (Views, App, B, Mn, $, _) {

		Views.FancoilScheduleLayout = Mn.LayoutView.extend({
			template: consumerTemplates["setupWizard/fancoil/fancoilSchedule"],
			className: "container",
			regions: {
                "radioYes": ".bb-yes",
				"radioNo": ".bb-no",
				"nextButton": ".bb-next-button"
			},
            ui: {
                pageTitle: "#fancoil_schedule_header"
            },
            
			
			events: {
				"change input[name=hcSchedule]": "handleRadioChange"
			},
			initialize: function () {
				_.bindAll(this, "handleRadioChange", "handleNextButtonClicked", "showNextPage");

				var initValue = true;
                var s1ComTerminals = this.model.getPropertyValue("S1ComTerminals"),
                        fanCoilType = this.model.getPropertyValue("FanCoilType");
                this.showSimpleTitle = true;
                if(s1ComTerminals === 1){
                    this.showSimpleTitle = false;
                }
                else if(s1ComTerminals === 2 && fanCoilType === 0){
                    this.showSimpleTitle = false;
                }
                else if(s1ComTerminals === 2 && fanCoilType === 1){
                    this.showSimpleTitle = true;
                }
                else{
                    this.showSimpleTitle = true;
                }
                this.registerRegion("radioYes", new App.Consumer.Views.RadioView({
                    model: new App.Consumer.Models.RadioViewModel({
                        value: 1,
                        name: "hcSchedule",
                        isChecked: initValue,
                        classes: "pull-right data-collection-radio"
                    })
                }));
                this.registerRegion("radioNo", new App.Consumer.Views.RadioView({
                    model: new App.Consumer.Models.RadioViewModel({
                        value: 0,
                        name: "hcSchedule",
                        isChecked: !initValue,
                        classes: "pull-right data-collection-radio"
                    })
                }));
               
                
				this.nextButtonView = this.registerRegion("nextButton", new App.Consumer.Views.SalusButtonPrimaryView({
					id: "save-data-collection-btn",
					buttonTextKey: "common.labels.next",
					classes: "center-relative padding-l-30 padding-r-30",
					clickedDelegate: this.handleNextButtonClicked
				}));
			},

			onRender: function () {
                this.hcSchedule = 1;
                if(!this.showSimpleTitle){
                    this.ui.pageTitle.text(App.translate("setupWizard.provisioning.customize.fancoil.heatingCoolingScheduleHeader"));
                }else{
                    this.ui.pageTitle.text(App.translate("setupWizard.provisioning.customize.fancoil.defaultScheduleHeader"));
                }
			},

		

			handleRadioChange: function () {
                //(0: No, 1:Yes
				var selected = this.$("input[name=hcSchedule]").filter(":checked").val();
                this.hcSchedule = parseInt(selected);

			},
			handleNextButtonClicked: function () {
                var that = this;
                

                if(this.hcSchedule === 1){
                    that.nextButtonView.showSpinner();
                    this.model.getSchedules().setDefaultSchedule(constants.scheduleConfigurationTypes.WORKWEEK).then(function(){
                        that.nextButtonView.hideSpinner();
                       
                        that.showNextPage();
                    });
//                    if(this.showSimpleTitle){
//                        //写defaule heating和cooling 5/2 schedule
//                        
//                    }else{
//                        //写defaule cooling schedule 5/2
//                        
//                    }
                }else{
                    
                    that.showNextPage();
                }
                
			},
            showNextPage: function(){
                App.hideModal();
                $(".modal-backdrop").remove();
                var that = this;
                var nextView = new App.Consumer.SetupWizard.Fancoil.Views.FancoilS2TerminalLayout({
                    model: that.model
                });
                
                App.modalRegion.show(new SalusModal({
                    contentView: nextView,
                    staticBackdrop: true,
                    size: "modal-lg"
                }));

                App.showModal(); 
            }
		}).mixin([App.Consumer.Views.Mixins.SalusView, App.Consumer.Views.Mixins.RegisteredRegions]);
	});

	return App.Consumer.SetupWizard.Fancoil.Views.FancoilScheduleLayout;
});