"use strict";

define([
	"app",
    "common/constants",
	"consumer/consumerTemplates",
    "consumer/views/SalusModal.view",
	"consumer/models/SalusRadioViewModel",
	"consumer/views/SalusRadio.view",
	"consumer/views/SalusButtonPrimary.view",
    "consumer/models/SalusCheckboxViewModel",
	"consumer/views/SalusCheckbox.view",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/mixins/mixin.registeredRegions",
    "consumer/setupWizard/views/fancoil/FancoilS2Terminal.view"
], function (App, constants, consumerTemplates, SalusModal) {

	App.module("Consumer.SetupWizard.Fancoil.Views", function (Views, App, B, Mn, $, _) {

		Views.FancoilScheduleNHCLayout = Mn.LayoutView.extend({
			template: consumerTemplates["setupWizard/fancoil/fancoilScheduleNHC"],
			className: "container",
			regions: {
                "checkboxNoRegion": ".bb-no-checkbox",
                "checkboxCoolingRegion": ".bb-cooling-checkbox",
                "checkboxHeatingRegion": ".bb-heating-checkbox",
				"nextButtonRegion": ".bb-next-button"
			},

			initialize: function () {
                var that = this;
				_.bindAll(this,  "handleNextButtonClicked", "checkNextButton", "showNextPage");
                

                this.checkboxNo = new App.Consumer.Views.CheckboxView({
                    model: new App.Consumer.Models.CheckboxViewModel({
                        name: "usageCheckbox",
                        value: 0,
                        secondaryIconClass: "",
                        checkColor:  "",
                        isChecked: true
                    })
                });
                
                this.checkboxCooling = new App.Consumer.Views.CheckboxView({
                    model: new App.Consumer.Models.CheckboxViewModel({
                         name: "usageCheckbox",
                         value: 1,
                         secondaryIconClass: "",
                         checkColor:  "",
                         isChecked: false
                     })
                });
                
                this.checkboxHeating = new App.Consumer.Views.CheckboxView({
                    model: new App.Consumer.Models.CheckboxViewModel({
                        name: "usageCheckbox",
                        value: 2,
                        secondaryIconClass: "",
                        checkColor:  "",
                        isChecked: false
                    })
                });
                this.listenTo(this.checkboxNo, "clicked:checkbox", function () {
                    if(that.checkboxNo.model.get("isChecked")){
                       that.checkboxCooling.model.set("isChecked",false);
                       that.checkboxHeating.model.set("isChecked",false);
                    }
                    that.checkNextButton();
                    
				});
                this.listenTo(this.checkboxCooling, "clicked:checkbox", function () {
                    if(that.checkboxCooling.model.get("isChecked")){
                         that.checkboxNo.model.set("isChecked",false);
                    }
                   that.checkNextButton();
				});
                this.listenTo(this.checkboxHeating, "clicked:checkbox", function () {
                    if(that.checkboxHeating.model.get("isChecked")){
                         that.checkboxNo.model.set("isChecked",false);
                    }
                    that.checkNextButton();
				});
 
                this.nextButton = new App.Consumer.Views.SalusButtonPrimaryView({
					id: "save-data-collection-btn",
					buttonTextKey: "common.labels.next",
					classes: "center-relative padding-l-30 padding-r-30",
					clickedDelegate: this.handleNextButtonClicked
				});

			},
            checkNextButton: function(){
                if(!this.checkboxNo.model.get("isChecked") && 
                   !this.checkboxCooling.model.get("isChecked") &&
                   !this.checkboxHeating.model.get("isChecked")){
                      this.nextButton.disable();
                }else{
                    this.nextButton.enable();
                }
            },

			onRender: function () {        
                var showHeating = false, showCooling = false;
                var value = this.model.getPropertyValue("HeatCoolSelection");
                if(value === 0){
                    showHeating = true;
                    showCooling = true;

                }else if(value === 1){
                    showCooling = true;
                    this.$(".bb-heating").addClass("hidden");
                }
                else if(value === 2){
                    showHeating = true;
                    this.$(".bb-cooling").addClass("hidden");
                }
                this.nextButtonRegion.show(this.nextButton);
                this.checkboxNoRegion.show(this.checkboxNo);
                
                if(showHeating){
                    this.checkboxHeatingRegion.show(this.checkboxHeating);
                }
                if(showCooling){
                    this.checkboxCoolingRegion.show(this.checkboxCooling);
                }
			},

		
			handleNextButtonClicked: function () {
                var that = this;
				var result = "";
               
                
                var arrays = this.$("input[checked=checked]");
                _.each(arrays, function(object){
                    result = result + object.value;
                });
                //this.$("input[checked=checked]").val()
                if(result === "0"){
                    that.showNextPage();
                    
                }else{
                    this.nextButton.showSpinner();
                    this.model.getSchedules().setDefaultSchedule(constants.scheduleConfigurationTypes.WORKWEEK).then(function(){
                        that.nextButton.hideSpinner(); 
                        that.showNextPage();
                       
                    });
                }
                
//                if(result === "0"){
//                    //No
//                }else if(result === "1"){
//                    //Cooling
//                }else if(result === "2"){
//                    //heating
//                }else if(result === "12"){
//                    //cooling&heating schedule
//                }
				
				
			},
            showNextPage: function(){
                App.hideModal();
                $(".modal-backdrop").remove();
                var that = this;
                var nextView = new App.Consumer.SetupWizard.Fancoil.Views.FancoilS2TerminalLayout({
                    model: that.model
                });

                App.modalRegion.show(new SalusModal({
                    contentView: nextView,
                    staticBackdrop: true,
                    size: "modal-lg"
                }));

                App.showModal(); 
            }
		}).mixin([App.Consumer.Views.Mixins.SalusView, App.Consumer.Views.Mixins.RegisteredRegions]);
	});

	return App.Consumer.SetupWizard.Fancoil.Views.FancoilScheduleNHCLayout;
});