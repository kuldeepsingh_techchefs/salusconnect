"use strict";

define([
	"app",
	"consumer/consumerTemplates",
    "consumer/views/SalusModal.view",
	"consumer/models/SalusRadioViewModel",
	"consumer/views/SalusRadio.view",
	"consumer/views/SalusButtonPrimary.view",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/mixins/mixin.registeredRegions",
    "consumer/setupWizard/views/fancoil/FancoilCopySetup.view"
], function (App, consumerTemplates, SalusModal) {

	App.module("Consumer.SetupWizard.Fancoil.Views", function (Views, App, B, Mn, $, _) {

		Views.FancoilSuccessSetupLayout = Mn.LayoutView.extend({
			template: consumerTemplates["setupWizard/fancoil/successSetup"],
			className: "container",
			regions: {
                "radioYes": ".bb-yes",
				"radioNo": ".bb-no",
				"nextButton": ".bb-next-button"
			},
           
			
			events: {
				"change input[name=hcSetup]": "handleRadioChange"
			},
			initialize: function () {
				_.bindAll(this, "handleRadioChange", "handleNextButtonClicked");

				var initValue = true;
                 this.hcSetup = 1;
                this.registerRegion("radioYes", new App.Consumer.Views.RadioView({
                   model: new App.Consumer.Models.RadioViewModel({
                       value: 1,
                       name: "hcSetup",
                       isChecked: initValue,
                       classes: "pull-right data-collection-radio"
                   })
                }));
                this.registerRegion("radioNo", new App.Consumer.Views.RadioView({
                   model: new App.Consumer.Models.RadioViewModel({
                       value: 0,
                       name: "hcSetup",
                       isChecked: !initValue,
                       classes: "pull-right data-collection-radio"
                   })
                }));
				this.nextButtonView = this.registerRegion("nextButton", new App.Consumer.Views.SalusButtonPrimaryView({
					id: "save-data-collection-btn",
					buttonTextKey: "common.labels.next",
					classes: "center-relative padding-l-30 padding-r-30",
					clickedDelegate: this.handleNextButtonClicked
				}));
			},

			
			handleRadioChange: function () {
               
                //(0: Heat/Cool, 1: Cooling, 2: Heating)
				var selected = this.$("input[name=hcSetup]").filter(":checked").val();
				this.hcSetup = parseInt(selected);
				
			},
			handleNextButtonClicked: function () {
                if(this.hcSetup === 1){
                    var that = this;
                    App.hideModal();
					$(".modal-backdrop").remove();
                    
                    var nextView = new App.Consumer.SetupWizard.Fancoil.Views.FancoilCopySetupLayout({
                        model: that.model
                    });

					 App.modalRegion.show(new SalusModal({
						 contentView: nextView,
						 size: "modal-lg"
					 }));

					 App.showModal(); 
                    //FancoilCopySetupLayout
                }else{
                    App.hideModal();
                }
				
				
			}
		}).mixin([App.Consumer.Views.Mixins.SalusView, App.Consumer.Views.Mixins.RegisteredRegions]);
	});

	return App.Consumer.SetupWizard.Fancoil.Views.FancoilSuccessSetupLayout;
});