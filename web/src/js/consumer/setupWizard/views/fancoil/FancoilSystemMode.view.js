"use strict";

define([
	"app",
	"consumer/consumerTemplates",
    "consumer/views/SalusModal.view",
	"consumer/models/SalusRadioViewModel",
	"consumer/views/SalusRadio.view",
	"consumer/views/SalusButtonPrimary.view",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/mixins/mixin.registeredRegions",
    "consumer/setupWizard/views/fancoil/FancoilScheduleNHC.view"
], function (App, consumerTemplates, SalusModal) {

	App.module("Consumer.SetupWizard.Fancoil.Views", function (Views, App, B, Mn, $, _) {

		Views.FancoilSystemModeLayout = Mn.LayoutView.extend({
			template: consumerTemplates["setupWizard/fancoil/systemMode"],
			className: "container",
			regions: {
				"radioCooling": ".bb-cooling",
				"radioHeating": ".bb-heating",
				"nextButton": ".bb-next-button"
			},
			
			events: {
				"change input[name=systemMode]": "handleRadioChange"
			},
			initialize: function () {
				_.bindAll(this, "handleRadioChange", "handleNextButtonClicked");
                
                var modeChecked = false;
                this.systemMode = 4;
                if(this.model.getPropertyValue("SystemMode") === 3){
                    modeChecked = true;
                    this.systemMode = 3;
                }
				this.registerRegion("radioCooling", new App.Consumer.Views.RadioView({
					model: new App.Consumer.Models.RadioViewModel({
						value: 3,
						name: "systemMode",
						isChecked: modeChecked,
						classes: "pull-right data-collection-radio"
					})
				}));

				this.registerRegion("radioHeating", new App.Consumer.Views.RadioView({
					model: new App.Consumer.Models.RadioViewModel({
						value: 4,
						name: "systemMode",
						isChecked: !modeChecked,
						classes: "pull-right data-collection-radio"
					})
				}));
               

				this.nextButtonView = this.registerRegion("nextButton", new App.Consumer.Views.SalusButtonPrimaryView({
					id: "save-data-collection-btn",
					buttonTextKey: "common.labels.next",
					classes: "center-relative padding-l-30 padding-r-30",
					clickedDelegate: this.handleNextButtonClicked
				}));
			},
		

			handleRadioChange: function () {
                //(3: Cool, 4: Heat)
				var selected = this.$("input[name=systemMode]").filter(":checked").val();
				this.systemMode = parseInt(selected);
				
			},
			handleNextButtonClicked: function () {
                var that = this;
				var setPropertyPromise,device = this.model;
				setPropertyPromise = device.setProperty("SystemMode", this.systemMode);
				setPropertyPromise.then(function(){
					App.hideModal();
					$(".modal-backdrop").remove();
                    var  nextView = new App.Consumer.SetupWizard.Fancoil.Views.FancoilScheduleNHCLayout({
                            model: that.model
                         });
					
					App.modalRegion.show(new SalusModal({
						contentView: nextView,
                        staticBackdrop: true,
						size: "modal-lg"
					}));

					App.showModal(); 
				});
				
			}
		}).mixin([App.Consumer.Views.Mixins.SalusView, App.Consumer.Views.Mixins.RegisteredRegions]);
	});

	return App.Consumer.SetupWizard.Fancoil.Views.FancoilSystemModeLayout;
});