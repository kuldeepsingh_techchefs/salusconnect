"use strict";

define([
	"app",
	"consumer/consumerTemplates",
    "consumer/views/SalusModal.view",
	"consumer/models/SalusRadioViewModel",
	"consumer/views/SalusRadio.view",
	"consumer/views/SalusButtonPrimary.view",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/mixins/mixin.registeredRegions",
    "consumer/setupWizard/views/fancoil/FancoilHCSelection.view",
    "consumer/setupWizard/views/fancoil/FancoilSchedule.view"
], function (App, consumerTemplates, SalusModal) {

	App.module("Consumer.SetupWizard.Fancoil.Views", function (Views, App, B, Mn, $, _) {

		Views.FancoilTerminalLayout = Mn.LayoutView.extend({
			template: consumerTemplates["setupWizard/fancoil/comTerminal"],
			className: "container",
			regions: {
				"radioManual": ".bb-manual",
				"radioSwitch": ".bb-switch",
                "radioAuto": ".bb-auto",
				"nextButton": ".bb-next-button"
			},
			
			events: {
				"change input[name=fancoilTerminal]": "handleRadioChange"
			},
			initialize: function () {
				_.bindAll(this, "handleRadioChange", "handleNextButtonClicked");

                var value = this.model.getPropertyValue("S1ComTerminals");
                var manualChecked = false, switchChecked = false, autoChecked = false;
                switch(value){
                    case 0:
                        this.S1ComTerminals = 0;
                        manualChecked = true;
                        break;
                    case 1:
                        this.S1ComTerminals = 1;
                        switchChecked = true;
                        break;
                    case 2:
                        this.S1ComTerminals = 2;
                        autoChecked = true;
                        break;
                    default:
                        this.S1ComTerminals = 0;
                        manualChecked = true;
                        break;
                }
				this.registerRegion("radioManual", new App.Consumer.Views.RadioView({
					model: new App.Consumer.Models.RadioViewModel({
						value: 0,
						name: "fancoilTerminal",
						isChecked: manualChecked,
						classes: "pull-right data-collection-radio"
					})
				}));

				this.registerRegion("radioSwitch", new App.Consumer.Views.RadioView({
					model: new App.Consumer.Models.RadioViewModel({
						value: 1,
						name: "fancoilTerminal",
						isChecked: switchChecked,
						classes: "pull-right data-collection-radio"
					})
				}));
                
                this.registerRegion("radioAuto", new App.Consumer.Views.RadioView({
                    model: new App.Consumer.Models.RadioViewModel({
                        value: 2,
                        name: "fancoilTerminal",
                        isChecked: autoChecked,
                        classes: "pull-right data-collection-radio"
                    })
				}));

				this.nextButtonView = this.registerRegion("nextButton", new App.Consumer.Views.SalusButtonPrimaryView({
					id: "save-data-collection-btn",
					buttonTextKey: "common.labels.next",
					classes: "center-relative padding-l-30 padding-r-30",
					clickedDelegate: this.handleNextButtonClicked
				}));
			},

			handleRadioChange: function () {
				var selected = this.$("input[name=fancoilTerminal]").filter(":checked").val();
				this.S1ComTerminals = parseInt(selected);
				
			},

			

			handleNextButtonClicked: function () {
				var that = this;
				var setPropertyPromise = [],device = this.model;
				setPropertyPromise.push(device.setProperty("S1ComTerminals", this.S1ComTerminals));
                if(that.S1ComTerminals !== 0){
                    setPropertyPromise.push(device.setProperty("HeatCoolSelection", 0));
                }
				P.all(setPropertyPromise).then(function(){
					App.hideModal();
					$(".modal-backdrop").remove();
                    //if Manual, go to HeatingCoolingSection page, otherwise go to schedule page
                    var nextView = null;
                    if(that.S1ComTerminals === 0){
                        nextView = new App.Consumer.SetupWizard.Fancoil.Views.FancoilHCSelectionLayout({
                            model: that.model
                        });
                    }else{
                        nextView = new App.Consumer.SetupWizard.Fancoil.Views.FancoilScheduleLayout({
                            model: that.model
                        });
                    }
					
					 App.modalRegion.show(new SalusModal({
						 contentView: nextView,
                         staticBackdrop: true,
						 size: "modal-lg"
					 }));

					 App.showModal(); 
				});

				
			}
		}).mixin([App.Consumer.Views.Mixins.SalusView, App.Consumer.Views.Mixins.RegisteredRegions]);
	});

	return App.Consumer.SetupWizard.Fancoil.Views.FancoilTerminalLayout;
});