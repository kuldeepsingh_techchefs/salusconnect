"use strict";

define([
	"app",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView"
], function (App, consumerTemplates, SalusView) {

	App.module("Consumer.Views", function (Views, App, B, Mn, $) {
		Views.HeaderView = Mn.LayoutView.extend({
			className: "container",
			template: consumerTemplates["base/header"],
			regions: {
				desktopLinks: "#bb-desktop-links"
			},
			attributes: {
				"role": "navigation"
			},
			ui: {
				"logoUrl": ".bb-logo-link",
				"logoImage": ".bb-header-icon"
			},
			events: {
				"click #bb-back": "_browserBack",
				"click .hamburger-button-cover": "_openMobileMenu"
			},
			initialize: function () {
				// Add body padding to accommodate our fixed navigation header
				$('body').addClass("pad-body-for-fixed-navbar");

				this.mobileMenu = null;

				this.listenTo(App.vent, "window:stop", function () {
					var src = this.ui.logoImage.attr("src");
					this.ui.logoImage.attr("src", src);
				});
			},
			onRender: function () {
				var path = B.history.getFragment(),
						onDash = path.indexOf("dashboard") > -1;
				
				// center logo horizontally as well when not on dash
				this.$("#bb-salus-logo-link").toggleClass("center-horizontal-vertical-absolute", !onDash);
				this.$("#bb-salus-logo-link").toggleClass("center-vertical-absolute", onDash);

				if (this.options.isError) {
					this.ui.logoUrl.attr("href", "#noOp");
					this.ui.logoUrl.click(function () {
						window.location.reload();
					});
				}
                
                if (App.hasSalusAlerts === 1) {
                    this.$(".bb-salus-alerts-icon").removeClass("hidden");
                } else {
                    this.$(".bb-salus-alerts-icon").addClass("hidden");
                }
			},
			_browserBack: function () {
				window.history.back();
			},
			_openMobileMenu: function () {
				App.modalRegion.show(this.mobileMenu);

				this.mobileMenu.$el.animate({
					"height": "100%"
				}, 300);

				App.hideHeader();
			},
			templateHelpers: function () {
				// don't show the back button when we are on dashboard
				var path = B.history.getFragment(),
						onDash = path.indexOf("dashboard") > -1;

				return {
					salusLogo: App.rootPath("/images/icons/icon_svg_logo_white.svg"),
					isDashboard: onDash
				};
			},
			onBeforeDestroy: function () {
				// Remove body padding for navigation header
				$('body').removeClass("pad-body-for-fixed-navbar");
				
				if (this.mobileMenu) {
					this.mobileMenu.destroy();
					this.mobileMenu = null;
				}
			}
		}).mixin([SalusView]);
	});

	return App.Consumer.Views.HeaderView;
});