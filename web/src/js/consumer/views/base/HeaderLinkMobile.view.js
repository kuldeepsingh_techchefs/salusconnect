"use strict";

define([
	"app",
	"consumer/consumerTemplates",
	"smartmenus",
	"consumer/models/HeaderLinkViewModel",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/base/HeaderScenesMobile.view"
], function (App, consumerTemplates, Smartmenus, HeaderModels, SalusViewMixin) {

	App.module("Consumer.Views", function (Views, App, B, Mn, $, _) {

		Views.HeaderMobileMenuView = Mn.ItemView.extend({
			template: consumerTemplates["base/mobileMenu"],
			className: "mobile-menu visible-xs container shadow",
			events: {
				"click .close-btn-cover": "_closeMenu",
				"click .bb-mobile-menu-back": "_closeSubMenu",
				"click .bb-myStatus-link": "_closeMenu"
			},
			ui: {
				"backButton": ".bb-mobile-menu-back",
				"logoIcon": ".logo-icon",
				"innerContent": ".bb-inner-content"

			},
			initialize: function (options) {
				var that = this;

				this.childViews = {};
				this._buildChildViewObj(options.collection);

				// close the mobile menu when the viewport changes back to display desktop
				this.listenTo(App.vent, "changed:viewport", function (oldViewPort) {
					if (oldViewPort === "xs") {
						that._closeMenu(true);
					}
				});
			},

			_buildChildViewObj: function () {
				var that = this;
				this.mainMenuView = new Views.MobileMenuCollectionView({
					collection: this.collection
				});

				this.listenTo(this.mainMenuView, "submenu:open", this._handleSubmenuOpen);

				this.listenTo(this.mainMenuView, "link:clicked", this._closeMenu);

				this.collection.each(function (model) {
					if (model.get("type") === "dropdown") {
						that.childViews[model.get("menuId")] = new Views.MobileMenuCollectionView({
							collection: new HeaderModels.LinkViewModelCollection(model.get("subLinks"))
						});
						that.listenTo(that.childViews[model.get("menuId")], "link:clicked", that._closeMenu);
					}
				});
			},

			_closeMenu: function (closeFast) {
				this.$el.animate({
					"height": "65px"
				}, closeFast ? 1 : 300, null, function () {
					App.modalRegion.empty({preventDestroy: true});
					App.showHeader();
				});
				// re-enable scroll
				$("body").removeClass("modal-open");
			},

			_handleSubmenuOpen: function (model) {
				this.subMenuId = model.get("menuId");
				this.$el.addClass("sub-menu-open");
				this.mainMenuView.$el.detach();
				this.ui.innerContent.append(this.childViews[this.subMenuId].render().$el);
			},

			_closeSubMenu: function () {
				this.childViews[this.subMenuId].$el.detach();
				this.subMenuId = null;
				this.$el.removeClass("sub-menu-open");

				this.ui.innerContent.append(this.mainMenuView.render().$el);
			},

			appendMainMenu: function () {
				this.ui.innerContent.append(this.mainMenuView.render().$el);
			},

			onRender: function () {
				this.appendMainMenu();
				// prevent background scroll
				$("body").addClass("modal-open");
			},
			onDestroy: function () {
				$("body").removeClass("modal-open");
				var that = this;
				_.each(_.keys(this.childViews), function (key) {
					if (that.childViews[key]) {
						that.childViews[key].destroy();
					}
				});
				
				if (this.collection) {
					//Clean up view models
					this.collection.reset();
				}
			}
		}).mixin([SalusViewMixin]);

		Views.MobileMenuItemView = Mn.ItemView.extend({
			className: "col-xs-12 mobile-menu-row",
			template: consumerTemplates["base/headerLink"],
			events: {
				"click": "_handleClick"
			},
			initialize: function () {
				if (this.model.get("isUserName")) {
					this.$el.addClass("user-name");
				}
			},
			templateHelpers: function () {
				return {
					hasSubMenu: this.model.get("type") === "dropdown"
				};
			},
			_handleClick: function (evt) {
				if (this.model.get("clickHandler")) {
					evt.preventDefault();
					this.model.get("clickHandler")();
				} else if (this.model.get("href").indexOf("mailto") > -1) {
					window.location.href = this.model.get("href");
				}

				this.trigger("link:clicked", this);
			}
		}).mixin([SalusViewMixin]);

		Views.MobileMenuCollectionView = Mn.CollectionView.extend({
			className: "row",
			getChildView: function (model) {
				if (model.get("type") === "myStatus") {
					return Views.HeaderScenesMobileView;
				}

				return Views.MobileMenuItemView;
			},
			childEvents: {
				"link:clicked": "_handleLinkClicked"
			},

			_handleLinkClicked: function (childView) {
				if (childView.model.get('type') !== "dropdown") {
					this.trigger("link:clicked");
					App.navigate(childView.model.get("href"));
				} else {
					this.trigger("submenu:open", childView.model);
				}
			}
		}).mixin([SalusViewMixin]);
	});

	return App.Consumer.Views.HeaderMobileMenuView;
});