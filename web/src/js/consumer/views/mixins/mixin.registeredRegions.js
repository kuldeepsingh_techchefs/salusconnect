"use strict";

define([
	"app"
], function (App) {

	App.module("Consumer.Views.Mixins", function (Mixins, App, B, Mn, $, _) {

		Mixins.RegisteredRegions = function () {

			this.setDefaults({
				registerRegion: function (name, view) {
					this.registeredRegions[name] = view;
					return view;
				},

				showRegisteredViews: function () {
					var that = this;

					_.each(this.registeredRegions, function (view, name) {
						that[name].show(view);
					});
				}
			});

			this.before("initialize", function () {
				this.registeredRegions = {};
			});

			this.after("onRender", function () {
				this.showRegisteredViews();
			});
		};
	});

	return App.Consumer.Views.Mixins.RegisteredRegions;
});