"use strict";

/**
 * The salus tex box is the central implementation of the model bound text boxes with labels used across the site.
 * Primary responsibilities include
 * - managing label movement for text boxes
 * - binding data between text boxes and models
 */
define([
	"app",
	"consumer/views/mixins/mixin.salusView"
], function (App, SalusView) {

	App.module("Consumer.Views.Mixins", function (Mixins, App, B, Mn, $, _) {
		Mixins.SalusTextBox = function (options) {
			this.mixin([
				SalusView // by mixing in SalusTextBox we also mixin SalusView
			], options);

			this.setDefaults({
				events: {
					"keyup input": "_checkVal",
					"blur input": "_handleInputBlur",
					"input": function (/*event*/) {
						if (this.options && !this.options.preventTrigger) {
							this.trigger("change:value", this);
						}
					}
				},
				ui: {
					"formControl": ".bb-form-group"
				},
				_checkVal: function () {
					if (this.$("input").val() !== "") {
						this.$("label, input").addClass("show");
					} else {
						this.$("label, input").removeClass("show");
					}
				},
				_handleInputBlur: function () {
					this.validateField();
				},
				getValue: function () {
					return this.$("input").val();
				},
				templateHelpers: function () {
					return {
						inputId: this.inputId,
						labelTextKey: this.options.labelText,
						placeholderText: App.translate(this.options.labelText),
						inputType: this.options.type || "text",
						feedback: this.options.feedback || "",
						maxLength: 30
					};
				}
			});

			this.before("initialize", function () {
				this.inputId = _.uniqueId(this.options.modelProperty || "salus-textbox");

				if (this.options.modelProperty) {
					var configuration = { observe: this.options.modelProperty };
					if(_.isFunction(this.options.bindingOnGet)) {
						configuration.onGet = this.options.bindingOnGet;
					}

					if(_.isFunction(this.options.bindingOnSet)) {
						configuration.onSet = this.options.bindingOnSet;
					}

					this.bindings = this.bindings || {};
					this.bindings["#" + this.inputId] =  configuration;

					this.listenTo(this.model, "error:" + this.options.modelProperty, this.showErrors);
					this.listenTo(this.model, "clear:error:" + this.options.modelProperty, this.hideErrors);

					this.listenTo(this.model, "change", this._checkVal);
				}

				this.isReadOnly = !!this.options.isReadOnly;
			});

			this.after("initialize", function () {
				// combine attributes
				this.attributes = _.extend({}, _.result(this, "attributes"));
			});

			this.after("render", function () {
				this.$el.addClass(this.options.classes);
                
                if(this.options.feedback) {
                    this.$("span").addClass("show-feedback");
                }

				if (this.isReadOnly) {
					this.$("input").prop('readOnly', true);
				}

				if (this.options.value) {
					this.$("input").val(this.options.value);
					this.$("label, input").addClass("show");
				}

				this._checkVal();

				this.$("input").prop("readonly", this.options.readonly);
			});
		};
	});


	return App.Consumer.Views.Mixins.SalusTextBox;
});
