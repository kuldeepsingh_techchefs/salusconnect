/**
 * test salus and ayla connectors
 */
define([
	"underscore",
	"spec/SpecHelper",
	"common/AylaConfig",
	"common/util/CookieMgr",
	"common/model/api/SalusConnector",
	"common/model/ayla/User"
], function (_, SpecHelper, AylaConfig, CookieMgr, SaluConnector, SalusUser) {
	'use strict';

	describe("Salus and Ayla connector spec", function () {
		describe("make sure headers are set up right", function () {
			beforeEach(function () {
				SpecHelper.ajax.setup();
			});

			afterEach(function () {
				SpecHelper.ajax.cleanup();
			});

			it("Expect the session manager to be constructable", function () {
				var sc = new SaluConnector();

				expect(sc).not.toBeNull();
			});

			it("Expect api calls to have proper headers", function (done) {
				var sc = new SaluConnector();

				SpecHelper.ajax.registerHandler("post", AylaConfig.endpoints.login, function (proto, url, verb, data, success, fail, raw) {
					expect(proto).toBe(AylaConfig.getURLProtocolForAylaCalls());
					expect(verb).toBe("POST");

					success({
						"access_token": "access_token_here",
						"refresh_token": "refresh_token_here",
						"expires_in": 86400,
						"role": "EndUser",
						"role_tags": []
					});
				});

				SpecHelper.ajax.registerHandler("ajax", AylaConfig.endpoints.userProfile.fetch, function (proto, url, verb, data, success, fail, raw) {
					expect(proto).toBe(AylaConfig.getURLProtocolForAylaCalls());
					expect(verb).toBe("GET");
					expect(data).toBeUndefined();
					expect(_.size(raw.headers)).toBe(1);
					expect(raw.headers.Authorization).toBe("auth_token accessToken_1");

					success({});
					done();
				});

				sc._sessionManager.set({"accessToken": "accessToken_1"});
				sc.getSessionUserPromise();
			});
		});

		describe("for basic api integration", function () {
			beforeEach(function () {
				SpecHelper.ajax.setup();
			});

			afterEach(function () {
				SpecHelper.ajax.cleanup();
			});
		});

		describe("Expect 401 error to result in login refresh", function () {
			var promiseCallback, sc;
			var loginCount = 0;

			beforeAll(function () {
				SpecHelper.ajax.setup();
				sc = new SaluConnector();

				var loginFunction = function (proto, url, verb, data, success, fail, raw) {
					expect(proto).toBe(AylaConfig.getURLProtocolForAylaCalls());
					expect(verb).toBe("POST");

					loginCount++;
					success({
						"access_token": "access_token_here." + loginCount,
						"refresh_token": "refresh_token_here." + loginCount,
						"expires_in": 86400,
						"role": "EndUser",
						"role_tags": []
					});
				};

				SpecHelper.ajax.registerHandler("post", AylaConfig.endpoints.login, loginFunction);
				SpecHelper.ajax.registerHandler("post", AylaConfig.endpoints.refreshToken, loginFunction);
			});

			afterAll(function () {
				SpecHelper.ajax.cleanup();
			});

			it("Expect login to set access and refresh tokens", function (done) {
				sc._sessionManager.loginWithUsername("a", "b");

				setTimeout(function () {
					expect(loginCount).toBe(1);
					expect(sc._sessionManager.get("accessToken")).toBe("access_token_here.1");
					expect(sc._sessionManager.get("refreshToken")).toBe("refresh_token_here.1");
					done();
				}, 1);
			});

			it("and 200 to result in populated user data", function (done) {
				var isFirstCall = true;
				var promiseCallbackWasCalled = false;

				SpecHelper.ajax.registerHandler("ajax", AylaConfig.endpoints.userProfile.fetch, function (proto, url, verb, data, success, fail, raw) {
					expect(proto).toBe(AylaConfig.getURLProtocolForAylaCalls());
					expect(verb).toBe("GET");
					expect(data).toBeUndefined();
					expect(_.size(raw.statusCode)).toBe(1);
					expect(raw.statusCode["401"]).toBeTruthy();

					if (isFirstCall) {
						isFirstCall = false;
						expect(raw.headers.Authorization).toBe("auth_token access_token_here.1");
						raw.statusCode["401"](null, null, null);
						return;
					}

					expect(raw.headers.Authorization).toBe("auth_token access_token_here.2");
					success({ // adapted from api call as returned July 2 2015
						"approved": true,
						"city": "cty",
						"company": "company",
						"confirmed_at": "2015-06-11T18:15:33Z",
						"country": "USA",
						"created_at": "2015-06-11T18:14:42Z",
						"email": "jesse@digitalfoundry.com",
						"firstname": "Jesse",
						"lastname": "Shaver",
						"phone_country_code": "0123",
						"phone": "45785",
						"state": "state",
						"street": "street",
						"updated_at": "2015-07-02T21:42:04Z",
						"zip": "01236"
					});
				});

				SpecHelper.ajax.registerHandler("ajax", AylaConfig.endpoints.userProfile.metaData.fetch,
						function (proto, url, verb, data, success, fail, raw) {
							expect(proto).toBe(AylaConfig.getURLProtocolForAylaCalls());
							expect(verb).toBe("GET");

							success({"datum": { "value": "{}"}});
						});

				promiseCallback = function (user) {
					promiseCallbackWasCalled = true;

					if (isFirstCall) { // fail - this should never happen
						spec.fail("promise should never resolve with authentication failure");
					} else {
						expect(user).not.toBeNull();
						expect(user.get("city")).toBe("cty");
						expect(user.get("email")).toBe("jesse@digitalfoundry.com");
						expect(user.get("firstname")).toBe("Jesse");
						expect(user.get("approved")).toBeTruthy();
						expect(user.get("lastname")).toBe("Shaver");
						expect(user.get("phone_country_code")).toBe("0123");
						expect(user.get("phone")).toBe("45785");
						expect(user.get("state")).toBe("state");
						expect(user.get("street")).toBe("street");
						expect(user.get("zip")).toBe("01236");
						done();
					}
				};

				sc._aylaConnector.set("currentUser", null);
				sc.getSessionUserPromise().then(promiseCallback);
			});
		});
	});
});