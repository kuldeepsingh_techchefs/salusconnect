/*jslint jasmine: true */
define([
	"underscore",
	"spec/SpecHelper",
	"common/AylaConfig",
	"common/model/api/SalusConnector",
	"common/model/ayla/Group.model",
	"common/model/ayla/deviceTypes/DefaultAylaDevice"
], function (_, SpecHelper, AylaConfig, SaluConnector, Group, Device) {
	'use strict';

	describe("Ayla Group ", function () {
		beforeEach(function () {
			SpecHelper.ajax.setup();
		});

		afterEach(function () {
			SpecHelper.ajax.cleanup();
		});

		describe("when calling loadDetails ", function () {
			it("should load and set data properly", function (done) {
				var deviceObj = {
					"key": 56,
					"name": "living room",
					"device_count": 0,
					"devices": []
				};

				SpecHelper.ajax.registerHandler("ajax", _.template(AylaConfig.endpoints.groups.load)({id: 56}),
					function (proto, url, verb, data, success, fail, raw) {
						expect(proto).toBe(AylaConfig.getURLProtocolForAylaCalls());
						expect(verb).toBe("GET");

						success(// based on API call to ayla Tue, 07 Jul 2015 23:29:54 GMT
							{
								"group": deviceObj
							}
						);
					});

				var group = new Group({key: 56});
				expect(group).propEquals("key", 56);

				group.refresh().then(function (result) {
					expect(group).toMatchObjectPropertiesExcept(deviceObj);
					done();
				});
			});
		});

		describe("when interacting with devices ", function () {
			it("should be allowed to add devices when passing a key", function (done) {
				var groupObj = { "group": {
					"key": 55219,
					"name": "living room",
					"device_count": 1,
					"devices": [
						{key: "52"}
					]
				} };

				SpecHelper.ajax.registerHandler("ajax", _.template(AylaConfig.endpoints.groups.addDevice)({key: 55219}),
					function (proto, url, verb, data, success, fail, raw) {
						expect(proto).toBe(AylaConfig.getURLProtocolForAylaCalls());
						expect(verb).toBe("POST");

						success(groupObj);
					});

				var group = new Group({
					"key": 55219,
					"name": "living_room",
					device_count: 0,
					devices: []
				});

				group.addDevice(52).then(function () {
					expect(group).toMatchObjectPropertiesExcept(groupObj.group, ["devices"]);
					expect(group.get("devices")).toEqual(["52"]);
					done();
				});
			});

			it("should be allowed to add devices when passing a backbone model", function (done) {
				var groupObj = { "group": {
					"key": 55219,
					"name": "living room",
					"device_count": 1,
					"devices": [
						{key: "52"}
					]
				} };

				SpecHelper.ajax.registerHandler("ajax", _.template(AylaConfig.endpoints.groups.addDevice)({key: 55219}),
					function (proto, url, verb, data, success, fail, raw) {
						expect(proto).toBe(AylaConfig.getURLProtocolForAylaCalls());
						expect(verb).toBe("POST");

						success(groupObj);
					});

				var group = new Group({
					"key": 55219,
					"name": "living_room",
					device_count: 0,
					devices: []
				});

				var device = new Device({
					key: 52
				});

				group.addDevice(device).then(function () {
					expect(group).toMatchObjectPropertiesExcept(groupObj.group, ["devices"]);
					expect(group.get("devices")).toEqual(["52"]);
					done();
				});
			});

			it("should be allowed to remove devices when passing a key", function (done) {
				var groupObj = { "group": {
					"key": 55219,
					"name": "living room",
					"device_count": 0
				} };

				SpecHelper.ajax.registerHandler("ajax",
					_.template(AylaConfig.endpoints.groups.removeDevice)({key: 55219, device_id: 52}),
					function (proto, url, verb, data, success, fail, raw) {
						expect(proto).toBe(AylaConfig.getURLProtocolForAylaCalls());
						expect(verb).toBe("DELETE");

						success(groupObj);
					});

				var group = new Group({
					"key": 55219,
					"name": "living_room",
					device_count: 1,
					devices: ["52"]
				});

				group.removeDevice(52).then(function () {
					expect(group).toMatchObjectPropertiesExcept(groupObj.group, ["devices"]);
					expect(group.get("devices")).toEqual([]);
					done();
				});
			});

			it("should be allowed to remove devices when passing a backbone model", function (done) {
				var groupObj = { "group": {
					"key": 55219,
					"name": "living room",
					"device_count": 0
				} };

				SpecHelper.ajax.registerHandler("ajax",
					_.template(AylaConfig.endpoints.groups.removeDevice)({key: 55219, device_id: 52}),
					function (proto, url, verb, data, success, fail, raw) {
						expect(proto).toBe(AylaConfig.getURLProtocolForAylaCalls());
						expect(verb).toBe("DELETE");

						success(groupObj);
					});

				var group = new Group({
					"key": 55219,
					"name": "living_room",
					device_count: 1,
					devices: ["52"]
				});

				var device = new Device({
					key: 52
				});

				group.removeDevice(device).then(function () {
					expect(group).toMatchObjectPropertiesExcept(groupObj.group, ["devices"]);
					expect(group.get("devices")).toEqual([]);
					done();
				});
			});
		});

		it("should call loadDetails() and load data properly when adding a device", function (done) {
			var createTriggered, detailsTriggered, didSync, addDone, addTriggered;
			var groupObj = {
				"name": "living room",
				"key": 55219,
				"device_count": 0,
				"devices": []
			};

			SpecHelper.ajax.registerHandler("ajax", AylaConfig.endpoints.groups.create,
				function (proto, url, verb, data, success, fail, raw) {
					expect(proto).toBe(AylaConfig.getURLProtocolForAylaCalls());
					expect(verb).toBe("POST");
					addTriggered = true;

					success({ "group": {"key": 55219, "name": "living room" } });
				});

			SpecHelper.ajax.registerHandler("ajax", _.template(AylaConfig.endpoints.groups.load)({id: 55219}),
				function (proto, url, verb, data, success, fail, raw) {
					expect(proto).toBe(AylaConfig.getURLProtocolForAylaCalls());
					expect(verb).toBe("GET");

					detailsTriggered = true;
					success([
						{group: groupObj}
					]);
				});

			var group = new Group(groupObj);

			/// because test is synchronous, created is actually called after sync...
			group.on("created", function () {
				createTriggered = true;
			});

			group.on("sync", function () {
				didSync = true;
			});

			group.set(groupObj);

			// because page is synchronous, then is called after created and sync
			group.add().then(function (result) {
				addDone = true;

				expect(addTriggered).toBeTruthy();
				expect(didSync).toBeTruthy();
				expect(addDone).toBeTruthy();
				expect(detailsTriggered).toBeTruthy();
				expect(createTriggered).toBeTruthy();
				done();
			});
		});
	});
});